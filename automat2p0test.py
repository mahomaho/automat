import regex

m=regex.match(r'\s*-?(\s*((sin|cos|log|asin|)\((?0)\)|(\d+(\.\d+)?|0[xX][0-9a-fA-F]+)|x\d+)\s*((\+|\-|\*|\/)(?1))?)', r'  -  ( -  sin(  - x1 - x2 +  12  *  cos(  - x2  )  )  )  ',partial=True)

import re
import time
starttime=time.perf_counter()

with open('test_artext/artexttest.swcd') as o:
	c=o.read()
	for m in re.finditer('\s+|/\*.*?\*/|//.*?\n|(?P<specials>[={},\[\]\(\)])|(?P<numerical>(?P<boolean>true|false)(?P<int>[+-]?0[xX][0-9a-fA-F]+|[+-]?0[bB][01]+|[+-]?\d+)|(?P<real>[+-]?\d+\.\d+(?:[eE][+-]?\d+)?|NaN|-?INF))|:(?P<enum>[a-zA-Z_]\w*)|\^?(?P<path>(?:[a-zA-Z_]\w*(?:\.\^?[a-zA-Z_]\w*)*\.(?:\*|[a-zA-Z_]\w*))|(?P<name>[a-zA-Z_]\w*))|"(?P<braced>\w+?)"|(?P<error>.)',c,re.MULTILINE|re.DOTALL|re.ASCII):
		pass
		#if m.lastgroup:
		#	print(m.lastgroup+':',end='')
		#	print(m.group())#,end='')#+str(m.span())+str(m.lastindex))

parsetime=time.perf_counter() - starttime
from AutoMAT import *
from AutoMAT import simplebase
#from AutoMAT.support import *
import itertools
import sys

a=simplebase.SimpleNumericalElement(4)

files='RteConfiguration.arxml_ ../../Utilities/BswTypes/src/AUTOSAR_Blueprint_addons.arxml ../../Utilities/TENG_Types/TE_Types.arxml ../../Utilities/E41_BSW_Common/swc/BswM2DcmIf.arxml ../../Application_Code_Files/SWU_AATD_CodeFiles/AATD.arxml ../../Application_Code_Files/SWU_ACBD_CodeFiles/ACBD.arxml ../../Application_Code_Files/SWU_ACCP_CodeFiles/ACCP.arxml ../../Application_Code_Files/SWU_ACLC_CodeFiles/ACLC.arxml ../../Application_Code_Files/SWU_ACOD_CodeFiles/ACOD.arxml ../../Application_Code_Files/SWU_ACPD_CodeFiles/ACPD.arxml ../../Application_Code_Files/SWU_ACRD_CodeFiles/ACRD.arxml ../../Application_Code_Files/SWU_ACTC_CodeFiles/ACTC.arxml ../../Application_Code_Files/SWU_ADCI_CodeFiles/ADCI.arxml ../../Application_Code_Files/SWU_AILM_CodeFiles/AILM.arxml ../../Application_Code_Files/SWU_AJTC_CodeFiles/AJTC.arxml ../../Application_Code_Files/SWU_APSS_CodeFiles/APSS.arxml ../../Application_Code_Files/SWU_ARES_CodeFiles/ARES.arxml ../../Application_Code_Files/SWU_ATLC_CodeFiles/ATLC.arxml ../../Application_Code_Files/SWU_BARD_CodeFiles/BARD.arxml ../../Application_Code_Files/SWU_BOCD_CodeFiles/BOCD.arxml ../../Application_Code_Files/SWU_BOCO_CodeFiles/BOCO.arxml ../../Application_Code_Files/SWU_BORE_CodeFiles/BORE.arxml ../../Application_Code_Files/SWU_BPPD_CodeFiles/BPPD.arxml ../../Application_Code_Files/SWU_BRKP_CodeFiles/BRKP.arxml ../../Application_Code_Files/SWU_BVAD_CodeFiles/BVAD.arxml ../../Application_Code_Files/SWU_CABD_CodeFiles/CABD.arxml ../../Application_Code_Files/SWU_CACD_CodeFiles/CACD.arxml ../../Application_Code_Files/SWU_CAMD_CodeFiles/CAMD.arxml ../../Application_Code_Files/SWU_CAND_CodeFiles/CAND.arxml ../../Application_Code_Files/SWU_CASD_CodeFiles/CASD.arxml ../../Application_Code_Files/SWU_CATC_CodeFiles/CATC.arxml ../../Application_Code_Files/SWU_CATD_CodeFiles/CATD.arxml ../../Application_Code_Files/SWU_CBNM_CodeFiles/CBNM.arxml ../../Application_Code_Files/SWU_CCPD_CodeFiles/CCPD.arxml ../../Application_Code_Files/SWU_CCVD_CodeFiles/CCVD.arxml ../../Application_Code_Files/SWU_CECD_CodeFiles/CECD.arxml ../../Application_Code_Files/SWU_CFMN_CodeFiles/CFMN.arxml ../../Application_Code_Files/SWU_CHTD_CodeFiles/CHTD.arxml ../../Application_Code_Files/SWU_CIDI_CodeFiles/CIDI.arxml ../../Application_Code_Files/SWU_COLD_CodeFiles/COLD.arxml ../../Application_Code_Files/SWU_COMA_CodeFiles/COMA.arxml ../../Application_Code_Files/SWU_CPPD_CodeFiles/CPPD.arxml ../../Application_Code_Files/SWU_CRAD_CodeFiles/CRAD.arxml ../../Application_Code_Files/SWU_CRPC_CodeFiles/CRPC.arxml ../../Application_Code_Files/SWU_CRSC_CodeFiles/CRSC.arxml ../../Application_Code_Files/SWU_CSED_CodeFiles/CSED.arxml ../../Application_Code_Files/SWU_CTDI_CodeFiles/CTDI.arxml ../../Application_Code_Files/SWU_CWTD_CodeFiles/CWTD.arxml ../../Application_Code_Files/SWU_DCDC_CodeFiles/DCDC.arxml ../../Application_Code_Files/SWU_DCWT_CodeFiles/DCWT.arxml ../../Application_Code_Files/SWU_DGPS_CodeFiles/DGPS.arxml ../../Application_Code_Files/SWU_DIPR_CodeFiles/DIPR.arxml ../../Application_Code_Files/SWU_DITD_CodeFiles/DITD.arxml ../../Application_Code_Files/SWU_DOSR_CodeFiles/DOSR.arxml ../../Application_Code_Files/SWU_DRSP_CodeFiles/DRSP.arxml ../../Application_Code_Files/SWU_DTOS_CodeFiles/DTOS.arxml ../../Application_Code_Files/SWU_DYNO_CodeFiles/DYNO.arxml ../../Application_Code_Files/SWU_ECOD_CodeFiles/ECOD.arxml ../../Application_Code_Files/SWU_ECOM_CodeFiles/ECOM.arxml ../../Application_Code_Files/SWU_ECPC_CodeFiles/ECPC.arxml ../../Application_Code_Files/SWU_ECWC_CodeFiles/ECWC.arxml ../../Application_Code_Files/SWU_ECWD_CodeFiles/ECWD.arxml ../../Application_Code_Files/SWU_EGCD_CodeFiles/EGCD.arxml ../../Application_Code_Files/SWU_EGRC_CodeFiles/EGRC.arxml ../../Application_Code_Files/SWU_EGRD_CodeFiles/EGRD.arxml ../../Application_Code_Files/SWU_EGTD_CodeFiles/EGTD.arxml ../../Application_Code_Files/SWU_EITC_CodeFiles/EITC.arxml ../../Application_Code_Files/SWU_EMAR_CodeFiles/EMAR.arxml ../../Application_Code_Files/SWU_ENTC_CodeFiles/ENTC.arxml ../../Application_Code_Files/SWU_EOLT_CodeFiles/EOLT.arxml ../../Application_Code_Files/SWU_EOTD_CodeFiles/EOTD.arxml ../../Application_Code_Files/SWU_ERIC_CodeFiles/ERIC.arxml ../../Application_Code_Files/SWU_ESDI_CodeFiles/ESDI.arxml ../../Application_Code_Files/SWU_ETME_CodeFiles/ETME.arxml ../../Application_Code_Files/SWU_EVCD_CodeFiles/EVCD.arxml ../../Application_Code_Files/SWU_EVLD_CodeFiles/EVLD.arxml ../../Application_Code_Files/SWU_EVPC_CodeFiles/EVPC.arxml ../../Application_Code_Files/SWU_EVPD_CodeFiles/EVPD.arxml ../../Application_Code_Files/SWU_EVRC_CodeFiles/EVRC.arxml ../../Application_Code_Files/SWU_EWAD_CodeFiles/EWAD.arxml ../../Application_Code_Files/SWU_EWTD_CodeFiles/EWTD.arxml ../../Application_Code_Files/SWU_EXPE_CodeFiles/EXPE.arxml ../../Application_Code_Files/SWU_EXTC_CodeFiles/EXTC.arxml ../../Application_Code_Files/SWU_FAND_CodeFiles/FAND.arxml ../../Application_Code_Files/SWU_FCDI_CodeFiles/FCDI.arxml ../../Application_Code_Files/SWU_FDMA_CodeFiles/FDMA.arxml ../../Application_Code_Files/SWU_FHPC_CodeFiles/FHPC.arxml ../../Application_Code_Files/SWU_FLSD_CodeFiles/FLSD.arxml ../../Application_Code_Files/SWU_FPCD_CodeFiles/FPCD.arxml ../../Application_Code_Files/SWU_FPCR_CodeFiles/FPCR.arxml ../../Application_Code_Files/SWU_FRPD_CodeFiles/FRPD.arxml ../../Application_Code_Files/SWU_FTPD_CodeFiles/FTPD.arxml ../../Application_Code_Files/SWU_FUEL_CodeFiles/FUEL.arxml ../../Application_Code_Files/SWU_FUID_CodeFiles/FUID.arxml ../../Application_Code_Files/SWU_FUPC_CodeFiles/FUPC.arxml ../../Application_Code_Files/SWU_FUSD_CodeFiles/FUSD.arxml ../../Application_Code_Files/SWU_FUTE_CodeFiles/FUTE.arxml ../../Application_Code_Files/SWU_FUTR_CodeFiles/FUTR.arxml ../../Application_Code_Files/SWU_GELC_CodeFiles/GELC.arxml ../../Application_Code_Files/SWU_GENC_CodeFiles/GENC.arxml ../../Application_Code_Files/SWU_GEND_CodeFiles/GEND.arxml ../../Application_Code_Files/SWU_GERC_CodeFiles/GERC.arxml ../../Application_Code_Files/SWU_GPFC_CodeFiles/GPFC.arxml ../../Application_Code_Files/SWU_GPFD_CodeFiles/GPFD.arxml ../../Application_Code_Files/SWU_GSCH_CodeFiles/GSCH.arxml ../../Application_Code_Files/SWU_GSEQ_CodeFiles/GSEQ.arxml ../../Application_Code_Files/SWU_HAMF_CodeFiles/HAMF.arxml ../../Application_Code_Files/SWU_HBSC_CodeFiles/HBSC.arxml ../../Application_Code_Files/SWU_HETE_CodeFiles/HETE.arxml ../../Application_Code_Files/SWU_HOOD_CodeFiles/HOOD.arxml ../../Application_Code_Files/SWU_HPFD_CodeFiles/HPFD.arxml ../../Application_Code_Files/SWU_HPUD_CodeFiles/HPUD.arxml ../../Application_Code_Files/SWU_HVSM_CodeFiles/HVSM.arxml ../../Application_Code_Files/SWU_IASH_CodeFiles/IASH.arxml ../../Application_Code_Files/SWU_ICOE_CodeFiles/ICOE.arxml ../../Application_Code_Files/SWU_ICPC_CodeFiles/ICPC.arxml ../../Application_Code_Files/SWU_IGCD_CodeFiles/IGCD.arxml ../../Application_Code_Files/SWU_ISCD_CodeFiles/ISCD.arxml ../../Application_Code_Files/SWU_ITCC_CodeFiles/ITCC.arxml ../../Application_Code_Files/SWU_KNOD_CodeFiles/KNOD.arxml ../../Application_Code_Files/SWU_KOCO_CodeFiles/KOCO.arxml ../../Application_Code_Files/SWU_LDMN_CodeFiles/LDMN.arxml ../../Application_Code_Files/SWU_LVBM_CodeFiles/LVBM.arxml ../../Application_Code_Files/SWU_MAPD_CodeFiles/MAPD.arxml ../../Application_Code_Files/SWU_MATD_CodeFiles/MATD.arxml ../../Application_Code_Files/SWU_MAVS_CodeFiles/MAVS.arxml ../../Application_Code_Files/SWU_MILD_CodeFiles/MILD.arxml ../../Application_Code_Files/SWU_MSFD_CodeFiles/MSFD.arxml ../../Application_Code_Files/SWU_MTCS_CodeFiles/MTCS.arxml ../../Application_Code_Files/SWU_MTLD_CodeFiles/MTLD.arxml ../../Application_Code_Files/SWU_NGSD_CodeFiles/NGSD.arxml ../../Application_Code_Files/SWU_O2FD_CodeFiles/O2FD.arxml ../../Application_Code_Files/SWU_O2RD_CodeFiles/O2RD.arxml ../../Application_Code_Files/SWU_OCTA_CodeFiles/OCTA.arxml ../../Application_Code_Files/SWU_OIPD_CodeFiles/OIPD.arxml ../../Application_Code_Files/SWU_OPEC_CodeFiles/OPEC.arxml ../../Application_Code_Files/SWU_OSHD_CodeFiles/OSHD.arxml ../../Application_Code_Files/SWU_OXYS_CodeFiles/OXYS.arxml ../../Application_Code_Files/SWU_PAPD_CodeFiles/PAPD.arxml ../../Application_Code_Files/SWU_PATD_CodeFiles/PATD.arxml ../../Application_Code_Files/SWU_PDLS_CodeFiles/PDLS.arxml ../../Application_Code_Files/SWU_PICD_CodeFiles/PICD.arxml ../../Application_Code_Files/SWU_PICO_CodeFiles/PICO.arxml ../../Application_Code_Files/SWU_PLMM_CodeFiles/PLMM.arxml ../../Application_Code_Files/SWU_PLRD_CodeFiles/PLRD.arxml ../../Application_Code_Files/SWU_POLC_CodeFiles/POLC.arxml ../../Application_Code_Files/SWU_PORS_CodeFiles/PORS.arxml ../../Application_Code_Files/SWU_PRCD_CodeFiles/PRCD.arxml ../../Application_Code_Files/SWU_PTRS_CodeFiles/PTRS.arxml ../../Application_Code_Files/SWU_PUCO_CodeFiles/PUCO.arxml ../../Application_Code_Files/SWU_RATD_CodeFiles/RATD.arxml ../../Application_Code_Files/SWU_RDSC_CodeFiles/RDSC.arxml ../../Application_Code_Files/SWU_RGDI_CodeFiles/RGDI.arxml ../../Application_Code_Files/SWU_SCAV_CodeFiles/SCAV.arxml ../../Application_Code_Files/SWU_SDAD_CodeFiles/SDAD.arxml ../../Application_Code_Files/SWU_SPAR_CodeFiles/SPAR.arxml ../../Application_Code_Files/SWU_SPRK_CodeFiles/SPRK.arxml ../../Application_Code_Files/SWU_SRCD_CodeFiles/SRCD.arxml ../../Application_Code_Files/SWU_SSDI_CodeFiles/SSDI.arxml ../../Application_Code_Files/SWU_SSDL_CodeFiles/SSDL.arxml ../../Application_Code_Files/SWU_SSRD_CodeFiles/SSRD.arxml ../../Application_Code_Files/SWU_SSSC_CodeFiles/SSSC.arxml ../../Application_Code_Files/SWU_SSSM_CodeFiles/SSSM.arxml ../../Application_Code_Files/SWU_SSTD_CodeFiles/SSTD.arxml ../../Application_Code_Files/SWU_STCW_CodeFiles/STCW.arxml ../../Application_Code_Files/SWU_STOC_CodeFiles/STOC.arxml ../../Application_Code_Files/SWU_SWIN_CodeFiles/SWIN.arxml ../../Application_Code_Files/SWU_SYVD_CodeFiles/SYVD.arxml ../../Application_Code_Files/SWU_TCAP_CodeFiles/TCAP.arxml ../../Application_Code_Files/SWU_TCTC_CodeFiles/TCTC.arxml ../../Application_Code_Files/SWU_TIRC_CodeFiles/TIRC.arxml ../../Application_Code_Files/SWU_TISC_CodeFiles/TISC.arxml ../../Application_Code_Files/SWU_TMMD_CodeFiles/TMMD.arxml ../../Application_Code_Files/SWU_TPLC_CodeFiles/TPLC.arxml ../../Application_Code_Files/SWU_TPSC_CodeFiles/TPSC.arxml ../../Application_Code_Files/SWU_TPSD_CodeFiles/TPSD.arxml ../../Application_Code_Files/SWU_TQMS_CodeFiles/TQMS.arxml ../../Application_Code_Files/SWU_TSPC_CodeFiles/TSPC.arxml ../../Application_Code_Files/SWU_TTSS_CodeFiles/TTSS.arxml ../../Application_Code_Files/SWU_TUPD_CodeFiles/TUPD.arxml ../../Application_Code_Files/SWU_TUTD_CodeFiles/TUTD.arxml ../../Application_Code_Files/SWU_TUVD_CodeFiles/TUVD.arxml ../../Application_Code_Files/SWU_TWPD_CodeFiles/TWPD.arxml ../../Application_Code_Files/SWU_UHRA_CodeFiles/UHRA.arxml ../../Application_Code_Files/SWU_V5RD_CodeFiles/V5RD.arxml ../../Application_Code_Files/SWU_VAVM_CodeFiles/VAVM.arxml ../../Application_Code_Files/SWU_VGTC_CodeFiles/VGTC.arxml ../../Application_Code_Files/SWU_VGTD_CodeFiles/VGTD.arxml ../../Application_Code_Files/SWU_VLOT_CodeFiles/VLOT.arxml ../../Application_Code_Files/SWU_VODO_CodeFiles/VODO.arxml ../../Application_Code_Files/SWU_VOPD_CodeFiles/VOPD.arxml ../../Application_Code_Files/SWU_VSPE_CodeFiles/VSPE.arxml ../../Application_Code_Files/SWU_VSSD_CodeFiles/VSSD.arxml ../../Application_Code_Files/SWU_VTAR_CodeFiles/VTAR.arxml ../../Application_Code_Files/SWU_WSSD_CodeFiles/WSSD.arxml ../../Application_Code_Files/SWU_WTDA_CodeFiles/WTDA.arxml ../../Application_Code_Files/SWU_XSID_CodeFiles/XSID.arxml ../../Specifications/NetworkSpecification.arxml ../../Application_Code_Files/SWU_CACE_CodeFiles/CACE.arxml ../../Application_Code_Files/SWU_CACE_CodeFiles/CACE_external_interface.arxml ../../Application_Code_Files/SWU_DPSC_CodeFiles/DPSC.arxml ../../Application_Code_Files/SWU_EARS_CodeFiles/EARS.arxml ../../Application_Code_Files/SWU_FEQR_CodeFiles/FEQR.arxml ../../Application_Code_Files/SWU_FINT_CodeFiles/FINT.arxml ../../Application_Code_Files/SWU_FINT_CodeFiles/FINT_external_interface.arxml ../../Application_Code_Files/SWU_HYDI_CodeFiles/HYDI.arxml ../../Application_Code_Files/SWU_PWRM_CodeFiles/PWRM.arxml ../../Application_Code_Files/SWU_PWRM_CodeFiles/PWRM_external_interface.arxml ../../Application_Code_Files/SWU_VPMM_CodeFiles/VPMM.arxml ../../core/arxml/autosar/AUTOSAR_MOD_BSWServiceInterfaces.arxml ../../core/arxml/generic/ArcCore_MOD_BSWServiceInterfaces_IoHwAb.arxml ../../core/arxml/generic/ArcCore_Types.arxml ../../core/arxml/generic/ArcCore_EcucDefs_Rte.arxml  ../../core/system/Os/rtos/arxml/ArcCore_EcucDefs_Os.arxml ../../Utilities/E41_BSW_Common/arxml/OsConfiguration.arxml'
files=files.split()

startloadtime=time.perf_counter()
LoadFile('test/AUTOSAR_MOD_ECUConfigurationParameters.arxml_')
for file in files:
	LoadFile(file)
	
#tree=
LoadFile('OsConfig.arxml_')
LoadFile('osNeeds.arxml_')
duration = time.perf_counter()-startloadtime
#autosar._setup()
print(f'It took {startloadtime-starttime}s to load code and files, {duration}s to just load the {len(files)} files, {duration/len(files)}s per file and {time.perf_counter()-startloadtime-duration} to setup code')
a=autosar.arPackage
b=a.__repr__()
print(b[0])
b=a.arPackage.element
autosar.AATD_pkg.aggregations()[9].__len__()
print(b.__repr__()[0])
autosar.AATD_pkg.AATD_swc.AATD.AATDve_e_AmbientAirTempVld.isOfType().__repr__()
autosar.AATD_pkg.AATD_swc.AATD.AATDve_e_AmbientAirTempVld.isOfType().AATDve_e_AmbientAirTempVld.isOfType().__repr__()
autosar.AATD_pkg.AATD_swc.AATD.AATD.Runnable_AATD_100ms.serverCallPoint[0].operation.iref()
#autosar._setxml(tree.getroot())
#tree=

#import automat2p0
q=autosar.SystemWeaver.CommunicationClusters.P_CAN.EMS_P_CAN.APMAX1.iSignal.ref()
autosar.SystemWeaver.CommunicationClusters.P_CAN.EMS_P_CAN.APMAX1.__dir__()

print(autosar.AATD_pkg.AATD_dt.Boolean.swDataDefProps.dataConstr.ref().dataConstrRule.internalConstrs.lowerLimit.intervalType.values())
autosar.AATD_pkg.arPackage.select.__dir__()
autosar.AATD_pkg.AATD_dt.Boolean.swDataDefProps.__dir__()
r=autosar.AATD_pkg.AATD_if.AITDve_T_AirIntakeTemp.references()

a=autosar.AATD_pkg.AATD_dt.Boolean.swDataDefProps.editor()
a.swIsVirtual=True
print(str(a.swIsVirtual))
a.swIsVirtual=False


del autosar.AATD_pkg.AATD_if.editor().AITDve_T_AirIntakeTemp
a=autosar.editor().AATD_pkg
b=a.AATD_swc.AATD.AATDve_e_AmbientAirTempVld.providedComSpec
b.__dir__()
b=b.initValue[0]
b.shortLabel='pelle'
b.variationPoint.__dir__()
b.variationPoint.shortLabel='sdf'
b.variationPoint.postBuildVariantCondition[0].__dir__()
a=b.variationPoint.postBuildVariantCondition[0].matchingCriterion
b.variationPoint.postBuildVariantCondition[0].value=3
b.variationPoint.postBuildVariantCondition[0].value.shortLabel='gfhfgh'
print(b.variationPoint.postBuildVariantCondition[0].value.shortLabel)
autosar.AATD_pkg.AATD_swc.AATD.AATDve_e_AmbientAirTempVld.references()
a=default(2,autosar).AATD_pkg.AATD_dt.Boolean.category.val()
a=default(2,autosar).AATD_pkg.AATD_dt.Boolean.shortNameFragment
#q=autosar.BSW_pkg.Os.definition
autosar.AATD_pkg.AATD_dt.Boolean.__dir__()
q=autosar.AATD_pkg.AATD_swc.AATD.AATD.dataTypeMapping[0]
b=autosar.arPackage[0]
e=autosar.arPackage['SystemWeaver']
e.__repr__()
a=b.shortName
autosar.arPackage[0].shortName
a=time.perf_counter()
autosar.BSW_pkg.Rte.__dir__()
autosar.BSW_pkg.Rte.acbd.RteEventToTaskMapping[0].RteEventRef.__str__()
b=time.perf_counter()-a
autosar.BSW_pkg.Rte.RteSwComponentInstance[0]
autosar.BSW_pkg.Rte.RteSwComponentInstance[20].RteEventToTaskMapping[0].RtePositionInTask
print(f'It took {time.perf_counter()-starttime}s to run whole test')
autosar_=autosar.model('test/AUTOSAR_MOD_ECUConfigurationParameters.arxml_')
autosar_.__dir__()

a=autosar.BSW_pkg.Rte.instances()
b=autosar_r4p0.Group.EcucAbstractReferenceDef.instances()
x=autosar.AATD_pkg.AATD_dt.COMPU_SPARte_e_Validity.compuInternalToPhys.compuScale[0]
x.compuConst.vt.val()
y=autosar.CACE_pkg.CACE_swc.CACE.CACEva_b_BswMComModeReq.providedComSpec[0].initValue[0].element[0].swValueCont
y.swValuesPhys.v
pass
