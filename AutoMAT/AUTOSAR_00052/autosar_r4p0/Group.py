
# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.


from ..support import *
from . import SimpleTypes
from .. import complexbase
from collections import defaultdict


class AbsoluteTolerance(complexbase.GroupBase):
	"""Maximum allowable deviation"""
	def __init__(self):
		super().__init__()
		self._absolute_child=ModelNone

class AbstractAccessPoint(complexbase.GroupBase):
	"""Abstract class indicating an access point from an ExecutableEntity."""
	def __init__(self):
		super().__init__()
		self._returnValueProvision_child=ModelNone

class AbstractCanCluster(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN, J1939 and CAN Cluster attributes."""
	def __init__(self):
		super().__init__()
		self._busOffRecovery_child=ModelNone
		self._canFdBaudrate_child=ModelNone
		self._canXlBaudrate_child=ModelNone

class AbstractCanClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class AbstractCanCommunicationConnector(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN and CAN CommunicationConnector attributes."""

class AbstractCanCommunicationController(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN and CAN Controller attributes."""
	def __init__(self):
		super().__init__()
		self._canControllerAttributes_child=[]

class AbstractCanCommunicationControllerAttributes(complexbase.GroupBase):
	"""For the configuration of the CanController parameters two different approaches can be used:
# Providing exact values which are taken by the ECU developer (CanControllerConfiguration).
# Providing ranges of values which are taken as requirements and have to be respected by the ECU developer (CanControllerConfigurationRequirements)."""
	def __init__(self):
		super().__init__()
		self._canControllerFdAttributes_child=ModelNone
		self._canControllerFdRequirements_child=ModelNone
		self._canControllerXlAttributes_child=ModelNone
		self._canControllerXlRequirements_child=ModelNone

class AbstractCanCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class AbstractCanPhysicalChannel(complexbase.GroupBase):
	"""Abstract class that is used to collect the common TtCAN and CAN PhysicalChannel attributes."""

class AbstractClassTailoring(complexbase.GroupBase):
	"""Tailoring of abstract classes in the AUTOSAR meta-model"""

class AbstractCondition(complexbase.GroupBase):
	"""A premise upon which the fulfillment of an agreement depends"""

class AbstractDoIpLogicAddressProps(complexbase.GroupBase):
	"""Abstract meta-class that collects common properties for all specialized DoIpLogicAddressProps."""

class AbstractEnumerationValueVariationPoint(complexbase.GroupBase):
	"""This is an abstract EnumerationValueVariationPoint. It is introduced to support the case that additional attributes are required for particular purposes."""
	base=complexbase.Attribute("base",SimpleTypes.Identifier,'BASE',False,"""This attribute reflects the base to be used in context of EnumerationMappingTable for this reference.""")
	enumTable=complexbase.Attribute("enumTable",SimpleTypes.Ref,'ENUM-TABLE',False,"""This represents the assigned enumeration table.""")

class AbstractEthernetFrame(complexbase.GroupBase):
	"""Ethernet specific attributes to the Frame."""

class AbstractEvent(complexbase.GroupBase):
	"""This meta-class represents the abstract ability to model an event that can be taken to implement application software or basic software in AUTOSAR."""
	def __init__(self):
		super().__init__()
		self._activationReasonRepresentation_child=ModelNone

class AbstractExecutionContext(complexbase.GroupBase):
	"""This meta-class acts as a base class for entities that execute code on different levels, e.g. container, process, thread, fiber."""

class AbstractGlobalTimeDomainProps(complexbase.GroupBase):
	"""This abstract class enables a GlobalTimeDomain to specify additional properties."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class AbstractIamRemoteSubject(complexbase.GroupBase):
	"""This abstract meta-class defines the proxy information about the remote node."""

class AbstractImplementationDataType(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for different flavors of ImplementationDataType."""

class AbstractImplementationDataTypeElement(complexbase.GroupBase):
	"""This meta-class represents the ability to act as an abstract base class for specific derived meta-classes that support the modeling of ImplementationDataTypes for a particular language binding."""

class AbstractMethodInExecutableInstanceRef(complexbase.GroupBase):
	""

class AbstractMultiplicityRestriction(complexbase.GroupBase):
	"""Restriction that specifies the valid number of occurrences of an element in the current context."""
	def __init__(self):
		super().__init__()
		self._lowerMultiplicity_child=ModelNone
		self._upperMultiplicity_child=ModelNone
		self._upperMultiplicityInfinite_child=ModelNone

class AbstractNumericalVariationPoint(complexbase.GroupBase):
	"""This is an abstract NumericalValueVariationPoint. It is introduced to support the case that additional attributes are required for particular purposes."""

class AbstractPortPrototypeInExecutableInstanceRef(complexbase.GroupBase):
	""

class AbstractPortPrototypeInSoftwareClusterDesignInstanceRef(complexbase.GroupBase):
	""

class AbstractProvidedPortPrototype(complexbase.GroupBase):
	"""This abstract class provides the ability to become a provided PortPrototype."""
	def __init__(self):
		super().__init__()
		self._providedComSpec_children=[]

class AbstractRawDataStreamEthernetCredentials(complexbase.GroupBase):
	"""This meta-class serves as an abstract base class for the configuration of network credentials."""
	def __init__(self):
		super().__init__()
		self._ipV4Address_child=ModelNone
		self._ipV6Address_child=ModelNone
		self._udpPort_child=ModelNone

class AbstractRawDataStreamInterface(complexbase.GroupBase):
	"""This meta-class serves as an abstract base class for PortInterfaces related to raw data streams."""

class AbstractRequiredPortPrototype(complexbase.GroupBase):
	"""This abstract class provides the ability to become a required PortPrototype."""
	def __init__(self):
		super().__init__()
		self._requiredComSpec_children=[]

class AbstractRuleBasedValueSpecification(complexbase.GroupBase):
	"""This represents an abstract base class for all rule-based value specifications."""

class AbstractSecurityEventFilter(complexbase.GroupBase):
	"""This meta-class acts as a base class for security event filters."""

class AbstractSecurityIdsmInstanceFilter(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for security event filters that apply for an entire IdsM."""

class AbstractServiceInstance(complexbase.GroupBase):
	"""Provided and Consumed Ethernet Service Instances that are available at the ApplicationEndpoint."""
	def __init__(self):
		super().__init__()
		self._capabilityRecord_children=[]
		self._majorVersion_child=ModelNone
		self._methodActivationRoutingGroup_children=[]
		self._routingGroup_children=[]
		self._variationPoint_child=ModelNone

class AbstractSignalBasedToISignalTriggeringMapping(complexbase.GroupBase):
	"""This meta-class is the common class for all SIgnalBased to ISignalTRiggering mappings."""

class AbstractSynchronizedTimeBaseInterface(complexbase.GroupBase):
	"""This meta-class provides the abstract ability to define a PortInterface for the interaction with Time Synchronization."""

class AbstractValueRestriction(complexbase.GroupBase):
	"""Restricts primitive values. A value is valid if all rules that are defined by this restriction evaluate to true."""
	def __init__(self):
		super().__init__()
		self._max_child=ModelNone
		self._maxLength_child=ModelNone
		self._min_child=ModelNone
		self._minLength_child=ModelNone
		self._pattern_child=ModelNone

class AbstractVariationRestriction(complexbase.GroupBase):
	"""Defines constraints on the usage of variation and on the valid binding times."""
	def __init__(self):
		super().__init__()
		self._variation_child=ModelNone
		self._validBindingTime_children=[]

class AccessCount(complexbase.GroupBase):
	"""This meta-class provides one count value for a AbstractAccessPoint."""
	def __init__(self):
		super().__init__()
		self._accessPoint_child=ModelNone
		self._value_child=ModelNone
		self._variationPoint_child=ModelNone

class AccessCountSet(complexbase.GroupBase):
	"""This meta-class provides a set of count values evaluated according to the rules of a specific countProfile."""
	def __init__(self):
		super().__init__()
		self._accessCount_children=[]
		self._countProfile_child=ModelNone
		self._variationPoint_child=ModelNone

class AclObjectSet(complexbase.GroupBase):
	"""This meta class represents the ability to denote a set of objects for which roles and rights (access control lists) shall be defined. It basically can define the objects based on

* the nature of objects
* the involved blueprints
* the artifact in which the objects are serialized
* the definition of the object (in a definition - value pattern)
* individual reference objects"""
	def __init__(self):
		super().__init__()
		self._aclObjectClass_children=[]
		self._aclScope_child=ModelNone
		self._collection_child=ModelNone
		self._derivedFromBlueprint_children=[]
		self._engineeringObject_children=[]
		self._objectDefinition_children=[]
		self._objectDefintion_children=[]
		self._object_children=[]

class AclOperation(complexbase.GroupBase):
	"""This meta class represents the ability to denote a particular operation which may be performed on objects in an AUTOSAR model."""
	def __init__(self):
		super().__init__()
		self._impliedOperation_children=[]

class AclPermission(complexbase.GroupBase):
	"""This meta class represents the ability to represent permissions granted on objects in an AUTOSAR model."""
	def __init__(self):
		super().__init__()
		self._aclContext_children=[]
		self._aclObject_children=[]
		self._aclOperation_children=[]
		self._aclRole_children=[]
		self._aclScope_child=ModelNone

class AclRole(complexbase.GroupBase):
	"""This meta class represents the ability to specify a particular role which is used to grant access rights to AUTOSAR model. The purpose of this meta-class is to support the mutual agreements between the involved parties."""
	def __init__(self):
		super().__init__()
		self._ldapUrl_child=ModelNone

class AdaptiveApplicationSwComponentType(complexbase.GroupBase):
	"""This meta-class represents the ability to support the formal modeling of application software on the AUTOSAR adaptive platform. Consequently, it shall only be used on the AUTOSAR adaptive platform."""
	def __init__(self):
		super().__init__()
		self._internalBehavior_children=[]

class AdaptiveFirewallModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the Firewall configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._stateDepFirewall_children=[]
		self._stateDependentFirewall_child=ModelNone

class AdaptiveFirewallToPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class maps the AdaptiveFirewall moduleInstantiation to the RPortPrototype that is typed by a FirewallModeSwitchInterface."""
	def __init__(self):
		super().__init__()
		self._firewall_child=ModelNone
		self._rPortPrototype_child=ModelNone

class AdaptiveModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the abstract attributes for the configuration of an adaptive autosar module instance on a specific machine."""

class AdaptivePlatformServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a service instance in an abstract way."""
	def __init__(self):
		super().__init__()
		self._e2eEventProtectionProps_children=[]
		self._e2eMethodProtectionProps_children=[]
		self._secureComConfig_children=[]
		self._serviceInterfaceDeployment_child=ModelNone

class AdaptiveSwcInternalBehavior(complexbase.GroupBase):
	"""This meta-class represents the ability to define an internal behavior of an AtomicSwComponentType used on the AUTOSAR adaptive platform. 

Please note that the model of internal behavior in this case, in stark contrast to the situation of the AUTOSAR classic platform, is very minimal."""
	def __init__(self):
		super().__init__()
		self._serviceDependency_children=[]
		self._variationPoint_child=ModelNone

class AdminData(complexbase.GroupBase):
	"""AdminData represents the ability to express administrative information and custom extensions for an element. This administration information is to be treated as meta-data such as revision id or state of the file. There are basically the following kinds of meta-data

* The language and/or used languages.

* Revision information covering e.g. revision number, state, release date, changes. Note that this information can be given in general as well as related to a particular company.

* Document meta-data specific for a company

Beside that a custom extension of model-data is possible by

* Special data"""
	def __init__(self):
		super().__init__()
		self._language_child=ModelNone
		self._usedLanguages_child=ModelNone
		self._docRevision_children=[]
		self._sdg_children=[]

class AgeConstraint(complexbase.GroupBase):
	"""Constrains the [ARMetaClassRole{scope}{AgeConstraint}] by a [ARMetaClassRole{minimum}{AgeConstraint}] and [ARMetaClassRole{maximum}{AgeConstraint}] time boundary."""
	def __init__(self):
		super().__init__()
		self._maximum_child=ModelNone
		self._minimum_child=ModelNone
		self._scope_child=ModelNone

class AggregationCondition(complexbase.GroupBase):
	"""The AggregationCondition evaluates to true, if the referenced aggregation is accepted by all rules of this condition."""
	def __init__(self):
		super().__init__()
		self._aggregation_child=ModelNone

class AggregationTailoring(complexbase.GroupBase):
	"""Tailoring of aggregations in the AUTOSAR meta-model"""
	def __init__(self):
		super().__init__()
		self._typeTailoring_children=[]

class AliasNameAssignment(complexbase.GroupBase):
	"""This meta-class represents the ability to associate an alternative name to a flat representations or an Identifiable.

The usage of this name is defined outside of AUTOSAR. For example this name can be used by MCD tools or as a name for component instances in the ECU extract.

Note that flatInstance and identifiable are mutually exclusive."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._label_child=ModelNone
		self._identifiable_child=ModelNone
		self._flatInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class AliasNameSet(complexbase.GroupBase):
	"""This meta-class represents a set of AliasNames. The AliasNameSet can for example be an input to the A2L-Generator."""
	def __init__(self):
		super().__init__()
		self._aliasName_children=[]

class AliveSupervision(complexbase.GroupBase):
	"""Defines an AliveSupervision for one checkpoint."""
	def __init__(self):
		super().__init__()
		self._aliveReferenceCycle_child=ModelNone
		self._checkpoint_child=ModelNone
		self._expectedAliveIndications_child=ModelNone
		self._failedReferenceCyclesTolerance_child=ModelNone
		self._maxMargin_child=ModelNone
		self._minMargin_child=ModelNone
		self._terminatingCheckpoint_child=ModelNone
		self._terminatingCheckpointTimeoutUntilTermination_child=ModelNone

class Allocator(complexbase.GroupBase):
	"""This meta-class represents the ability to specify an optional custom C++ allocator for a C++ type which may dynamically grow beyond it's initial allocated size during it's lifetime.  Any storage principles are defined in the implementation of the allocator itself, which should implement the ISO C++ std::allocator_traits interface."""
	def __init__(self):
		super().__init__()
		self._headerFile_child=ModelNone
		self._namespace_children=[]

class AnalyzedExecutionTime(complexbase.GroupBase):
	"""AnalyzedExecutionTime provides an analytic method for specifying the best and worst case execution time."""
	def __init__(self):
		super().__init__()
		self._bestCaseExecutionTime_child=ModelNone
		self._worstCaseExecutionTime_child=ModelNone

class Annotation(complexbase.GroupBase):
	"""This is a plain annotation which does not have further formal data."""

class AnyInstanceRef(complexbase.GroupBase):
	"""Describes a reference to any instance in an AUTOSAR model. This is the most generic form of an instance ref. Refer to the superclass notes for more details."""
	def __init__(self):
		super().__init__()
		self._contextElement_children=[]
		self._target_child=ModelNone
		self._variationPoint_child=ModelNone

class ApApplicationEndpoint(complexbase.GroupBase):
	"""An application endpoint is the endpoint on an Ecu in terms of application addressing (e.g. UDP or TCP Port)."""
	def __init__(self):
		super().__init__()
		self._priority_child=ModelNone
		self._tpConfiguration_child=[]

class ApApplicationError(complexbase.GroupBase):
	"""This meta-class represents the ability to formally specify the semantics of an application error on the AUTOSAR adaptive platform"""
	def __init__(self):
		super().__init__()
		self._errorCode_child=ModelNone
		self._errorDomain_child=ModelNone

class ApApplicationErrorDomain(complexbase.GroupBase):
	"""This meta-class represents the ability to define a global error domain for an ApApplicationError."""
	def __init__(self):
		super().__init__()
		self._namespace_children=[]
		self._value_child=ModelNone

class ApApplicationErrorSet(complexbase.GroupBase):
	"""This meta-class acts as a reference target that represents an entire collection of APApplicationErrors. This takes the burden from ClientServerOperations that reference a larger number of ApApplicationErrors."""
	def __init__(self):
		super().__init__()
		self._apApplicationError_children=[]

class ApSomeipTransformationProps(complexbase.GroupBase):
	"""SOME/IP serialization properties."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._byteOrder_child=ModelNone
		self._implementsLegacyStringSerialization_child=ModelNone
		self._isDynamicLengthFieldSize_child=ModelNone
		self._sessionHandling_child=ModelNone
		self._sizeOfArrayLengthField_child=ModelNone
		self._sizeOfStringLengthField_child=ModelNone
		self._sizeOfStructLengthField_child=ModelNone
		self._sizeOfUnionLengthField_child=ModelNone
		self._sizeOfUnionTypeSelectorField_child=ModelNone
		self._stringEncoding_child=ModelNone

class AppOsTaskProxyToEcuTaskProxyMapping(complexbase.GroupBase):
	"""This meta-class is used to map an OsTaskProxy that was created in the context of a SwComponent to an OsTaskProxy that was created in the context of an Ecu."""
	def __init__(self):
		super().__init__()
		self._appTaskProxy_child=ModelNone
		self._ecuTaskProxy_child=ModelNone
		self._offset_child=ModelNone

class ApplicabilityInfo(complexbase.GroupBase):
	"""ApplicabilityInfo describes the imposition time and the applied standard of a dedicated constraint."""
	def __init__(self):
		super().__init__()
		self._it_child=ModelNone
		self._remark_child=ModelNone
		self._std_children=[]
		self._trace_child=ModelNone

class ApplicabilityInfoSet(complexbase.GroupBase):
	"""This meta class represents the ability to attach an imposition time and standard name information to a particular set of elements."""
	def __init__(self):
		super().__init__()
		self._applicabilityInfo_children=[]
		self._appliedStandard_children=[]
		self._usedImpositionTimeDefinitionGroup_child=ModelNone

class ApplicationArrayDataType(complexbase.GroupBase):
	"""An application data type which is an array, each element is of the same application data type."""
	def __init__(self):
		super().__init__()
		self._dynamicArraySizeProfile_child=ModelNone
		self._element_child=ModelNone

class ApplicationArrayElement(complexbase.GroupBase):
	"""Describes the properties of the elements of an application array data type."""
	def __init__(self):
		super().__init__()
		self._arraySizeHandling_child=ModelNone
		self._arraySizeSemantics_child=ModelNone
		self._indexDataType_child=ModelNone
		self._maxNumberOfElements_child=ModelNone

class ApplicationAssocMapDataType(complexbase.GroupBase):
	"""An application data type which is a map and consists of a key and a value"""
	def __init__(self):
		super().__init__()
		self._key_child=ModelNone
		self._value_child=ModelNone

class ApplicationAssocMapElement(complexbase.GroupBase):
	"""Describes the properties of the elements of an application map data type."""

class ApplicationAssocMapElementValueSpecification(complexbase.GroupBase):
	"""This meta-class represents the ability to define the initialization of the elements of an ApplicationAssocMapDataType."""
	def __init__(self):
		super().__init__()
		self._key_child=[]
		self._value_child=[]

class ApplicationAssocMapValueSpecification(complexbase.GroupBase):
	"""This meta-class represents the ability to define the initialization of an ApplicationAssocMapDataType."""
	def __init__(self):
		super().__init__()
		self._mapElementTuple_children=[]

class ApplicationCompositeDataType(complexbase.GroupBase):
	"""Abstract base class for all application data types composed of other data types."""

class ApplicationCompositeDataTypeSubElementRef(complexbase.GroupBase):
	"""This meta-class represents the specialization of SubElementMapping with respect to ApplicationCompositeDataTypes."""
	def __init__(self):
		super().__init__()
		self._applicationCompositeElement_child=ModelNone

class ApplicationCompositeElementDataPrototype(complexbase.GroupBase):
	"""This class represents a data prototype which is aggregated within a composite application data type (record or array). It is introduced to provide a better distinction between target and context in instanceRefs."""
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone

class ApplicationCompositeElementInPortInterfaceInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._rootDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ApplicationDataPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._rootDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ApplicationDataType(complexbase.GroupBase):
	"""ApplicationDataType defines a data type from the application point of view. Especially it should be used whenever something \"physical\" is at stake. 

An ApplicationDataType represents a set of values as seen in the application model, such as measurement units. It does not consider implementation details such as bit-size, endianess, etc.

It should be possible to model the application level aspects of a VFB system by using ApplicationDataTypes only."""

class ApplicationDeferredDataType(complexbase.GroupBase):
	"""A placeholder data type in which the precise application data type is deferred to a later stage."""

class ApplicationEndpoint(complexbase.GroupBase):
	"""An application endpoint is the endpoint on an Ecu in terms of application addressing (e.g. socket). The application endpoint represents e.g. the listen socket in client-server-based communication."""
	def __init__(self):
		super().__init__()
		self._consumedServiceInstance_children=[]
		self._discoveryTechnology_child=ModelNone
		self._maxNumberOfConnections_child=ModelNone
		self._networkEndpoint_child=ModelNone
		self._priority_child=ModelNone
		self._providedServiceInstance_children=[]
		self._remotingTechnology_child=ModelNone
		self._serializationTechnology_child=ModelNone
		self._tlsCryptoMapping_child=ModelNone
		self._tpConfiguration_child=[]

class ApplicationEndpointRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ApplicationEndpoint_child=ModelNone
		self._variationPoint_child=ModelNone

class ApplicationEntry(complexbase.GroupBase):
	"""Schedule table entry for application messages."""
	def __init__(self):
		super().__init__()
		self._frameTriggering_child=ModelNone

class ApplicationError(complexbase.GroupBase):
	"""This is a user-defined error that is associated with an element of an AUTOSAR interface. It is specific for the particular functionality or service provided by the AUTOSAR software component."""
	def __init__(self):
		super().__init__()
		self._errorCode_child=ModelNone

class ApplicationErrorMapping(complexbase.GroupBase):
	"""In client server communication, the server may return any value within the application error range.

The ApplicationError is mapped to the responseGroup."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class ApplicationInterface(complexbase.GroupBase):
	"""This represents the ability to define a PortInterface that consists of a composition of commands (method calls), indications (events) and attributes (fields)"""
	def __init__(self):
		super().__init__()
		self._attribute_children=[]
		self._command_children=[]
		self._indication_children=[]

class ApplicationPartition(complexbase.GroupBase):
	"""ApplicationPartition to which SwComponentPrototypes are mapped at a point in time when the corresponding EcuInstance is not yet known or defined. In a later methodology step the ApplicationPartition can be assigned to an EcuPartition."""

class ApplicationPartitionToEcuPartitionMapping(complexbase.GroupBase):
	"""Maps ApplicationPartitions to EcuPartitions. With this mapping an OEM has the option to predefine an allocation of Software Components to EcuPartitions in the System Design phase. The final and complete assignment is described in the OS Configuration."""
	def __init__(self):
		super().__init__()
		self._applicationPartition_children=[]
		self._ecuPartition_child=ModelNone
		self._variationPoint_child=ModelNone

class ApplicationPrimitiveDataType(complexbase.GroupBase):
	"""A primitive data type defines a set of allowed values."""

class ApplicationRecordDataType(complexbase.GroupBase):
	"""An application data type which can be decomposed into prototypes of other application data types."""
	def __init__(self):
		super().__init__()
		self._element_children=[]

class ApplicationRecordElement(complexbase.GroupBase):
	"""Describes the properties of one particular element of an application record data type."""
	def __init__(self):
		super().__init__()
		self._isOptional_child=ModelNone
		self._variationPoint_child=ModelNone

class ApplicationRuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class represents rule based values for DataPrototypes typed by ApplicationDataTypes (ApplicationArrayDataType or a compound ApplicationPrimitiveDataType which also boils down to an array-nature)."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._swAxisCont_children=[]
		self._swValueCont_child=ModelNone

class ApplicationSwComponentType(complexbase.GroupBase):
	"""The ApplicationSwComponentType is used to represent the application software."""

class ApplicationValueSpecification(complexbase.GroupBase):
	"""This meta-class represents values for DataPrototypes typed by ApplicationDataTypes (this includes in particular compound primitives). 

For further details refer to ASAM CDF 2.0. This meta-class corresponds to some extent with SW-INSTANCE in ASAM CDF 2.0."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._swAxisCont_children=[]
		self._swValueCont_child=ModelNone

class AppliedStandard(complexbase.GroupBase):
	"""AppliedStandard describes the applied standard of a dedicated constraint."""
	def __init__(self):
		super().__init__()
		self._appliesTo_child=ModelNone

class ARElement(complexbase.GroupBase):
	"""An element that can be defined stand-alone, i.e. without being part of another element (except for packages of course)."""

class ARObject(complexbase.GroupBase):
	"""Implicit base class of all classes in meta-model."""
	checksum=complexbase.Attribute("checksum",SimpleTypes.String,'S',False,"""Checksum calculated by the user's tool environment for an ArObject. May be used in an own tool environment to determine if an ArObject has changed. The checksum has no semantic meaning for an AUTOSAR model and there is no requirement for AUTOSAR tools to manage the checksum.""")
	timestamp=complexbase.Attribute("timestamp",SimpleTypes.DateTime,'T',False,"""Timestamp calculated by the user's tool environment for an ArObject. May be used in an own tool environment to determine the last change of an ArObject. The timestamp has no semantic meaning for an AUTOSAR model and there is no requirement for AUTOSAR tools to manage the timestamp.""")

class ARPackage(complexbase.GroupBase):
	"""AUTOSAR package, allowing to create top level packages to structure the contained ARElements.

ARPackages are open sets. This means that in a file based description system multiple files can be used to partially describe the contents of a package.

This is an extended version of MSR's SW-SYSTEM."""
	def __init__(self):
		super().__init__()
		self._referenceBase_children=[]
		self._element_children=[]
		self._arPackage_children=[]
		self._variationPoint_child=ModelNone

class ArParameterInImplementationDataInstanceRef(complexbase.GroupBase):
	"""This class represents the ability to navigate into an element inside of an ParameterDataPrototype typed  by an ImplementationDatatype.

Note that it shall not be used if the target is the ParameterDataPrototype itself (e.g. if the target is a primitive data type).

Note that this class follows the pattern of an InstanceRef but is not implemented based on the abstract classes because the ImplementationDataType isn't either, especially because ImplementationDataTypeElement (intentionally) isn't derived from AtpPrototype."""
	def __init__(self):
		super().__init__()
		self._contextDataPrototype_children=[]
		self._portPrototype_child=ModelNone
		self._rootParameterDataPrototype_child=ModelNone
		self._targetDataPrototype_child=ModelNone

class ArVariableInImplementationDataInstanceRef(complexbase.GroupBase):
	"""This class represents the ability to navigate into a data element inside of an VariableDataPrototype which is typed  by an ImplementationDatatype.

Note that it shall not be used if the target is the VariableDataPrototype itself (e.g. if its a primitive).

Note that this class follows the pattern of an InstanceRef but is not implemented based on the abstract classes because the ImplementationDataType isn't either, especially because ImplementationDataTypeElement isn't derived from AtpPrototype."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._rootVariableDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ArbitraryEventTriggering(complexbase.GroupBase):
	"""Describes that an event occurs occasionally, singly, irregularly or randomly.

The primary purpose of this event triggering is to abstract event occurrences captured by data acquisition tools (background debugger, trace analyzer, etc.) during system runtime."""
	def __init__(self):
		super().__init__()
		self._minimumDistance_children=[]
		self._maximumDistance_children=[]
		self._confidenceInterval_children=[]

class Area(complexbase.GroupBase):
	"""This element specifies a region in an image map. Image maps enable authors to specify regions in an object (e.g. a graphic) and to assign a specific activity to each region (e.g. load a document, launch a program etc.).

For more details refer to the specification of HTML."""
	accesskey=complexbase.Attribute("accesskey",SimpleTypes.String,'ACCESSKEY',False,"""This attribute assigns an access key to an element.
An access key is an individual character (e.g. \"B\") within the document character
range. If an access key with an element assigned to it is pressed, the element comes
into focus. The activity performed when an element comes into focus, is dependent on
the element itself""")
	alt=complexbase.Attribute("alt",SimpleTypes.String,'ALT',False,"""This attribute specifies the text to be inserted as an alternative to illustrations, shapes or applets, where these
cannot be displayed by user agents.""")
	class_=complexbase.Attribute("class_",SimpleTypes.String,'CLASS',False,"""Blank separated list of classes""")
	coords=complexbase.Attribute("coords",SimpleTypes.String,'COORDS',False,"""This attribute specifies the position and shape on the screen.
The number of values and their order depend on the geometrical figure defined.""")
	href=complexbase.Attribute("href",SimpleTypes.String,'HREF',False,"""This attribute specifies the memory location of a web resource. It is therefore able to specify a link between the current element and the target element.""")
	nohref=complexbase.Attribute("nohref",SimpleTypes.AreaEnumNohref,'NOHREF',False,"""If this attribute is set, the Area has no associated link.""")
	onblur=complexbase.Attribute("onblur",SimpleTypes.String,'ONBLUR',False,"""The ONBLUR-Event occurs, when focus is switched away from an element. 

A script can be stored in this attribute to be performed in the Event.""")
	onclick=complexbase.Attribute("onclick",SimpleTypes.String,'ONCLICK',False,"""The ONCLICK-Event occurs, if the current element is clicked-on. 

A script can be stored in this attribute to be performed in the Event.""")
	ondblclick=complexbase.Attribute("ondblclick",SimpleTypes.String,'ONDBLCLICK',False,"""The ONCLICK-Event occurs, if the current element is \"double\" clicked-on. 

A script can be stored in this attribute to be performed in the Event.""")
	onfocus=complexbase.Attribute("onfocus",SimpleTypes.String,'ONFOCUS',False,"""The ONFOCUS-Event occurs, if an element comes into focus (e.g., through navigation using the tab button). 

A script can be stored in this attribute to be performed in the Event.""")
	onkeydown=complexbase.Attribute("onkeydown",SimpleTypes.String,'ONKEYDOWN',False,"""The ONKEYDOWN-Event occurs, if a button on the current element is pressed down. 

A script can be stored in this attribute to be performed in the event.""")
	onkeypress=complexbase.Attribute("onkeypress",SimpleTypes.String,'ONKEYPRESS',False,"""The ONKEYPRESS-Event occurs, if a button on the current element is pressed down and released. 

A script can be stored in this attribute to be performed in the Event.""")
	onkeyup=complexbase.Attribute("onkeyup",SimpleTypes.String,'ONKEYUP',False,"""The ONKEYUP-Event occurs, if a button on the current element is released. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousedown=complexbase.Attribute("onmousedown",SimpleTypes.String,'ONMOUSEDOWN',False,"""The ONMOUSEDOWN-Event occurs, if the mouse button used for clicking is held down on the current element. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousemove=complexbase.Attribute("onmousemove",SimpleTypes.String,'ONMOUSEMOVE',False,"""The ONMOUSEMOVE-Event occurs, if the mouse pointer is moved on the current 
element (i.e. it is located on the current element). 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseout=complexbase.Attribute("onmouseout",SimpleTypes.String,'ONMOUSEOUT',False,"""The ONMOUSEOUT-Event occurs, if the mouse pointer is moved from the current element.

A script can be stored in this attribute to be performed in the Event.""")
	onmouseover=complexbase.Attribute("onmouseover",SimpleTypes.String,'ONMOUSEOVER',False,"""The ONMOUSEOVER-Event occurs, if the mouse pointer is moved to the current element
from another location outside it. 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseup=complexbase.Attribute("onmouseup",SimpleTypes.String,'ONMOUSEUP',False,"""The ONMOUSEUP-Event occurs if the mouse button used for clicking is released on the
current element. 

A script can be stored in this attribute to be performed in the Event.""")
	shape=complexbase.Attribute("shape",SimpleTypes.AreaEnumShape,'SHAPE',False,"""The shape of the area. Note that in HTML this is defaulted to RECT.""")
	style=complexbase.Attribute("style",SimpleTypes.String,'STYLE',False,"""Information on the associated style""")
	tabindex=complexbase.Attribute("tabindex",SimpleTypes.String,'TABINDEX',False,"""This attribute specifies the position of the current element in tabbing-order for the corresponding document.

The value shall lie between 0 and 32767. The Tabbing Order defines the sequence in which elements are focused on, when the user navigates using the keyboard.""")
	title=complexbase.Attribute("title",SimpleTypes.String,'TITLE',False,"""Title information of the Area element""")

class ArgumentDataPrototype(complexbase.GroupBase):
	"""An argument of an operation, much like a data element, but also carries direction information and is owned by a particular ClientServerOperation."""
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._serverArgumentImplPolicy_child=ModelNone
		self._typeBlueprint_children=[]
		self._variationPoint_child=ModelNone

class ArrayValueSpecification(complexbase.GroupBase):
	"""Specifies the values for an array."""
	def __init__(self):
		super().__init__()
		self._element_children=[]
		self._intendedPartialInitializationCount_child=ModelNone

class ArtifactChecksum(complexbase.GroupBase):
	"""This meta-class provides the ability to associate a checksum with a given artifact identified by its URI."""
	def __init__(self):
		super().__init__()
		self._checksumValue_child=ModelNone
		self._uri_child=ModelNone

class ArtifactChecksumToCryptoProviderMapping(complexbase.GroupBase):
	"""This meta-class provides the ability to associate a CryptoProvider with a collection of the checksums computed for artifacts."""
	def __init__(self):
		super().__init__()
		self._artifactChecksum_children=[]
		self._cryptoProvider_child=ModelNone

class ArtifactLocator(complexbase.GroupBase):
	"""This meta-class has the ability to define the location of an artifact that is represented by a model element, e.g. Executable."""
	def __init__(self):
		super().__init__()
		self._representedModelElement_child=ModelNone
		self._uri_child=ModelNone

class AssemblySwConnector(complexbase.GroupBase):
	"""AssemblySwConnectors are exclusively used to connect SwComponentPrototypes in the context of a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._provider_child=ModelNone
		self._requester_child=ModelNone

class AssignFrameId(complexbase.GroupBase):
	"""Schedule entry for an  Assign Frame Id master request."""
	def __init__(self):
		super().__init__()
		self._assignedFrameTriggering_child=ModelNone
		self._messageId_child=ModelNone

class AssignFrameIdRange(complexbase.GroupBase):
	"""AssignFrameIdRange generates an assign frame PID range request."""
	def __init__(self):
		super().__init__()
		self._framePid_children=[]
		self._startIndex_child=ModelNone

class AssignNad(complexbase.GroupBase):
	"""Schedule entry for an Assign NAD master request."""
	def __init__(self):
		super().__init__()
		self._newNad_child=ModelNone

class AsynchronousServerCallPoint(complexbase.GroupBase):
	"""An AsynchronousServerCallPoint is used for asynchronous invocation of a ClientServerOperation.

IMPORTANT: a ServerCallPoint cannot be used concurrently. Once the client RunnableEntity has made the invocation, the ServerCallPoint cannot be used until the call returns (or an error occurs!) at which point the ServerCallPoint becomes available again."""

class AsynchronousServerCallResultPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns a AsynchronousServerCallResultPoint it is entitled to get the result of the referenced AsynchronousServerCallPoint.
If it is associated with AsynchronousServerCallReturnsEvent, this RTEEvent notifies the completion of the required ClientServerOperation or a timeout. The occurrence of this event can either unblock a WaitPoint or can lead to the invocation of a RunnableEntity."""
	def __init__(self):
		super().__init__()
		self._asynchronousServerCallPoint_child=ModelNone
		self._variationPoint_child=ModelNone

class AsynchronousServerCallReturnsEvent(complexbase.GroupBase):
	"""This event is raised when an asynchronous server call is finished."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class AtomicSwComponentType(complexbase.GroupBase):
	"""An atomic software component is atomic in the sense that it cannot be further decomposed and distributed across multiple ECUs."""
	def __init__(self):
		super().__init__()
		self._internalBehavior_children=[]
		self._symbolProps_child=ModelNone

class AtpBlueprint(complexbase.GroupBase):
	"""This meta-class represents the ability to act as a Blueprint. As this class is an abstract one, particular blueprint meta-classes inherit from this one."""
	def __init__(self):
		super().__init__()
		self._blueprintPolicy_children=[]
		self._shortNamePattern_child=ModelNone

class AtpBlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to express a particular mapping between a blueprint and an element derived from this blueprint.

Particular mappings are defined by specializations of this meta-class."""

class AtpBlueprintable(complexbase.GroupBase):
	"""This meta-class represents the ability to be derived from a Blueprint. As this class is an abstract one, particular blueprintable meta-classes inherit from this one."""

class AtpClassifier(complexbase.GroupBase):
	"""A classifier classifies M0 instances according to their features. Or: a classifier is something that has instances - an M1 classifier has M0 instances."""

class AtpDefinition(complexbase.GroupBase):
	"""This abstract meta class represents \"definition\"-elements which identify the respective values. For example the value of a particular system constant is identified by the definition of this system constant."""

class AtpFeature(complexbase.GroupBase):
	"""Features are properties via which a classifier classifies instances. Or: a classifier has features and every M0 instance of it will have those features."""

class AtpInstanceRef(complexbase.GroupBase):
	"""An M0 instance of a classifier may be represented as a tree rooted at that instance, where under each node come the sub-trees representing the instances which act as features under that node. 

An instance ref specifies a navigation path from any M0 tree-instance of the base (which is a classifier) to a leaf (which is an instance of the target)."""

class AtpPrototype(complexbase.GroupBase):
	"""A prototype is a typed feature. A prototype in a classifier indicates that instances of that classifier will have a feature, and the structure of that feature is given by the its type. An instance of that type will play the role indicated by the feature in the owning classifier.

A feature is not an instance but an indication of an instance-to-be."""

class AtpStructureElement(complexbase.GroupBase):
	"""A structure element is both a classifier and a feature. As a feature, its structure is given by the feature it owns as a classifier."""

class AtpType(complexbase.GroupBase):
	"""A type is a classifier that may serve to type prototypes. It is a reusable classifier."""

class AttributeCondition(complexbase.GroupBase):
	"""The AttributeCondition evaluates to true, if the referenced attribute is accepted by all rules of this condition."""

class AttributeTailoring(complexbase.GroupBase):
	"""Tailoring of Attributes"""
	def __init__(self):
		super().__init__()
		self._multiplicityRestriction_child=ModelNone
		self._variationRestriction_child=ModelNone

class AttributeValueVariationPoint(complexbase.GroupBase):
	"""This class represents the ability to derive the value of the Attribute from a system constant (by SwSystemconstDependentFormula). It also provides a bindingTime."""
	bindingTime=complexbase.Attribute("bindingTime",SimpleTypes.BindingTimeEnum,'BINDING-TIME',False,"""This is the binding time in which the attribute value needs to be bound.

If this attribute is missing, the attribute is not a variation point. In particular this means that It needs to be a single value according to the type specified in the pure model. It is an error if it is still a formula.""")
	blueprintValue=complexbase.Attribute("blueprintValue",SimpleTypes.String,'BLUEPRINT-VALUE',False,"""This represents a description that documents how the value shall be defined when deriving objects from the blueprint.""")
	sd=complexbase.Attribute("sd",SimpleTypes.String,'SD',False,"""This special data is provided to allow synchronization of Attribute value variation points with variant management systems. The usage is subject of agreement between the involved parties.""")
	shortLabel=complexbase.Attribute("shortLabel",SimpleTypes.PrimitiveIdentifier,'SHORT-LABEL',False,"""This allows to identify the variation point. It is also intended to allow RTE support for CompileTime Variation points.""")

class AUTOSAR(complexbase.GroupBase):
	"""Root element of an AUTOSAR description, also the root element in corresponding XML documents."""
	def __init__(self):
		super().__init__()
		self._fileInfoComment_child=ModelNone
		self._adminData_child=ModelNone
		self._introduction_child=ModelNone
		self._arPackage_children=[]

class AutosarDataPrototype(complexbase.GroupBase):
	"""Base class for prototypical roles of an AutosarDataType."""
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone

class AutosarDataPrototypeInExecutableInstanceRef(complexbase.GroupBase):
	""

class AutosarDataType(complexbase.GroupBase):
	"""Abstract base class for user defined AUTOSAR data types for software."""
	def __init__(self):
		super().__init__()
		self._swDataDefProps_child=ModelNone

class AutosarDataTypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._AutosarDataType_child=ModelNone
		self._variationPoint_child=ModelNone

class AutosarEngineeringObject(complexbase.GroupBase):
	"""This denotes an engineering object being part of the process. It is a specialization of the abstract class EngineeringObject for usage within AUTOSAR."""

class AutosarOperationArgumentInstance(complexbase.GroupBase):
	"""This class represents a reference to an argument instance. This way it is possible to reference an argument instance in the occurrence expression formula. The argument instance can target to one of the following arguments:

* a whole argument used in an operation of a PortPrototype with ClientServerInterface

* an element inside of a composite argument used in an operation of a PortPrototype with ClientServerInterface"""
	def __init__(self):
		super().__init__()
		self._operationArgumentInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class AutosarParameterRef(complexbase.GroupBase):
	"""This class represents a reference to a parameter within AUTOSAR which can be one of the following use cases:

localParameter:

* localParameter which is used as whole (e.g.  sharedAxis for curve)

autosarVariable:

* a parameter provided via PortPrototype which is used as whole (e.g. parameterAccess)

* an element inside of a composite local parameter typed by ApplicationDatatype (e.g. sharedAxis for a curve)

* an element inside of a composite parameter provided via Port and typed by ApplicationDatatype (e.g. sharedAxis for a curve)

autosarParameterInImplDatatype:

* an element inside of a composite local parameter typed by ImplementationDatatype

* an element inside of a composite parameter provided via PortPrototype and typed by ImplementationDatatype"""
	def __init__(self):
		super().__init__()
		self._autosarParameter_child=ModelNone
		self._localParameter_child=ModelNone

class AutosarVariableInstance(complexbase.GroupBase):
	"""This class represents a reference to a variable instance within AUTOSAR. This way it is possible to reference a variable instance in the occurrence expression formula. The variable instance can target to one of the following variables:

* a variable provided via a PortPrototype as whole

* an element inside of a composite variable provided via a PortPrototype"""
	def __init__(self):
		super().__init__()
		self._variableInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class AutosarVariableRef(complexbase.GroupBase):
	"""This class represents a reference to a variable within AUTOSAR which can be one of the following use cases:

localVariable:

* localVariable which is used as whole (e.g. InterRunnableVariable, inputValue for curve)

autosarVariable:

* a variable provided via Port which is used as whole (e.g. dataAccesspoints)

* an element inside of a composite local variable typed by ApplicationDatatype (e.g. inputValue for a curve)

* an element inside of a composite variable provided via Port and typed by ApplicationDatatype (e.g. inputValue for a curve)

autosarVariableInImplDatatype:

* an element inside of a composite local variable typed by ImplementationDatatype  (e.g. nvramData mapping)

* an element inside of a composite variable provided via Port and typed by ImplementationDatatype (e.g. inputValue for a curve)"""
	def __init__(self):
		super().__init__()
		self._autosarVariableInImplDatatype_child=ModelNone
		self._autosarVariable_child=ModelNone
		self._localVariable_child=ModelNone

class BackgroundEvent(complexbase.GroupBase):
	"""This event is used to start RunnableEntities that are supposed to be executed in the background."""

class BaseType(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to specify a platform dependent base type."""
	def __init__(self):
		super().__init__()
		self._nativeDeclaration_child=ModelNone
		self._byteOrder_child=ModelNone
		self._memAlignment_child=ModelNone
		self._baseTypeEncoding_child=ModelNone
		self._maxBaseTypeSize_child=ModelNone
		self._baseTypeSize_child=ModelNone

class BaseTypeDefinition(complexbase.GroupBase):
	"""This meta-class represents the ability to define a basetype."""

class BaseTypeDirectDefinition(complexbase.GroupBase):
	"""This BaseType is defined directly (as opposite to a derived BaseType)"""
	def __init__(self):
		super().__init__()
		self._baseTypeSize_child=ModelNone
		self._maxBaseTypeSize_child=ModelNone
		self._baseTypeEncoding_child=ModelNone
		self._memAlignment_child=ModelNone
		self._byteOrder_child=ModelNone
		self._nativeDeclaration_child=ModelNone

class Baseline(complexbase.GroupBase):
	"""Specification of the baseline of the AUTOSAR standard this Data Exchange Point relates to. The baseline is specified by listing the AUTOSAR products and their revisions. Custom defined functionality and deviations to the standard can be provided as well. 
All references to specification elements in this Data Exchange Point refer to specification elements that are part of this specification baseline."""
	def __init__(self):
		super().__init__()
		self._standardRevision_children=[]
		self._customSpecification_children=[]
		self._customSdgDef_children=[]

class BinaryManifestAddressableObject(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for addressable objects in the context of the binary manifest of a CP software cluster."""
	def __init__(self):
		super().__init__()
		self._address_child=ModelNone
		self._symbol_child=ModelNone

class BinaryManifestItem(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a specific handle or auxiliary field in the context of  binary manifest resource."""
	def __init__(self):
		super().__init__()
		self._isUnused_child=ModelNone
		self._value_child=[]
		self._defaultValue_child=[]
		self._auxiliaryField_children=[]

class BinaryManifestItemDefinition(complexbase.GroupBase):
	"""This meta-class provides the ability to define the handle definition or an auxiliary field of a binary manifest resource."""
	def __init__(self):
		super().__init__()
		self._auxiliaryFieldDefinition_children=[]
		self._isOptional_child=ModelNone
		self._size_child=ModelNone

class BinaryManifestItemNumericalValue(complexbase.GroupBase):
	"""This meta-class has the ability to provide a numerical value for a binary manifest item."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class BinaryManifestItemPointerValue(complexbase.GroupBase):
	"""This meta-class has the ability to provide a value for a pointer in the context of a binary manifest item."""
	def __init__(self):
		super().__init__()
		self._address_child=ModelNone
		self._symbol_child=ModelNone

class BinaryManifestItemValue(complexbase.GroupBase):
	"""This meta-class has the ability to act as an abstract base class for values of binary manifest item."""

class BinaryManifestMetaDataField(complexbase.GroupBase):
	"""This meta-class provides the ability to define a meta-data field for the binary manifest descriptor."""
	def __init__(self):
		super().__init__()
		self._size_child=ModelNone
		self._value_child=ModelNone

class BinaryManifestProvideResource(complexbase.GroupBase):
	"""This meta-class represents a provided resource in the binary manifest."""
	def __init__(self):
		super().__init__()
		self._numberOfNotifierSets_child=ModelNone
		self._supportsMultipleNotifierSets_child=ModelNone

class BinaryManifestRequireResource(complexbase.GroupBase):
	"""This meta-class represents a required resource in the binary manifest."""
	def __init__(self):
		super().__init__()
		self._connectionIsMandatory_child=ModelNone

class BinaryManifestResource(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for specializations."""
	def __init__(self):
		super().__init__()
		self._globalResourceId_child=ModelNone
		self._item_children=[]
		self._resourceDefinition_child=ModelNone
		self._resourceGuardValue_child=ModelNone
		self._resource_child=ModelNone

class BinaryManifestResourceDefinition(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a resource definition that provides information that can be shared by all resources that refer to the respective resource definition."""
	def __init__(self):
		super().__init__()
		self._itemDefinition_children=[]

class BlockState(complexbase.GroupBase):
	"""This meta-class defines a block state that is part of the collection of block states belonging to a specific IdsmInstance.
The IdsM shall discard any reported security event that is mapped to a filter chain containing a SecurityEventStateFilter that references the block state which is currently active in the IdsM."""

class BlueprintFormula(complexbase.GroupBase):
	"""This class express the extension of the Formula Language to provide formalized blueprint-Value resp. blueprintCondition."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_children=[]
		self._ecuc_children=[]
		self._verbatim_children=[]

class BlueprintGenerator(complexbase.GroupBase):
	"""This class express the Extended Language to generate blueprint derivates in complex descriptions."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._expression_child=ModelNone

class BlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map two an object and its blueprint."""
	def __init__(self):
		super().__init__()
		self._blueprint_child=ModelNone
		self._derivedObject_child=ModelNone

class BlueprintMappingSet(complexbase.GroupBase):
	"""This represents a container of mappings between \"actual\" model elements and the \"blueprint\" that has been taken for their creation."""
	def __init__(self):
		super().__init__()
		self._blueprintMap_children=[]

class BlueprintPolicy(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate whether blueprintable elements will be modifiable or not modifiable."""
	def __init__(self):
		super().__init__()
		self._attributeName_child=ModelNone

class BlueprintPolicyList(complexbase.GroupBase):
	"""The class represents that the related attribute is modifiable during the blueprinting. It applies only to attribute with upper multiplicity greater than 1."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfElements_child=ModelNone
		self._minNumberOfElements_child=ModelNone

class BlueprintPolicyModifiable(complexbase.GroupBase):
	"""The class represents that the related attribute is modifiable during the blueprinting."""
	def __init__(self):
		super().__init__()
		self._blueprintDerivationGuide_child=ModelNone

class BlueprintPolicyNotModifiable(complexbase.GroupBase):
	"""The class represents that the related attribute is not modifiable during the blueprinting."""

class BlueprintPolicySingle(complexbase.GroupBase):
	"""The class represents that the related attribute is modifiable during the blueprinting. It applies only to attribute with upper multiplicity equal 1."""

class BooleanValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Boolean attributes.

Note that this class might be used in the extended meta-model on"""

class Br(complexbase.GroupBase):
	"""This element is the same as function here as in a HTML document i.e. it forces a line break."""

class BswApiOptions(complexbase.GroupBase):
	"""This meta-class represents the ability to define options for the definition of the signature of function prototypes."""
	def __init__(self):
		super().__init__()
		self._enableTakeAddress_child=ModelNone

class BswAsynchronousServerCallPoint(complexbase.GroupBase):
	"""Represents an asynchronous procedure call point via the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._calledEntry_child=ModelNone

class BswAsynchronousServerCallResultPoint(complexbase.GroupBase):
	"""The callback point for an BswAsynchronousServerCallPoint i.e. the point at which the result can be retrieved from the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._asynchronousServerCallPoint_child=ModelNone

class BswAsynchronousServerCallReturnsEvent(complexbase.GroupBase):
	"""This is the \"callback\" event for asynchronous Client-Server-Communication via the BSW Scheduler which is thrown after completion of the asynchronous Client-Server call.

Its eventSource specifies the call point to be used for retrieving the result."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class BswBackgroundEvent(complexbase.GroupBase):
	"""A recurring BswEvent which is used to perform background activities. It is similar to a BswTimingEvent but has no fixed time period and is activated only with low priority."""

class BswCalledEntity(complexbase.GroupBase):
	"""BSW module entity which is designed to be called from another BSW module or cluster."""

class BswClientPolicy(complexbase.GroupBase):
	"""The requiredClientServerEntry for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._requiredClientServerEntry_child=ModelNone
		self._variationPoint_child=ModelNone

class BswCompositionTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints for a set of BswImplementations representing a BSW composition. A constraint defined at this level holds true for all referenced BswImplementations. Note, that multiple implementations of the same basic software module could be involved.

TimingDescriptions aggregated by BswCompositionTiming are restricted to event chains referring to events which are derived from the class TDEventBswInternalBehavior and TDEventBsw."""
	def __init__(self):
		super().__init__()
		self._implementation_children=[]

class BswDataReceivedEvent(complexbase.GroupBase):
	"""This event is thrown on reception of the referenced data via Sender-Receiver-Communication over the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._data_child=ModelNone

class BswDataReceptionPolicy(complexbase.GroupBase):
	"""Specifies the reception policy for the referred data in sender-receiver communication over the BSW Scheduler. To be used for inter-partition and/or inter-core communication."""
	def __init__(self):
		super().__init__()
		self._receivedData_child=ModelNone
		self._variationPoint_child=ModelNone

class BswDataSendPolicy(complexbase.GroupBase):
	"""The data sent over the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._providedData_child=ModelNone
		self._proviedeData_child=ModelNone
		self._variationPoint_child=ModelNone

class BswDebugInfo(complexbase.GroupBase):
	"""Collects the information on the data provided to the AUTOSAR debug module."""
	def __init__(self):
		super().__init__()
		self._localDebugData_children=[]
		self._parameterAccessedForDebug_children=[]
		self._variableAccessedForDebug_children=[]
		self._variationPoint_child=ModelNone

class BswDirectCallPoint(complexbase.GroupBase):
	"""Represents a concrete point in the code from where a BswModuleEntry is called directly, i.e. not via the BSW Scheduler. 

This information can be used to analyze call tree and resource locking scenarios. It is not needed to configure the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._calledEntry_child=ModelNone
		self._calledFromWithinExclusiveArea_child=ModelNone

class BswDistinguishedPartition(complexbase.GroupBase):
	"""Each instance of this meta-class represents an abstract partition in which context the code of the enclosing  BswModuleBehavior can be executed.

The intended use case is to distinguish between several partitions in order to implement different behavior per partition, for example to behave either as a master or satellite in a multicore ECU with shared BSW code."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class BswEntryRelationship(complexbase.GroupBase):
	"""Describes a relationship between two BswModuleEntrys and the type of relationship."""
	def __init__(self):
		super().__init__()
		self._from__child=ModelNone
		self._to_child=ModelNone
		self._bswEntryRelationshipType_child=ModelNone

class BswEntryRelationshipSet(complexbase.GroupBase):
	"""Describes a set of relationships between two BswModuleEntrys."""
	def __init__(self):
		super().__init__()
		self._bswEntryRelationship_children=[]

class BswEvent(complexbase.GroupBase):
	"""Base class of various kinds of events which are used to trigger a BswModuleEntity of this BSW module or cluster. The event is local to the BSW module or cluster. The short name of the meta-class instance is intended as an input to configure the required API of the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._contextLimitation_children=[]
		self._disabledInMode_children=[]
		self._startsOnEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class BswExclusiveAreaPolicy(complexbase.GroupBase):
	"""The ExclusiveArea for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._apiPrinciple_child=ModelNone
		self._exclusiveArea_child=ModelNone
		self._variationPoint_child=ModelNone

class BswExternalTriggerOccurredEvent(complexbase.GroupBase):
	"""A BswEvent resulting from a trigger released by another module or cluster."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class BswImplementation(complexbase.GroupBase):
	"""Contains the implementation specific information in addition to the generic specification (BswModuleDescription and BswBehavior).
It is possible to have several different BswImplementations referring to the same BswBehavior."""
	def __init__(self):
		super().__init__()
		self._arReleaseVersion_child=ModelNone
		self._behavior_child=ModelNone
		self._debugInfo_children=[]
		self._preconfiguredConfiguration_children=[]
		self._recommendedConfiguration_children=[]
		self._vendorApiInfix_child=ModelNone
		self._vendorSpecificModuleDef_children=[]

class BswInternalBehavior(complexbase.GroupBase):
	"""Specifies the behavior of a BSW module or a BSW cluster w.r.t. the code entities visible by the BSW Scheduler.
It is possible to have several different BswInternalBehaviors referring to the same BswModuleDescription."""
	def __init__(self):
		super().__init__()
		self._arTypedPerInstanceMemory_children=[]
		self._bswPerInstanceMemoryPolicy_children=[]
		self._clientPolicy_children=[]
		self._exclusiveAreaPolicy_children=[]
		self._includedDataTypeSet_children=[]
		self._includedModeDeclarationGroupSet_children=[]
		self._internalTriggeringPointPolicy_children=[]
		self._parameterPolicy_children=[]
		self._releasedTriggerPolicy_children=[]
		self._sendPolicy_children=[]
		self._variationPointProxy_children=[]
		self._internalTriggeringPoint_children=[]
		self._entity_children=[]
		self._event_children=[]
		self._triggerDirectImplementation_children=[]
		self._modeSenderPolicy_children=[]
		self._modeReceiverPolicy_children=[]
		self._serviceDependency_children=[]
		self._perInstanceParameter_children=[]
		self._schedulerNamePrefix_children=[]
		self._receptionPolicy_children=[]
		self._distinguishedPartition_children=[]

class BswInternalTriggerOccurredEvent(complexbase.GroupBase):
	"""A BswEvent, which can happen sporadically. The event is activated by explicit calls from the module to the BSW Scheduler. The main purpose for such an event is to cause a context switch, e.g. from an ISR context into a task context. Activation and switching are handled within the same module or cluster only."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class BswInternalTriggeringPoint(complexbase.GroupBase):
	"""Represents the activation point for one or more BswInternalTriggerOccurredEvents."""
	def __init__(self):
		super().__init__()
		self._swImplPolicy_child=ModelNone
		self._variationPoint_child=ModelNone

class BswInternalTriggeringPointPolicy(complexbase.GroupBase):
	"""The internal triggering point for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._bswInternalTriggeringPoint_child=ModelNone
		self._variationPoint_child=ModelNone

class BswInternalTriggeringPointRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BswInternalTriggeringPoint_child=ModelNone
		self._variationPoint_child=ModelNone

class BswInterruptEntity(complexbase.GroupBase):
	"""BSW module entity, which is designed to be triggered by an interrupt."""
	def __init__(self):
		super().__init__()
		self._interruptCategory_child=ModelNone
		self._interruptSource_child=ModelNone

class BswInterruptEvent(complexbase.GroupBase):
	"""This meta-class represents an event triggered by an interrupt."""

class BswMgrNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Basic Software Manager for one \"user\"."""

class BswModeManagerErrorEvent(complexbase.GroupBase):
	"""This represents the ability to react on errors occurring during mode handling."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class BswModeReceiverPolicy(complexbase.GroupBase):
	"""Specifies the details for the reception of a mode switch for the referred mode group."""
	def __init__(self):
		super().__init__()
		self._enhancedModeApi_child=ModelNone
		self._requiredModeGroup_child=ModelNone
		self._supportsAsynchronousModeSwitch_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModeSenderPolicy(complexbase.GroupBase):
	"""Specifies the details for the sending of a mode switch for the referred mode group."""
	def __init__(self):
		super().__init__()
		self._ackRequest_child=ModelNone
		self._enhancedModeApi_child=ModelNone
		self._providedModeGroup_child=ModelNone
		self._queueLength_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModeSwitchAckRequest(complexbase.GroupBase):
	"""Requests acknowledgements that a mode switch has been processed  successfully"""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone

class BswModeSwitchEvent(complexbase.GroupBase):
	"""A BswEvent resulting from a mode switch."""
	def __init__(self):
		super().__init__()
		self._activation_child=ModelNone
		self._mode_children=[]

class BswModeSwitchedAckEvent(complexbase.GroupBase):
	"""The event is raised after a switch of the referenced mode group has been acknowledged or an error occurs. The referenced mode group shall be provided by this module."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class BswModuleCallPoint(complexbase.GroupBase):
	"""Represents a point at which a BswModuleEntity handles a procedure call into a BswModuleEntry, either directly or via the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._contextLimitation_children=[]
		self._variationPoint_child=ModelNone

class BswModuleClientServerEntry(complexbase.GroupBase):
	"""This meta-class represents a single API entry into the BSW module or cluster that has the ability to be called in client-server fashion via the BSW Scheduler.

In this regard it is more special than BswModuleEntry and can be seen as a wrapper around the BswModuleEntry to which it refers (property encapsulatedEntry)."""
	def __init__(self):
		super().__init__()
		self._encapsulatedEntry_child=ModelNone
		self._isReentrant_child=ModelNone
		self._isSynchronous_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleDependency(complexbase.GroupBase):
	"""This class collects the dependencies of a BSW module or cluster on a certain other BSW module."""
	def __init__(self):
		super().__init__()
		self._targetModuleId_child=ModelNone
		self._targetModuleRef_children=[]
		self._requiredEntry_children=[]
		self._expectedCallback_children=[]
		self._serviceItem_children=[]
		self._variationPoint_child=ModelNone

class BswModuleDescription(complexbase.GroupBase):
	"""Root element for the description of a single BSW module or BSW cluster.
In case it describes a BSW module, the short name of this element equals the name of the BSW module."""
	def __init__(self):
		super().__init__()
		self._expectedEntry_children=[]
		self._implementedEntry_children=[]
		self._moduleId_child=ModelNone
		self._bswModuleDocumentation_children=[]
		self._providedEntry_children=[]
		self._outgoingCallback_children=[]
		self._bswModuleDependency_children=[]
		self._providedModeGroup_children=[]
		self._requiredModeGroup_children=[]
		self._releasedTrigger_children=[]
		self._requiredTrigger_children=[]
		self._providedClientServerEntry_children=[]
		self._requiredClientServerEntry_children=[]
		self._providedData_children=[]
		self._requiredData_children=[]
		self._internalBehavior_children=[]

class BswModuleDescriptionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BswModuleDescription_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleEntity(complexbase.GroupBase):
	"""Specifies the smallest code fragment which can be described for a BSW module or cluster within AUTOSAR."""
	def __init__(self):
		super().__init__()
		self._accessedModeGroup_children=[]
		self._activationPoint_children=[]
		self._callPoint_children=[]
		self._calledEntry_children=[]
		self._dataReceivePoint_children=[]
		self._dataSendPoint_children=[]
		self._implementedEntry_child=ModelNone
		self._issuedTrigger_children=[]
		self._managedModeGroup_children=[]
		self._schedulerNamePrefix_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleEntry(complexbase.GroupBase):
	"""This class represents a single API entry (C-function prototype) into the BSW module or cluster.

The name of the C-function is equal to the short name of this element with one exception: In case of multiple instances of a module on the same CPU, special rules for \"infixes\" apply, see description of class BswImplementation."""
	def __init__(self):
		super().__init__()
		self._functionPrototypeEmitter_child=ModelNone
		self._serviceId_child=ModelNone
		self._role_child=ModelNone
		self._isReentrant_child=ModelNone
		self._isSynchronous_child=ModelNone
		self._callType_child=ModelNone
		self._executionContext_child=ModelNone
		self._swServiceImplPolicy_child=ModelNone
		self._bswEntryKind_child=ModelNone
		self._returnType_child=ModelNone
		self._argument_children=[]

class BswModuleEntryRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BswModuleEntry_child=ModelNone
		self._variationPoint_child=ModelNone

class BswModuleTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints for the BswInternalBehavior of one BSW Module. Thereby, for each BswInternalBehavior a separate timing can be specified.

A constraint defined at this level holds true for all Implementations of that BswInternalBehavior.

TimingDescriptions aggregated by BswModuleTiming are restricted to event chains referring to events which are derived from the class TDEventBswInternalBehavior."""
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone

class BswOperationInvokedEvent(complexbase.GroupBase):
	"""This event is thrown on operation invocation in Client-Server-Communication via the BSW Scheduler. Its \"entry\" reference provides the BswClientServerEntry that is called subsequently.

Note this event is not needed in case of direct function calls."""
	def __init__(self):
		super().__init__()
		self._entry_child=ModelNone

class BswOsTaskExecutionEvent(complexbase.GroupBase):
	"""This BswEvent is supposed to execute BswSchedulableEntitys which have to react on the execution of specific OsTasks. Therefore, this event is unconditionally raised whenever the OsTask on which it is mapped is executed.
The main use case for this event is scheduling of Runnables of Complex Drivers which have to react on task executions."""

class BswParameterPolicy(complexbase.GroupBase):
	"""The perInstanceParameter for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._perInstanceParameter_child=ModelNone
		self._variationPoint_child=ModelNone

class BswPerInstanceMemoryPolicy(complexbase.GroupBase):
	"""The per-instance memory for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._arTypedPerInstanceMemory_child=ModelNone
		self._variationPoint_child=ModelNone

class BswQueuedDataReceptionPolicy(complexbase.GroupBase):
	"""Reception policy attributes specific for queued receiving."""
	def __init__(self):
		super().__init__()
		self._queueLength_child=ModelNone

class BswReleasedTriggerPolicy(complexbase.GroupBase):
	"""The Trigger for which the BSW Scheduler using this policy."""
	def __init__(self):
		super().__init__()
		self._releasedTrigger_child=ModelNone
		self._variationPoint_child=ModelNone

class BswSchedulableEntity(complexbase.GroupBase):
	"""BSW module entity, which is designed for control by the BSW Scheduler. It may for example implement a so-called \"main\" function."""

class BswScheduleEvent(complexbase.GroupBase):
	"""BswEvent that is able to start a BswSchedulabeEntity."""

class BswSchedulerNamePrefix(complexbase.GroupBase):
	"""A prefix to be used in names of generated code artifacts which make up the interface of a BSW module to the BswScheduler."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class BswServiceDependency(complexbase.GroupBase):
	"""Specialization of ServiceDependency in the context of an BswInternalBehavior. It allows to associate BswModuleEntries and data defined for a BSW module or cluster to a given ServiceNeeds element."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._assignedData_children=[]
		self._assignedEntryRole_children=[]
		self._serviceNeeds_child=[]
		self._variationPoint_child=ModelNone

class BswServiceDependencyIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable BswServiceDependency."""

class BswSynchronousServerCallPoint(complexbase.GroupBase):
	"""Represents a synchronous procedure call point via the BSW Scheduler."""
	def __init__(self):
		super().__init__()
		self._calledEntry_child=ModelNone
		self._calledFromWithinExclusiveArea_child=ModelNone

class BswTimingEvent(complexbase.GroupBase):
	"""A recurring BswEvent driven by a time period."""
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone

class BswTriggerDirectImplementation(complexbase.GroupBase):
	"""Specifies a released trigger to be directly implemented via OS calls, for example in a Complex Driver module."""
	def __init__(self):
		super().__init__()
		self._cat2Isr_child=ModelNone
		self._masteredTrigger_child=ModelNone
		self._task_child=ModelNone
		self._variationPoint_child=ModelNone

class BswVariableAccess(complexbase.GroupBase):
	"""The presence of a BswVariableAccess implies that a BswModuleEntity needs access to a VariableDataPrototype via the BSW Scheduler.

The kind of access is specified by the role in which the class is used."""
	def __init__(self):
		super().__init__()
		self._accessedVariable_child=ModelNone
		self._contextLimitation_children=[]
		self._variationPoint_child=ModelNone

class BufferProperties(complexbase.GroupBase):
	"""Configuration of the buffer properties the transformer needs to work."""
	def __init__(self):
		super().__init__()
		self._bufferComputation_child=ModelNone
		self._headerLength_child=ModelNone
		self._inPlace_child=ModelNone

class BuildAction(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a build action."""
	def __init__(self):
		super().__init__()
		self._predecessorAction_children=[]
		self._followUpAction_children=[]
		self._createdData_children=[]
		self._inputData_children=[]
		self._modifiedData_children=[]
		self._requiredEnvironment_child=ModelNone
		self._variationPoint_child=ModelNone

class BuildActionEntity(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a build action entity which might be specialized to environments as well as to individual build actions."""
	def __init__(self):
		super().__init__()
		self._deliveryArtifact_children=[]
		self._invocation_child=ModelNone

class BuildActionEnvironment(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a build action environment."""
	def __init__(self):
		super().__init__()
		self._sdg_children=[]
		self._variationPoint_child=ModelNone

class BuildActionInvocator(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the invocation of a task in a build action."""
	def __init__(self):
		super().__init__()
		self._command_child=ModelNone
		self._sdg_children=[]

class BuildActionIoElement(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the input/output entities of a BuildAction."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._sdg_children=[]
		self._ecucDefinition_child=ModelNone
		self._engineeringObject_child=ModelNone
		self._foreignModelReference_child=ModelNone
		self._modelObjectReference_child=ModelNone
		self._role_child=ModelNone

class BuildActionManifest(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a manifest for processing artifacts. An example use case is the processing of ECUC parameter values."""
	def __init__(self):
		super().__init__()
		self._startAction_children=[]
		self._tearDownAction_children=[]
		self._buildAction_children=[]
		self._buildActionEnvironment_children=[]
		self._dynamicAction_children=[]

class BuildActionManifestRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._BuildActionManifest_child=ModelNone
		self._variationPoint_child=ModelNone

class BuildEngineeringObject(complexbase.GroupBase):
	"""This meta-class represents the ability to denote an artifact which is processed within a particular build action."""
	def __init__(self):
		super().__init__()
		self._fileType_child=ModelNone
		self._intendedFilename_child=ModelNone
		self._parentCategory_child=ModelNone
		self._parentShortLabel_child=ModelNone
		self._shortLabelPattern_child=ModelNone
		self._fileTypePattern_child=ModelNone

class BulkNvDataDescriptor(complexbase.GroupBase):
	"""This meta-class represents one bulk NV Data Block that is read-only for the application software. The purpose of a bulk NV Data Block is to provide access to information uploaded to the vehicle at e.g. the end of the production line."""
	def __init__(self):
		super().__init__()
		self._bulkNvBlock_child=ModelNone
		self._nvBlockDataMapping_children=[]
		self._variationPoint_child=ModelNone

class BurstPatternEventTriggering(complexbase.GroupBase):
	"""Describes the maximum number of occurrences of the same event in a given time interval.  Typically used to model a worst case activation scenario."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfOccurrences_child=ModelNone
		self._minimumInterArrivalTime_child=ModelNone
		self._patternJitter_child=ModelNone
		self._patternLength_child=ModelNone
		self._patternPeriod_child=ModelNone
		self._minNumberOfOccurrences_child=ModelNone

class BusMirrorCanIdRangeMapping(complexbase.GroupBase):
	"""This element defines a rule for remapping a set of CAN IDs."""
	def __init__(self):
		super().__init__()
		self._destinationBaseId_child=ModelNone
		self._sourceCanIdCode_child=ModelNone
		self._sourceCanIdMask_child=ModelNone

class BusMirrorCanIdToCanIdMapping(complexbase.GroupBase):
	"""This element defines a rule for remapping a single CAN ID."""
	def __init__(self):
		super().__init__()
		self._remappedCanId_child=ModelNone
		self._souceCanId_child=ModelNone

class BusMirrorChannel(complexbase.GroupBase):
	"""This element assigns a busMirrorNetworkId to the referenced channel."""
	def __init__(self):
		super().__init__()
		self._busMirrorNetworkId_child=ModelNone
		self._channel_children=[]

class BusMirrorChannelMapping(complexbase.GroupBase):
	"""This element defines a bus mirroring in which the traffic from one communication bus (sourceChannel) is forwarded to another one (targetChannel)."""
	def __init__(self):
		super().__init__()
		self._mirroringProtocol_child=ModelNone
		self._sourceChannel_child=ModelNone
		self._targetChannel_child=ModelNone
		self._targetPduTriggering_children=[]

class BusMirrorChannelMappingCan(complexbase.GroupBase):
	"""This element defines the bus mirroring between a CAN or LIN sourceChannel and a CAN targetChannel."""
	def __init__(self):
		super().__init__()
		self._canIdRangeMapping_children=[]
		self._canIdToCanIdMapping_children=[]
		self._linPidToCanIdMapping_children=[]
		self._mirrorSourceLinToCanRangeBaseId_child=ModelNone
		self._mirrorStatusCanId_child=ModelNone

class BusMirrorChannelMappingFlexray(complexbase.GroupBase):
	"""This element defines the bus mirroring between a CAN, LIN or FlexRay sourceChannel and a FlexRay targetChannel."""
	def __init__(self):
		super().__init__()
		self._transmissionDeadline_child=ModelNone

class BusMirrorChannelMappingIp(complexbase.GroupBase):
	"""This element defines the bus mirroring between a CAN, LIN or FlexRay sourceChannel and an Ethernet IP targetChannel."""
	def __init__(self):
		super().__init__()
		self._transmissionDeadline_child=ModelNone

class BusMirrorChannelMappingUserDefined(complexbase.GroupBase):
	"""This element defines the bus mirroring between a CAN, LIN or FlexRay sourceChannel and a UserDefined targetChannel."""
	def __init__(self):
		super().__init__()
		self._transmissionDeadline_child=ModelNone

class BusMirrorLinPidToCanIdMapping(complexbase.GroupBase):
	"""This element defines a rule for remapping a single LIN Frame."""
	def __init__(self):
		super().__init__()
		self._remappedCanId_child=ModelNone
		self._sourceLinPid_child=ModelNone

class BusspecificNmEcu(complexbase.GroupBase):
	"""Busspecific NmEcu attributes."""

class CalibrationParameterValue(complexbase.GroupBase):
	"""Specifies instance specific calibration parameter values used to initialize the memory objects implementing calibration parameters in the generated RTE code.

RTE generator will use the implInitValue to override the initial values specified for the DataPrototypes of a component type. 

The applInitValue is used to exchange init values with the component vendor not publishing the transformation algorithm between ApplicationDataTypes and ImplementationDataTypes or defining an instance specific initialization of components which are only defined with ApplicationDataTypes.

Note: If both representations of init values are available these need to represent the same content.

Note further that in this case an explicit mapping of ValueSpecification is not implemented because calibration parameters are delivered back after the calibration phase."""
	def __init__(self):
		super().__init__()
		self._applInitValue_child=[]
		self._implInitValue_child=[]
		self._initializedParameter_child=ModelNone
		self._variationPoint_child=ModelNone

class CalibrationParameterValueSet(complexbase.GroupBase):
	"""Specification of a constant that can be part of a package, i.e. it can be defined stand-alone."""
	def __init__(self):
		super().__init__()
		self._calibrationParameterValue_children=[]

class CanCluster(complexbase.GroupBase):
	"""CAN bus specific cluster attributes."""
	def __init__(self):
		super().__init__()
		self._canClusterVariant_children=[]

class CanClusterBusOffRecovery(complexbase.GroupBase):
	"""This element contains the attributes that are used to configure the CAN bus off monitoring / recovery at system level."""
	def __init__(self):
		super().__init__()
		self._borCounterL1ToL2_child=ModelNone
		self._borTimeL1_child=ModelNone
		self._borTimeL2_child=ModelNone
		self._borTimeTxEnsured_child=ModelNone
		self._mainFunctionPeriod_child=ModelNone

class CanClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class CanClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CanCommunicationConnector(complexbase.GroupBase):
	"""CAN bus specific communication connector attributes."""
	def __init__(self):
		super().__init__()
		self._pncWakeupCanId_child=ModelNone
		self._pncWakeupCanIdExtended_child=ModelNone
		self._pncWakeupCanIdMask_child=ModelNone
		self._pncWakeupDataMask_child=ModelNone
		self._pncWakeupDlc_child=ModelNone

class CanCommunicationController(complexbase.GroupBase):
	"""CAN bus specific communication port attributes."""
	def __init__(self):
		super().__init__()
		self._canCommunicationControllerVariant_children=[]

class CanCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class CanCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CanControllerConfiguration(complexbase.GroupBase):
	"""This element is used for the specification of the exact CAN Bit Timing configuration parameter values."""
	def __init__(self):
		super().__init__()
		self._propSeg_child=ModelNone
		self._syncJumpWidth_child=ModelNone
		self._timeSeg1_child=ModelNone
		self._timeSeg2_child=ModelNone

class CanControllerConfigurationRequirements(complexbase.GroupBase):
	"""This element allows the specification of ranges for the CAN Bit Timing configuration parameters. These ranges are taken as requirements and have to be respected by the ECU developer."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfTimeQuantaPerBit_child=ModelNone
		self._maxSamplePoint_child=ModelNone
		self._maxSyncJumpWidth_child=ModelNone
		self._minNumberOfTimeQuantaPerBit_child=ModelNone
		self._minSamplePoint_child=ModelNone
		self._minSyncJumpWidth_child=ModelNone

class CanControllerFdConfiguration(complexbase.GroupBase):
	"""Bit timing related configuration of a CAN controller for payload and CRC of a CAN FD frame."""
	def __init__(self):
		super().__init__()
		self._paddingValue_child=ModelNone
		self._propSeg_child=ModelNone
		self._sspOffset_child=ModelNone
		self._syncJumpWidth_child=ModelNone
		self._timeSeg1_child=ModelNone
		self._timeSeg2_child=ModelNone
		self._trcvDelayCompensationOffset_child=ModelNone
		self._txBitRateSwitch_child=ModelNone

class CanControllerFdConfigurationRequirements(complexbase.GroupBase):
	"""This element allows the specification of ranges for the CanFD bit timing configuration parameters. These ranges are taken as requirements and shall be respected by the ECU developer."""
	def __init__(self):
		super().__init__()
		self._maxNumberOfTimeQuantaPerBit_child=ModelNone
		self._maxSamplePoint_child=ModelNone
		self._maxSyncJumpWidth_child=ModelNone
		self._maxTrcvDelayCompensationOffset_child=ModelNone
		self._minNumberOfTimeQuantaPerBit_child=ModelNone
		self._minSamplePoint_child=ModelNone
		self._minSyncJumpWidth_child=ModelNone
		self._minTrcvDelayCompensationOffset_child=ModelNone
		self._paddingValue_child=ModelNone
		self._txBitRateSwitch_child=ModelNone

class CanControllerXlConfiguration(complexbase.GroupBase):
	"""This meta-class represents the CAN XL-specific controller attributes."""
	def __init__(self):
		super().__init__()
		self._errorSignalingEnabled_child=ModelNone
		self._propSeg_child=ModelNone
		self._pwmL_child=ModelNone
		self._pwmO_child=ModelNone
		self._pwmS_child=ModelNone
		self._sspOffset_child=ModelNone
		self._syncJumpWidth_child=ModelNone
		self._timeSeg1_child=ModelNone
		self._timeSeg2_child=ModelNone
		self._trcvPwmModeEnabled_child=ModelNone

class CanControllerXlConfigurationRequirements(complexbase.GroupBase):
	"""This element allows the specification of ranges for the CAN XL configuration parameters. These ranges are taken as requirements and have to be respected by the ECU developer."""
	def __init__(self):
		super().__init__()
		self._errorSignalingEnabled_child=ModelNone
		self._maxNumberOfTimeQuantaPerBit_child=ModelNone
		self._maxPwmL_child=ModelNone
		self._maxPwmO_child=ModelNone
		self._maxPwmS_child=ModelNone
		self._maxSamplePoint_child=ModelNone
		self._maxSyncJumpWidth_child=ModelNone
		self._maxTrcvDelayCompensationOffset_child=ModelNone
		self._minNumberOfTimeQuantaPerBit_child=ModelNone
		self._minPwmL_child=ModelNone
		self._minPwmO_child=ModelNone
		self._minPwmS_child=ModelNone
		self._minSamplePoint_child=ModelNone
		self._minSyncJumpWidth_child=ModelNone
		self._minTrcvDelayCompensationOffset_child=ModelNone
		self._trcvPwmModeEnabled_child=ModelNone

class CanFrame(complexbase.GroupBase):
	"""CAN specific Frame element. This element shall also be used for TTCan."""

class CanFrameTriggering(complexbase.GroupBase):
	"""CAN specific attributes to the FrameTriggering"""
	def __init__(self):
		super().__init__()
		self._absolutelyScheduledTiming_children=[]
		self._canAddressingMode_child=ModelNone
		self._canFdFrameSupport_child=ModelNone
		self._canFrameRxBehavior_child=ModelNone
		self._canFrameTxBehavior_child=ModelNone
		self._canXlFrameTriggeringProps_child=ModelNone
		self._identifier_child=ModelNone
		self._j1939requestable_child=ModelNone
		self._rxIdentifierRange_child=ModelNone
		self._rxMask_child=ModelNone
		self._txMask_child=ModelNone

class CanGlobalTimeDomainProps(complexbase.GroupBase):
	"""Enables the definition of Can Global Time specific properties."""
	def __init__(self):
		super().__init__()
		self._fupDataIDList_children=[]
		self._ofnsDataIDList_children=[]
		self._ofsDataIDList_children=[]
		self._syncDataIDList_children=[]

class CanNmCluster(complexbase.GroupBase):
	"""Can specific NmCluster attributes"""
	def __init__(self):
		super().__init__()
		self._nmBusloadReductionActive_child=ModelNone
		self._nmCarWakeUpBitPosition_child=ModelNone
		self._nmCarWakeUpFilterEnabled_child=ModelNone
		self._nmCarWakeUpFilterNodeId_child=ModelNone
		self._nmCarWakeUpRxEnabled_child=ModelNone
		self._nmCbvPosition_child=ModelNone
		self._nmChannelActive_child=ModelNone
		self._nmImmediateNmCycleTime_child=ModelNone
		self._nmImmediateNmTransmissions_child=ModelNone
		self._nmMessageTimeoutTime_child=ModelNone
		self._nmMsgCycleTime_child=ModelNone
		self._nmNetworkTimeout_child=ModelNone
		self._nmNidPosition_child=ModelNone
		self._nmRemoteSleepIndicationTime_child=ModelNone
		self._nmRepeatMessageTime_child=ModelNone
		self._nmUserDataLength_child=ModelNone
		self._nmWaitBusSleepTime_child=ModelNone

class CanNmClusterCoupling(complexbase.GroupBase):
	"""CAN attributes that are valid for each of the referenced (coupled) CAN clusters."""
	def __init__(self):
		super().__init__()
		self._coupledCluster_children=[]
		self._nmBusloadReductionEnabled_child=ModelNone
		self._nmImmediateRestartEnabled_child=ModelNone

class CanNmEcu(complexbase.GroupBase):
	"""CAN specific attributes."""
	def __init__(self):
		super().__init__()
		self._nmRepeatMsgIndicationEnabled_child=ModelNone

class CanNmNode(complexbase.GroupBase):
	"""CAN specific NM Node attributes."""
	def __init__(self):
		super().__init__()
		self._allNmMessagesKeepAwake_child=ModelNone
		self._canXlNmProps_child=ModelNone
		self._nmCarWakeUpFilterEnabled_child=ModelNone
		self._nmCarWakeUpRxEnabled_child=ModelNone
		self._nmMsgCycleOffset_child=ModelNone
		self._nmMsgReducedTime_child=ModelNone
		self._nmRangeConfig_child=ModelNone

class CanNmRangeConfig(complexbase.GroupBase):
	"""Defines the CANID ranges that are used for Nm. This range definition is redundant to the attribute \"rxIdentifierRange\" of CanFrameTriggering. For backward compatibility reasons this redundancy shall be preserved and both shall be defined.
In future this element will be removed from the model."""
	def __init__(self):
		super().__init__()
		self._lowerCanId_child=ModelNone
		self._upperCanId_child=ModelNone

class CanPhysicalChannel(complexbase.GroupBase):
	"""CAN bus specific physical channel attributes."""

class CanTpAddress(complexbase.GroupBase):
	"""An ECUs TP address on the referenced channel. This represents the diagnostic Address."""
	def __init__(self):
		super().__init__()
		self._tpAddress_child=ModelNone
		self._tpAddressExtensionValue_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpChannel(complexbase.GroupBase):
	"""Configuration parameters of the CanTp channel."""
	def __init__(self):
		super().__init__()
		self._channelId_child=ModelNone
		self._channelMode_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpConfig(complexbase.GroupBase):
	"""This element defines exactly one CAN TP Configuration. 

One CanTpConfig element shall be created for each CAN Network in the System."""
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpChannel_children=[]
		self._tpConnection_children=[]
		self._tpEcu_children=[]
		self._tpNode_children=[]

class CanTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The CanTp module routes a Pdu through this connection. 

atpVariation: Derived, because TpNode can vary."""
	def __init__(self):
		super().__init__()
		self._addressingFormat_child=ModelNone
		self._canTpChannel_child=ModelNone
		self._cancellation_child=ModelNone
		self._dataPdu_child=ModelNone
		self._flowControlPdu_child=ModelNone
		self._maxBlockSize_child=ModelNone
		self._multicast_child=ModelNone
		self._paddingActivation_child=ModelNone
		self._receiver_children=[]
		self._taType_child=ModelNone
		self._timeoutBr_child=ModelNone
		self._timeoutBs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._timeoutCs_child=ModelNone
		self._tpSdu_child=ModelNone
		self._transmitCancellation_child=ModelNone
		self._transmitter_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpEcu(complexbase.GroupBase):
	"""ECU specific TP configuration parameters. Each TpEcu element has a reference to exactly one ECUInstance in the topology."""
	def __init__(self):
		super().__init__()
		self._cycleTimeMainFunction_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class CanTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	def __init__(self):
		super().__init__()
		self._connector_child=ModelNone
		self._maxFcWait_child=ModelNone
		self._stMin_child=ModelNone
		self._timeoutAr_child=ModelNone
		self._timeoutAs_child=ModelNone
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class CanXlFrameTriggeringProps(complexbase.GroupBase):
	"""This element indicates the frame being CAN XL and contains further CAN XL specific attributes."""
	def __init__(self):
		super().__init__()
		self._acceptanceField_child=ModelNone
		self._priorityId_child=ModelNone
		self._sduType_child=ModelNone
		self._vcid_child=ModelNone

class CanXlNmNodeProps(complexbase.GroupBase):
	"""Definition of CAN attributes in the context of CanXL usage on the adaptive platform."""
	def __init__(self):
		super().__init__()
		self._canNmTxCanId_child=ModelNone
		self._rxIdentifierRange_child=ModelNone
		self._rxMask_child=ModelNone

class CanXlProps(complexbase.GroupBase):
	"""'''begin restrict to AP'''
This meta-class is used to configure Machine specific CAN XL attributes.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._canBaudrate_child=ModelNone
		self._canConfig_child=ModelNone
		self._canFdBaudrate_child=ModelNone
		self._canFdConfig_child=ModelNone
		self._canXlBaudrate_child=ModelNone
		self._canXlConfig_child=ModelNone
		self._canXlConfigReqs_child=ModelNone

class Caption(complexbase.GroupBase):
	"""This meta-class represents the ability to express a caption which is a title, and a shortName."""
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone

class Chapter(complexbase.GroupBase):
	"""This meta-class represents a chapter of a document. Chapters are the primary structuring element in documentation."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.

Maybe it is a concatenated Identifier, but as of now we leave it as an arbitrary string.""")
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone
		self._msrQueryChapter_children=[]
		self._chapter_children=[]
		self._msrQueryTopic1_children=[]
		self._topic1_children=[]
		self._prms_children=[]
		self._traceableTable_children=[]
		self._table_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]
		self._msrQueryP1_children=[]

class ChapterContent(complexbase.GroupBase):
	"""This class represents the content which is directly in a chapter. It is basically the same as the one in a Topic but might have additional complex structures (e.g. Synopsis)"""
	def __init__(self):
		super().__init__()
		self._prms_children=[]
		self._traceableTable_children=[]
		self._table_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]
		self._msrQueryP1_children=[]

class ChapterModel(complexbase.GroupBase):
	"""This is the basic content model of a chapter except the Chapter title. This can be utilized in general chapters as well as in predefined chapters.

A chapter has content on three levels:

1. chapter content

2. topics

3. subchapters"""
	def __init__(self):
		super().__init__()
		self._msrQueryChapter_children=[]
		self._chapter_children=[]
		self._msrQueryTopic1_children=[]
		self._topic1_children=[]
		self._prms_children=[]
		self._traceableTable_children=[]
		self._table_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]
		self._msrQueryP1_children=[]

class ChapterOrMsrQuery(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a particular chapter or a query returning a chapter."""
	def __init__(self):
		super().__init__()
		self._chapter_children=[]
		self._msrQueryChapter_children=[]

class CheckpointTransition(complexbase.GroupBase):
	"""Defines one transition between two checkpoints."""
	def __init__(self):
		super().__init__()
		self._source_child=ModelNone
		self._target_child=ModelNone

class ClassContentConditional(complexbase.GroupBase):
	"""Specifies the valid content of the class. The content can optionally depend on a condition. (E.g. value of attribute 'category')"""
	def __init__(self):
		super().__init__()
		self._condition_child=[]
		self._attributeTailoring_children=[]
		self._constraintTailoring_children=[]
		self._sdgTailoring_children=[]

class ClassTailoring(complexbase.GroupBase):
	"""The ClassTailoring is an abstract class that allows the tailoring of its attributes, applicable constraints and Sdgs."""
	def __init__(self):
		super().__init__()
		self._multiplicityRestriction_child=ModelNone
		self._variationRestriction_child=ModelNone
		self._classContent_children=[]

class ClientComSpec(complexbase.GroupBase):
	"""'''begin restrict to CP'''
Client-specific communication attributes (RPortPrototype typed by ClientServerInterface).
'''end restrict to CP'''
'''begin restrict to AP'''
Client-specific communication attributes (RPortPrototype typed by ServiceInterface) that are relevant for methods and field getters and setters.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._clientIntent_child=ModelNone
		self._endToEndCallResponseTimeout_child=ModelNone
		self._getter_child=ModelNone
		self._operation_child=ModelNone
		self._setter_child=ModelNone
		self._transformationComSpecProps_children=[]

class ClientIdDefinition(complexbase.GroupBase):
	"""Several clients in one client-ECU can communicate via inter-ECU client-server communication with a server on a different ECU, if a client identifier is used to distinguish the different clients.
The Client Identifier of the transaction handle that is used by the RTE can be defined by this element."""
	def __init__(self):
		super().__init__()
		self._clientId_child=ModelNone
		self._clientServerOperation_child=ModelNone
		self._variationPoint_child=ModelNone

class ClientIdDefinitionSet(complexbase.GroupBase):
	"""Set of Client Identifiers that are used for inter-ECU client-server communication in the System."""
	def __init__(self):
		super().__init__()
		self._clientIdDefinition_children=[]

class ClientIdMapping(complexbase.GroupBase):
	"""In case of a server on one ECU with multiple clients on other ECUs, the client server communication shall use different unique COM signals and signal groups for each client to allow the identification of the client associated with each system signal.

The ClientId is mapped to the requestGroup and to the responseGroup."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class ClientIdRange(complexbase.GroupBase):
	"""With this element it is possible to restrict the Client Identifier of the transaction handle that is generated by the client RTE for inter-Ecu Client/Server communication to an allowed range of numerical values."""
	def __init__(self):
		super().__init__()
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone

class ClientServerAnnotation(complexbase.GroupBase):
	"""Annotation to a port regarding a certain Operation."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone

class ClientServerApplicationErrorMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map ApplicationErrors onto each other."""
	def __init__(self):
		super().__init__()
		self._firstApplicationError_child=ModelNone
		self._secondApplicationError_child=ModelNone

class ClientServerArrayElementMapping(complexbase.GroupBase):
	"""The ApplicationArrayElement may be a primitive one or a composite one. If the element is primitive, it will be mapped to the \"SystemSignal\" (multiplicity 1). If the ArgumentDataPrototype that is referenced by ClientServerCompositeTypeMapping is typed by an ApplicationDataType the reference to the ApplicationArrayElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference to the ImplementationArrayElement shall be used. 

If the element is composite, there will be no mapping to the \"SystemSignal\" (multiplicity 0). In this case the \"ArrayElementMapping\" Element will aggregate the \"TypeMapping\" Element. In that way also the composite datatypes can be mapped to SystemSignals. 

Regardless whether composite or primitive array element is mapped the indexed array element always needs to be specified."""
	def __init__(self):
		super().__init__()
		self._complexTypeMapping_child=[]
		self._indexedArrayElement_child=ModelNone
		self._systemSignal_child=ModelNone

class ClientServerArrayTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is an Array, the \"ArrayTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._arrayElementMapping_children=[]

class ClientServerCompositeTypeMapping(complexbase.GroupBase):
	"""Two mappings exist for the composite data types: \"ArrayTypeMapping\" and \"RecordTypeMapping\".
In both, a primitive datatype will be mapped to a system signal. 

But it is also possible to combine the arrays and the records, so that an \"array\" could be an element of a \"record\" and in the same manner a \"record\" could be an element of an \"array\". Nesting these data types is also possible.

If an element of a composite data type is again a composite one, the \"CompositeTypeMapping\" element will be used one more time (aggregation between the ArrayElementMapping and CompositeTypeMapping or aggregation between the RecordElementMapping and CompositeTypeMapping)."""
	def __init__(self):
		super().__init__()
		self._argument_child=ModelNone

class ClientServerInterface(complexbase.GroupBase):
	"""A client/server interface declares a number of operations that can be invoked on a server by a client."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._possibleError_children=[]

class ClientServerInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of ClientServerOperations in context of two different ClientServerInterfaces."""
	def __init__(self):
		super().__init__()
		self._errorMapping_children=[]
		self._operationMapping_children=[]

class ClientServerInterfaceToBswModuleEntryBlueprintMapping(complexbase.GroupBase):
	"""This represents a mapping between one ClientServerInterface blueprint and BswModuleEntry blueprint in order to express the intended implementation of ClientServerOperations by specific BswModuleEntries under consideration of PortDefinedArguments.
Such a mapping enables the formal check whether the number of arguments and the data types of arguments of the operation + additional PortDefinedArguments matches the signature of the BswModuleEntry."""
	def __init__(self):
		super().__init__()
		self._clientServerInterface_child=ModelNone
		self._operationMapping_children=[]
		self._portDefinedArgumentBlueprint_children=[]

class ClientServerOperation(complexbase.GroupBase):
	"""'''begin restrict to CP AP'''
An operation declared within the scope of a client/server interface.
'''begin restrict to FO'''
A remote procedure call declared within the scope of the current interface.
'''end restrict to FO'''"""
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._diagArgIntegrity_child=ModelNone
		self._fireAndForget_child=ModelNone
		self._possibleApError_children=[]
		self._possibleApErrorSet_children=[]
		self._possibleError_children=[]
		self._variationPoint_child=ModelNone

class ClientServerOperationBlueprintMapping(complexbase.GroupBase):
	"""This class describes a specific mapping between a ClientServerOperation in a ClientServerInterface blueprint and a BswModuleEntry blueprint."""
	def __init__(self):
		super().__init__()
		self._blueprintMappingGuide_child=ModelNone
		self._bswModuleEntry_child=ModelNone
		self._clientServerOperation_child=ModelNone
		self._variationPoint_child=ModelNone

class ClientServerOperationComProps(complexbase.GroupBase):
	"""Defines additional attributes for the implementation of Client Server communication between software clusters"""
	def __init__(self):
		super().__init__()
		self._queueLength_child=ModelNone

class ClientServerOperationMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular ClientServerOperations in context of two different ClientServerInterfaces."""
	def __init__(self):
		super().__init__()
		self._argumentMapping_children=[]
		self._firstOperation_child=ModelNone
		self._firstToSecondDataTransformation_child=ModelNone
		self._secondOperation_child=ModelNone

class ClientServerPrimitiveTypeMapping(complexbase.GroupBase):
	"""Mapping of an argument with a primitive datatype to a signal."""
	def __init__(self):
		super().__init__()
		self._argument_child=ModelNone
		self._systemSignal_child=ModelNone

class ClientServerRecordElementMapping(complexbase.GroupBase):
	"""Mapping of a primitive record element to a SystemSignal. If the ArgumentDataPrototype that is referenced by ClientServerCompositeTypeMapping is typed by an ApplicationDataType the reference to the ApplicationRecordElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference to the ImplementationRecordElement shall be used. 

If the element is composite, there will be no mapping (multiplicity 0). In this case the \"RecordElementMapping\" Element will aggregate the \"TypeMapping\" Element. In that way also the composite datatypes can be mapped to SystemSignals. 

Regardless whether composite or primitive record element is mapped the record element always needs to be specified."""
	def __init__(self):
		super().__init__()
		self._applicationRecordElement_child=ModelNone
		self._complexTypeMapping_child=[]
		self._implementationRecordElement_child=ModelNone
		self._systemSignal_child=ModelNone

class ClientServerRecordTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is a Record, the \"RecordTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._recordElementMapping_children=[]

class ClientServerToSignalGroupMapping(complexbase.GroupBase):
	"""This mapping is deprecated and will be removed in future. It is replaced by the ClientServerToSignalMapping.

Old description:
Mapping of client server operation arguments to signals of a signal group. Arguments with a primitive datatype will be mapped via the \"ClientServerPrimitiveTypeMapping\" element. 
Arguments with composite datatypes will be mapped via the \"CompositeTypeMapping\" element."""
	def __init__(self):
		super().__init__()
		self._applicationError_child=ModelNone
		self._clientID_child=ModelNone
		self._compositeTypeMapping_children=[]
		self._emptySignal_child=ModelNone
		self._mappedOperation_child=ModelNone
		self._primitiveTypeMapping_children=[]
		self._requestGroup_child=ModelNone
		self._responseGroup_child=ModelNone
		self._sequenceCounter_child=ModelNone

class ClientServerToSignalMapping(complexbase.GroupBase):
	"""This element maps the ClientServerOperation to call- and return-SystemSignals."""
	def __init__(self):
		super().__init__()
		self._callSignal_child=ModelNone
		self._clientServerOperation_child=ModelNone
		self._lengthClientId_child=ModelNone
		self._lengthSequenceCounter_child=ModelNone
		self._returnSignal_child=ModelNone
		self._serializer_child=ModelNone

class Code(complexbase.GroupBase):
	"""A generic code descriptor. The type of the code (source or object) is defined via the category attribute of the associated engineering object."""
	def __init__(self):
		super().__init__()
		self._artifactDescriptor_children=[]
		self._callbackHeader_children=[]

class CollectableElement(complexbase.GroupBase):
	"""This meta-class specifies the ability to be part of a specific  AUTOSAR collection of ARPackages or ARElements.

The scope of collection has been extended beyond CollectableElement with Revision 4.0.3. For compatibility reasons the name of this meta Class was not changed."""

class Collection(complexbase.GroupBase):
	"""This meta-class specifies a collection of elements. A collection can be utilized to express additional aspects for a set of elements. 

Note that Collection is an ARElement. Therefore it is applicable e.g. for EvaluatedVariant, even if this is not obvious.

Usually the category of a Collection is \"SET\". On the other hand, a Collection can also express an arbitrary relationship between elements. This is denoted by the category \"RELATION\" (see also [TPS_GST_00347]). 

In this case the collection represents an association from \"sourceElement\" to \"targetElement\" in the role \"role\"."""
	def __init__(self):
		super().__init__()
		self._autoCollect_child=ModelNone
		self._collectionSemantics_child=ModelNone
		self._elementRole_child=ModelNone
		self._element_children=[]
		self._sourceElement_children=[]
		self._collectedInstance_children=[]
		self._sourceInstance_children=[]

class Colspec(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the properties of a column in a  table."""
	align=complexbase.Attribute("align",SimpleTypes.AlignEnum,'ALIGN',False,"""Specifies how the cell entries shall be horizontally aligned within the specified column.
Default is \"LEFT\"""")
	colname=complexbase.Attribute("colname",SimpleTypes.String,'COLNAME',False,"""Specifies the name of the column.""")
	colnum=complexbase.Attribute("colnum",SimpleTypes.String,'COLNUM',False,"""column number (allows to sort the columns).""")
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates whether a line should be displayed right of this column in the column specification.""")
	colwidth=complexbase.Attribute("colwidth",SimpleTypes.String,'COLWIDTH',False,"""Width of the column.
You can enter absolute values such as 4 cm, 
or relative values marked with * (e.g.,  2* for column widths double those of other columns with 1*).
The unit can be added to the number in the string. Possible units are: cm, mm, px, pt.""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates whether a line should be displayed at the bottom end of the cells of the column defined in the Colspec.""")

class ComCertificateToCryptoCertificateMapping(complexbase.GroupBase):
	"""This meta-class maps the CryptoServiceCertificate defined in the COM deployment to the CryptoCertificate defined in the Crypto Stack."""
	def __init__(self):
		super().__init__()
		self._cryptoCertificate_child=ModelNone
		self._cryptoServiceCertificate_child=ModelNone

class ComEventGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to grant access to a ServiceInterface.event."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._serviceDeployment_child=ModelNone

class ComEventGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define a Grant for a ServiceInterface.event."""
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone

class ComFieldGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to grant access to a ServiceInterface.field."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._role_child=ModelNone
		self._serviceDeployment_child=ModelNone

class ComFieldGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define a Grant for a ServiceInterface.field."""
	def __init__(self):
		super().__init__()
		self._field_child=ModelNone
		self._role_child=ModelNone

class ComFindServiceGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to grant the finding a service."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._serviceInstance_child=ModelNone

class ComFindServiceGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define a Grant for finding a service."""
	def __init__(self):
		super().__init__()
		self._requiredServicePort_child=ModelNone

class ComGrant(complexbase.GroupBase):
	"""This meta-class serves as the abstract base class for defining specific ComGrants"""
	def __init__(self):
		super().__init__()
		self._remoteSubject_children=[]
		self._serviceInstance_child=ModelNone

class ComGrantDesign(complexbase.GroupBase):
	"""This meta-class serves as an abstract base class for the description of com grants on design level."""
	def __init__(self):
		super().__init__()
		self._remoteSubject_children=[]

class ComKeyToCryptoKeySlotMapping(complexbase.GroupBase):
	"""This meta-class maps the CryptoServiceKey defined in the COM deployment to the CryptoKeySlot defined in the Crypto Stack."""
	def __init__(self):
		super().__init__()
		self._cryptoServiceKey_child=ModelNone
		self._keySlot_child=ModelNone

class ComManagementMapping(complexbase.GroupBase):
	"""Describes a mapping between one or several Mode Management PortGroups and communication channels."""
	def __init__(self):
		super().__init__()
		self._comManagementGroup_children=[]
		self._comManagementPortGroup_children=[]
		self._physicalChannel_children=[]
		self._variationPoint_child=ModelNone

class ComMethodGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to grant access to a ServiceInterface.method."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._serviceDeployment_child=ModelNone

class ComMethodGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define a Grant for a ServiceInterface.method."""
	def __init__(self):
		super().__init__()
		self._method_child=ModelNone

class ComMgrUserNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Communication Manager for one \"user\"."""
	def __init__(self):
		super().__init__()
		self._maxCommMode_child=ModelNone

class ComOfferServiceGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to grant the offering of a service."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._serviceInstance_child=ModelNone

class ComOfferServiceGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define a Grant for offering a service."""
	def __init__(self):
		super().__init__()
		self._providedServicePort_child=ModelNone

class ComSecOcToCryptoKeySlotMapping(complexbase.GroupBase):
	"""This meta-class maps the ServiceElementSecureComConfig defined in the COM deployment to the CryptoKeySlot defined in the Crypto Stack."""
	def __init__(self):
		super().__init__()
		self._cryptoKeySlot_child=ModelNone
		self._serviceElementSecureComConfig_child=ModelNone

class ComTriggerGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to grant access to a ServiceInterface.trigger"""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._serviceDeployment_child=ModelNone

class ComTriggerGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define a Grant for a ServiceInterface.trigger."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class CommConnectorPort(complexbase.GroupBase):
	"""The Ecu communication relationship defines which signals, Pdus and frames are actually received and transmitted by this ECU. 

For each signal, Pdu or Frame that is transmitted or received and used by the Ecu an association between an ISignalPort, IPduPort or FramePort with the corresponding Triggering shall be created. 
An ISignalPort shall be created only if the corresponding signal is handled by COM (RTE or Signal Gateway). If a Pdu Gateway ECU only routes the Pdu without being interested in the content only a FramePort and an IPduPort needs to be created."""
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._variationPoint_child=ModelNone

class CommonSignalPath(complexbase.GroupBase):
	"""The CommonSignalPath describes that two or more SwcToSwcSignals and/or SwcToSwcOperationArguments shall take the same way (Signal Path) in the topology."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._signal_children=[]

class CommunicationBufferLocking(complexbase.GroupBase):
	"""The aggregation of this meta-class specifies that a RunnableEntity supports locked communication buffers supplied by the RTE. It is able to cope with the error RTE_E_COM_BUSY."""
	def __init__(self):
		super().__init__()
		self._supportBufferLocking_child=ModelNone

class CommunicationCluster(complexbase.GroupBase):
	"""The CommunicationCluster is the main element to describe the topological connection of communicating ECUs.

A cluster describes the ensemble of ECUs, which are linked by a communication medium of arbitrary topology (bus, star, ring, ...). The nodes within the cluster share the same communication protocol, which may be event-triggered, time-triggered or a combination of both.

A CommunicationCluster aggregates one or more physical channels."""
	def __init__(self):
		super().__init__()
		self._baudrate_child=ModelNone
		self._physicalChannel_children=[]
		self._protocolName_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._speed_child=ModelNone

class CommunicationClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CommunicationConnector(complexbase.GroupBase):
	"""The connection between the referencing ECU and the referenced channel via the referenced controller.

Connectors are used to describe the bus interfaces of the ECUs and to specify the sending/receiving behavior. 
Each CommunicationConnector has a reference to exactly one communicationController. 

Note: Several CommunicationConnectors can be assigned to one PhysicalChannel in the scope of one ECU Instance."""
	def __init__(self):
		super().__init__()
		self._commController_child=ModelNone
		self._createEcuWakeupSource_child=ModelNone
		self._dynamicPncToChannelMappingEnabled_child=ModelNone
		self._ecuCommPortInstance_children=[]
		self._pncFilterArrayMask_children=[]
		self._pncGatewayType_child=ModelNone
		self._variationPoint_child=ModelNone

class CommunicationConnectorRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._CommunicationConnector_child=ModelNone
		self._variationPoint_child=ModelNone

class CommunicationController(complexbase.GroupBase):
	"""The communication controller is a dedicated hardware device by means of which hosts are sending frames to and receiving frames from the communication medium."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone
		self._wakeUpByControllerSupported_child=ModelNone

class CommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class CommunicationControllerMapping(complexbase.GroupBase):
	"""CommunicationControllerMapping specifies the CommunicationPeripheral
hardware (defined in the ECU Resource Template) to realize the specified CommunicationController in a physical topology."""
	def __init__(self):
		super().__init__()
		self._communicationController_child=ModelNone
		self._hwCommunicationController_child=ModelNone

class CommunicationCycle(complexbase.GroupBase):
	"""The communication cycle where the frame is sent."""

class Compiler(complexbase.GroupBase):
	"""Specifies the compiler attributes. In case of source code this specifies requirements how the compiler shall be invoked. In case of object code this documents the used compiler settings."""
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._options_child=ModelNone
		self._vendor_child=ModelNone
		self._version_child=ModelNone

class ComplexDeviceDriverSwComponentType(complexbase.GroupBase):
	"""The ComplexDeviceDriverSwComponentType is a special AtomicSwComponentType that has direct access to hardware on an ECU and which is therefore linked to a specific ECU or specific hardware. The ComplexDeviceDriverSwComponentType introduces the possibility to link from the software representation to its hardware description provided by the ECU Resource Template."""
	def __init__(self):
		super().__init__()
		self._hardwareElement_children=[]

class ComponentClustering(complexbase.GroupBase):
	"""Constraint that forces the mapping of all referenced SW component instances  to the same ECU, Core, Partition depending on the defined mappingScope attribute.  If mappingScope is not specified then mappingScopeEcu shall be assumed."""
	def __init__(self):
		super().__init__()
		self._clusteredComponent_children=[]
		self._mappingScope_child=ModelNone

class ComponentInCompositionInstanceRef(complexbase.GroupBase):
	"""The ComponentInCompositionInstanceRef points to a concrete SwComponentPrototype within a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._targetComponent_child=ModelNone

class ComponentInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._targetComponent_child=ModelNone

class ComponentSeparation(complexbase.GroupBase):
	"""Constraint that forces the two referenced SW components (called A and B in the following) not to be mapped to the same ECU, Core, Partition depending on the defined mappingScope attribute. If mappingScope is not specified then mappingScopeEcu shall be assumed. 

If a SW component (e.g. A) is a composition, none of the atomic SW components making up the A composition shall be mapped together with any of the atomic SW components making up the B composition. Furthermore, A and B shall be disjoint."""
	def __init__(self):
		super().__init__()
		self._mappingScope_child=ModelNone
		self._separatedComponent_children=[]

class CompositeNetworkRepresentation(complexbase.GroupBase):
	"""This meta-class is used to define the network representation of leaf elements of composite application data types."""
	def __init__(self):
		super().__init__()
		self._leafElement_child=ModelNone
		self._networkRepresentation_child=ModelNone

class CompositeRuleBasedValueArgument(complexbase.GroupBase):
	"""This meta-class has the ability to serve as the abstract base class for ValueSpecifications that can be used for compound primitive data types."""

class CompositeRuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class represents rule-based values for DataPrototypes typed by composite AutosarDataTypes."""
	def __init__(self):
		super().__init__()
		self._rule_child=ModelNone
		self._argument_children=[]
		self._compoundPrimitiveArgument_children=[]
		self._maxSizeToFill_child=ModelNone

class CompositeValueSpecification(complexbase.GroupBase):
	"""This abstract meta-class acts a base class for ValueSpecifications that have a composite form."""

class CompositionPPortToExecutablePPortMapping(complexbase.GroupBase):
	"""This meta-class has the ability to associate a PPortPrototype defined in the context of a SwClusterDesign to a PPortPrototype in the context of an Executable."""
	def __init__(self):
		super().__init__()
		self._executableProvidedPort_child=ModelNone
		self._swClusterDesignProvidedPort_child=ModelNone

class CompositionPortToExecutablePortMapping(complexbase.GroupBase):
	"""This abstract meta-class acts as a base class for the specification of a mapping between a PortPrototype owned by a RootSwClusterDesignComponentPrototype to a PortPrototype owned by a ComponentPrototype inside an Executable.rootSwComponentType."""
	def __init__(self):
		super().__init__()
		self._processDesign_child=ModelNone

class CompositionRPortToExecutableRPortMapping(complexbase.GroupBase):
	"""This meta-class has the ability to associate an RPortPrototype defined in the context of a SwClusterDesign to an RPortPrototype in the context of an Executable."""
	def __init__(self):
		super().__init__()
		self._executableRequiredPort_child=ModelNone
		self._swClusterDesignRequiredPort_child=ModelNone

class CompositionSwComponentType(complexbase.GroupBase):
	"""A [ARTechTerm{CompositionSwComponentType}] aggregates [ARMetaClass{SwComponentPrototype}]s (that in turn are typed by [ARMetaClass{SwComponentTypes}]) as well as [ARMetaClass{SwConnector}]s for primarily connecting [ARMetaClass{SwComponentPrototype}]s among each others and towards the surface of the [ARTechTerm{CompositionSwComponentType}]. By this means, a hierarchical structures of software-components can be created."""
	def __init__(self):
		super().__init__()
		self._component_children=[]
		self._connector_children=[]
		self._constantValueMapping_children=[]
		self._dataTypeMapping_children=[]
		self._instantiationRTEEventProps_children=[]
		self._physicalDimensionMapping_child=ModelNone

class CompositionSwComponentTypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._CompositionSwComponentType_child=ModelNone
		self._variationPoint_child=ModelNone

class Compu(complexbase.GroupBase):
	"""This meta-class represents the ability to express one particular computation."""
	def __init__(self):
		super().__init__()
		self._compuDefaultValue_child=ModelNone
		self._compuScale_children=[]

class CompuConst(complexbase.GroupBase):
	"""This meta-class represents the fact that the value of a computation method scale is constant."""
	def __init__(self):
		super().__init__()
		self._vt_child=ModelNone
		self._v_child=ModelNone
		self._vf_child=ModelNone

class CompuConstContent(complexbase.GroupBase):
	"""This meta-class represents the fact that the constant value of the computation method can be numerical or textual."""

class CompuConstFormulaContent(complexbase.GroupBase):
	"""This meta-class represents the fact that the constant value of the computation method is represented by a variation point. This difference is due to compatibility with ASAM HDO."""
	def __init__(self):
		super().__init__()
		self._vf_child=ModelNone

class CompuConstNumericContent(complexbase.GroupBase):
	"""This meta-class represents the fact that the constant value of the computation method is a numerical value. It is separated from CompuConstFormulaContent to support compatibility with ASAM HDO."""
	def __init__(self):
		super().__init__()
		self._v_child=ModelNone

class CompuConstTextContent(complexbase.GroupBase):
	"""This meta-class represents the textual content of a scale."""
	def __init__(self):
		super().__init__()
		self._vt_child=ModelNone

class CompuContent(complexbase.GroupBase):
	"""This abstract meta-class represents the various definition means of a computation method."""

class CompuGenericMath(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a generic formula expression."""
	level=complexbase.Attribute("level",SimpleTypes.PrimitiveIdentifier,'LEVEL',False,"""Placeholder to describe an indicator of a language level for the mathematics e.g. INFORMAL, ASAMHDO.
May be refined by particular use-cases.""")

class CompuMethod(complexbase.GroupBase):
	"""This meta-class represents the ability to express the relationship between a physical value and the mathematical representation. 

Note that this is still independent of the technical implementation in data types. It only specifies the formula how the internal value corresponds to its physical pendant."""
	def __init__(self):
		super().__init__()
		self._displayFormat_child=ModelNone
		self._unit_child=ModelNone
		self._compuInternalToPhys_child=ModelNone
		self._compuPhysToInternal_child=ModelNone

class CompuNominatorDenominator(complexbase.GroupBase):
	"""This class represents the ability to express a polynomial either as Nominator or as Denominator."""
	def __init__(self):
		super().__init__()
		self._v_children=[]

class CompuRationalCoeffs(complexbase.GroupBase):
	"""This meta-class represents the ability to express a rational function by specifying the coefficients of nominator and denominator."""
	def __init__(self):
		super().__init__()
		self._compuNumerator_child=ModelNone
		self._compuDenominator_child=ModelNone

class CompuScale(complexbase.GroupBase):
	"""This meta-class represents the ability to specify one segment of a segmented computation method."""
	def __init__(self):
		super().__init__()
		self._a2lDisplayText_child=ModelNone
		self._shortLabel_child=ModelNone
		self._symbol_child=ModelNone
		self._desc_child=ModelNone
		self._mask_child=ModelNone
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone
		self._compuInverseValue_child=ModelNone
		self._variationPoint_child=ModelNone
		self._compuRationalCoeffs_child=ModelNone
		self._compuConst_child=ModelNone

class CompuScaleConstantContents(complexbase.GroupBase):
	"""This meta-class represents the fact that a particular scale of the computation method is constant."""
	def __init__(self):
		super().__init__()
		self._compuConst_child=ModelNone

class CompuScaleContents(complexbase.GroupBase):
	"""This abstract meta-class represents the content of one particular scale."""

class CompuScaleRationalFormula(complexbase.GroupBase):
	"""This meta-class represents the fact that the computation in this scale is represented as rational term."""
	def __init__(self):
		super().__init__()
		self._compuRationalCoeffs_child=ModelNone

class CompuScales(complexbase.GroupBase):
	"""This meta-class represents the ability to stepwise express a computation method."""
	def __init__(self):
		super().__init__()
		self._compuScale_children=[]

class ConcreteClassTailoring(complexbase.GroupBase):
	"""Tailoring of concrete meta classes."""
	def __init__(self):
		super().__init__()
		self._validationRoot_child=ModelNone

class ConcretePatternEventTriggering(complexbase.GroupBase):
	"""Describes the behavior of an event that occurs according to a precisely known pattern."""
	def __init__(self):
		super().__init__()
		self._patternJitter_child=ModelNone
		self._patternPeriod_child=ModelNone
		self._offset_children=[]
		self._patternLength_child=ModelNone

class ConditionByFormula(complexbase.GroupBase):
	"""This class represents a condition which is computed based on system constants according to the specified expression. The expected result is considered as boolean value.

The result of the expression is interpreted as a condition. 

* \"0\" represents \"false\";  

* a value other than zero is considered \"true\""""
	bindingTime=complexbase.Attribute("bindingTime",SimpleTypes.BindingTimeEnum,'BINDING-TIME',False,"""This attribute specifies the point in time when condition may be evaluated at earliest. At this point in time all referenced system constants shall have a value.""")

class ConditionalChangeNad(complexbase.GroupBase):
	"""Generates an conditional change NAD request. See ISO 17987 protocol specification for more information."""
	def __init__(self):
		super().__init__()
		self._byte_child=ModelNone
		self._id_child=ModelNone
		self._invert_child=ModelNone
		self._mask_child=ModelNone
		self._newNad_child=ModelNone

class ConfidenceInterval(complexbase.GroupBase):
	"""Additionally to the list of measured distances of event occurrences, a confidence interval can be specified for the expected distance of two consecutive event occurrences with a given probability."""
	def __init__(self):
		super().__init__()
		self._lowerBound_child=ModelNone
		self._propability_child=ModelNone
		self._upperBound_child=ModelNone

class ConsistencyNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to define requirements on the implicit communication behavior."""
	def __init__(self):
		super().__init__()
		self._dpgDoesNotRequireCoherency_children=[]
		self._dpgRequiresCoherency_children=[]
		self._regDoesNotRequireStability_children=[]
		self._regRequiresStability_children=[]
		self._variationPoint_child=ModelNone

class ConsistencyNeedsBlueprintSet(complexbase.GroupBase):
	"""This meta class represents the ability to specify a set of blueprint for ConsistencyNeeds."""
	def __init__(self):
		super().__init__()
		self._consistencyNeeds_children=[]

class ConstantReference(complexbase.GroupBase):
	"""Instead of defining this value inline, a constant is referenced."""
	def __init__(self):
		super().__init__()
		self._constant_child=ModelNone

class ConstantSpecification(complexbase.GroupBase):
	"""Specification of a constant that can be part of a package, i.e. it can be defined stand-alone."""
	def __init__(self):
		super().__init__()
		self._valueSpec_child=[]

class ConstantSpecificationMapping(complexbase.GroupBase):
	"""This meta-class is used to create an association of two ConstantSpecifications. One ConstantSpecification is supposed to be defined in the application domain while the other should be defined in the implementation domain.

Hence the ConstantSpecificationMapping needs to be used where a ConstantSpecification defined in one domain needs to be associated to a ConstantSpecification in the other domain.

This information is crucial for the RTE generator."""
	def __init__(self):
		super().__init__()
		self._applConstant_child=ModelNone
		self._implConstant_child=ModelNone

class ConstantSpecificationMappingSet(complexbase.GroupBase):
	"""This meta-class represents the ability to map two ConstantSpecifications to each others. One ConstantSpecification is supposed to be described in the application domain and the other should be described in the implementation domain."""
	def __init__(self):
		super().__init__()
		self._mapping_children=[]

class ConstraintTailoring(complexbase.GroupBase):
	"""Tailoring of constraints. If a constraint is in scope, then the severity defines its Error Severity Level. If it is not in scope, then the constraint is disabled."""
	def __init__(self):
		super().__init__()
		self._constraint_child=ModelNone

class ConsumedEventGroup(complexbase.GroupBase):
	"""This element represents an event-group to which the service consumer wants to subscribe."""
	def __init__(self):
		super().__init__()
		self._applicationEndpoint_child=ModelNone
		self._autoRequire_child=ModelNone
		self._eventGroupIdentifier_child=ModelNone
		self._eventMulticastAddress_children=[]
		self._instanceIdentifier_child=ModelNone
		self._pduActivationRoutingGroup_children=[]
		self._priority_child=ModelNone
		self._routingGroup_children=[]
		self._sdClientConfig_child=ModelNone
		self._sdClientTimerConfig_children=[]
		self._variationPoint_child=ModelNone

class ConsumedProvidedServiceInstanceGroup(complexbase.GroupBase):
	"""The AUTOSAR ServiceDiscovery is able to start and to stop ClientServices and ServerServices,respectively, at runtime. A SdServiceGroup contains several ClientServices and ServerServices, respectively."""
	def __init__(self):
		super().__init__()
		self._consumedServiceInstance_children=[]
		self._providedServiceInstance_children=[]

class ConsumedProvidedServiceInstanceGroupRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ConsumedProvidedServiceInstanceGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class ConsumedServiceInstance(complexbase.GroupBase):
	"""Service instances that are consumed by the ECU that is connected via the ApplicationEndpoint to a CommunicationConnector."""
	def __init__(self):
		super().__init__()
		self._allowedServiceProvider_children=[]
		self._autoRequire_child=ModelNone
		self._blacklistedVersion_children=[]
		self._blocklistedVersion_children=[]
		self._consumedEventGroup_children=[]
		self._eventMulticastSubscriptionAddress_children=[]
		self._instanceIdentifier_child=ModelNone
		self._localUnicastAddress_children=[]
		self._minorVersion_child=ModelNone
		self._providedServiceInstance_child=ModelNone
		self._remoteUnicastAddress_children=[]
		self._sdClientConfig_child=ModelNone
		self._sdClientTimerConfig_children=[]
		self._serviceIdentifier_child=ModelNone
		self._versionDrivenFindBehavior_child=ModelNone

class ConsumedServiceInstanceRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ConsumedServiceInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class ContainedIPduProps(complexbase.GroupBase):
	"""Defines the aspects of an IPdu which can be collected inside a ContainerIPdu."""
	def __init__(self):
		super().__init__()
		self._collectionSemantics_child=ModelNone
		self._containedPduTriggering_child=ModelNone
		self._headerIdLongHeader_child=ModelNone
		self._headerIdShortHeader_child=ModelNone
		self._offset_child=ModelNone
		self._priority_child=ModelNone
		self._timeout_child=ModelNone
		self._trigger_child=ModelNone
		self._updateIndicationBitPosition_child=ModelNone

class ContainerIPdu(complexbase.GroupBase):
	"""Allows to collect several IPdus in one ContainerIPdu based on the headerType."""
	def __init__(self):
		super().__init__()
		self._containedIPduTriggeringProps_children=[]
		self._containedPduTriggering_children=[]
		self._containerTimeout_child=ModelNone
		self._containerTrigger_child=ModelNone
		self._headerType_child=ModelNone
		self._minimumRxContainerQueueSize_child=ModelNone
		self._minimumTxContainerQueueSize_child=ModelNone
		self._rxAcceptContainedIPdu_child=ModelNone
		self._thresholdSize_child=ModelNone
		self._unusedBitPattern_child=ModelNone

class CouplingElement(complexbase.GroupBase):
	"""A CouplingElement is used to connect EcuInstances to the VLAN of an EthernetCluster. CouplingElements can reach from a simple hub to a complex managed switch or even devices with functionalities in higher layers. A CouplingElement that is not related to an EcuInstance occurs as a dedicated single device."""
	def __init__(self):
		super().__init__()
		self._communicationCluster_child=ModelNone
		self._couplingElementDetails_children=[]
		self._couplingPort_children=[]
		self._couplingType_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._firewallRule_children=[]

class CouplingElementAbstractDetails(complexbase.GroupBase):
	"""Collection of specific details for the CouplingElement."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class CouplingElementSwitchDetails(complexbase.GroupBase):
	"""Collection of specific details for the CouplingElement of couplingType switch."""
	def __init__(self):
		super().__init__()
		self._flowMetering_children=[]
		self._streamFilter_children=[]
		self._streamGate_children=[]
		self._switchStreamIdentification_children=[]
		self._trafficShaperGroup_children=[]

class CouplingPort(complexbase.GroupBase):
	"""A CouplingPort is used to connect a CouplingElement with an EcuInstance or two CouplingElements with each other via a CouplingPortConnection. Optionally, the CouplingPort may also have a reference to a macMulticastGroup and a defaultVLAN."""
	def __init__(self):
		super().__init__()
		self._connectionNegotiationBehavior_child=ModelNone
		self._couplingPortDetails_child=ModelNone
		self._couplingPortRole_child=ModelNone
		self._couplingPortSpeed_child=ModelNone
		self._defaultVlan_child=ModelNone
		self._macLayerType_child=ModelNone
		self._macMulticastAddress_children=[]
		self._macSecProps_children=[]
		self._physicalLayerType_child=ModelNone
		self._plcaProps_child=ModelNone
		self._pncMapping_children=[]
		self._receiveActivity_child=ModelNone
		self._vlanMembership_children=[]
		self._vlanModifier_child=ModelNone
		self._wakeupSleepOnDatalineConfig_child=ModelNone
		self._variationPoint_child=ModelNone

class CouplingPortAbstractShaper(complexbase.GroupBase):
	"""Abstract class for the definition of coupling port shapers."""

class CouplingPortAsynchronousTrafficShaper(complexbase.GroupBase):
	"""Defines an Asynchronous Traffic Shaper (ATS) for the CouplingPort egress structure."""
	def __init__(self):
		super().__init__()
		self._committedBurstSize_child=ModelNone
		self._committedInformationRate_child=ModelNone
		self._trafficShaperGroup_child=ModelNone

class CouplingPortConnection(complexbase.GroupBase):
	"""Connection between two CouplingPorts (firstPort and secondPort) or between a collection of Ports that are all referenced by the portCollection reference."""
	def __init__(self):
		super().__init__()
		self._firstPort_child=ModelNone
		self._nodePort_children=[]
		self._plcaLocalNodeCount_child=ModelNone
		self._plcaTransmitOpportunityTimer_child=ModelNone
		self._secondPort_child=ModelNone
		self._variationPoint_child=ModelNone

class CouplingPortCreditBasedShaper(complexbase.GroupBase):
	"""Defines a Credit Based Shaper (CBS) for the CouplingPort egress structure."""
	def __init__(self):
		super().__init__()
		self._idleSlope_child=ModelNone
		self._lowerBoundary_child=ModelNone
		self._upperBoundary_child=ModelNone

class CouplingPortDetails(complexbase.GroupBase):
	"""Defines details of a CouplingPort.

May be used to configure the structures of a switch."""
	def __init__(self):
		super().__init__()
		self._couplingPortStructuralElement_children=[]
		self._ethernetPriorityRegeneration_children=[]
		self._ethernetTrafficClassAssignment_children=[]
		self._globalTimeProps_child=ModelNone
		self._lastEgressScheduler_child=ModelNone
		self._ratePolicy_children=[]

class CouplingPortFifo(complexbase.GroupBase):
	"""Defines a FIFO for the CouplingPort egress structure."""
	def __init__(self):
		super().__init__()
		self._assignedTrafficClass_children=[]
		self._minimumFifoLength_child=ModelNone
		self._shaper_child=[]

class CouplingPortRatePolicy(complexbase.GroupBase):
	"""Defines a rate policy on a CouplingPort."""
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._policyAction_child=ModelNone
		self._priority_child=ModelNone
		self._timeInterval_child=ModelNone
		self._vLan_children=[]

class CouplingPortRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._CouplingPort_child=ModelNone
		self._variationPoint_child=ModelNone

class CouplingPortScheduler(complexbase.GroupBase):
	"""Defines a scheduler for the CouplingPort egress structure."""
	def __init__(self):
		super().__init__()
		self._portScheduler_child=ModelNone
		self._predecessor_children=[]

class CouplingPortShaper(complexbase.GroupBase):
	"""Defines a shaper for the CouplingPort egress structure."""
	def __init__(self):
		super().__init__()
		self._idleSlope_child=ModelNone
		self._predecessorFifo_child=ModelNone

class CouplingPortStructuralElement(complexbase.GroupBase):
	"""General class to define structural elements a CouplingPort may consist of."""

class CouplingPortTrafficClassAssignment(complexbase.GroupBase):
	"""Defines the assignment of Traffic Class to a frame."""
	def __init__(self):
		super().__init__()
		self._priority_children=[]
		self._trafficClass_child=ModelNone

class CpSoftwareCluster(complexbase.GroupBase):
	"""This meta class provides the ability to define a CP Software Cluster.
Each CP Software Cluster can be integrated and build individually.
It defines the sub-set of hierarchical tree(s) of Software Components belonging to this CP Software Cluster. Resources required or provided by this CP Software Cluster are given in the according mappings."""
	def __init__(self):
		super().__init__()
		self._softwareClusterId_child=ModelNone
		self._swComponentAssignment_children=[]
		self._swComposition_children=[]

class CpSoftwareClusterBinaryManifestDescriptor(complexbase.GroupBase):
	"""This meta-class has the ability to act as a hub for all information related to the binary manifest of a given CP software cluster. The manifest is subject to integrator work and therefore not a part of the definition of the CP software cluster itself."""
	def __init__(self):
		super().__init__()
		self._cpSoftwareCluster_child=ModelNone
		self._metaDataField_children=[]
		self._provideResource_children=[]
		self._requireResource_children=[]
		self._resourceDefinition_children=[]
		self._softwareClusterId_child=ModelNone

class CpSoftwareClusterCommunicationResource(complexbase.GroupBase):
	"""Represents a single resource required or provided by a CP Software Cluster which relates to the port based communication on VFB level."""
	def __init__(self):
		super().__init__()
		self._comProps_child=ModelNone
		self._communicationResourceProps_child=[]

class CpSoftwareClusterCommunicationResourceProps(complexbase.GroupBase):
	"""Communication properties for cross cluster communication."""

class CpSoftwareClusterMappingSet(complexbase.GroupBase):
	"""This meta-class represents the ability to aggregate a collection of CP Software Cluster relevant mappings. This is applicable if a CP Software Cluster is described besides a concrete System, e.g. a reusable CP Software Cluster."""
	def __init__(self):
		super().__init__()
		self._portElementToComResourceMapping_children=[]
		self._resourceToApplicationPartitionMapping_children=[]
		self._softwareClusterToApplicationPartitionMapping_child=ModelNone
		self._softwareClusterToResourceMapping_children=[]
		self._swcToApplicationPartitionMapping_children=[]

class CpSoftwareClusterRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._CpSoftwareCluster_child=ModelNone
		self._variationPoint_child=ModelNone

class CpSoftwareClusterResource(complexbase.GroupBase):
	"""Represents a single resource required or provided by a CP Software Cluster."""
	def __init__(self):
		super().__init__()
		self._dependentResource_children=[]
		self._globalResourceId_child=ModelNone
		self._isMandatory_child=ModelNone

class CpSoftwareClusterResourcePool(complexbase.GroupBase):
	"""Represents the pool of resources which can be provided or required by CP Software Clusters."""
	def __init__(self):
		super().__init__()
		self._ecuScope_children=[]
		self._resource_children=[]

class CpSoftwareClusterResourceToApplicationPartitionMapping(complexbase.GroupBase):
	"""This meta class maps a Software Cluster resource to an Application Partition to restrict the usage."""
	def __init__(self):
		super().__init__()
		self._applicationPartition_child=ModelNone
		self._resource_child=ModelNone
		self._variationPoint_child=ModelNone

class CpSoftwareClusterServiceResource(complexbase.GroupBase):
	"""Represents a single resource required or provided by a CP Software Cluster which relates to the BSW."""
	def __init__(self):
		super().__init__()
		self._resourceNeeds_children=[]

class CpSoftwareClusterToApplicationPartitionMapping(complexbase.GroupBase):
	"""This meta class defines ApplicationPartitions that are applicable for the CpSoftwareCluster."""
	def __init__(self):
		super().__init__()
		self._applicationPartition_children=[]
		self._softwareCluster_child=ModelNone
		self._variationPoint_child=ModelNone

class CpSoftwareClusterToEcuInstanceMapping(complexbase.GroupBase):
	"""This meta class maps a CpSoftwareCluster to a EcuInstance."""
	def __init__(self):
		super().__init__()
		self._ecuInstance_child=ModelNone
		self._machineId_child=ModelNone
		self._swCluster_children=[]
		self._variationPoint_child=ModelNone

class CpSoftwareClusterToResourceMapping(complexbase.GroupBase):
	"""This meta class maps a service resource to CP Software Clusters. By this mapping it's specified whether the Software Cluster has to provide or to require the resource."""
	def __init__(self):
		super().__init__()
		self._provider_child=ModelNone
		self._requester_children=[]
		self._serviceResource_child=ModelNone
		self._variationPoint_child=ModelNone

class CpSwClusterResourceToDiagDataElemMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a CpSoftwareClusterResource with a DiagnosticDataElement. This allows for indicating that the CpSoftwareClusterResource is used to convey the DiagnosticDataElement."""
	def __init__(self):
		super().__init__()
		self._cpSoftwareClusterResource_child=ModelNone
		self._diagnosticDataElement_child=ModelNone

class CpSwClusterResourceToDiagFunctionIdMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a CpSoftwareClusterResource with a subfunction of a DiagnosticFunctionIdentifier. This allows for indicating that the CpSoftwareClusterResource is used to convey the execution permission associated with the mapped function identifier."""
	def __init__(self):
		super().__init__()
		self._cpSoftwareClusterResource_child=ModelNone
		self._functionIdentifier_child=ModelNone

class CpSwClusterToDiagEventMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a CpSoftwareClusterResource with a DiagnosticEvent. This allows for indicating that the CpSoftwareClusterResource is used to convey the reporting or status query of the mapped DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._cpSoftwareClusterResource_child=ModelNone
		self._diagnosticEvent_child=ModelNone

class CpSwClusterToDiagRoutineSubfunctionMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a CpSoftwareClusterResource with a subfunction of a DiagnosticRoutine. This allows for indicating that the CpSoftwareClusterResource is used to convey the calling or result return of the mapped DiagnosticRoutine."""
	def __init__(self):
		super().__init__()
		self._cpSoftwareClusterResource_child=ModelNone
		self._routineSubfunction_child=ModelNone

class CppImplementationDataType(complexbase.GroupBase):
	"""This meta-class represents the way to specify a reusable data type definition taken as a the basis for a C++ language binding"""
	def __init__(self):
		super().__init__()
		self._arraySize_child=ModelNone
		self._headerFile_child=ModelNone
		self._namespace_children=[]
		self._subElement_children=[]
		self._templateArgument_children=[]
		self._typeEmitter_child=ModelNone
		self._typeReference_child=ModelNone

class CppImplementationDataTypeContextTarget(complexbase.GroupBase):
	"""This meta-class has the ability to serve as the context in instanceRef-like modeling for CppImplementationDataType and CppImplementationDataTypeElement"""

class CppImplementationDataTypeElement(complexbase.GroupBase):
	"""Declares a data object which is locally aggregated. Such an element can only be used within the scope where it is aggregated. 
A CppImplementationDataTypeElement is used to represent an element of a structure, defining its type."""
	def __init__(self):
		super().__init__()
		self._isOptional_child=ModelNone
		self._typeReference_child=ModelNone

class CppImplementationDataTypeElementQualifier(complexbase.GroupBase):
	"""This element qualifies the typeReference of the CppImplementationDataTypeElement to the CppImplementationDataType."""
	def __init__(self):
		super().__init__()
		self._inplace_child=ModelNone
		self._typeReference_child=ModelNone

class CppTemplateArgument(complexbase.GroupBase):
	"""This meta-class has the ability to define properties for template arguments."""
	def __init__(self):
		super().__init__()
		self._allocator_child=ModelNone
		self._category_child=ModelNone
		self._inplace_child=ModelNone
		self._templateType_child=ModelNone

class CryptoCertificate(complexbase.GroupBase):
	"""This meta-class represents the ability to model a cryptographic certificate."""
	def __init__(self):
		super().__init__()
		self._isPrivate_child=ModelNone

class CryptoCertificateInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for a CryptoCertificate."""
	def __init__(self):
		super().__init__()
		self._isPrivate_child=ModelNone
		self._writeAccess_child=ModelNone

class CryptoCertificateKeySlotNeeds(complexbase.GroupBase):
	"""This meta-class shall be taken to indicate that the SwcServiceDependecy modeled with this kind of ServiceNeeds defines a relationship between a CryptoKeySlot and a CryptoCertificate."""

class CryptoCertificateToCryptoKeySlotMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a CryptoKeySlot and a CryptoCertificate."""
	def __init__(self):
		super().__init__()
		self._cryptoCertificate_child=ModelNone
		self._cryptoKeySlot_children=[]

class CryptoCertificateToPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a CryptoCertificate on deployment level to a given PortPrototype that is typed by a CryptoCertificateInterface."""
	def __init__(self):
		super().__init__()
		self._cryptoCertificate_child=ModelNone
		self._portPrototype_child=ModelNone
		self._process_child=ModelNone
		self._writeAccess_child=ModelNone

class CryptoEllipticCurveProps(complexbase.GroupBase):
	"""This meta-class provides attributes to specify the properties of elliptic curves."""
	def __init__(self):
		super().__init__()
		self._namedCurveId_child=ModelNone

class CryptoInterface(complexbase.GroupBase):
	"""This meta-class provides the abstract ability to define a PortInterface for the support of crypto use cases."""

class CryptoKeyManagementNeeds(complexbase.GroupBase):
	"""This meta-class can be used to indicate a service use case for key management."""

class CryptoKeySlot(complexbase.GroupBase):
	"""This meta-class represents the ability to define a concrete key to be used for a crypto operation."""
	def __init__(self):
		super().__init__()
		self._allocateShadowCopy_child=ModelNone
		self._cryptoAlgId_child=ModelNone
		self._cryptoObjectType_child=ModelNone
		self._keySlotAllowedModification_child=ModelNone
		self._keySlotContentAllowedUsage_children=[]
		self._slotCapacity_child=ModelNone
		self._slotType_child=ModelNone

class CryptoKeySlotAllowedModification(complexbase.GroupBase):
	"""This meta-class restricts the allowed modification of a key stored in the key slot."""
	def __init__(self):
		super().__init__()
		self._allowContentTypeChange_child=ModelNone
		self._exportability_child=ModelNone
		self._maxNumberOfAllowedUpdates_child=ModelNone
		self._restrictUpdate_child=ModelNone

class CryptoKeySlotContentAllowedUsage(complexbase.GroupBase):
	"""This meta-class restricts the allowed usage of a key stored in the key slot."""
	def __init__(self):
		super().__init__()
		self._allowedKeyslotUsage_child=ModelNone

class CryptoKeySlotInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for Crypto Key Slots."""
	def __init__(self):
		super().__init__()
		self._allocateShadowCopy_child=ModelNone
		self._cryptoAlgId_child=ModelNone
		self._cryptoObjectType_child=ModelNone
		self._keySlotAllowedModification_child=ModelNone
		self._keySlotContentAllowedUsage_children=[]
		self._slotCapacity_child=ModelNone
		self._slotType_child=ModelNone

class CryptoKeySlotToPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a CryptoKeySlot on deployment level to a given PortPrototype that is typed by a CryptoKeySlotInterface."""
	def __init__(self):
		super().__init__()
		self._keySlot_child=ModelNone
		self._portPrototype_child=ModelNone
		self._process_child=ModelNone

class CryptoModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the configuration  for the Crypto stack on a specific machine."""
	def __init__(self):
		super().__init__()
		self._certificateToKeySlotMapping_children=[]
		self._cryptoCertificate_children=[]
		self._cryptoProvider_children=[]

class CryptoNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of Crypto."""

class CryptoProvider(complexbase.GroupBase):
	"""CryptoProvider implements cryptographic primitives (algorithms) supported by the stack. Implementation of this component may be software or hardware based (HSM/TPM)."""
	def __init__(self):
		super().__init__()
		self._cryptoProviderDocumentation_child=ModelNone
		self._keySlot_children=[]

class CryptoProviderInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for a CryptoProvider."""

class CryptoProviderToPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a CryptoProvider on deployment level to a given PortPrototype that is typed by a CryptoProviderInterface."""
	def __init__(self):
		super().__init__()
		self._cryptoProvider_child=ModelNone
		self._portPrototype_child=ModelNone
		self._process_child=ModelNone

class CryptoServiceCertificate(complexbase.GroupBase):
	"""This meta-class represents the ability to model a cryptographic certificate."""
	def __init__(self):
		super().__init__()
		self._algorithmFamily_child=ModelNone
		self._format_child=ModelNone
		self._maximumLength_child=ModelNone
		self._nextHigherCertificate_child=ModelNone
		self._serverNameIdentification_child=ModelNone

class CryptoServiceJobNeeds(complexbase.GroupBase):
	"""This meta-class shall be taken to indicate that the service use case modeled with this kind of ServiceNeeds assumes the usage of the crypto job API."""

class CryptoServiceKey(complexbase.GroupBase):
	"""This meta-class has the ability to represent a crypto key."""
	def __init__(self):
		super().__init__()
		self._algorithmFamily_child=ModelNone
		self._developmentValue_child=[]
		self._keyGeneration_child=ModelNone
		self._keyStorageType_child=ModelNone
		self._length_child=ModelNone

class CryptoServiceMapping(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for specializations of crypto service mappings."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class CryptoServiceNeeds(complexbase.GroupBase):
	"""Specifies the needs on the configuration of the CryptoServiceManager for one ConfigID (see Specification AUTOSAR_SWS_CSM.doc). An instance of this class is used to find out which ports of a software-component belong to this ConfigID."""
	def __init__(self):
		super().__init__()
		self._algorithmFamily_child=ModelNone
		self._algorithmMode_child=ModelNone
		self._cryptoKeyDescription_child=ModelNone
		self._maximumKeyLength_child=ModelNone

class CryptoServicePrimitive(complexbase.GroupBase):
	"""This meta-class has the ability to represent a crypto primitive."""
	def __init__(self):
		super().__init__()
		self._algorithmFamily_child=ModelNone
		self._algorithmMode_child=ModelNone
		self._algorithmSecondaryFamily_child=ModelNone

class CryptoServiceQueue(complexbase.GroupBase):
	"""This meta-class has the ability to represent a crypto queue."""
	def __init__(self):
		super().__init__()
		self._queueSize_child=ModelNone

class CryptoSignatureScheme(complexbase.GroupBase):
	"""This meta-class provides attributes to specify the TLS Signature Scheme."""
	def __init__(self):
		super().__init__()
		self._signatureSchemeId_child=ModelNone

class CryptoTrustMasterInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for TrustMaster."""

class CustomCppImplementationDataType(complexbase.GroupBase):
	"""This meta-class represents the way to specify a data type definition that is taken as the basis for a C++ language binding to a custom implementation that is declared in the configured header file. The ShortName of this CustomCppImplementationDataType defines the Class-Name of the custom implementation."""

class CycleCounter(complexbase.GroupBase):
	"""The communication cycle where the frame is send is described by the attribute \"cycleCounter\"."""
	def __init__(self):
		super().__init__()
		self._CycleCounter_child=ModelNone

class CycleRepetition(complexbase.GroupBase):
	"""The communication cycle where the frame is send is described by the
attributes baseCycle and cycleRepetition."""
	def __init__(self):
		super().__init__()
		self._BaseCycle_child=ModelNone
		self._CycleRepetition_child=ModelNone

class CyclicTiming(complexbase.GroupBase):
	"""Specification of a cyclic sending behavior."""
	def __init__(self):
		super().__init__()
		self._timeOffset_child=ModelNone
		self._timePeriod_child=ModelNone

class DataComProps(complexbase.GroupBase):
	"""Represents a single resource required or provided by a CP Software Cluster which relates to the port based communication on VFB level."""
	def __init__(self):
		super().__init__()
		self._dataConsistencyPolicy_child=ModelNone
		self._sendIndication_child=ModelNone

class DataConstr(complexbase.GroupBase):
	"""This meta-class represents the ability to specify constraints on data."""
	def __init__(self):
		super().__init__()
		self._dataConstrRule_children=[]

class DataConstrRule(complexbase.GroupBase):
	"""This meta-class represents the ability to express one specific data constraint rule."""
	def __init__(self):
		super().__init__()
		self._constrLevel_child=ModelNone
		self._physConstrs_child=ModelNone
		self._internalConstrs_child=ModelNone

class DataDumpEntry(complexbase.GroupBase):
	"""This service is reserved for initial configuration of a slave node by the slave node supplier and the format of this message is supplier specific."""
	def __init__(self):
		super().__init__()
		self._byteValue_children=[]

class DataExchangePoint(complexbase.GroupBase):
	"""The Data Exchange Point describes the relationship between a work product and its intended use in the methodology with a tailoring of the AUTOSAR templates. 

An informal description is provided by the 'desc' and 'introduction' attributes of the DataExchangePoint. The informal description SHOULD include the subject that is described by this data exchange point.
E.g. 
* producible data of tool A, version x
* consumable data of tool B, version y
* agreed profile between partner A and partner B in project xyz"""
	def __init__(self):
		super().__init__()
		self._kind_child=ModelNone
		self._referencedBaseline_child=ModelNone
		self._specificationScope_child=ModelNone
		self._dataFormatTailoring_child=ModelNone

class DataFilter(complexbase.GroupBase):
	"""Base class for data filters. The type of the filter is specified in attribute dataFilterType. Some of the filter types require additional arguments which are specified as attributes of this class."""
	def __init__(self):
		super().__init__()
		self._dataFilterType_child=ModelNone
		self._mask_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone
		self._offset_child=ModelNone
		self._period_child=ModelNone
		self._x_child=ModelNone

class DataFormatElementReference(complexbase.GroupBase):
	"""Superclass of all references to specification elements that have direct impact on the data exchange format (Meta-Classes, Meta-Attributes, constraints, SdgDefs)"""

class DataFormatElementScope(complexbase.GroupBase):
	"""This class specifies if a Meta Class, Meta Attribute, Constraint or SdgDef is relevant for the Data Exchange Point."""

class DataFormatTailoring(complexbase.GroupBase):
	"""This class collects all rules that tailor the AUTOSAR templates for a specific data exchange point."""
	def __init__(self):
		super().__init__()
		self._classTailoring_children=[]
		self._constraintTailoring_children=[]

class DataInterface(complexbase.GroupBase):
	"""The purpose of this meta-class is to act as an abstract base class for subclasses that share the semantics of being concerned about data (as opposed to e.g. operations)."""

class DataLinkLayerRule(complexbase.GroupBase):
	"""Configuration of filter rules on the DataLink layer"""
	def __init__(self):
		super().__init__()
		self._destinationMacAddress_child=ModelNone
		self._destinationMacAddressMask_child=ModelNone
		self._etherType_child=ModelNone
		self._sourceMacAddress_child=ModelNone
		self._sourceMacAddressMask_child=ModelNone
		self._vlanId_child=ModelNone
		self._vlanPriority_child=ModelNone

class DataMapping(complexbase.GroupBase):
	"""Mapping of port elements (data elements and parameters) to frames and signals."""
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._eventGroup_children=[]
		self._eventHandler_children=[]
		self._introduction_child=ModelNone
		self._serviceInstance_children=[]
		self._variationPoint_child=ModelNone

class DataPrototype(complexbase.GroupBase):
	"""Base class for prototypical roles of any data type."""
	def __init__(self):
		super().__init__()
		self._swDataDefProps_child=ModelNone

class DataPrototypeGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to define a collection of DataPrototypes that are subject to the formal definition of implicit communication behavior. The definition of the collection can be nested."""
	def __init__(self):
		super().__init__()
		self._dataPrototypeGroup_children=[]
		self._implicitDataAccess_children=[]
		self._variationPoint_child=ModelNone

class DataPrototypeInClientServerInterfaceInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._rootDataPrototypeInCs_child=ModelNone
		self._contextDataPrototypeInCs_children=[]
		self._targetDataPrototypeInCs_child=ModelNone

class DataPrototypeInPortInterfaceInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to:
* refer to a DataPrototype in the context of a PortInterface.
* refer to the internal structure of a DataPrototype which is typed by an ApplicationDatatype in the context of a PortInterface."""

class DataPrototypeInPortInterfaceRef(complexbase.GroupBase):
	"""This class represents a RootDataPrototype that is typed by an ApplicationDataType or ImplementationDataType or a DataTypeElement that is aggregated within a composite application data type (record or array)."""
	def __init__(self):
		super().__init__()
		self._dataPrototypeInClientServerInterface_child=ModelNone
		self._dataPrototypeInSenderReceiverInterface_child=ModelNone
		self._dataPrototypeInServiceInterface_child=ModelNone
		self._dataPrototype_child=[]

class DataPrototypeInSenderReceiverInterfaceInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._rootDataPrototypeInSr_child=ModelNone
		self._contextDataPrototypeInSr_children=[]
		self._targetDataPrototypeInSr_child=ModelNone

class DataPrototypeInServiceInterfaceInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._rootDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class DataPrototypeInServiceInterfaceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to an AUTOSAR DataPrototype in the context of a ServiceInterface."""
	def __init__(self):
		super().__init__()
		self._dataPrototype_child=ModelNone
		self._elementInImplDatatype_child=ModelNone

class DataPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._rootDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class DataPrototypeInSystemRef(complexbase.GroupBase):
	"""This meta-class provides the ability to reference a DataPrototype."""
	def __init__(self):
		super().__init__()
		self._tagId_child=ModelNone

class DataPrototypeMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular VariableDataPrototypes, ParameterDataPrototypes or ArgumentDataPrototypes with non-equal shortNames, non-equal structure (specific condition is described by [constr_1187]), and/or non-equal semantic (resolution or range) in context of two different SenderReceiverInterface, NvDataInterface or ParameterInterface or Operations.

If the semantic is unequal, the following rules apply:
The textTableMapping is only applicable if the referred DataPrototypes are typed by AutosarDataType referring to CompuMethods of category TEXTTABLE, SCALE_LINEAR_AND_TEXTTABLE or BITFIELD_TEXTTABLE.

In the case that the  DataPrototypes are typed by AutosarDataType either referring to CompuMethods of category LINEAR, IDENTICAL or referring to no CompuMethod (which is similar as  IDENTICAL) the linear conversion factor is calculated out of the factorSiToUnit and offsetSiToUnit attributes of the referred Units and the CompuRationalCoeffs of a compuInternalToPhys of the referred CompuMethods."""
	def __init__(self):
		super().__init__()
		self._firstDataPrototype_child=ModelNone
		self._firstToSecondDataTransformation_child=ModelNone
		self._secondDataPrototype_child=ModelNone
		self._secondToFirstDataTransformation_child=ModelNone
		self._subElementMapping_children=[]
		self._textTableMapping_children=[]

class DataPrototypeReference(complexbase.GroupBase):
	"""This meta-class provides the ability to reference a DataPrototype."""
	def __init__(self):
		super().__init__()
		self._tagId_child=ModelNone

class DataPrototypeTransformationProps(complexbase.GroupBase):
	"""DataPrototypeTransformationProps allows to set the attributes for the different TransformationTechnologies that are DataPrototype specific."""
	def __init__(self):
		super().__init__()
		self._dataProtototypeInPortInterfaceRef_child=[]
		self._dataPrototypeInPortInterfaceRef_child=[]
		self._dataPrototypeRef_child=[]
		self._networkRepresentationProps_child=ModelNone
		self._transformationProps_child=ModelNone

class DataPrototypeWithApplicationDataTypeInSystemRef(complexbase.GroupBase):
	"""This class represents a DataPrototype that is typed by an ApplicationDataType and may be aggregated within a composite application data type (record or array)."""
	def __init__(self):
		super().__init__()
		self._dataPrototype_child=ModelNone

class DataReceiveErrorEvent(complexbase.GroupBase):
	"""This event is raised when the Com layer detects and notifies an error concerning the reception of the referenced VariableDataPrototype."""
	def __init__(self):
		super().__init__()
		self._data_child=ModelNone

class DataReceivedEvent(complexbase.GroupBase):
	"""This event is raised when the referenced data element is received."""
	def __init__(self):
		super().__init__()
		self._data_child=ModelNone

class DataSendCompletedEvent(complexbase.GroupBase):
	"""This event is raised when the referenced explicit data element has been sent or an error occurred."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class DataTransformation(complexbase.GroupBase):
	"""A DataTransformation represents a transformer chain. It is an ordered list of transformers."""
	def __init__(self):
		super().__init__()
		self._dataTransformationKind_child=ModelNone
		self._executeDespiteDataUnavailability_child=ModelNone
		self._transformerChain_children=[]
		self._variationPoint_child=ModelNone

class DataTransformationRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DataTransformation_child=ModelNone
		self._variationPoint_child=ModelNone

class DataTransformationSet(complexbase.GroupBase):
	"""This element is the system wide container of DataTransformations which represent transformer chains."""
	def __init__(self):
		super().__init__()
		self._dataTransformation_children=[]
		self._transformationTechnology_children=[]

class DataTypeMap(complexbase.GroupBase):
	"""This class represents the relationship between ApplicationDataType and its implementing AbstractImplementationDataType."""
	def __init__(self):
		super().__init__()
		self._applicationDataType_child=ModelNone
		self._implementationDataType_child=ModelNone

class DataTypeMappingSet(complexbase.GroupBase):
	"""This class represents a list of mappings between ApplicationDataTypes and ImplementationDataTypes. In addition, it can contain mappings between ImplementationDataTypes and ModeDeclarationGroups."""
	def __init__(self):
		super().__init__()
		self._dataTypeMap_children=[]
		self._modeRequestTypeMap_children=[]

class DataWriteCompletedEvent(complexbase.GroupBase):
	"""This event is raised when an implicit write access was successful or an error occurred."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class DcmIPdu(complexbase.GroupBase):
	"""Represents the IPdus handled by Dcm."""
	def __init__(self):
		super().__init__()
		self._diagPduType_child=ModelNone

class DdsCpConfig(complexbase.GroupBase):
	"""Collection of DDS definitions."""
	def __init__(self):
		super().__init__()
		self._ddsDomain_children=[]
		self._ddsQosProfile_children=[]

class DdsCpConsumedServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a consumed (required) service instance in a concrete implementation on top of DDS."""
	def __init__(self):
		super().__init__()
		self._consumedDdsOperation_children=[]
		self._consumedDdsServiceEvent_children=[]
		self._localUnicastAddress_children=[]
		self._minorVersion_child=ModelNone
		self._staticRemoteMulticastAddress_children=[]
		self._staticRemoteUnicastAddress_children=[]

class DdsCpDomain(complexbase.GroupBase):
	"""Definition of a DDS Domain."""
	def __init__(self):
		super().__init__()
		self._ddsPartition_children=[]
		self._ddsTopic_children=[]
		self._domainId_child=ModelNone

class DdsCpISignalToDdsTopicMapping(complexbase.GroupBase):
	"""Mapping of an ISignal to a DdsTopic."""
	def __init__(self):
		super().__init__()
		self._ddsTopic_child=ModelNone
		self._iSignal_child=ModelNone
		self._variationPoint_child=ModelNone

class DdsCpPartition(complexbase.GroupBase):
	"""Definition of a DDS Partition."""
	def __init__(self):
		super().__init__()
		self._partitionName_child=ModelNone

class DdsCpProvidedServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a provided service instance in a concrete implementation on top of DDS."""
	def __init__(self):
		super().__init__()
		self._localUnicastAddress_children=[]
		self._minorVersion_child=ModelNone
		self._providedDdsOperation_children=[]
		self._providedDdsServiceInstanceEvent_children=[]
		self._staticRemoteMulticastAddress_children=[]
		self._staticRemoteUnicastAddress_children=[]

class DdsCpQosProfile(complexbase.GroupBase):
	"""Definition of a DDS QOS Profile."""
	def __init__(self):
		super().__init__()
		self._deadline_child=ModelNone
		self._destinationOrder_child=ModelNone
		self._durability_child=ModelNone
		self._durabilityService_child=ModelNone
		self._history_child=ModelNone
		self._latencyBudget_child=ModelNone
		self._lifespan_child=ModelNone
		self._liveliness_child=ModelNone
		self._ownership_child=ModelNone
		self._ownershipStrength_child=ModelNone
		self._reliability_child=ModelNone
		self._resourceLimits_child=ModelNone
		self._topicData_child=ModelNone
		self._transportPriority_child=ModelNone

class DdsCpServiceInstance(complexbase.GroupBase):
	"""Provided and Consumed Dds Service Instances that are available at the ApplicationEndpoint."""
	def __init__(self):
		super().__init__()
		self._ddsFieldReplyTopic_child=ModelNone
		self._ddsFieldRequestTopic_child=ModelNone
		self._ddsMethodReplyTopic_child=ModelNone
		self._ddsMethodRequestTopic_child=ModelNone
		self._ddsServiceQosProfile_child=ModelNone
		self._serviceInstanceId_child=ModelNone
		self._serviceInterfaceId_child=ModelNone

class DdsCpServiceInstanceEvent(complexbase.GroupBase):
	"""This element represents an event as part of the Provided Service Instance."""
	def __init__(self):
		super().__init__()
		self._ddsEventQosProfile_child=ModelNone
		self._ddsEvent_child=ModelNone
		self._ddsEventTopic_child=ModelNone
		self._variationPoint_child=ModelNone

class DdsCpServiceInstanceOperation(complexbase.GroupBase):
	"""This element represents an operation as part of the Provided Service Instance."""
	def __init__(self):
		super().__init__()
		self._ddsOperationRequestTriggering_child=ModelNone
		self._ddsOperationResponseTriggering_child=ModelNone
		self._variationPoint_child=ModelNone

class DdsCpTopic(complexbase.GroupBase):
	"""Definition of a DDS Partition."""
	def __init__(self):
		super().__init__()
		self._ddsPartition_child=ModelNone
		self._topicName_child=ModelNone

class DdsDeadline(complexbase.GroupBase):
	"""Describes the DDS DEADLINE QoS policy."""
	def __init__(self):
		super().__init__()
		self._deadlinePeriod_child=ModelNone

class DdsDestinationOrder(complexbase.GroupBase):
	"""Describes the DDS DESTINATION_ORDER QoS policy."""
	def __init__(self):
		super().__init__()
		self._destinationOrderKind_child=ModelNone

class DdsDomainRange(complexbase.GroupBase):
	"""DDS Domain ID range."""
	def __init__(self):
		super().__init__()
		self._max_child=ModelNone
		self._min_child=ModelNone

class DdsDurability(complexbase.GroupBase):
	"""Describes the DDS DURABILITY QoS policy."""
	def __init__(self):
		super().__init__()
		self._durabilityKind_child=ModelNone

class DdsDurabilityService(complexbase.GroupBase):
	"""Describes the DDS DURABILITY_SERVICE QoS policy."""
	def __init__(self):
		super().__init__()
		self._durabilityServiceCleanupDelay_child=ModelNone
		self._durabilityServiceHistoryDepth_child=ModelNone
		self._durabilityServiceHistoryKind_child=ModelNone
		self._durabilityServiceMaxInstances_child=ModelNone
		self._durabilityServiceMaxSamples_child=ModelNone
		self._durabilityServiceMaxSamplesPerInstance_child=ModelNone

class DdsEventDeployment(complexbase.GroupBase):
	"""DDS configuration settings for an Event."""
	def __init__(self):
		super().__init__()
		self._eventTopicAccessRule_child=ModelNone
		self._topicName_child=ModelNone
		self._transportProtocol_children=[]

class DdsEventQosProps(complexbase.GroupBase):
	"""Configuration properties of the Event using DDS as the underlying network binding."""
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone

class DdsFieldDeployment(complexbase.GroupBase):
	"""DDS configuration settings for a Field."""
	def __init__(self):
		super().__init__()
		self._notifier_child=ModelNone

class DdsFieldQosProps(complexbase.GroupBase):
	"""Configuration properties of the Field interaction when using DDS as the underlying network binding."""
	def __init__(self):
		super().__init__()
		self._field_child=ModelNone

class DdsHistory(complexbase.GroupBase):
	"""Describes the DDS HISTORY QoS policy."""
	def __init__(self):
		super().__init__()
		self._historyKind_child=ModelNone
		self._historyOrderDepth_child=ModelNone

class DdsLatencyBudget(complexbase.GroupBase):
	"""Describes the DDS LATENCY_BUDGET QoS policy."""
	def __init__(self):
		super().__init__()
		self._latencyBudgetDuration_child=ModelNone

class DdsLifespan(complexbase.GroupBase):
	"""Describes the DDS LIFESPAN QoS policy."""
	def __init__(self):
		super().__init__()
		self._lifespanDuration_child=ModelNone

class DdsLiveliness(complexbase.GroupBase):
	"""Describes the DDS LIVELINESS QoS policy."""
	def __init__(self):
		super().__init__()
		self._livelinessLeaseDuration_child=ModelNone
		self._livenessKind_child=ModelNone

class DdsOwnership(complexbase.GroupBase):
	"""Describes the DDS OWNERSHIP QoS policy."""
	def __init__(self):
		super().__init__()
		self._ownershipKind_child=ModelNone

class DdsOwnershipStrength(complexbase.GroupBase):
	"""Describes the DDS OWNERSHIP_STRENGTH QoS policy."""
	def __init__(self):
		super().__init__()
		self._ownershipStrength_child=ModelNone

class DdsProvidedServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a provided service instance in a concrete implementation on top of DDS."""
	def __init__(self):
		super().__init__()
		self._discoveryType_child=ModelNone
		self._eventQosProps_children=[]
		self._fieldNotifierQosProps_children=[]
		self._resourceIdentifierType_child=ModelNone
		self._serviceInstanceId_child=ModelNone

class DdsQosProps(complexbase.GroupBase):
	"""QoS configuration properties for the DDS entities associated with an event, method, or field provided by or requested from a Service Instance using DDS as the underlying network binding."""
	def __init__(self):
		super().__init__()
		self._qosProfile_child=ModelNone

class DdsReliability(complexbase.GroupBase):
	"""Describes the DDS RELIABILITY QoS policy."""
	def __init__(self):
		super().__init__()
		self._reliabilityKind_child=ModelNone
		self._reliabilityMaxBlockingTime_child=ModelNone

class DdsRequiredServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a required service instance in a concrete implementation on top of DDS."""
	def __init__(self):
		super().__init__()
		self._blocklistedVersion_children=[]
		self._discoveryType_child=ModelNone
		self._eventQosProps_children=[]
		self._fieldNotifierQosProps_children=[]
		self._requiredServiceInstanceId_child=ModelNone

class DdsResourceLimits(complexbase.GroupBase):
	"""Describes the DDS RESOURCE_LIMITS QoS policy."""
	def __init__(self):
		super().__init__()
		self._maxInstances_child=ModelNone
		self._maxSamples_child=ModelNone
		self._maxSamplesPerInstance_child=ModelNone

class DdsRule(complexbase.GroupBase):
	"""Configuration of a DDS firewall rule"""
	def __init__(self):
		super().__init__()
		self._appId_child=ModelNone
		self._hostId_child=ModelNone
		self._instanceId_child=ModelNone
		self._majorProtocolVersion_child=ModelNone
		self._minorProtocolVersion_child=ModelNone
		self._productId_child=ModelNone
		self._readerEntityId_child=ModelNone
		self._submessageType_child=ModelNone
		self._vendorId_child=ModelNone
		self._writerEntityId_child=ModelNone

class DdsSecureComProps(complexbase.GroupBase):
	"""Identity and governance information of participants in case of DDS Security."""
	def __init__(self):
		super().__init__()
		self._governance_child=ModelNone
		self._identity_child=ModelNone

class DdsSecureGovernance(complexbase.GroupBase):
	"""Configuration of DDS Security for all applications joining a specific set of DDS Domains."""
	def __init__(self):
		super().__init__()
		self._allowUnauthenticatedParticipants_child=ModelNone
		self._discoveryProtectionKind_child=ModelNone
		self._domainId_children=[]
		self._enableJoinAccessControl_child=ModelNone
		self._identityCertificateAuthority_child=ModelNone
		self._livelinessProtectionKind_child=ModelNone
		self._permissionCertificateAuthority_child=ModelNone
		self._rtpsProtectionKind_child=ModelNone

class DdsServiceInstanceProps(complexbase.GroupBase):
	"""Common configuration properties for the DDS entities provided by or requested from a Service Instance using DDS as the underlying network binding."""
	def __init__(self):
		super().__init__()
		self._domainId_child=ModelNone

class DdsServiceInstanceToMachineMapping(complexbase.GroupBase):
	"""This meta-class allows to map DdsServiceInstances to a CommunicationConnector of a Machine."""
	def __init__(self):
		super().__init__()
		self._secureComPropsForDds_child=ModelNone

class DdsServiceInterfaceDeployment(complexbase.GroupBase):
	"""DDS configuration settings for a ServiceInterface."""
	def __init__(self):
		super().__init__()
		self._fieldReplyTopicName_child=ModelNone
		self._fieldRequestTopicName_child=ModelNone
		self._fieldTopicsAccessRule_child=ModelNone
		self._methodReplyTopicName_child=ModelNone
		self._methodRequestTopicName_child=ModelNone
		self._methodTopicsAccessRule_child=ModelNone
		self._serviceInterfaceId_child=ModelNone
		self._transportProtocol_children=[]

class DdsServiceVersion(complexbase.GroupBase):
	"""Definition of a Dds service version."""
	def __init__(self):
		super().__init__()
		self._majorVersion_child=ModelNone
		self._minorVersion_child=ModelNone

class DdsTopicAccessRule(complexbase.GroupBase):
	"""DDS Topic access rule definition."""
	def __init__(self):
		super().__init__()
		self._dataProtectionKind_child=ModelNone
		self._enableDiscoveryProtection_child=ModelNone
		self._enableLivelinessProtection_child=ModelNone
		self._enableReadAccessControl_child=ModelNone
		self._enableWriteAccessControl_child=ModelNone
		self._metadataProtectionKind_child=ModelNone

class DdsTopicData(complexbase.GroupBase):
	"""Describes the DDS TOPIC_DATA QoS policy."""
	def __init__(self):
		super().__init__()
		self._topicData_child=ModelNone

class DdsTransportPriority(complexbase.GroupBase):
	"""Describes the DDS TRANSPORT_PRIORITY QoS policy."""
	def __init__(self):
		super().__init__()
		self._transportPriority_child=ModelNone

class DeadlineSupervision(complexbase.GroupBase):
	"""Defines an DeadlineSupervision for one transition."""
	def __init__(self):
		super().__init__()
		self._maxDeadline_child=ModelNone
		self._minDeadline_child=ModelNone
		self._transition_child=ModelNone

class DefItem(complexbase.GroupBase):
	"""This represents an entry in a definition list. The defined item is specified using shortName and longName."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	def __init__(self):
		super().__init__()
		self._def__child=ModelNone
		self._variationPoint_child=ModelNone

class DefList(complexbase.GroupBase):
	"""This meta-class represents the ability to express a list of definitions. Note that a definition list might be rendered similar to a labeled list but has a particular semantics to denote definitions."""
	def __init__(self):
		super().__init__()
		self._defItem_children=[]
		self._variationPoint_child=ModelNone

class DefaultValueElement(complexbase.GroupBase):
	"""The default value consists of a number of elements. Each element is one byte long and the number of elements is specified by SduLength."""
	def __init__(self):
		super().__init__()
		self._elementByteValue_child=ModelNone
		self._elementPosition_child=ModelNone

class DelegatedPortAnnotation(complexbase.GroupBase):
	"""Annotation to a \"delegated port\" to specify the Signal Fan In or Signal Fan Out inside the CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._signalFan_child=ModelNone

class DelegationSwConnector(complexbase.GroupBase):
	"""A delegation connector delegates one inner PortPrototype  (a port of a component that is used inside the composition) to a outer PortPrototype of compatible type that belongs directly to the composition (a port that is owned by the composition)."""
	def __init__(self):
		super().__init__()
		self._innerPort_child=[]
		self._outerPort_child=ModelNone

class DependencyOnArtifact(complexbase.GroupBase):
	"""Dependency on the existence of another artifact, e.g. a library."""
	def __init__(self):
		super().__init__()
		self._artifactDescriptor_child=ModelNone
		self._usage_children=[]
		self._variationPoint_child=ModelNone

class Describable(complexbase.GroupBase):
	"""This meta-class represents the ability to add a descriptive documentation to non identifiable elements."""
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone
		self._category_child=ModelNone
		self._introduction_child=ModelNone
		self._adminData_child=ModelNone

class DevelopmentError(complexbase.GroupBase):
	"""The reported failure is classified as development error."""

class DhcpServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of DHCP servers that are running on the network endpoint. It is possible that an Ipv4DhcpServer and an Ipv6DhcpServer run on the same Ecu."""
	def __init__(self):
		super().__init__()
		self._ipv4DhcpServerConfiguration_child=ModelNone
		self._ipv6DhcpServerConfiguration_child=ModelNone

class Dhcpv6Props(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for DHCPv6."""
	def __init__(self):
		super().__init__()
		self._tcpIpDhcpV6CnfDelayMax_child=ModelNone
		self._tcpIpDhcpV6CnfDelayMin_child=ModelNone
		self._tcpIpDhcpV6InfDelayMax_child=ModelNone
		self._tcpIpDhcpV6InfDelayMin_child=ModelNone
		self._tcpIpDhcpV6SolDelayMax_child=ModelNone
		self._tcpIpDhcpV6SolDelayMin_child=ModelNone

class DiagEventDebounceAlgorithm(complexbase.GroupBase):
	"""This class represents the ability to specify the pre-debounce algorithm which is selected and/or required by the particular monitor.

This class inherits from Identifiable in order to allow further documentation of the expected or implemented debouncing and to use the category for the identification of the expected / implemented debouncing."""

class DiagEventDebounceCounterBased(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that the counter-based debounce algorithm shall be used by the DEM for this diagnostic monitor.

This is related to set the ECUC choice container DemDebounceAlgorithmClass to DemDebounceCounterBased."""
	def __init__(self):
		super().__init__()
		self._counterBasedFdcThresholdStorageValue_child=ModelNone
		self._counterDecrementStepSize_child=ModelNone
		self._counterFailedThreshold_child=ModelNone
		self._counterIncrementStepSize_child=ModelNone
		self._counterJumpDown_child=ModelNone
		self._counterJumpDownValue_child=ModelNone
		self._counterJumpUp_child=ModelNone
		self._counterJumpUpValue_child=ModelNone
		self._counterPassedThreshold_child=ModelNone

class DiagEventDebounceMonitorInternal(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that no Dem pre-debounce algorithm shall be used for this diagnostic monitor. The SWC might implement an internal debouncing algorithm and report qualified (debounced) results to the Dem/DM."""

class DiagEventDebounceTimeBased(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that the time-based pre-debounce algorithm shall be used by the Dem for this diagnostic monitor.

This is related to set the EcuC choice container DemDebounceAlgorithmClass to DemDebounceTimeBase."""
	def __init__(self):
		super().__init__()
		self._timeBasedFdcThresholdStorageValue_child=ModelNone
		self._timeFailedThreshold_child=ModelNone
		self._timePassedThreshold_child=ModelNone

class DiagnosticAbstractAliasEvent(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for all diagnostic alias events."""

class DiagnosticAbstractDataIdentifier(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for the modeling of a diagnostic data identifier (DID)."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone

class DiagnosticAbstractDataIdentifierInterface(complexbase.GroupBase):
	"""This meta-class serves as the abstract base class of PortInterfaces dedicated to the access of diagnostic data identifiers on the AUTOSAR adaptive platform."""

class DiagnosticAbstractParameter(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for modeling a diagnostic parameter."""
	def __init__(self):
		super().__init__()
		self._bitOffset_child=ModelNone
		self._dataElement_children=[]
		self._parameterSize_child=ModelNone

class DiagnosticAbstractRoutineInterface(complexbase.GroupBase):
	"""This meta-class serves as the abstract base class of PortInterfaces dedicated to routine execution on the AUTOSAR adaptive platform."""

class DiagnosticAccessPermission(complexbase.GroupBase):
	"""This represents the specification of whether a given service can be accessed according to the existence of meta-classes referenced by a particular DiagnosticAccessPermission.

In other words, this meta-class acts as a mapping element between several (otherwise unrelated) pieces of information that are put into context for the purpose of checking for access rights."""
	def __init__(self):
		super().__init__()
		self._authenticationEnabled_child=ModelNone
		self._authenticationRole_children=[]
		self._diagnosticSession_children=[]
		self._environmentalCondition_child=ModelNone
		self._securityLevel_children=[]
		self._sovdLock_child=ModelNone

class DiagnosticAging(complexbase.GroupBase):
	"""Defines the aging algorithm."""
	def __init__(self):
		super().__init__()
		self._agingCycle_children=[]
		self._threshold_child=ModelNone

class DiagnosticAuthRole(complexbase.GroupBase):
	"""This meta-class represents the ability to specify an authentication role that can be used to deliver fine-grained access rights."""
	def __init__(self):
		super().__init__()
		self._bitPosition_child=ModelNone
		self._isDefault_child=ModelNone

class DiagnosticAuthRoleProxy(complexbase.GroupBase):
	"""This meta-class indicates that an authentication is generally foreseen. The question whether the authentication is done in general or whether it is done role-specific depends on the existence of references to DiagAuthRole."""
	def __init__(self):
		super().__init__()
		self._authenticationRole_children=[]

class DiagnosticAuthTransmitCertificate(complexbase.GroupBase):
	"""This meta-class represents the sub-function to transmit a certificate"""
	def __init__(self):
		super().__init__()
		self._certificateEvaluation_children=[]

class DiagnosticAuthTransmitCertificateEvaluation(complexbase.GroupBase):
	"""This meta-class represents the ability to configure a certificate evaluation in the context of a diagnostic authentication."""
	def __init__(self):
		super().__init__()
		self._evaluationId_child=ModelNone
		self._function_child=ModelNone

class DiagnosticAuthTransmitCertificateMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a CryptoServiceCertificate with a DiagnosticAuthCertificateEvaluation with the purpose to configure the evaluation of the certificate."""
	def __init__(self):
		super().__init__()
		self._cryptoServiceCertificate_children=[]
		self._serviceInstance_child=ModelNone

class DiagnosticAuthentication(complexbase.GroupBase):
	"""This meta-class represents the ability to configure the usage of the UDS service Authentication in the Diagnostic extract."""
	def __init__(self):
		super().__init__()
		self._authenticationClass_child=ModelNone
		self._authenticationTimeout_child=ModelNone

class DiagnosticAuthenticationClass(complexbase.GroupBase):
	"""This meta-class contains configuration shared by all instances of the Authentication diagnostic service."""

class DiagnosticAuthenticationConfiguration(complexbase.GroupBase):
	"""This meta-class represents the subfunction to configure the authentication."""

class DiagnosticAuthenticationInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a focused PortInterface for handling the diagnostic service \"authentication\" on the adaptive platform."""

class DiagnosticAuthenticationPortMapping(complexbase.GroupBase):
	"""This mapping class identifies the PortPrototype in the application software that handles the client authentication."""
	def __init__(self):
		super().__init__()
		self._diagnosticAuthentication_child=ModelNone
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticCapabilityElement(complexbase.GroupBase):
	"""This class identifies the capability to provide generic information about diagnostic capabilities"""
	def __init__(self):
		super().__init__()
		self._audience_children=[]
		self._diagRequirement_child=ModelNone
		self._securityAccessLevel_child=ModelNone

class DiagnosticClearCondition(complexbase.GroupBase):
	"""This meta-class describes a clear condition for diagnostic purposes."""

class DiagnosticClearConditionGroup(complexbase.GroupBase):
	"""Clear condition group which includes one or several clear conditions."""
	def __init__(self):
		super().__init__()
		self._clearCondition_children=[]

class DiagnosticClearConditionNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to set a clear condition."""

class DiagnosticClearConditionPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticClearCondition is mapped."""
	def __init__(self):
		super().__init__()
		self._clearCondition_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticClearDiagnosticInformation(complexbase.GroupBase):
	"""This represents an instance of the \"Clear Diagnostic Information\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._clearDiagnosticInformationClass_child=ModelNone

class DiagnosticClearDiagnosticInformationClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Clear Diagnostic Information\" diagnostic service."""

class DiagnosticClearResetEmissionRelatedInfo(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x04 service."""
	def __init__(self):
		super().__init__()
		self._clearResetEmissionRelatedDiagnosticInfoClass_child=ModelNone

class DiagnosticClearResetEmissionRelatedInfoClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Clear Reset Emission Related Data\" OBD diagnostic service."""

class DiagnosticComControl(complexbase.GroupBase):
	"""This represents an instance of the \"Communication Control\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._comControlClass_child=ModelNone
		self._customSubFunctionNumber_child=ModelNone

class DiagnosticComControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Communication Control\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._allChannels_children=[]
		self._allPhysicalChannels_children=[]
		self._specificChannel_children=[]
		self._subNodeChannel_children=[]

class DiagnosticComControlInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a focused PortInterface for handling the diagnostic service communication control on the adaptive platform."""

class DiagnosticComControlSpecificChannel(complexbase.GroupBase):
	"""This represents the ability to add further attributes to the definition of a specific channel that is subject to the diagnostic service \"communication control\"."""
	def __init__(self):
		super().__init__()
		self._specificChannel_child=ModelNone
		self._specificPhysicalChannel_child=ModelNone
		self._subnetNumber_child=ModelNone

class DiagnosticComControlSubNodeChannel(complexbase.GroupBase):
	"""This represents the ability to add further attributes to the definition of a specific sub-node channel that is subject to the diagnostic service \"communication control\"."""
	def __init__(self):
		super().__init__()
		self._subNodeChannel_child=ModelNone
		self._subNodeNumber_child=ModelNone
		self._subNodePhysicalChannel_child=ModelNone

class DiagnosticCommonElement(complexbase.GroupBase):
	"""This meta-class represents a common base class for all diagnostic elements. It does not contribute any specific functionality other than the ability to become the target of a reference."""

class DiagnosticCommonElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticCommonElement_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticCommonProps(complexbase.GroupBase):
	"""This meta-class aggregates a number of common properties that are shared among a diagnostic extract."""
	def __init__(self):
		super().__init__()
		self._diagnosticCommonPropsVariant_children=[]
		self._agingRequiresTestedCycle_child=ModelNone
		self._authenticationTimeout_child=ModelNone
		self._clearDtcLimitation_child=ModelNone
		self._debounceAlgorithmProps_children=[]
		self._defaultEndianness_child=ModelNone
		self._diagnosticAddress_children=[]
		self._dtcStatusAvailabilityMask_child=ModelNone
		self._environmentDataCapture_child=ModelNone
		self._eventCombinationReportingBehavior_child=ModelNone
		self._eventDisplacementStrategy_child=ModelNone
		self._externalAuthentication_children=[]
		self._maxNumberOfEventEntries_child=ModelNone
		self._maxNumberOfRequestCorrectlyReceivedResponsePending_child=ModelNone
		self._memoryEntryStorageTrigger_child=ModelNone
		self._occurrenceCounterProcessing_child=ModelNone
		self._resetConfirmedBitOnOverflow_child=ModelNone
		self._resetPendingBitOnOverflow_child=ModelNone
		self._responseOnAllRequestSids_child=ModelNone
		self._responseOnSecondDeclinedRequest_child=ModelNone
		self._securityDelayTimeOnBoot_child=ModelNone
		self._statusBitHandlingTestFailedSinceLastClear_child=ModelNone
		self._statusBitStorageTestFailed_child=ModelNone
		self._typeOfDtcSupported_child=ModelNone
		self._typeOfEventCombinationSupported_child=ModelNone
		self._typeOfFreezeFrameRecordNumeration_child=ModelNone

class DiagnosticCommonPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class DiagnosticCommonPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticCommunicationManagerNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (Dcm) which are not related to a particular item (e.g. a PID or DiagnosticRoutineNeeds). The main use case is the mapping of service ports to the Dcm which are not related to a particular item."""
	def __init__(self):
		super().__init__()
		self._serviceRequestCallbackType_child=ModelNone

class DiagnosticComponentNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to specify the service needs for the configuration of component events."""

class DiagnosticCondition(complexbase.GroupBase):
	"""Abstract element for StorageConditions and EnableConditions."""
	def __init__(self):
		super().__init__()
		self._initValue_child=ModelNone

class DiagnosticConditionGroup(complexbase.GroupBase):
	"""Abstract element for StorageConditionGroups and EnableConditionGroups."""

class DiagnosticConditionInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to process requests for diagnostic conditions on the adaptive platform."""

class DiagnosticConnectedIndicator(complexbase.GroupBase):
	"""Description of indicators that are defined per DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone
		self._healingCycleCounterThreshold_child=ModelNone
		self._healingCycle_child=ModelNone
		self._indicatorFailureCycleCounterThreshold_child=ModelNone
		self._indicator_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticConnection(complexbase.GroupBase):
	"""DiagnosticConncection that is used to describe the relationship between several TP connections."""
	def __init__(self):
		super().__init__()
		self._functionalRequest_children=[]
		self._periodicResponseUudt_children=[]
		self._physicalRequest_child=ModelNone
		self._responseOnEvent_child=ModelNone
		self._response_child=ModelNone

class DiagnosticConnectionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticConnection_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticContributionSet(complexbase.GroupBase):
	"""This meta-class represents a root node of a diagnostic extract. It bundles a given set of diagnostic model elements. The granularity of the DiagonsticContributionSet is arbitrary in order to support the aspect of decentralized configuration, i.e. different contributors can come up with an own DiagnosticContributionSet."""
	def __init__(self):
		super().__init__()
		self._commonProperties_child=ModelNone
		self._ecuInstance_children=[]
		self._element_children=[]
		self._serviceTable_children=[]

class DiagnosticControlDTCSetting(complexbase.GroupBase):
	"""This represents an instance of the \"Control DTC Setting\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._dtcSettingClass_child=ModelNone
		self._dtcSettingParameter_child=ModelNone

class DiagnosticControlDTCSettingClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Control DTC Setting\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._controlOptionRecordPresent_child=ModelNone

class DiagnosticControlEnableMaskBit(complexbase.GroupBase):
	"""This meta-class has the ability to represent one bit in the control enable mask record."""
	def __init__(self):
		super().__init__()
		self._bitNumber_child=ModelNone
		self._controlledDataElement_children=[]

class DiagnosticControlNeeds(complexbase.GroupBase):
	"""This meta-class indicates a service use-case for reporting the controlled status by diagnostic services."""

class DiagnosticCustomServiceClass(complexbase.GroupBase):
	"""This represents the ability to define a custom diagnostic service class and assign an ID to it. Further configuration is not foreseen from the point of view of the diagnostic extract and consequently needs to be done on the level of ECUC."""
	def __init__(self):
		super().__init__()
		self._customServiceId_child=ModelNone

class DiagnosticCustomServiceInstance(complexbase.GroupBase):
	"""This meta-class has the ability to define an instance of a custom diagnostic service."""
	def __init__(self):
		super().__init__()
		self._customServiceClass_child=ModelNone

class DiagnosticDataByIdentifier(complexbase.GroupBase):
	"""This represents an abstract base class for all diagnostic services that access data by identifier."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone

class DiagnosticDataChangeTrigger(complexbase.GroupBase):
	"""This represents the ability to define a trigger based on the change of a given DiagnosticDataIdentifier."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone

class DiagnosticDataElement(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a concrete piece of data to be taken into account for diagnostic purposes."""
	def __init__(self):
		super().__init__()
		self._arraySizeSemantics_child=ModelNone
		self._maxNumberOfElements_child=ModelNone
		self._scalingInfoSize_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticDataElementInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a element-of-DID-focused PortInterface for diagnostics on the adaptive platform."""
	def __init__(self):
		super().__init__()
		self._read_child=ModelNone

class DiagnosticDataIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to model a diagnostic data identifier (DID) that is fully specified regarding the payload at configuration-time."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._didSize_child=ModelNone
		self._representsVin_child=ModelNone
		self._supportInfoByte_child=ModelNone

class DiagnosticDataIdentifierGenericInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a generic DID-focused PortInterface for diagnostics on the adaptive platform."""

class DiagnosticDataIdentifierInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a DID-focused PortInterface for diagnostics on the adaptive platform."""
	def __init__(self):
		super().__init__()
		self._read_child=ModelNone
		self._write_child=ModelNone

class DiagnosticDataIdentifierSet(complexbase.GroupBase):
	"""This represents the ability to define a list of DiagnosticDataIdentifiers that can be reused in different contexts."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_children=[]

class DiagnosticDataIdentifierSetRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticDataIdentifierSet_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticDataPortMapping(complexbase.GroupBase):
	"""This meta-class provides the ability to define a diagnostic access to an entire DID."""
	def __init__(self):
		super().__init__()
		self._diagnosticDataElement_child=ModelNone
		self._diagnosticDataIdentifier_child=ModelNone
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticDataTransfer(complexbase.GroupBase):
	"""This represents an instance of the \"Data Transfer\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._dataTransferClass_child=ModelNone

class DiagnosticDataTransferClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Data Transfer\" diagnostic service."""

class DiagnosticDeAuthentication(complexbase.GroupBase):
	"""This meta-class represents the subfunction to remove the authentication"""

class DiagnosticDebounceAlgorithmProps(complexbase.GroupBase):
	"""Defines properties for the debounce algorithm class."""
	def __init__(self):
		super().__init__()
		self._debounceAlgorithm_child=[]
		self._debounceBehavior_child=ModelNone
		self._debounceCounterStorage_child=ModelNone

class DiagnosticDebounceBehaviorEnumValueVariationPoint(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticDemProvidedDataMapping(complexbase.GroupBase):
	"""This represents the ability to define the nature of a data access for a DiagnosticDataElement in the Dem."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._dataProvider_child=ModelNone

class DiagnosticDoIPActivationLineInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to implement the DoIPActivationLine on the adaptive platform."""

class DiagnosticDoIPEntityIdentificationInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to implement the DoIP Entity Identification on the adaptive platform."""

class DiagnosticDoIPGroupIdentificationInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to implement the DoIP Group Identification on the adaptive platform."""

class DiagnosticDoIPPowerModeInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to implement the DoIP Power Mode on the adaptive platform."""

class DiagnosticDoIPTriggerVehicleAnnouncementInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to implement the DoIPTriggerVehicleAnnouncement on the adaptive platform."""

class DiagnosticDownloadInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to process requests for downloading data using diagnostic channels on the adaptive platform."""

class DiagnosticDtcChangeTrigger(complexbase.GroupBase):
	"""This represents the ability to define a trigger that executes on the change of any DiagnosticTroubleCode."""
	def __init__(self):
		super().__init__()
		self._dtcStatusMask_child=ModelNone

class DiagnosticDTCInformationInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to access the properties of DTCs  on the adaptive platform."""

class DiagnosticDynamicDataIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic data identifier (DID) at run-time."""

class DiagnosticDynamicallyDefineDataIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Dynamically Define Data Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone
		self._dynamicallyDefineDataIdentifierClass_child=ModelNone
		self._maxSourceElement_child=ModelNone

class DiagnosticDynamicallyDefineDataIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Dynamically Define Data Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._checkPerSourceId_child=ModelNone
		self._configurationHandling_child=ModelNone
		self._subfunction_children=[]

class DiagnosticEcuInstanceProps(complexbase.GroupBase):
	"""This meta-class represents the ability to model properties that are specific for a given EcuInstance but on the other hand represent purely diagnostic-related information. 

In the spirit of decentralized configuration it is therefore possible to specify the diagnostic-related information related to a given EcuInstance even if the EcuInstance does not yet exist."""
	def __init__(self):
		super().__init__()
		self._dtcStatusAvailabilityMask_child=ModelNone
		self._ecuInstance_children=[]
		self._obdSupport_child=ModelNone
		self._sendRespPendOnTransToBoot_child=ModelNone

class DiagnosticEcuProps(complexbase.GroupBase):
	"""This meta-class is defined to gather diagnostic-related properties that apply in the scope of an entire ECU."""
	def __init__(self):
		super().__init__()
		self._isObdRelevant_child=ModelNone
		self._sendRespPendOnTransToBoot_child=ModelNone

class DiagnosticEcuReset(complexbase.GroupBase):
	"""This represents an instance of the \"ECU Reset\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._customSubFunctionNumber_child=ModelNone
		self._ecuResetClass_child=ModelNone
		self._respondToReset_child=ModelNone

class DiagnosticEcuResetClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Ecu Reset\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._respondToReset_child=ModelNone

class DiagnosticEcuResetInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a focused PortInterface for handling the diagnostic service EcuReset on the adaptive platform."""

class DiagnosticEnableCondition(complexbase.GroupBase):
	"""Specification of an enable condition."""

class DiagnosticEnableConditionGroup(complexbase.GroupBase):
	"""Enable condition group which includes one or several enable conditions."""
	def __init__(self):
		super().__init__()
		self._enableCondition_children=[]

class DiagnosticEnableConditionNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to set an enable condition."""
	def __init__(self):
		super().__init__()
		self._initialStatus_child=ModelNone

class DiagnosticEnableConditionPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticEnableCondition is mapped."""
	def __init__(self):
		super().__init__()
		self._enableCondition_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependencyInSystem_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticEnableConditionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticEnableCondition_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticEnvBswModeElement(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to a specific ModeDeclaration in the scope of a BswModuleDescription."""
	def __init__(self):
		super().__init__()
		self._mode_child=ModelNone

class DiagnosticEnvCompareCondition(complexbase.GroupBase):
	"""DiagnosticCompareConditions are atomic conditions. They are based on the idea of a comparison at runtime of some variable data with something constant. The type of the comparison (==, !=, <, <=, ...) is specified in  DiagnosticCompareCondition.compareType."""
	def __init__(self):
		super().__init__()
		self._compareType_child=ModelNone

class DiagnosticEnvConditionFormula(complexbase.GroupBase):
	"""A DiagnosticEnvConditionFormula embodies the computation instruction that is to be evaluated at runtime to determine if the DiagnosticEnvironmentalCondition is currently present (i.e. the formula is evaluated to true) or not (otherwise). The formula itself consists of parts which are combined by the logical operations specified by DiagnosticEnvConditionFormula.op.

If a diagnostic functionality cannot be executed because an environmental condition fails then the diagnostic stack shall send a negative response code (NRC) back to the client. The value of the NRC is directly related to the specific formula and is therefore formalized in the attribute DiagnosticEnvConditionFormula.nrcValue."""
	def __init__(self):
		super().__init__()
		self._nrcValue_child=ModelNone
		self._op_child=ModelNone
		self._part_children=[]

class DiagnosticEnvConditionFormulaPart(complexbase.GroupBase):
	"""A DiagnosticEnvConditionFormulaPart can either be a atomic condition, e.g. a DiagnosticEnvCompareCondition, or a DiagnosticEnvConditionFormula, again, which allows arbitrary nesting."""

class DiagnosticEnvDataCondition(complexbase.GroupBase):
	"""A DiagnosticEnvDataCondition is an atomic condition that compares the current value of the referenced DiagnosticDataElement with a constant value defined by the ValueSpecification. All compareTypes are supported."""
	def __init__(self):
		super().__init__()
		self._compareValue_child=[]
		self._dataElement_child=ModelNone

class DiagnosticEnvDataElementCondition(complexbase.GroupBase):
	"""This meta-class represents the ability to formulate a diagnostic environment condition based on the value of a data element owned by the application software."""
	def __init__(self):
		super().__init__()
		self._compareValue_child=[]
		self._dataPrototype_child=ModelNone
		self._pPortPrototype_child=ModelNone
		self._process_child=ModelNone
		self._swDataDefProps_child=ModelNone

class DiagnosticEnvModeCondition(complexbase.GroupBase):
	"""DiagnosticEnvModeCondition are atomic condition based on the comparison of the active ModeDeclaration in a ModeDeclarationGroupProtoype with the constant value of a ModeDeclaration.

The formulation of this condition uses only one DiagnosticEnvElement, which contains enough information to deduce the variable part (i.e. the part that changes at runtime) as well as the constant part of the comparison.

Only DiagnosticCompareTypeEnum.isEqual or DiagnosticCompareTypeEnum.isNotEqual are eligible values for DiagnosticAtomicCondition.compareType."""
	def __init__(self):
		super().__init__()
		self._modeElement_child=ModelNone

class DiagnosticEnvModeElement(complexbase.GroupBase):
	"""All ModeDeclarations that are referenced in a DiagnosticEnvModeCondition shall be defined as a DiagnosticEnvModeElement of this DiagnosticEnvironmentalCondition.

This concept keeps the ARXML clean: It avoids that the DiagnosticEnvConditionFormula is cluttered by lengthy InstanceRef definitions. 

Furthermore, it allows that an InstanceRef only needs to be defined once and can be used multiple times in the different DiagnosticEnvModeConditions."""

class DiagnosticEnvSwcModeElement(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to a ModeDeclaration in a concrete System context."""
	def __init__(self):
		super().__init__()
		self._mode_child=ModelNone

class DiagnosticEnvironmentalCondition(complexbase.GroupBase):
	"""The meta-class DiagnosticEnvironmentalCondition formalizes the idea of a condition which is evaluated during runtime of the ECU by looking at \"environmental\" states (e.g. one such condition is that the vehicle is not driving, i.e. vehicle speed == 0)."""
	def __init__(self):
		super().__init__()
		self._formula_child=ModelNone
		self._modeElement_children=[]

class DiagnosticEvent(complexbase.GroupBase):
	"""This element is used to configure DiagnosticEvents."""
	def __init__(self):
		super().__init__()
		self._agingAllowed_child=ModelNone
		self._associatedEventIdentification_child=ModelNone
		self._clearEventAllowedBehavior_child=ModelNone
		self._clearEventBehavior_child=ModelNone
		self._confirmationThreshold_child=ModelNone
		self._connectedIndicator_children=[]
		self._eventClearAllowed_child=ModelNone
		self._eventFailureCycleCounterThreshold_child=ModelNone
		self._eventKind_child=ModelNone
		self._prestorageFreezeFrame_child=ModelNone
		self._prestoredFreezeframeStoredInNvm_child=ModelNone
		self._recoverableInSameOperationCycle_child=ModelNone

class DiagnosticEventInfoNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component interested to get information regarding specific DTCs."""
	def __init__(self):
		super().__init__()
		self._dtcKind_child=ModelNone
		self._dtcNumber_child=ModelNone
		self._obdDtcNumber_child=ModelNone
		self._udsDtcNumber_child=ModelNone

class DiagnosticEventInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to access the properties of diagnostic events  on the adaptive platform."""

class DiagnosticEventManagerNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Event Manager (Dem) which are not related to a particular item."""

class DiagnosticEventNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Diagnostic Event Manager for one diagnostic event. Its shortName can be regarded as a symbol identifying the diagnostic event from the viewpoint of the component or module which owns this element.

In case the diagnostic event specifies a production error, the shortName shall be the name of the production error."""
	def __init__(self):
		super().__init__()
		self._considerPtoStatus_child=ModelNone
		self._deferringFid_children=[]
		self._diagEventDebounceAlgorithm_child=[]
		self._dtcKind_child=ModelNone
		self._dtcNumber_child=ModelNone
		self._inhibitingFid_child=ModelNone
		self._inhibitingSecondaryFid_children=[]
		self._obdDtcNumber_child=ModelNone
		self._prestoredFreezeframeStoredInNvm_child=ModelNone
		self._reportBehavior_child=ModelNone
		self._udsDtcNumber_child=ModelNone
		self._usesMonitorData_child=ModelNone

class DiagnosticEventPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticEvent is mapped."""
	def __init__(self):
		super().__init__()
		self._bswServiceDependency_child=ModelNone
		self._diagnosticEvent_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependencyInSystem_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticEventRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticEventToDebounceAlgorithmMapping(complexbase.GroupBase):
	"""Defines which Debounce Algorithm is applicable for a DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._debounceAlgorithm_child=ModelNone
		self._diagnosticEvent_child=ModelNone

class DiagnosticEventToEnableConditionGroupMapping(complexbase.GroupBase):
	"""Defines which EnableConditionGroup is applicable for a DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._enableConditionGroup_child=ModelNone

class DiagnosticEventToOperationCycleMapping(complexbase.GroupBase):
	"""Defines which OperationCycle is applicable for a DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._operationCycle_child=ModelNone

class DiagnosticEventToSecurityEventMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a security event that is defined in the context of the Security Extract to a diagnostic event defined on the context of the DiagnosticExtract."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._securityEventProps_child=ModelNone

class DiagnosticEventToStorageConditionGroupMapping(complexbase.GroupBase):
	"""Defines which StorageConditionGroup is applicable for a DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._storageConditionGroup_child=ModelNone

class DiagnosticEventToTroubleCodeJ1939Mapping(complexbase.GroupBase):
	"""By means of this meta-class it is possible to associate a DiagnosticEvent to a DiagnosticTroubleCodeJ1939."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._troubleCodeJ1939_child=ModelNone

class DiagnosticEventToTroubleCodeUdsMapping(complexbase.GroupBase):
	"""Defines which UDS Diagnostic Trouble Code is applicable for a DiagnosticEvent."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._troubleCodeUds_child=ModelNone

class DiagnosticEventWindow(complexbase.GroupBase):
	"""This represents the ability to define the characteristics of the applicable event window"""
	def __init__(self):
		super().__init__()
		self._eventWindowTime_child=ModelNone
		self._storageStateEvaluation_child=ModelNone

class DiagnosticExtendedDataRecord(complexbase.GroupBase):
	"""Description of an extended data record."""
	def __init__(self):
		super().__init__()
		self._customTrigger_child=ModelNone
		self._recordElement_children=[]
		self._recordNumber_child=ModelNone
		self._trigger_child=ModelNone
		self._update_child=ModelNone

class DiagnosticExtendedDataRecordRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticExtendedDataRecord_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticExternalAuthenticationIdentification(complexbase.GroupBase):
	"""This meta-class represents the ability to support the authentication of diagnostic clients."""
	def __init__(self):
		super().__init__()
		self._sourceAddressCode_child=ModelNone
		self._sourceAddressMask_child=ModelNone

class DiagnosticExternalAuthenticationInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a focused PortInterface for handling the diagnostic client authentication (i.e. convey the Authentication state to the Diagnostic Server instance of the DM) on the adaptive platform."""

class DiagnosticExternalAuthenticationPortMapping(complexbase.GroupBase):
	"""This mapping class identifies the PortPrototype in the application software that handles the external authentication."""
	def __init__(self):
		super().__init__()
		self._diagnosticAuthentication_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticFimAliasEvent(complexbase.GroupBase):
	"""This meta-class is used to represent a given event semantics.
However, the name of the actual events used in a specific project is sometimes not
defined yet, not known or not in the responsibility of the author. Therefore, the
DiagnosticFimAliasEvent has a reference to the actual DiagnosticEvent and by this the
final connection is created."""

class DiagnosticFimAliasEventGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to define an alias for a Fim summarized event. This alias can be used in early phases of the configuration process until a further refinement is possible."""
	def __init__(self):
		super().__init__()
		self._groupedAliasEvent_children=[]

class DiagnosticFimAliasEventGroupMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a DiagnosticFimEventGroup to a DiagnosticFimAliasEventGroup. By this means the \"preliminary\" modeling by way of a DiagnosticFimAliasEventGroup is further substantiated."""
	def __init__(self):
		super().__init__()
		self._actualEvent_child=ModelNone
		self._aliasEvent_child=ModelNone

class DiagnosticFimAliasEventMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to model the mapping of a DiagnosticEvent to a DiagnosticAliasEvent. By this means the \"preliminary\" modeling by way of a DiagnosticAliasEvent is further substantiated."""
	def __init__(self):
		super().__init__()
		self._actualEvent_child=ModelNone
		self._aliasEvent_child=ModelNone

class DiagnosticFimEventGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to model a Fim event group, also known as a summary event in Fim terminology. This represents a group of single diagnostic events."""
	def __init__(self):
		super().__init__()
		self._event_children=[]

class DiagnosticFimFunctionMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a function identifier (FID) and the corresponding SwcServiceDependency in the application software resp.  basic software."""
	def __init__(self):
		super().__init__()
		self._mappedBswServiceDependency_child=ModelNone
		self._mappedFlatSwcServiceDependency_child=ModelNone
		self._mappedFunction_child=ModelNone
		self._mappedSwcServiceDependency_child=ModelNone

class DiagnosticFreezeFrame(complexbase.GroupBase):
	"""This element describes combinations of DIDs for a non OBD relevant freeze frame."""
	def __init__(self):
		super().__init__()
		self._customTrigger_child=ModelNone
		self._recordNumber_child=ModelNone
		self._trigger_child=ModelNone
		self._update_child=ModelNone

class DiagnosticFreezeFrameRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticFreezeFrame_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticFunctionIdentifier(complexbase.GroupBase):
	"""This meta-class represents a diagnostic function identifier (a.k.a. FID)."""

class DiagnosticFunctionIdentifierInhibit(complexbase.GroupBase):
	"""This meta-class represents the ability to define the inhibition of a specific function identifier within the Fim configuration."""
	def __init__(self):
		super().__init__()
		self._functionIdentifier_child=ModelNone
		self._inhibitSource_children=[]
		self._inhibitionMask_child=ModelNone

class DiagnosticFunctionInhibitSource(complexbase.GroupBase):
	"""This meta-class represents the ability to define an inhibition source in the context of the Fim configuration."""
	def __init__(self):
		super().__init__()
		self._eventGroup_child=ModelNone
		self._event_child=ModelNone

class DiagnosticGenericUdsInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a generic UDS PortInterface for diagnostics on the adaptive platform."""

class DiagnosticGenericUdsNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to process a generic UDS service."""

class DiagnosticIndicator(complexbase.GroupBase):
	"""Definition of an indicator."""
	def __init__(self):
		super().__init__()
		self._healingCycleCounterThreshold_child=ModelNone
		self._type_child=ModelNone

class DiagnosticIndicatorInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to implement indicator functionality on the adaptive platform."""

class DiagnosticIndicatorNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to implement an indicator."""

class DiagnosticIndicatorPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticIndicator is mapped."""
	def __init__(self):
		super().__init__()
		self._indicator_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticIndicatorTypeEnumValueVariationPoint(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticInfoType(complexbase.GroupBase):
	"""This meta-class represents the ability to model an OBD info type."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._id_child=ModelNone

class DiagnosticInhibitSourceEventMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a DiagnosticFunctionInhibitSource directly to alternatively one DiagnosticEvent or one DiagnosticFimSummaryEvent. This model element shall be used if the approach via the alias events is not applicable, i.e. when diagnostic events defined by the Dem are already available at the time the Fim configuration within the diagnostic extract is created."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._eventGroup_child=ModelNone
		self._inhibitionSource_child=ModelNone

class DiagnosticIOControl(complexbase.GroupBase):
	"""This represents an instance of the \"I/O Control\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._controlEnableMaskBit_children=[]
		self._dataIdentifier_child=ModelNone
		self._freezeCurrentState_child=ModelNone
		self._ioControlClass_child=ModelNone
		self._resetToDefault_child=ModelNone
		self._shortTermAdjustment_child=ModelNone

class DiagnosticIoControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"IO Control\" diagnostic service."""

class DiagnosticIoControlNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (DCM) which are not related to a particular item (e.g. a PID). The main use case is the mapping of service ports to the Dcm which are not related to a particular item."""
	def __init__(self):
		super().__init__()
		self._currentValue_child=ModelNone
		self._didNumber_child=ModelNone
		self._freezeCurrentStateSupported_child=ModelNone
		self._resetToDefaultSupported_child=ModelNone
		self._shortTermAdjustmentSupported_child=ModelNone

class DiagnosticIumpr(complexbase.GroupBase):
	"""This meta-class represents the ability to model the in-use monitor performance ratio. The latter computes to the number of times a fault could have been found divided by the number of times the vehicle conditions have been properly fulfilled."""
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone
		self._ratioKind_child=ModelNone

class DiagnosticIumprDenominatorGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to model a IUMPR denominator groups."""
	def __init__(self):
		super().__init__()
		self._iumpr_children=[]

class DiagnosticIumprGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to model a IUMPR groups."""
	def __init__(self):
		super().__init__()
		self._groupIdentifier_child=ModelNone
		self._iumprGroupIdentifier_children=[]
		self._iumpr_children=[]

class DiagnosticIumprGroupIdentifier(complexbase.GroupBase):
	"""This meta-class provides the ability to the define the group identifier for an IumprGroup."""
	def __init__(self):
		super().__init__()
		self._groupId_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticIumprToFunctionIdentifierMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a DiagnosticFunctionIdentifier with a DiagnosticIumpr."""
	def __init__(self):
		super().__init__()
		self._functionIdentifier_child=ModelNone
		self._iumpr_child=ModelNone

class DiagnosticJ1939ExpandedFreezeFrame(complexbase.GroupBase):
	"""This meta-class represents the ability to model an expanded J1939 Freeze Frame."""
	def __init__(self):
		super().__init__()
		self._node_child=ModelNone
		self._spn_children=[]

class DiagnosticJ1939FreezeFrame(complexbase.GroupBase):
	"""This meta-class represents the ability to model a J1939 Freeze Frame."""
	def __init__(self):
		super().__init__()
		self._node_child=ModelNone
		self._spn_children=[]

class DiagnosticJ1939Node(complexbase.GroupBase):
	"""This meta-class represents the diagnostic configuration of a J1939 Nm node, which in turn represents a \"virtual Ecu\" on the J1939 communication bus."""
	def __init__(self):
		super().__init__()
		self._nmNode_child=ModelNone

class DiagnosticJ1939Spn(complexbase.GroupBase):
	"""This meta-class represents the ability to model a J1939 Suspect Parameter Number (SPN)."""
	def __init__(self):
		super().__init__()
		self._spn_child=ModelNone

class DiagnosticJ1939SpnMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between an SPN and a SystemSignal. The existence of a mapping means that neither the SPN nor the SystemSignal need to be updated if the relation between the two changes."""
	def __init__(self):
		super().__init__()
		self._sendingNode_children=[]
		self._spn_child=ModelNone
		self._systemSignal_child=ModelNone

class DiagnosticJ1939SwMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a piece of application software to a J1939DiagnosticNode. By this means the diagnostic configuration can be associated with the application software."""
	def __init__(self):
		super().__init__()
		self._node_child=ModelNone
		self._swComponentPrototype_child=ModelNone

class DiagnosticMapping(complexbase.GroupBase):
	"""Abstract element for different kinds of diagnostic mappings."""
	def __init__(self):
		super().__init__()
		self._providerSoftwareCluster_child=ModelNone
		self._requesterSoftwareCluster_child=ModelNone

class DiagnosticMasterToSlaveEventMapping(complexbase.GroupBase):
	"""This meta-class provides the ability to map a master diagnostic event with a slave diagnostic event such that reporting of the master event with a given value also reports the slave event with the same value"""
	def __init__(self):
		super().__init__()
		self._masterEvent_child=ModelNone
		self._slaveEvent_child=ModelNone

class DiagnosticMasterToSlaveEventMappingSet(complexbase.GroupBase):
	"""This meta-class provides the ability to gather a collection of DiagnosticMasterToSlaveEventMappings"""
	def __init__(self):
		super().__init__()
		self._masterToSlaveEventMapping_children=[]

class DiagnosticMeasurementIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a measurement identifier."""
	def __init__(self):
		super().__init__()
		self._obdMid_child=ModelNone

class DiagnosticMemoryAddressableRangeAccess(complexbase.GroupBase):
	"""This abstract base class"""
	def __init__(self):
		super().__init__()
		self._memoryRange_children=[]

class DiagnosticMemoryByAddress(complexbase.GroupBase):
	"""This represents an abstract base class for diagnostic services that deal with accessing memory by address."""

class DiagnosticMemoryDestination(complexbase.GroupBase):
	"""This abstract meta-class represents a possible memory destination for a diagnostic event."""
	def __init__(self):
		super().__init__()
		self._agingRequiresTestedCycle_child=ModelNone
		self._clearDtcLimitation_child=ModelNone
		self._dtcStatusAvailabilityMask_child=ModelNone
		self._eventDisplacementStrategy_child=ModelNone
		self._maxNumberOfEventEntries_child=ModelNone
		self._memoryEntryStorageTrigger_child=ModelNone
		self._statusBitHandlingTestFailedSinceLastClear_child=ModelNone
		self._statusBitStorageTestFailed_child=ModelNone
		self._typeOfFreezeFrameRecordNumeration_child=ModelNone

class DiagnosticMemoryDestinationMirror(complexbase.GroupBase):
	"""This represents a mirror memory for a diagnostic event."""

class DiagnosticMemoryDestinationPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticMemoryDestination."""
	def __init__(self):
		super().__init__()
		self._memoryDestination_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticMemoryDestinationPrimary(complexbase.GroupBase):
	"""This represents a primary memory for a diagnostic event."""
	def __init__(self):
		super().__init__()
		self._typeOfDtcSupported_child=ModelNone

class DiagnosticMemoryDestinationUserDefined(complexbase.GroupBase):
	"""This represents a user-defined memory for a diagnostic event."""
	def __init__(self):
		super().__init__()
		self._authRole_children=[]
		self._authenticationRole_child=ModelNone
		self._memoryId_child=ModelNone

class DiagnosticMemoryIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to define memory properties from the diagnostics point of view."""
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone
		self._id_child=ModelNone
		self._memoryHighAddress_child=ModelNone
		self._memoryHighAddressLabel_child=ModelNone
		self._memoryLowAddress_child=ModelNone
		self._memoryLowAddressLabel_child=ModelNone

class DiagnosticMonitorInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a monitor-focused PortInterface for diagnostics on the adaptive platform."""

class DiagnosticMonitorPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service port the Diagnostic Monitor is mapped."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticMultipleConditionInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a condition-focused PortInterface for diagnostics on the adaptive platform. In contrast to the DiagnosticConditionInterface, the DiagnosticMultipleConditionInterface allows for handling more than one condition in the scope of a single PortPrototype."""

class DiagnosticMultipleConditionPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service port that can handle a collection of diagostic conditions the specific condition is mapped."""
	def __init__(self):
		super().__init__()
		self._diagnosticCondition_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticMultipleEventInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a event-focused PortInterface for diagnostics on the adaptive platform. In contrast to the DiagnosticEventInterface, the DiagnosticMultipleMonitorInterface allows for handling more than one event in the scope of a single PortPrototype."""

class DiagnosticMultipleEventPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service port that can handle a collection of event status the specific event is mapped."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticMultipleMonitorInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a monitor-focused PortInterface for diagnostics on the adaptive platform. In contrast to the DiagnosticMonitorInterface, the DiagnosticMultipleMonitorInterface allows for handling more than one event in the scope of a single PortPrototype."""

class DiagnosticMultipleMonitorPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service port that can handle a collection of monitors the specific event is mapped"""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone

class DiagnosticMultipleResourceInterface(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for diagnostic port interfaces that affect multiple diagnostic resources (e.g. multiple events in one port interface)."""

class DiagnosticMultipleResourcePortMapping(complexbase.GroupBase):
	"""This abstract base class enables the mapping of diagnostic PortInterfaces that deal with multiple diagnostic resources."""
	def __init__(self):
		super().__init__()
		self._overrideId_child=ModelNone

class DiagnosticOperationCycle(complexbase.GroupBase):
	"""Definition of an operation cycle that is the base of the event qualifying and for Dem scheduling."""
	def __init__(self):
		super().__init__()
		self._automaticEnd_child=ModelNone
		self._cycleAutostart_child=ModelNone
		self._cycleStatusStorage_child=ModelNone
		self._type_child=ModelNone

class DiagnosticOperationCycleInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to process requests for operation cycles on the adaptive platform."""

class DiagnosticOperationCycleNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide information regarding the operation cycle management to the Dem module."""
	def __init__(self):
		super().__init__()
		self._operationCycle_child=ModelNone
		self._operationCycleAutomaticEnd_child=ModelNone
		self._operationCycleAutostart_child=ModelNone

class DiagnosticOperationCyclePortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticOperationCycle is mapped."""
	def __init__(self):
		super().__init__()
		self._operationCycle_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototypeInExecutable_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependencyInSystem_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticOperationCycleRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticOperationCycle_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticParameter(complexbase.GroupBase):
	"""This meta-class represents the ability to describe information relevant for the execution of a specific diagnostic service, i.e. it can be taken to parameterize the service."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._supportInfo_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticParameterElement(complexbase.GroupBase):
	"""This meta-class represents an element of a DiagnosticParameter if the DiagnosticParameter represents a structure."""
	def __init__(self):
		super().__init__()
		self._arraySize_child=ModelNone
		self._subElement_children=[]

class DiagnosticParameterElementAccess(complexbase.GroupBase):
	"""This meta-class acts as a single point for defining structured references to a specific DiagnosticParameterElement."""
	def __init__(self):
		super().__init__()
		self._contextElement_children=[]
		self._targetElement_child=ModelNone

class DiagnosticParameterIdent(complexbase.GroupBase):
	"""This meta-class has been created to introduce the ability to become referenced into the meta-class AbstractDiagnosticParameter without breaking backwards compatibility."""
	def __init__(self):
		super().__init__()
		self._subElement_children=[]

class DiagnosticParameterIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to model a diagnostic parameter identifier (PID) for the purpose of executing on-board diagnostics (OBD)."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._id_child=ModelNone
		self._pidSize_child=ModelNone
		self._supportInfoByte_child=ModelNone

class DiagnosticParameterSupportInfo(complexbase.GroupBase):
	"""This represents a way to define which bit of the supportInfo is representing this part of the PID"""
	def __init__(self):
		super().__init__()
		self._supportInfoBit_child=ModelNone

class DiagnosticPeriodicRate(complexbase.GroupBase):
	"""This represents the ability to define a periodic rate for the specification of the \"read data by periodic ID\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone
		self._periodicRateCategory_child=ModelNone

class DiagnosticPortInterface(complexbase.GroupBase):
	"""This meta-class serves as an abstract base-class for all diagnostics-related PortInterfaces."""

class DiagnosticPowertrainFreezeFrame(complexbase.GroupBase):
	"""This meta-class represents a powertrain-related freeze-frame. In theory, this meta-class would need an additional id attribute. However, legal regulations requires only a single value for this attribute anyway."""
	def __init__(self):
		super().__init__()
		self._pid_children=[]

class DiagnosticProofOfOwnership(complexbase.GroupBase):
	"""This meta-class represents the subfunction to provide proof of ownership."""

class DiagnosticProtocol(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic protocol."""
	def __init__(self):
		super().__init__()
		self._diagnosticConnection_children=[]
		self._priority_child=ModelNone
		self._protocolKind_child=ModelNone
		self._sendRespPendOnTransToBoot_child=ModelNone
		self._serviceTable_children=[]

class DiagnosticProvidedDataMapping(complexbase.GroupBase):
	"""This represents the ability to define the nature of a data access for a DiagnosticDataElement based on a data provider that cannot be modeled explicitly."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._dataProvider_child=ModelNone

class DiagnosticReadDataByIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Read Data by Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._readClass_child=ModelNone

class DiagnosticReadDataByIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Data by Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._maxDidToRead_child=ModelNone

class DiagnosticReadDataByPeriodicID(complexbase.GroupBase):
	"""This represents an instance of the \"Read Data by periodic Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone
		self._readDataClass_child=ModelNone

class DiagnosticReadDataByPeriodicIDClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Data by periodic Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._maxPeriodicDidToRead_child=ModelNone
		self._periodicRate_children=[]
		self._schedulerMaxNumber_child=ModelNone

class DiagnosticReadDTCInformation(complexbase.GroupBase):
	"""This represents an instance of the \"Read DTC Information\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._readDTCInformationClass_child=ModelNone

class DiagnosticReadDTCInformationClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"ReadDTCInformation\" diagnostic service."""

class DiagnosticReadMemoryByAddress(complexbase.GroupBase):
	"""This represents an instance of the \"Read Memory by Address\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._readClass_child=ModelNone

class DiagnosticReadMemoryByAddressClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Memory by Address\" diagnostic service."""

class DiagnosticReadScalingDataByIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Read Scaling Data by Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._readScalingDataClass_child=ModelNone

class DiagnosticReadScalingDataByIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Read Scaling Data by Identifier\" diagnostic service."""

class DiagnosticRequestControlOfOnBoardDevice(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x08 service."""
	def __init__(self):
		super().__init__()
		self._requestControlOfOnBoardDeviceClass_child=ModelNone
		self._testId_child=ModelNone

class DiagnosticRequestControlOfOnBoardDeviceClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request Control Of On-Board Device\" OBD diagnostic service."""

class DiagnosticRequestCurrentPowertrainData(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x01 service."""
	def __init__(self):
		super().__init__()
		self._pid_child=ModelNone
		self._requestCurrentPowertrainDiagnosticDataClass_child=ModelNone

class DiagnosticRequestCurrentPowertrainDataClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request current Powertrain Data\" OBD diagnostic service."""

class DiagnosticRequestDownload(complexbase.GroupBase):
	"""This represents an instance of the \"Request Download\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._requestDownloadClass_child=ModelNone

class DiagnosticRequestDownloadClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Request Download\" diagnostic service."""

class DiagnosticRequestEmissionRelatedDTC(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x03/0x07 service."""
	def __init__(self):
		super().__init__()
		self._requestEmissionRelatedDtcClass_child=ModelNone

class DiagnosticRequestEmissionRelatedDTCClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request Emission Related DTC\" OBD diagnostic service."""

class DiagnosticRequestEmissionRelatedDTCPermanentStatus(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x0A service."""
	def __init__(self):
		super().__init__()
		self._requestEmissionRelatedDtcClassPermanentStatus_child=ModelNone

class DiagnosticRequestEmissionRelatedDTCPermanentStatusClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request Emission Related DTC Permanent Status\" OBD diagnostic service."""

class DiagnosticRequestFileTransfer(complexbase.GroupBase):
	"""This diagnostic service instance implements the UDS service 0x38."""
	def __init__(self):
		super().__init__()
		self._requestFileTransferClass_child=ModelNone

class DiagnosticRequestFileTransferClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Request File transfer\" diagnostic service."""

class DiagnosticRequestFileTransferInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to process requests for file transfer using diagnostic channels on the adaptive platform."""

class DiagnosticRequestFileTransferNeeds(complexbase.GroupBase):
	"""This meta-class indicates the existence of a service use case that involves UDS service 0x38, Request File Transfer."""

class DiagnosticRequestOnBoardMonitoringTestResults(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x06 service."""
	def __init__(self):
		super().__init__()
		self._diagnosticTestResult_children=[]
		self._requestOnBoardMonitoringTestResultsClass_child=ModelNone
		self._testResult_child=ModelNone

class DiagnosticRequestOnBoardMonitoringTestResultsClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request On-Board Monitoring Test Results\" OBD diagnostic service."""

class DiagnosticRequestPowertrainFreezeFrameData(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x02 service."""
	def __init__(self):
		super().__init__()
		self._freezeFrame_child=ModelNone
		self._requestPowertrainFreezeFrameData_child=ModelNone

class DiagnosticRequestPowertrainFreezeFrameDataClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request Powertrain Freeze Frame Data\" OBD diagnostic service."""

class DiagnosticRequestRoutineResults(complexbase.GroupBase):
	"""This meta-class represents the ability to define the result of a diagnostic routine execution."""
	def __init__(self):
		super().__init__()
		self._request_children=[]
		self._response_children=[]

class DiagnosticRequestUpload(complexbase.GroupBase):
	"""This represents an instance of the \"Request Upload\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._requestUploadClass_child=ModelNone

class DiagnosticRequestUploadClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Request Upload\" diagnostic service."""

class DiagnosticRequestVehicleInfo(complexbase.GroupBase):
	"""This meta-class represents the ability to model an instance of the OBD mode 0x09 service."""
	def __init__(self):
		super().__init__()
		self._infoType_child=ModelNone
		self._requestVehicleInformationClass_child=ModelNone

class DiagnosticRequestVehicleInfoClass(complexbase.GroupBase):
	"""This meta-class represents the ability to define common properties for all instances of the \"Request Vehicle Info\" OBD diagnostic service."""

class DiagnosticResponseOnEvent(complexbase.GroupBase):
	"""This represents an instance of the \"Response on Event\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._event_children=[]
		self._eventWindow_children=[]
		self._responseOnEventAction_child=ModelNone
		self._responseOnEventClass_child=ModelNone
		self._storeEventSupport_child=ModelNone

class DiagnosticResponseOnEventClass(complexbase.GroupBase):
	"""This represents the ability to define common properties for all instances of the \"Response on Event\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._interMessageTime_child=ModelNone
		self._maxNumChangeOfDataIdentfierEvents_child=ModelNone
		self._maxNumComparisionOfValueEvents_child=ModelNone
		self._maxNumberOfStoredDTCStatusChangedEvents_child=ModelNone
		self._maxSupportedDIDLength_child=ModelNone
		self._responseOnEventSchedulerRate_child=ModelNone
		self._storeEventEnabled_child=ModelNone
		self._storeEventSupport_child=ModelNone

class DiagnosticResponseOnEventNeeds(complexbase.GroupBase):
	"""This meta-class indicates a service use-case for the diagnostic service ResponseOnEvent."""

class DiagnosticResponseOnEventTrigger(complexbase.GroupBase):
	"""This represents the ability to further specify the events that are associated with the execution of the diagnostic service."""
	def __init__(self):
		super().__init__()
		self._initialEventStatus_child=ModelNone

class DiagnosticRoutine(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic routine."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._requestResult_child=ModelNone
		self._routineInfo_child=ModelNone
		self._start_child=ModelNone
		self._stop_child=ModelNone

class DiagnosticRoutineControl(complexbase.GroupBase):
	"""This represents an instance of the \"Routine Control\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._routineControlClass_child=ModelNone
		self._routine_child=ModelNone

class DiagnosticRoutineControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Routine Control\" diagnostic service."""

class DiagnosticRoutineGenericInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a generic Routine-focused PortInterface for diagnostics on the adaptive platform."""

class DiagnosticRoutineInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a routine-focused PortInterface for diagnostics on the adaptive platform."""
	def __init__(self):
		super().__init__()
		self._requestResult_child=ModelNone
		self._start_child=ModelNone
		self._stop_child=ModelNone

class DiagnosticRoutineNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (Dcm) which are not related to a particular item (e.g. a PID). The main use case is the mapping of service ports to the Dcm which are not related to a particular item."""
	def __init__(self):
		super().__init__()
		self._diagRoutineType_child=ModelNone
		self._ridNumber_child=ModelNone

class DiagnosticRoutineSubfunction(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class to routine subfunctions."""
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone

class DiagnosticSecureCodingMapping(complexbase.GroupBase):
	"""This meta-class acts a mapping element to select diagnostic service instances that are used in the secure coding procedure. 

Besides, there are already defined diagnostic capabilities they furthermore have a additional semantics that is used during the secure coding process. 

In particular, this class references write data by identifier instances that mandatory require a secure coding and this class references a diagnostic routine control that is used to provide the signature and to persist the data in non-volatile memory."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_children=[]
		self._validation_child=ModelNone

class DiagnosticSecurityAccess(complexbase.GroupBase):
	"""This represents an instance of the \"Security Access\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._requestSeedId_child=ModelNone
		self._securityAccessClass_child=ModelNone
		self._securityDelayTimeOnBoot_child=ModelNone
		self._securityLevel_child=ModelNone

class DiagnosticSecurityAccessClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Security Access\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._sharedTimer_child=ModelNone

class DiagnosticSecurityEventReportingModeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a location in a DID with a security event. The purpose of this mapping is that the location in the DID contains the setting of the reporting mode for the specific security event. This means that the reporting mode of the security event can be set via the diagnostic service WriteDataByIdentifier."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._securityEvent_child=ModelNone

class DiagnosticSecurityLevel(complexbase.GroupBase):
	"""This meta-class represents the ability to define a security level considered for diagnostic purposes."""
	def __init__(self):
		super().__init__()
		self._accessDataRecordSize_child=ModelNone
		self._keySize_child=ModelNone
		self._numFailedSecurityAccess_child=ModelNone
		self._securityDelayTime_child=ModelNone
		self._seedSize_child=ModelNone

class DiagnosticSecurityLevelInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a security-level-focused PortInterface for diagnostics on the adaptive platform."""

class DiagnosticSecurityLevelPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports the DiagnosticSecurityLevel is mapped."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone
		self._securityLevel_child=ModelNone

class DiagnosticServiceClass(complexbase.GroupBase):
	"""This meta-class provides the ability to define common properties that are shared among all instances of sub-classes of DiagnosticServiceInstance."""
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone
		self._accessPermissionValidity_child=ModelNone

class DiagnosticServiceDataMapping(complexbase.GroupBase):
	"""This represents the ability to define a mapping of a diagnostic service to a software-component. 

This kind of service mapping is applicable for the usage of SenderReceiverInterfaces or event/notifier semantics in ServiceInterfaces on the adaptive platform."""
	def __init__(self):
		super().__init__()
		self._diagnosticDataElement_child=ModelNone
		self._mappedDataElement_child=ModelNone
		self._parameterElementAccess_child=ModelNone
		self._diagnosticParameter_child=ModelNone

class DiagnosticServiceGenericMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a generic generic mapping for select diagnostics services on the adaptive platform."""
	def __init__(self):
		super().__init__()
		self._diagnosticServiceInstance_child=ModelNone
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticServiceInstance(complexbase.GroupBase):
	"""This represents a concrete instance of a diagnostic service."""
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone

class DiagnosticServiceMappingDiagTarget(complexbase.GroupBase):
	"""This meta-class serves as a base class for diagnostics-related targets of subclasses of DiagnosticSwMapping"""

class DiagnosticServiceSwMapping(complexbase.GroupBase):
	"""This represents the ability to define a mapping of a diagnostic service to a software-component or a basic-software module. If the former is used then this kind of service mapping is applicable for the usage of ClientServerInterfaces."""
	def __init__(self):
		super().__init__()
		self._accessedDataPrototype_child=ModelNone
		self._diagnosticDataElement_child=ModelNone
		self._diagnosticParameter_child=ModelNone
		self._mappedBswServiceDependency_child=ModelNone
		self._mappedFlatSwcServiceDependency_child=ModelNone
		self._mappedSwcServiceDependencyInSystem_child=ModelNone
		self._mappedSwcServiceDependency_child=ModelNone
		self._parameterElementAccess_child=ModelNone
		self._serviceInstance_child=ModelNone

class DiagnosticServiceTable(complexbase.GroupBase):
	"""This meta-class represents a model of a diagnostic service table, i.e. the UDS services applicable for a given ECU."""
	def __init__(self):
		super().__init__()
		self._diagnosticConnection_children=[]
		self._ecuInstance_child=ModelNone
		self._protocolKind_child=ModelNone
		self._serviceInstance_children=[]

class DiagnosticServiceTableRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticServiceTable_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticServiceValidationConfiguration(complexbase.GroupBase):
	"""This meta-class has the ability to configure the order of manufacturer/supplier-checks."""
	def __init__(self):
		super().__init__()
		self._manufacturerValidationOrder_children=[]
		self._sovdValidationOrder_children=[]
		self._supplierValidationOrder_children=[]

class DiagnosticServiceValidationInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to process requests for service validation on the adaptive platform."""

class DiagnosticServiceValidationMapping(complexbase.GroupBase):
	"""This meta-class provides the ability to specify manufacturer/supplier checks to be executed before diagnostic services can be processed."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticSession(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic session."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._jumpToBootLoader_child=ModelNone
		self._p2ServerMax_child=ModelNone
		self._p2StarServerMax_child=ModelNone

class DiagnosticSessionControl(complexbase.GroupBase):
	"""This represents an instance of the \"Session Control\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._diagnosticSession_child=ModelNone
		self._sessionControlClass_child=ModelNone

class DiagnosticSessionControlClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Session Control\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._s3ServerTimeout_child=ModelNone

class DiagnosticSovdAuthorizationInterface(complexbase.GroupBase):
	"""This meta-class is used to type a PPortPrototype for implementing the SOVD authorization."""

class DiagnosticSovdAuthorizationPortMapping(complexbase.GroupBase):
	"""This mapping class identifies the PortPrototype in the application software that handles the SOVD authorization."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticSovdBulkData(complexbase.GroupBase):
	"""This meta-class represents a \"Bulk Data\" SOVD service instance."""
	def __init__(self):
		super().__init__()
		self._bulkDataCategory_child=ModelNone

class DiagnosticSovdBulkDataInterface(complexbase.GroupBase):
	"""This meta-class is used to type a PPortPrototype for implementing the SOVD bulk data transmission."""

class DiagnosticSovdBulkDataPortMapping(complexbase.GroupBase):
	"""This mapping associates a PPortPrototype typed by a DiagnosticSovdBulkDataInterface to to the corresponding SOVD service instance that is modeled as DiagnosticSovdBulkData."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone
		self._serviceInstance_child=ModelNone

class DiagnosticSovdConfiguration(complexbase.GroupBase):
	"""This abstract meta-class represents a \"configuration\" SOVD service instance. The concrete nature of the service instance is defined by sub-classes."""

class DiagnosticSovdConfigurationBulkData(complexbase.GroupBase):
	"""This meta-class represents a \"Configuration Bulk Data\" SOVD service instance."""
	def __init__(self):
		super().__init__()
		self._contentType_child=ModelNone
		self._version_child=ModelNone

class DiagnosticSovdConfigurationDataIdentifierMapping(complexbase.GroupBase):
	"""This mapping associates a SOVD configuration service instance to a DiagnosticDataIdentifier that carries the configuration payload."""
	def __init__(self):
		super().__init__()
		self._dataIdentifier_child=ModelNone
		self._serviceInstance_child=ModelNone

class DiagnosticSovdConfigurationInterface(complexbase.GroupBase):
	"""This meta-class is used to configure a PortInterface for the exchange of configuration content."""

class DiagnosticSovdConfigurationParameter(complexbase.GroupBase):
	"""This meta-class represents a \"Configuration Parameter\" SOVD service instance."""

class DiagnosticSovdConfigurationPortMapping(complexbase.GroupBase):
	"""This mapping associates a PPortPrototype typed by a DiagnosticSovdConfigurationInterface to to the corresponding SOVD service instance that is modeled as DiagnosticSovdConfiguration."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone
		self._serviceInstance_child=ModelNone

class DiagnosticSovdLock(complexbase.GroupBase):
	"""This meta-class represents the ability to define an SOVD lock to be considered for the computation of a diagnostic access permission."""

class DiagnosticSovdLog(complexbase.GroupBase):
	"""This meta-class represents a \"Log\" SOVD service instance."""

class DiagnosticSovdMethod(complexbase.GroupBase):
	"""A DiagnosticSovdMethod represents a re-usable complex operation (that consists of primitive operations) in the context of the communication of an SOVD server."""
	def __init__(self):
		super().__init__()
		self._delete_children=[]
		self._get_children=[]
		self._post_children=[]
		self._put_children=[]

class DiagnosticSovdMethodPrimitive(complexbase.GroupBase):
	"""This meta-class represents a primitive operation inside a DiagnosticSovdMethod."""
	def __init__(self):
		super().__init__()
		self._accessPermission_child=ModelNone

class DiagnosticSovdPortInterface(complexbase.GroupBase):
	"""This abstract meta-class acts as a base class for all diagnostics-related PortInterfaces used in the context of SOVD."""

class DiagnosticSovdProximityChallengeInterface(complexbase.GroupBase):
	"""This meta-class is used to type a PPortPrototype for implementing the SOVD proximity challenge."""

class DiagnosticSovdProximityChallengePortMapping(complexbase.GroupBase):
	"""This mapping class identifies the PortPrototype in the application software that handles the SOVD proximity challenge."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticSovdServiceInstance(complexbase.GroupBase):
	"""This abstract class serves as the base class for the configuration of SOVD service instances."""
	def __init__(self):
		super().__init__()
		self._method_child=ModelNone

class DiagnosticSovdServiceValidationInterface(complexbase.GroupBase):
	"""This meta-class is used to type a PPortPrototype for implementing the SOVD service validation."""

class DiagnosticSovdServiceValidationPortMapping(complexbase.GroupBase):
	"""This mapping class identifies the PortPrototype in the application software that handles the SOVD service validation."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class DiagnosticSovdUpdate(complexbase.GroupBase):
	"""This meta-class represents a \"Update\" SOVD service instance."""

class DiagnosticSovdUpdateInterface(complexbase.GroupBase):
	"""This meta-class is used to type a PPortPrototype for implementing the SOVD update procedure."""

class DiagnosticSovdUpdatePortMapping(complexbase.GroupBase):
	"""This mapping associates a PPortPrototype typed by an DiagnosticSovdUpdateInterface with the corresponding SOVD service instance that is modeled as a DiagnosticSovdUpdate."""
	def __init__(self):
		super().__init__()
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone
		self._serviceInstance_child=ModelNone

class DiagnosticStartRoutine(complexbase.GroupBase):
	"""This represents the ability to start a diagnostic routine."""
	def __init__(self):
		super().__init__()
		self._request_children=[]
		self._response_children=[]

class DiagnosticStopRoutine(complexbase.GroupBase):
	"""This represents the ability to stop a diagnostic routine."""
	def __init__(self):
		super().__init__()
		self._request_children=[]
		self._response_children=[]

class DiagnosticStorageCondition(complexbase.GroupBase):
	"""Specification of a storage condition."""

class DiagnosticStorageConditionGroup(complexbase.GroupBase):
	"""Storage condition group which includes one or several storage conditions."""
	def __init__(self):
		super().__init__()
		self._storageCondition_children=[]

class DiagnosticStorageConditionNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to provide the capability to set a storage condition."""
	def __init__(self):
		super().__init__()
		self._initialStatus_child=ModelNone

class DiagnosticStorageConditionPortMapping(complexbase.GroupBase):
	"""Defines to which SWC service ports with DiagnosticStorageConditionNeeds the DiagnosticStorageCondition is mapped."""
	def __init__(self):
		super().__init__()
		self._diagnosticStorageCondition_child=ModelNone
		self._swcFlatServiceDependency_child=ModelNone
		self._swcServiceDependencyInSystem_child=ModelNone
		self._swcServiceDependency_child=ModelNone

class DiagnosticStorageConditionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticStorageCondition_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticSupportInfoByte(complexbase.GroupBase):
	"""This meta-class defines the support information (typically byte A) to declare the usability of the DataElements within the so-called packeted PIDs (e.g. PID$68)."""
	def __init__(self):
		super().__init__()
		self._position_child=ModelNone
		self._size_child=ModelNone

class DiagnosticSwMapping(complexbase.GroupBase):
	"""This represents the ability to define a mapping between a diagnostic information (at this point there is no way to become more specific about the semantics) to a software-component."""

class DiagnosticTestIdentifier(complexbase.GroupBase):
	"""This meta-class represents the ability to create a diagnostic test identifier."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._uasId_child=ModelNone

class DiagnosticTestResult(complexbase.GroupBase):
	"""This meta-class represents the ability to define diagnostic test results."""
	def __init__(self):
		super().__init__()
		self._diagnosticEvent_children=[]
		self._event_child=ModelNone
		self._monitoredIdentifier_child=ModelNone
		self._testIdentifier_child=ModelNone
		self._updateKind_child=ModelNone

class DiagnosticTestResultUpdateEnumValueVariationPoint(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticTestRoutineIdentifier(complexbase.GroupBase):
	"""This represents the test id of the DiagnosticTestIdentifier."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._requestDataSize_child=ModelNone
		self._responseDataSize_child=ModelNone

class DiagnosticTransferExit(complexbase.GroupBase):
	"""This represents an instance of the \"Transfer Exit\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._transferExitClass_child=ModelNone

class DiagnosticTransferExitClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Transfer Exit\" diagnostic service."""

class DiagnosticTroubleCode(complexbase.GroupBase):
	"""A diagnostic trouble code defines a unique identifier that is shown to the diagnostic tester."""

class DiagnosticTroubleCodeGroup(complexbase.GroupBase):
	"""The diagnostic trouble code group defines the DTCs belonging together and thereby forming a group."""
	def __init__(self):
		super().__init__()
		self._dtc_children=[]
		self._groupNumber_child=ModelNone

class DiagnosticTroubleCodeJ1939(complexbase.GroupBase):
	"""This meta-class represents the ability to model specific trouble-code related properties for J1939."""
	def __init__(self):
		super().__init__()
		self._dtcProps_child=ModelNone
		self._fmi_child=ModelNone
		self._j1939DtcValue_child=ModelNone
		self._kind_child=ModelNone
		self._node_child=ModelNone
		self._spn_child=ModelNone

class DiagnosticTroubleCodeObd(complexbase.GroupBase):
	"""This element is used to define OBD-relevant DTCs."""
	def __init__(self):
		super().__init__()
		self._considerPtoStatus_child=ModelNone
		self._dtcProps_child=ModelNone
		self._eventObdReadinessGroup_child=ModelNone
		self._eventReadinessGroup_children=[]
		self._obdDTCValue_child=ModelNone
		self._obdDTCValue3Byte_child=ModelNone

class DiagnosticTroubleCodeProps(complexbase.GroupBase):
	"""This element defines common Dtc properties that can be reused by different non OBD-relevant DTCs."""
	def __init__(self):
		super().__init__()
		self._agingAllowed_child=ModelNone
		self._aging_child=ModelNone
		self._diagnosticMemory_child=ModelNone
		self._environmentCaptureToReporting_child=ModelNone
		self._extendedDataRecord_children=[]
		self._fdcThresholdStorageValue_child=ModelNone
		self._freezeFrame_children=[]
		self._freezeFrameContent_child=ModelNone
		self._freezeFrameContentWwhObd_child=ModelNone
		self._immediateNvDataStorage_child=ModelNone
		self._legislatedFreezeFrameContentUdsObd_children=[]
		self._legislatedFreezeFrameContentWwhObd_children=[]
		self._maxNumberFreezeFrameRecords_child=ModelNone
		self._memoryDestination_children=[]
		self._priority_child=ModelNone
		self._significance_child=ModelNone
		self._snapshotRecordContent_children=[]

class DiagnosticTroubleCodeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DiagnosticTroubleCode_child=ModelNone
		self._variationPoint_child=ModelNone

class DiagnosticTroubleCodeUds(complexbase.GroupBase):
	"""This element is used to describe non OBD-relevant DTCs."""
	def __init__(self):
		super().__init__()
		self._considerPtoStatus_child=ModelNone
		self._dtcProps_child=ModelNone
		self._eventObdReadinessGroup_child=ModelNone
		self._eventReadinessGroup_children=[]
		self._functionalUnit_child=ModelNone
		self._obdDtcValue3Byte_child=ModelNone
		self._severity_child=ModelNone
		self._udsDtcValue_child=ModelNone
		self._wwhObdDtcClass_child=ModelNone

class DiagnosticTroubleCodeUdsToClearConditionGroupMapping(complexbase.GroupBase):
	"""This meta-class provides the ability to map a DiagnosticClearConditionGroup to a collection of DiagnosticTroubleCodeUds."""
	def __init__(self):
		super().__init__()
		self._clearConditionGroup_child=ModelNone
		self._troubleCodeUds_child=ModelNone

class DiagnosticTroubleCodeUdsToTroubleCodeObdMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a UDS trouble code to an OBD trouble code."""
	def __init__(self):
		super().__init__()
		self._troubleCodeObd_child=ModelNone
		self._troubleCodeUds_child=ModelNone

class DiagnosticUdsSeverityEnumValueVariationPoint(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticUploadDownloadNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to specify needs regarding upload and download by means of diagnostic services."""

class DiagnosticUploadInterface(complexbase.GroupBase):
	"""This meta-class represents the ability to implement a PortInterface to process requests for uploading data using diagnostic channels on the adaptive platform."""

class DiagnosticValueNeeds(complexbase.GroupBase):
	"""Specifies the general needs on the configuration of the  Diagnostic Communication Manager (DCM) which are not related to a particular item (e.g. a PID). The main use case is the mapping of service ports to the DCM which are not related to a particular item.

In the case of using a sender receiver communicated value, the related value shall be taken via assignedData in the role \"signalBasedDiagnostics\".

In case of using a client/server communicated value, the related value shall be communicated via the port referenced by assignedPort. The details of this communication (e.g. appropriate naming conventions) are specified in the related software specifications (SWS)."""
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._diagnosticValueAccess_child=ModelNone
		self._didNumber_child=ModelNone
		self._fixedLength_child=ModelNone
		self._processingStyle_child=ModelNone

class DiagnosticVerifyCertificateBidirectional(complexbase.GroupBase):
	"""This meta-class represents the subfunction to do a bidirectional verification of the certificate."""

class DiagnosticVerifyCertificateUnidirectional(complexbase.GroupBase):
	"""This meta-class represents the subfunction to do a unidirectional verification of the certificate."""

class DiagnosticWriteDataByIdentifier(complexbase.GroupBase):
	"""This represents an instance of the \"Write Data by Identifier\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._writeClass_child=ModelNone

class DiagnosticWriteDataByIdentifierClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Write Data by Identifier\" diagnostic service."""

class DiagnosticWriteMemoryByAddress(complexbase.GroupBase):
	"""This represents an instance of the \"Write Memory by Address\" diagnostic service."""
	def __init__(self):
		super().__init__()
		self._writeClass_child=ModelNone

class DiagnosticWriteMemoryByAddressClass(complexbase.GroupBase):
	"""This meta-class contains attributes shared by all instances of the \"Write Memory by Address\" diagnostic service."""

class DiagnosticWwhObdDtcClassEnumValueVariationPoint(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class DiagnosticsCommunicationSecurityNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component to verify the access to security level via diagnostic services."""

class DiscoveryTechnology(complexbase.GroupBase):
	"""This element is deprecated and will be removed in future. This information is replaced by the runtimePortConfiguration and runtimeIpAddressConfiguration attributes in the SocketConnection.

Old description:
Discovery technology information."""
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._version_child=ModelNone

class DltApplication(complexbase.GroupBase):
	"""This meta-class represents the application from which the log and trace message originates."""
	def __init__(self):
		super().__init__()
		self._applicationDescription_child=ModelNone
		self._applicationId_child=ModelNone
		self._context_children=[]
		self._variationPoint_child=ModelNone

class DltApplicationToProcessMapping(complexbase.GroupBase):
	"""This element assigns a DltApplicationId to a Process."""
	def __init__(self):
		super().__init__()
		self._dltApplication_child=ModelNone
		self._process_child=ModelNone

class DltArgument(complexbase.GroupBase):
	"""This element defines an Argument in a DltMessage."""
	def __init__(self):
		super().__init__()
		self._dltArgumentEntry_children=[]
		self._length_child=ModelNone
		self._networkRepresentation_child=ModelNone
		self._optional_child=ModelNone
		self._predefinedText_child=ModelNone
		self._variableLength_child=ModelNone

class DltConfig(complexbase.GroupBase):
	"""This element defines a Dlt configuration for a specific Ecu."""
	def __init__(self):
		super().__init__()
		self._dltEcuId_child=ModelNone
		self._dltEcu_child=ModelNone
		self._dltLogChannel_children=[]
		self._sessionIdSupport_child=ModelNone
		self._timestampSupport_child=ModelNone

class DltContext(complexbase.GroupBase):
	"""This meta-class represents the Context that groups Log and Trace Messages that are generated by an application."""
	def __init__(self):
		super().__init__()
		self._contextDescription_child=ModelNone
		self._contextId_child=ModelNone
		self._dltMessage_children=[]

class DltContextRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DltContext_child=ModelNone
		self._variationPoint_child=ModelNone

class DltEcu(complexbase.GroupBase):
	"""This element represents an Ecu or Machine that produces logging and tracing information."""
	def __init__(self):
		super().__init__()
		self._application_children=[]
		self._ecuId_child=ModelNone

class DltLogChannel(complexbase.GroupBase):
	"""This element contains the settings for the log/trace message output for a tuple of ApplicationId and ContextId (verbose mode) or a SessionId (non-verbose mode)."""
	def __init__(self):
		super().__init__()
		self._applicationContext_children=[]
		self._applicationDescription_child=ModelNone
		self._applicationId_child=ModelNone
		self._contextDescription_child=ModelNone
		self._contextId_child=ModelNone
		self._defaultTraceState_child=ModelNone
		self._dltMessage_children=[]
		self._logChannelId_child=ModelNone
		self._logTraceDefaultLogThreshold_child=ModelNone
		self._nonVerboseMode_child=ModelNone
		self._rxPduTriggering_child=ModelNone
		self._segmentationSupported_child=ModelNone
		self._sessionId_child=ModelNone
		self._txPduTriggering_child=ModelNone

class DltLogSink(complexbase.GroupBase):
	"""The meta-class defines the output sink for DltLogMessages"""
	def __init__(self):
		super().__init__()
		self._bufferOutput_child=ModelNone
		self._defaultLogThreshold_child=ModelNone
		self._defaultTraceState_child=ModelNone
		self._endpointConfiguration_child=ModelNone
		self._logChannelId_child=ModelNone
		self._nonVerboseMode_child=ModelNone
		self._path_child=ModelNone
		self._queueSize_child=ModelNone
		self._segmentationSupported_child=ModelNone

class DltLogSinkToPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class maps a PortPrototype to an output sink of a log and trace message."""
	def __init__(self):
		super().__init__()
		self._dltContext_child=ModelNone
		self._dltLogSink_children=[]
		self._dltSessionId_child=ModelNone
		self._pPortPrototype_child=ModelNone
		self._process_child=ModelNone
		self._rPortPrototype_child=ModelNone

class DltMessage(complexbase.GroupBase):
	"""This element defines a DltMessage."""
	def __init__(self):
		super().__init__()
		self._dltArgument_children=[]
		self._messageId_child=ModelNone
		self._messageLineNumber_child=ModelNone
		self._messageSourceFile_child=ModelNone
		self._messageTypeInfo_child=ModelNone
		self._privacyLevel_child=ModelNone
		self._variationPoint_child=ModelNone

class DltMessageCollectionSet(complexbase.GroupBase):
	"""Collection of DltMessages"""
	def __init__(self):
		super().__init__()
		self._dltMessage_children=[]

class DltMessageRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._DltMessage_child=ModelNone
		self._variationPoint_child=ModelNone

class DltUserNeeds(complexbase.GroupBase):
	"""This meta-class specifies the needs on the configuration of the Diagnostic Log and Trace module for one SessionId. 

This class currently contains no attributes. 

An instance of this class is used to find out which PortPrototypes of an AtomicSwComponentType belong to this SessionId in order to group the request and response PortPrototypes of the same SessionId. 

The actual SessionId value is stored in the PortDefinedArgumentValue of the respective PortPrototype specification."""

class DoIpActivationLineNeeds(complexbase.GroupBase):
	"""A DoIP entity needs to be informed when an external tester is attached or activated. The DoIpActivationServiceNeeds specifies the trigger for such an event. Examples would be a Pdu via a regular communication bus, a PWM signal, or an I/O. For details please refer to the ISO 13400."""

class DoIpConfig(complexbase.GroupBase):
	"""This element defines the DoIp configuration for a specific Ecu."""
	def __init__(self):
		super().__init__()
		self._doipInterface_children=[]
		self._logicAddress_child=ModelNone

class DoIpEntity(complexbase.GroupBase):
	"""ECU providing this infrastructure service is a DoIP-Entity."""
	def __init__(self):
		super().__init__()
		self._doIpEntityRole_child=ModelNone

class DoIpGidNeeds(complexbase.GroupBase):
	"""The DoIpGidNeeds indicates that the software-component owning this ServiceNeeds is providing the GID number either after a GID Synchronisation or by other means like e.g. flashed EEPROM parameter. This need can be used independent from DoIpGidSynchronizationNeeds and is necessary if the GID can not be provided out of the DoIP configuration options."""

class DoIpGidSynchronizationNeeds(complexbase.GroupBase):
	"""The DoIpGidSynchronizationNeeds indicates that the software-component owning this ServiceNeeds is triggered by the DoIP entity to start a synchronization of the GID (Group Identification) on the DoIP service 0x0001, 0x0002, 0x0003 or before announcement via service 0x0004 according to ISO 13400-2:2012 if necessary. Note that this need is only relevant for DoIP synchronization masters."""

class DoIpInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the DoIP configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._eid_child=ModelNone
		self._entityStatusMaxByteFieldUse_child=ModelNone
		self._gid_child=ModelNone
		self._gidInvalidityPattern_child=ModelNone
		self._logicalAddress_child=ModelNone
		self._maxRequestBytes_child=ModelNone
		self._networkInterface_children=[]
		self._requestConfiguration_children=[]
		self._vinInvalidityPattern_child=ModelNone

class DoIpInterface(complexbase.GroupBase):
	"""A logical interface over which the DoIP Node is able to communicate via DoIP independently from other existing DoIpInterfaces."""
	def __init__(self):
		super().__init__()
		self._aliveCheckResponseTimeout_child=ModelNone
		self._doIpRoutingActivation_children=[]
		self._doipChannelCollection_child=ModelNone
		self._doipConnection_children=[]
		self._generalInactivityTime_child=ModelNone
		self._initialInactivityTime_child=ModelNone
		self._initialVehicleAnnouncementTime_child=ModelNone
		self._isActivationLineDependent_child=ModelNone
		self._maxTesterConnections_child=ModelNone
		self._socketConnection_children=[]
		self._useMacAddressForIdentification_child=ModelNone
		self._useVehicleIdentificationSyncStatus_child=ModelNone
		self._vehicleAnnouncementCount_child=ModelNone
		self._vehicleAnnouncementInterval_child=ModelNone

class DoIpLogicAddress(complexbase.GroupBase):
	"""The logical DoIP address."""
	def __init__(self):
		super().__init__()
		self._address_child=ModelNone
		self._doIpLogicAddressProps_child=[]

class DoIpLogicTargetAddressProps(complexbase.GroupBase):
	"""This meta-class acts as a target for references to the DoIpLogicTargetAddress and collects DoIpLogicTargetAddress specific settings."""

class DoIpLogicTesterAddressProps(complexbase.GroupBase):
	"""This meta-class acts as a target for references to the DoIpLogicTesterAddress and collects DoIpLogicTesterAddress specific settings."""
	def __init__(self):
		super().__init__()
		self._doIpTesterRoutingActivation_children=[]

class DoIpNetworkConfiguration(complexbase.GroupBase):
	"""This element collects DoIP properties that are network interface specific."""
	def __init__(self):
		super().__init__()
		self._eidRetrieval_child=ModelNone
		self._isActivationLineDependent_child=ModelNone
		self._maxInitialVehicleAnnouncementTime_child=ModelNone
		self._maxTesterConnections_child=ModelNone
		self._networkConfiguration_children=[]
		self._networkInterfaceId_child=ModelNone
		self._tcpAliveCheckResponseTimeout_child=ModelNone
		self._tcpGeneralInactivityTime_child=ModelNone
		self._tcpInitialInactivityTime_child=ModelNone
		self._vehicleAnnouncementCount_child=ModelNone
		self._vehicleAnnouncementInterval_child=ModelNone
		self._vehicleIdentificationSyncStatus_child=ModelNone

class DoIpPowerModeStatusNeeds(complexbase.GroupBase):
	"""The DoIpPowerModeStatusNeeds indicates that the software-component owning this ServiceNeeds is providing the PowerModeStatus for the DoIP service 0x4003 according to ISO 13400-2:2012."""

class DoIpRequestConfiguration(complexbase.GroupBase):
	"""This meta-class specifies a range of target addresses and its interpretation as either physical or functional request."""
	def __init__(self):
		super().__init__()
		self._endAddress_child=ModelNone
		self._requestType_child=ModelNone
		self._startAddress_child=ModelNone

class DoIpRoutingActivation(complexbase.GroupBase):
	"""This meta-class defines a DoIP routing activation possibility that activates the routing to the referenced doIPTargetAddress. This means that the diagnostic request messages related to the specified doIPTargetAddress received by socketConnections that are referenced by the same DoIpInterface that aggregates this DoIpRoutingActivation are activated."""
	def __init__(self):
		super().__init__()
		self._doIpTargetAddress_children=[]

class DoIpRoutingActivationAuthenticationNeeds(complexbase.GroupBase):
	"""DoIPRoutingActivationAuthenticationNeeds indicates that the software-component owning this ServiceNeeds will have an authentication required for a DoIP routing activation service (0x0005) according to ISO 13400-2:2012."""
	def __init__(self):
		super().__init__()
		self._dataLengthRequest_child=ModelNone
		self._dataLengthResponse_child=ModelNone
		self._routingActivationType_child=ModelNone

class DoIpRoutingActivationConfirmationNeeds(complexbase.GroupBase):
	"""DoIpRoutingActivationConfirmationNeeds indicates that the software-component that owns this ServiceNeeds will have a confirmation required for a DoIP routing activation service (0x0005) according to ISO 13400-2:2012."""
	def __init__(self):
		super().__init__()
		self._dataLengthRequest_child=ModelNone
		self._dataLengthResponse_child=ModelNone
		self._routingActivationType_child=ModelNone

class DoIpRule(complexbase.GroupBase):
	"""Configuration of a generic firewall rule"""
	def __init__(self):
		super().__init__()
		self._destinationMaxAddress_child=ModelNone
		self._destinationMinAddress_child=ModelNone
		self._inverseProtocolVersion_child=ModelNone
		self._payloadLength_child=ModelNone
		self._payloadType_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._sourceMaxAddress_child=ModelNone
		self._sourceMinAddress_child=ModelNone
		self._udsService_child=ModelNone

class DoIpServiceNeeds(complexbase.GroupBase):
	"""This represents an abstract base class for ServiceNeeds related to DoIP."""

class DoIpTpConfig(complexbase.GroupBase):
	"""This element defines exactly one DoIpTp Configuration that is used to configure all DoIPChannels available in a DoIpInterface. Each DoIPChannel describes a connection between a doIpSourceAddress and a doIpTargetAddress and the exchange of DcmIPdus between the PduR and DoIP."""
	def __init__(self):
		super().__init__()
		self._doIpLogicAddress_children=[]
		self._tpConnection_children=[]

class DoIpTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The DoIp module routes a tpSdu through this connection."""
	def __init__(self):
		super().__init__()
		self._doIpSourceAddress_child=ModelNone
		self._doIpTargetAddress_child=ModelNone
		self._tpSdu_child=ModelNone

class DocRevision(complexbase.GroupBase):
	"""This meta-class represents the ability to maintain information which relates to revision management of documents or objects."""
	def __init__(self):
		super().__init__()
		self._revisionLabel_child=ModelNone
		self._revisionLabelP1_child=ModelNone
		self._revisionLabelP2_child=ModelNone
		self._state_child=ModelNone
		self._issuedBy_child=ModelNone
		self._date_child=ModelNone
		self._modification_children=[]

class DocumentElementScope(complexbase.GroupBase):
	"""Specifies if a specification element such as a requirement, specification, deliverable, artifact, task definition or activity is in scope of this data exchange point. The DocumentElementScope may reference all specification elements that have a name or ID. The only exception are Meta Classes, Meta Attribute and constraints which are handled in the Data Format Tailoring section of the Profile of Data Exchange Point.

Elements of Autosar specification documents are referenced via their ID (requirement, specification items) or name (deliverable, artifact, task definition or activity)"""
	def __init__(self):
		super().__init__()
		self._customDocumentElement_child=ModelNone
		self._tailoring_children=[]

class DocumentViewSelectable(complexbase.GroupBase):
	"""This meta-class represents the ability to be dedicated to a particular audience or document view."""
	si=complexbase.Attribute("si",SimpleTypes.NameTokens,'SI',False,"""This attribute allows to denote a semantic information which is used to identify documentation objects to be selected in customizable document views. It shall be defined in agreement between the involved parties.""")
	view=complexbase.Attribute("view",SimpleTypes.ViewTokens,'VIEW',False,"""This attribute lists the document views in which the object shall appear. If it is missing, the object appears in all document views.""")

class Documentation(complexbase.GroupBase):
	"""This meta-class represents the ability to handle a so called standalone documentation. Standalone means, that such a documentation is not embedded in another ARElement or identifiable object.  The standalone documentation is an entity of its own which denotes its context by reference to other objects and instances."""
	def __init__(self):
		super().__init__()
		self._context_children=[]
		self._documentationContent_child=ModelNone

class DocumentationBlock(complexbase.GroupBase):
	"""This class represents a documentation block. It is made of basic text structure elements which can be displayed in a table cell."""
	def __init__(self):
		super().__init__()
		self._msrQueryP2_children=[]
		self._p_children=[]
		self._verbatim_children=[]
		self._list_children=[]
		self._defList_children=[]
		self._labeledList_children=[]
		self._formula_children=[]
		self._figure_children=[]
		self._note_children=[]
		self._trace_children=[]
		self._structuredReq_children=[]

class DocumentationContext(complexbase.GroupBase):
	"""This class represents the ability to denote a context of a so called standalone  documentation. Note that this is an <<atpMixed>>. The contents needs to be considered as ordered."""
	def __init__(self):
		super().__init__()
		self._feature_child=ModelNone
		self._identifiable_child=ModelNone

class DtcStatusChangeNotificationNeeds(complexbase.GroupBase):
	"""This meta-class represents the needs of a software-component interested to get information regarding any DTC status change."""
	def __init__(self):
		super().__init__()
		self._dtcFormatType_child=ModelNone
		self._notificationTime_child=ModelNone

class DynamicPart(complexbase.GroupBase):
	"""Dynamic part of a multiplexed I-Pdu. 
Reserved space which is used to transport varying SignalIPdus at the same position, controlled by the corresponding selectorFieldCode."""
	def __init__(self):
		super().__init__()
		self._dynamicPartAlternative_children=[]
		self._variationPoint_child=ModelNone

class DynamicPartAlternative(complexbase.GroupBase):
	"""One of the Com IPdu alternatives that are transmitted in the Dynamic Part of the MultiplexedIPdu. The selectorFieldCode
specifies which Com IPdu is contained in the DynamicPart within a
certain transmission of a multiplexed PDU."""
	def __init__(self):
		super().__init__()
		self._iPdu_child=ModelNone
		self._initialDynamicPart_child=ModelNone
		self._selectorFieldCode_child=ModelNone

class E2EProfileCompatibilityProps(complexbase.GroupBase):
	"""This meta-class collects settings for configuration of the E2E state machine."""
	def __init__(self):
		super().__init__()
		self._transitToInvalidExtended_child=ModelNone

class E2EProfileConfiguration(complexbase.GroupBase):
	"""This element holds E2E profile specific configuration settings."""
	def __init__(self):
		super().__init__()
		self._clearFromValidToInvalid_child=ModelNone
		self._dataIdMode_child=ModelNone
		self._e2eProfileCompatibilityProps_child=ModelNone
		self._maxDeltaCounter_child=ModelNone
		self._maxErrorStateInit_child=ModelNone
		self._maxErrorStateInvalid_child=ModelNone
		self._maxErrorStateValid_child=ModelNone
		self._minOkStateInit_child=ModelNone
		self._minOkStateInvalid_child=ModelNone
		self._minOkStateValid_child=ModelNone
		self._profileName_child=ModelNone
		self._windowSizeInit_child=ModelNone
		self._windowSizeInvalid_child=ModelNone
		self._windowSizeValid_child=ModelNone

class E2EProfileConfigurationSet(complexbase.GroupBase):
	"""This meta-class represents the ability to aggregate a collection of E2EProfileConfigurations."""
	def __init__(self):
		super().__init__()
		self._e2eProfileConfiguration_children=[]

class EcuAbstractionSwComponentType(complexbase.GroupBase):
	"""The ECUAbstraction is a special AtomicSwComponentType that resides between a software-component that wants to access ECU periphery and the Microcontroller Abstraction. The EcuAbstractionSwComponentType introduces the possibility to link from the software representation to its hardware description provided by the ECU Resource Template."""
	def __init__(self):
		super().__init__()
		self._hardwareElement_children=[]

class EcuInstance(complexbase.GroupBase):
	"""ECUInstances are used to define the ECUs used in the topology. The type of the ECU is defined by a reference to an ECU specified with the ECU resource description."""
	def __init__(self):
		super().__init__()
		self._associatedComIPduGroup_children=[]
		self._associatedConsumedProvidedServiceInstanceGroup_children=[]
		self._associatedPdurIPduGroup_children=[]
		self._canTpAddress_children=[]
		self._channelSynchronousWakeup_child=ModelNone
		self._clientIdRange_child=ModelNone
		self._comConfigurationGwTimeBase_child=ModelNone
		self._comConfigurationRxTimeBase_child=ModelNone
		self._comConfigurationTxTimeBase_child=ModelNone
		self._comEnableMDTForCyclicTransmission_child=ModelNone
		self._commController_children=[]
		self._connector_children=[]
		self._diagnosticAddress_child=ModelNone
		self._diagnosticProps_child=ModelNone
		self._dltConfig_child=ModelNone
		self._doIpConfig_child=ModelNone
		self._ecuInstanceProps_children=[]
		self._ecuTaskProxy_children=[]
		self._ethSwitchPortGroupDerivation_child=ModelNone
		self._firewallRule_children=[]
		self._partition_children=[]
		self._pnResetTime_child=ModelNone
		self._pncNmRequest_child=ModelNone
		self._pncPrepareSleepTimer_child=ModelNone
		self._pncSynchronousWakeup_child=ModelNone
		self._sleepModeSupported_child=ModelNone
		self._tcpIpIcmpProps_child=ModelNone
		self._tcpIpProps_child=ModelNone
		self._tpAddress_children=[]
		self._v2xSupported_child=ModelNone
		self._wakeUpOverBusSupported_child=ModelNone

class EcuInstanceProps(complexbase.GroupBase):
	"""This element describes additional properties of the EcuInstance that may vary in different Variants of the Ecu."""
	def __init__(self):
		super().__init__()
		self._diagnosticAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class EcuInstanceRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._EcuInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class ECUMapping(complexbase.GroupBase):
	"""ECUMapping allows to assign an ECU hardware type (defined in the ECU Resource Template) to an ECUInstance used in a physical topology."""
	def __init__(self):
		super().__init__()
		self._commControllerMapping_children=[]
		self._ecuInstance_child=ModelNone
		self._ecu_child=ModelNone
		self._hwPortMapping_children=[]
		self._variationPoint_child=ModelNone

class EcuPartition(complexbase.GroupBase):
	"""Partitions are used as error containment regions. They permit the grouping of SWCs and resources and allow to describe recovery policies individually for each partition. Partitions can be terminated or restarted during run-time as a result of a detected error."""
	def __init__(self):
		super().__init__()
		self._execInUserMode_child=ModelNone

class EcuResourceEstimation(complexbase.GroupBase):
	"""Resource estimations for RTE and BSW of a single ECU instance."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._bswResourceEstimation_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._rteResourceEstimation_child=ModelNone
		self._swCompToEcuMapping_children=[]
		self._variationPoint_child=ModelNone

class EcuStateMgrUserNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the ECU State  Manager for one \"user\". This class currently contains no attributes. Its name can be regarded as a symbol identifying the user  from the viewpoint of the component or module which owns this class."""

class EcuTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints within the scope of one ECU configuration.

TimingDescriptions aggregated by EcuTiming are allowed to use all events derived from the class TimingDescriptionEvent."""
	def __init__(self):
		super().__init__()
		self._ecuConfiguration_child=ModelNone

class EcucAbstractConfigurationClass(complexbase.GroupBase):
	"""Specifies the ValueConfigurationClass of a parameter/reference or the MultiplicityConfigurationClass of a parameter/reference or a container for each ConfigurationVariant of the EcucModuleDef."""
	def __init__(self):
		super().__init__()
		self._configClass_child=ModelNone
		self._configVariant_child=ModelNone

class EcucAbstractExternalReferenceDef(complexbase.GroupBase):
	"""Common abstract class to gather attributes for external references (where the destination is not located in the ECU Configuration Description but in an another AUTOSAR Template)."""

class EcucAbstractInternalReferenceDef(complexbase.GroupBase):
	"""Common abstract class to gather attributes for internal references (where the destination is located in the Ecu Configuration Description)."""
	def __init__(self):
		super().__init__()
		self._requiresSymbolicNameValue_child=ModelNone

class EcucAbstractReferenceDef(complexbase.GroupBase):
	"""Common class to gather the attributes for the definition of references."""
	def __init__(self):
		super().__init__()
		self._withAuto_child=ModelNone

class EcucAbstractReferenceValue(complexbase.GroupBase):
	"""Abstract class to be used as common parent for all reference values in the ECU Configuration Description."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._annotation_children=[]
		self._isAutoValue_child=ModelNone
		self._variationPoint_child=ModelNone

class EcucAbstractStringParamDef(complexbase.GroupBase):
	"""Abstract class that is used to collect the common properties for StringParamDefs, LinkerSymbolDef, FunctionNameDef and MultilineStringParamDefs. 

atpVariation: [RS_ECUC_00083]"""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._maxLength_child=ModelNone
		self._minLength_child=ModelNone
		self._regularExpression_child=ModelNone

class EcucAbstractStringParamDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucAddInfoParamDef(complexbase.GroupBase):
	"""Configuration Parameter Definition for the specification of formatted text in the ECU Configuration Parameter Description."""

class EcucAddInfoParamValue(complexbase.GroupBase):
	"""This parameter corresponds to EcucAddInfoParamDef."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucBooleanParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Boolean. Allowed values are true and false."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone

class EcucChoiceContainerDef(complexbase.GroupBase):
	"""Used to define configuration containers that provide a choice between several EcucParamConfContainerDef. But in the actual ECU Configuration Values only one instance from the choice list will be present."""
	def __init__(self):
		super().__init__()
		self._choice_children=[]

class EcucChoiceReferenceDef(complexbase.GroupBase):
	"""Specify alternative references where in the ECU Configuration description only one of the specified references will actually be used."""
	def __init__(self):
		super().__init__()
		self._destination_children=[]

class EcucCommonAttributes(complexbase.GroupBase):
	"""Attributes used by Configuration Parameters as well as References."""
	def __init__(self):
		super().__init__()
		self._configurationClassAffection_child=ModelNone
		self._implementationConfigClass_children=[]
		self._multiplicityConfigClass_children=[]
		self._origin_child=ModelNone
		self._postBuildVariantMultiplicity_child=ModelNone
		self._postBuildVariantValue_child=ModelNone
		self._requiresIndex_child=ModelNone
		self._valueConfigClass_children=[]

class EcucConditionFormula(complexbase.GroupBase):
	"""This formula shall yield a boolean expression depending on ecuc queries. Note that the EcucConditionFormula is a mixed string. Therefore, the properties have the upper multiplicity 1."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_children=[]
		self._ecucQueryString_children=[]

class EcucConditionSpecification(complexbase.GroupBase):
	"""Allows to define existence dependencies based on the value of  parameter values."""
	def __init__(self):
		super().__init__()
		self._conditionFormula_child=ModelNone
		self._ecucQuery_children=[]
		self._informalFormula_child=ModelNone

class EcucConfigurationClassAffection(complexbase.GroupBase):
	"""Specifies in the \"VendorSpecificModuleDefinition\" whether changes on this parameter do affect other parameters in a later configuration step.

This element is removed from the specifications and shall not be used."""
	def __init__(self):
		super().__init__()
		self._affected_children=[]
		self._affectionKind_child=ModelNone

class EcucContainerDef(complexbase.GroupBase):
	"""Base class used to gather common attributes of configuration container definitions."""
	def __init__(self):
		super().__init__()
		self._destinationUri_children=[]
		self._multiplicityConfigClass_children=[]
		self._origin_child=ModelNone
		self._postBuildChangeable_child=ModelNone
		self._postBuildVariantMultiplicity_child=ModelNone
		self._requiresIndex_child=ModelNone

class EcucContainerValue(complexbase.GroupBase):
	"""Represents a Container definition in the ECU Configuration Description."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._parameterValue_children=[]
		self._referenceValue_children=[]
		self._subContainer_children=[]
		self._variationPoint_child=ModelNone

class EcucDefinitionCollection(complexbase.GroupBase):
	"""This represents the anchor point of an ECU Configuration Parameter Definition within the AUTOSAR templates structure."""
	def __init__(self):
		super().__init__()
		self._module_children=[]

class EcucDefinitionElement(complexbase.GroupBase):
	"""Common class used to express the commonalities of configuration parameters, references and containers.
If not stated otherwise the default multiplicity is exactly one mandatory occurrence of the specified element."""
	def __init__(self):
		super().__init__()
		self._relatedTraceItem_child=ModelNone
		self._ecucValidationCond_children=[]
		self._ecucCond_child=ModelNone
		self._lowerMultiplicity_child=ModelNone
		self._upperMultiplicity_child=ModelNone
		self._upperMultiplicityInfinite_child=ModelNone
		self._scope_child=ModelNone
		self._validationConds=[]
		self._validationCondGrouped=None

class EcucDerivationSpecification(complexbase.GroupBase):
	"""Allows to define configuration items that are calculated based on the value of
* other parameter values
* elements (attributes/classes) defined in other AUTOSAR templates such as System template and SW component template"""
	def __init__(self):
		super().__init__()
		self._calculationFormula_child=ModelNone
		self._ecucQuery_children=[]
		self._informalFormula_child=ModelNone

class EcucDestinationUriDef(complexbase.GroupBase):
	"""Description of an EcucDestinationUriDef that is used as target of EcucUriReferenceDefs."""
	def __init__(self):
		super().__init__()
		self._destinationUriPolicy_child=ModelNone

class EcucDestinationUriDefSet(complexbase.GroupBase):
	"""This class represents a list of EcucDestinationUriDefs."""
	def __init__(self):
		super().__init__()
		self._destinationUriDef_children=[]

class EcucDestinationUriPolicy(complexbase.GroupBase):
	"""The EcucDestinationUriPolicy describes the EcucContainerDef that will be targeted by EcucUriReferenceDefs. The type of the description is dependent of the destinationUriNestingContract attribute."""
	def __init__(self):
		super().__init__()
		self._container_children=[]
		self._destinationUriNestingContract_child=ModelNone
		self._parameter_children=[]
		self._reference_children=[]

class EcucEnumerationLiteralDef(complexbase.GroupBase):
	"""Configuration parameter type for enumeration literals definition."""
	def __init__(self):
		super().__init__()
		self._ecucCond_child=ModelNone
		self._origin_child=ModelNone

class EcucEnumerationParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Enumeration."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._literal_children=[]

class EcucFloatParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Float."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone

class EcucForeignReferenceDef(complexbase.GroupBase):
	"""Specify a reference to an XML description of an entity described in another AUTOSAR template."""
	def __init__(self):
		super().__init__()
		self._destinationType_child=ModelNone

class EcucFunctionNameDef(complexbase.GroupBase):
	"""Configuration parameter type for Function Names like those used to specify callback functions."""
	def __init__(self):
		super().__init__()
		self._ecucFunctionNameDefVariant_children=[]

class EcucFunctionNameDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucFunctionNameDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucImplementationConfigurationClass(complexbase.GroupBase):
	"""Specifies which ConfigurationClass this parameter has in the individual ConfigurationVariants.

This element is removed from the specifications and therefore it shall not be used."""
	def __init__(self):
		super().__init__()
		self._configClass_child=ModelNone
		self._configVariant_child=ModelNone

class EcucIndexableValue(complexbase.GroupBase):
	"""Used to support the specification of ordering of parameter values."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone

class EcucInstanceReferenceDef(complexbase.GroupBase):
	"""Specify a reference to an XML description of an entity described in another AUTOSAR template using the INSTANCE REFERENCE semantics."""
	def __init__(self):
		super().__init__()
		self._destinationContext_child=ModelNone
		self._destinationType_child=ModelNone

class EcucInstanceReferenceValue(complexbase.GroupBase):
	"""InstanceReference representation in the ECU Configuration."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucIntegerParamDef(complexbase.GroupBase):
	"""Configuration parameter type for Integer."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone

class EcucLinkerSymbolDef(complexbase.GroupBase):
	"""Configuration parameter type for Linker Symbol Names like those used to specify memory locations of variables and constants."""
	def __init__(self):
		super().__init__()
		self._ecucLinkerSymbolDefVariant_children=[]

class EcucLinkerSymbolDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucLinkerSymbolDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucModuleConfigurationValues(complexbase.GroupBase):
	"""Head of the configuration of one Module. A Module can be a BSW module as well as the RTE and ECU Infrastructure.

As part of the BSW module description, the EcucModuleConfigurationValues element has two different roles:

The recommendedConfiguration contains parameter values recommended by the BSW module vendor. 

The preconfiguredConfiguration contains values for those parameters which are fixed by the implementation and cannot be changed.

These two EcucModuleConfigurationValues are used when the base EcucModuleConfigurationValues (as part of the base ECU configuration) is created to fill parameters with initial values."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._ecucDefEdition_child=ModelNone
		self._implementationConfigVariant_child=ModelNone
		self._moduleDescription_child=ModelNone
		self._postBuildVariantUsed_child=ModelNone
		self._container_children=[]

class EcucModuleConfigurationValuesRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._EcucModuleConfigurationValues_child=ModelNone
		self._variationPoint_child=ModelNone

class EcucModuleDef(complexbase.GroupBase):
	"""Used as the top-level element for configuration definition for Software Modules, including BSW and RTE as well as ECU Infrastructure."""
	def __init__(self):
		super().__init__()
		self._apiServicePrefix_child=ModelNone
		self._postBuildVariantSupport_child=ModelNone
		self._refinedModuleDef_child=ModelNone
		self._supportedConfigVariant_children=[]
		self._container_children=[]

class EcucMultilineStringParamDef(complexbase.GroupBase):
	"""Configuration parameter type for multiline Strings (including \"carriage return\")."""
	def __init__(self):
		super().__init__()
		self._ecucMultilineStringParamDefVariant_children=[]

class EcucMultilineStringParamDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucMultilineStringParamDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucMultiplicityConfigurationClass(complexbase.GroupBase):
	"""Specifies the MultiplicityConfigurationClass of a parameter/reference or a container for each ConfigurationVariant of the EcucModuleDef."""

class EcucNumericalParamValue(complexbase.GroupBase):
	"""Holding the value which is subject to variant handling."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucParamConfContainerDef(complexbase.GroupBase):
	"""Used to define configuration containers that can hierarchically contain other containers and/or parameter definitions."""
	def __init__(self):
		super().__init__()
		self._multipleConfigurationContainer_child=ModelNone
		self._parameter_children=[]
		self._reference_children=[]
		self._subContainer_children=[]

class EcucParameterDef(complexbase.GroupBase):
	"""Abstract class used to define the similarities of all ECU Configuration Parameter types defined as subclasses."""
	def __init__(self):
		super().__init__()
		self._derivation_child=ModelNone
		self._symbolicNameValue_child=ModelNone
		self._withAuto_child=ModelNone

class EcucParameterDerivationFormula(complexbase.GroupBase):
	"""This formula is intended to specify how an ecu parameter can be derived from other information in the Autosar Templates."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_children=[]
		self._ecucQueryString_children=[]

class EcucParameterValue(complexbase.GroupBase):
	"""Common class to all types of configuration values."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._annotation_children=[]
		self._isAutoValue_child=ModelNone
		self._variationPoint_child=ModelNone

class EcucQuery(complexbase.GroupBase):
	"""Defines a query to the ECUC Description."""
	def __init__(self):
		super().__init__()
		self._ecucQueryExpression_child=ModelNone

class EcucQueryExpression(complexbase.GroupBase):
	"""Defines a query expression to the ECUC Description and output the result as an numerical value. Due to the \"mixedString\" nature of the formula there can be several EcuQueryExpressions used."""
	def __init__(self):
		super().__init__()
		self._configElementDefGlobal_children=[]
		self._configElementDefLocal_children=[]

class EcucReferenceDef(complexbase.GroupBase):
	"""Specify references within the ECU Configuration Description between parameter containers."""
	def __init__(self):
		super().__init__()
		self._destination_child=ModelNone

class EcucReferenceValue(complexbase.GroupBase):
	"""Used to represent a configuration value that has a parameter definition of type EcucAbstractReferenceDef (used for all of its specializations excluding EcucInstanceReferenceDef)."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucStringParamDef(complexbase.GroupBase):
	"""Configuration parameter type for String."""
	def __init__(self):
		super().__init__()
		self._ecucStringParamDefVariant_children=[]

class EcucStringParamDefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EcucStringParamDefContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EcucSymbolicNameReferenceDef(complexbase.GroupBase):
	"""This meta-class specifies that the implementation of the reference is done using a symbolic name defined by the referenced Container's shortName."""
	def __init__(self):
		super().__init__()
		self._destination_child=ModelNone

class EcucTextualParamValue(complexbase.GroupBase):
	"""Holding a value which is not subject to variation."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class EcucUriReferenceDef(complexbase.GroupBase):
	"""Definition of reference with a destination that is specified via a destinationUri. With such a reference it is possible to define a reference to a EcucContainerDef in a different module independent from the concrete definition of the target container."""
	def __init__(self):
		super().__init__()
		self._destinationUri_child=ModelNone

class EcucValidationCondition(complexbase.GroupBase):
	"""Validation condition to perform a formula calculation based on EcucQueries."""
	def __init__(self):
		super().__init__()
		self._ecucQuery_children=[]
		self._validationFormula_child=ModelNone

class EcucValueCollection(complexbase.GroupBase):
	"""This represents the anchor point of the ECU configuration description."""
	def __init__(self):
		super().__init__()
		self._ecuExtract_child=ModelNone
		self._ecucValue_children=[]

class EcucValueConfigurationClass(complexbase.GroupBase):
	"""Specifies the ValueConfigurationClass of a parameter/reference for each ConfigurationVariant of the EcucModuleDef."""

class EmphasisText(complexbase.GroupBase):
	"""This is an emphasized text. As a compromise it contains some rendering oriented attributes such as color and font."""
	color=complexbase.Attribute("color",SimpleTypes.String,'COLOR',False,"""This allows to recommend a color of the emphasis. It is specified bases on 6 digits RGB hex-code.""")
	font=complexbase.Attribute("font",SimpleTypes.EEnumFont,'FONT',False,"""This specifies the font style in which the emphasized text shall be rendered.""")
	type=complexbase.Attribute("type",SimpleTypes.EEnum,'TYPE',False,"""Indicates how the text may be emphasized. Note that this is only a proposal which can be overridden or ignored by particular formatting engines. Default is BOLD.""")
	def __init__(self):
		super().__init__()
		self._sub_children=[]
		self._sup_children=[]
		self._tt_children=[]

class EmptySignalMapping(complexbase.GroupBase):
	"""If no actual data is configured for a client server communication the element EmptySignalMapping shall be used. 
An EmptySignalMapping shall only reference a SystemSignal that is referenced by an ISignal with length equal to zero. In this case there shall be an \"update-bit\" configured. The EmptySignal can be mapped to the response group or to request group."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class End2EndEventProtectionProps(complexbase.GroupBase):
	"""This element allows to protect an event or a field notifier with an E2E profile."""
	def __init__(self):
		super().__init__()
		self._dataId_children=[]
		self._dataLength_child=ModelNone
		self._dataUpdatePeriod_child=ModelNone
		self._e2eProfileConfiguration_child=ModelNone
		self._event_child=ModelNone
		self._maxDataLength_child=ModelNone
		self._minDataLength_child=ModelNone

class End2EndMethodProtectionProps(complexbase.GroupBase):
	"""This element allows to protect a method, a field setter or a field getter with an E2E profile."""
	def __init__(self):
		super().__init__()
		self._dataId_children=[]
		self._dataLength_child=ModelNone
		self._dataUpdatePeriod_child=ModelNone
		self._e2eProfileConfiguration_child=ModelNone
		self._maxDataLength_child=ModelNone
		self._method_child=ModelNone
		self._minDataLength_child=ModelNone
		self._sourceId_child=ModelNone

class EndToEndDescription(complexbase.GroupBase):
	"""This meta-class contains information about end-to-end protection. The set of applicable attributes depends on the actual value of the category attribute of EndToEndProtection."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._dataId_children=[]
		self._dataIdMode_child=ModelNone
		self._dataLength_child=ModelNone
		self._maxDeltaCounterInit_child=ModelNone
		self._crcOffset_child=ModelNone
		self._counterOffset_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._dataIdNibbleOffset_child=ModelNone

class EndToEndProtection(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a particular end to end protection."""
	def __init__(self):
		super().__init__()
		self._endToEndProfile_child=ModelNone
		self._endToEndProtectionISignalIPdu_children=[]
		self._endToEndProtectionVariablePrototype_children=[]
		self._variationPoint_child=ModelNone

class EndToEndProtectionISignalIPdu(complexbase.GroupBase):
	"""It is possible to protect the inter-ECU data exchange of safety-related ISignalGroups at the level of COM IPdus using protection mechanisms provided by E2E Library. For each ISignalGroup to be protected, a separate EndToEndProtectionISignalIPdu element shall be created within the EndToEndProtectionSet. 

The EndToEndProtectionISignalIPdu element refers to the ISignalGroup that is to be protected and to the ISignalIPdu that transmits the protected ISignalGroup. The information how the referenced ISignalGroup shall be protected (through which E2E Profile and with which E2E settings) is defined in the EndToEndDescription element."""
	def __init__(self):
		super().__init__()
		self._dataOffset_child=ModelNone
		self._iSignalGroup_child=ModelNone
		self._iSignalIPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class EndToEndProtectionSet(complexbase.GroupBase):
	"""This represents a container for collection EndToEndProtectionInformation."""
	def __init__(self):
		super().__init__()
		self._endToEndProtection_children=[]

class EndToEndProtectionVariablePrototype(complexbase.GroupBase):
	"""It is possible to protect the data exchanged between software components. For this purpose, for each communication to be protected,  the user defines a separate EndToEndProtection (specifying a set of protection settings) and refers to a variableDataPrototype in the role of sender and to one or many variableDataPrototypes in the role of receiver. For details, see EndToEnd Library.

Caveat: The E2E wrapper approach involves technologies that are not subjected to the AUTOSAR standard and is superseded by the superior E2E transformer approach (which is fully standardized by AUTOSAR). Hence, new projects (without legacy constraints due to carry-over parts) shall use the fully standardized E2E transformer approach."""
	def __init__(self):
		super().__init__()
		self._receiver_children=[]
		self._sender_child=ModelNone
		self._shortLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class EndToEndTransformationComSpecProps(complexbase.GroupBase):
	"""The class EndToEndTransformationIComSpecProps specifies port specific
configuration properties for EndToEnd transformer attributes."""
	def __init__(self):
		super().__init__()
		self._clearFromValidToInvalid_child=ModelNone
		self._disableEndToEndCheck_child=ModelNone
		self._disableEndToEndStateMachine_child=ModelNone
		self._e2eProfileCompatibilityProps_child=ModelNone
		self._maxDeltaCounter_child=ModelNone
		self._maxErrorStateInit_child=ModelNone
		self._maxErrorStateInvalid_child=ModelNone
		self._maxErrorStateValid_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._minOkStateInit_child=ModelNone
		self._minOkStateInvalid_child=ModelNone
		self._minOkStateValid_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._windowSize_child=ModelNone
		self._windowSizeInit_child=ModelNone
		self._windowSizeInvalid_child=ModelNone
		self._windowSizeValid_child=ModelNone

class EndToEndTransformationDescription(complexbase.GroupBase):
	"""EndToEndTransformationDescription holds these attributes which are profile specific and have the same value for all E2E transformers."""
	def __init__(self):
		super().__init__()
		self._clearFromValidToInvalid_child=ModelNone
		self._counterOffset_child=ModelNone
		self._crcOffset_child=ModelNone
		self._dataIdMode_child=ModelNone
		self._dataIdNibbleOffset_child=ModelNone
		self._e2eProfileCompatibilityProps_child=ModelNone
		self._maxDeltaCounter_child=ModelNone
		self._maxErrorStateInit_child=ModelNone
		self._maxErrorStateInvalid_child=ModelNone
		self._maxErrorStateValid_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._minOkStateInit_child=ModelNone
		self._minOkStateInvalid_child=ModelNone
		self._minOkStateValid_child=ModelNone
		self._offset_child=ModelNone
		self._profileBehavior_child=ModelNone
		self._profileName_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._upperHeaderBitsToShift_child=ModelNone
		self._windowSize_child=ModelNone
		self._windowSizeInit_child=ModelNone
		self._windowSizeInvalid_child=ModelNone
		self._windowSizeValid_child=ModelNone

class EndToEndTransformationISignalProps(complexbase.GroupBase):
	"""Holds all the ISignal specific attributes for the EndToEndTransformer."""
	def __init__(self):
		super().__init__()
		self._endToEndTransformationISignalPropsVariant_children=[]
		self._dataId_children=[]
		self._dataLength_child=ModelNone
		self._maxDataLength_child=ModelNone
		self._minDataLength_child=ModelNone
		self._sourceId_child=ModelNone

class EndToEndTransformationISignalPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EndToEndTransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EngineeringObject(complexbase.GroupBase):
	"""This class specifies an engineering object. Usually such an object is represented by a file artifact. The properties of engineering object are such that the artifact can be found  by querying an ASAM catalog file.

The engineering object is uniquely identified by domain+category+shortLabel+revisionLabel."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._revisionLabel_children=[]
		self._domain_child=ModelNone

class EnterExitTimeout(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a pair of timeouts, one for entering, and one for exiting."""
	def __init__(self):
		super().__init__()
		self._enterTimeoutValue_child=ModelNone
		self._exitTimeoutValue_child=ModelNone

class Entry(complexbase.GroupBase):
	"""This represents one particular table cell."""
	align=complexbase.Attribute("align",SimpleTypes.AlignEnum,'ALIGN',False,"""Specifies how the cell ENTRY shall be horizontally aligned.  Default is \"LEFT\"""")
	bgcolor=complexbase.Attribute("bgcolor",SimpleTypes.String,'BGCOLOR',False,"""This allows to recommend a background color of the entry. It is specified bases on 6 digits RGB hex-code.""")
	colname=complexbase.Attribute("colname",SimpleTypes.String,'COLNAME',False,"""Indicate the name of the column, where the entry should appear.""")
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates whether a line should be displayed end of this entry.""")
	morerows=complexbase.Attribute("morerows",SimpleTypes.String,'MOREROWS',False,"""Number of additional rows. Default is \"0\"""")
	nameend=complexbase.Attribute("nameend",SimpleTypes.String,'NAMEEND',False,"""When an entry spans multiple column this is the name of the last column.""")
	namest=complexbase.Attribute("namest",SimpleTypes.String,'NAMEST',False,"""When an entry spans multiple column this is the name of the first column.""")
	rotate=complexbase.Attribute("rotate",SimpleTypes.String,'ROTATE',False,"""Indicates if the cellcontent shall be rotated. Default is 0; 1 would rotate the contents 90 degree counterclockwise. This attribute is defined by OASIS.""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates whether a line should be displayed at the bottom end of the cell.""")
	spanname=complexbase.Attribute("spanname",SimpleTypes.String,'SPANNAME',False,"""Capture the name of entry merging multiple columns.""")
	valign=complexbase.Attribute("valign",SimpleTypes.ValignEnum,'VALIGN',False,"""Indicates how the content of the cell shall be aligned. Default is inherited from row or tbody, otherwise \"TOP\"""")
	def __init__(self):
		super().__init__()
		self._bgcolorRemoved_child=ModelNone
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class EnumerationMappingEntry(complexbase.GroupBase):
	"""This class specifies the entry elements of the enumeration mapping table.

Note that this class might be used in the extended meta-model only."""
	def __init__(self):
		super().__init__()
		self._numericalValue_child=ModelNone
		self._enumeratorValue_child=ModelNone

class EnumerationMappingTable(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Enumeration attributes.

Note that this class might be used in the extended meta-model only."""
	def __init__(self):
		super().__init__()
		self._entry_children=[]

class EOCEventRef(complexbase.GroupBase):
	"""This is used to define a reference to an RTE or BSW Event."""
	def __init__(self):
		super().__init__()
		self._bswModuleInstance_child=ModelNone
		self._component_child=ModelNone
		self._event_child=ModelNone
		self._successor_children=[]

class EOCExecutableEntityRef(complexbase.GroupBase):
	"""This is used to define a reference to an ExecutableEntity

If the ExecutionOrderConstraint is defined on VFB, System or ECU level, a reference to the SwComponentPrototype, via the ComponentInCompositionInstanceRef, the referenced ExecutableEntity belongs to, shall be provided as context information."""
	def __init__(self):
		super().__init__()
		self._bswModuleInstance_child=ModelNone
		self._component_child=ModelNone
		self._executable_child=ModelNone
		self._successor_children=[]

class EOCExecutableEntityRefAbstract(complexbase.GroupBase):
	"""This is the abstractions for Execution Order Constraint Executable Entity References (leaves) and Execution Order Constraint Executable Entity Reference Groups (composites)."""
	def __init__(self):
		super().__init__()
		self._directSuccessor_children=[]

class EOCExecutableEntityRefGroup(complexbase.GroupBase):
	"""This is used to specify a group (composite) consisting of Execution Order Constraint Executable Entity References (leaves) and/or further Execution Order Constraint Executable Entity Reference Groups (composite)."""
	def __init__(self):
		super().__init__()
		self._letDataExchangeParadigm_child=ModelNone
		self._letInterval_children=[]
		self._maxCycleRepetitions_child=ModelNone
		self._maxCycles_child=ModelNone
		self._maxSlots_child=ModelNone
		self._maxSlotsPerCycle_child=ModelNone
		self._nestedElement_children=[]
		self._successor_children=[]
		self._triggeringEvent_child=ModelNone

class ErrorTracerNeeds(complexbase.GroupBase):
	"""Specifies the need to report failures to the error tracer."""
	def __init__(self):
		super().__init__()
		self._tracedFailure_children=[]

class EthGlobalTimeDomainProps(complexbase.GroupBase):
	"""Enables the definition of Ethernet Global Time specific properties."""
	def __init__(self):
		super().__init__()
		self._crcFlags_child=ModelNone
		self._destinationPhysicalAddress_child=ModelNone
		self._fupDataIDList_children=[]
		self._managedCouplingPort_children=[]
		self._messageCompliance_child=ModelNone
		self._vlanPriority_child=ModelNone

class EthGlobalTimeManagedCouplingPort(complexbase.GroupBase):
	"""Specifies a CouplingPort which is managed by an Ethernet Global Time Domain."""
	def __init__(self):
		super().__init__()
		self._couplingPort_child=ModelNone
		self._globalTimePortRole_child=ModelNone
		self._globalTimeTxPeriod_child=ModelNone
		self._pdelayLatencyThreshold_child=ModelNone
		self._pdelayRequestPeriod_child=ModelNone
		self._pdelayRespAndRespFollowUpTimeout_child=ModelNone
		self._pdelayResponseEnabled_child=ModelNone

class EthIpProps(complexbase.GroupBase):
	"""'''begin restrict to CP'''
This meta-class is used to configure the EcuInstance specific IP attributes.
'''end restrict to CP'''
'''begin restrict to AP'''
This meta-class is used to configure the Machine specific IP attributes.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._ipv4Props_child=ModelNone
		self._ipv6Props_child=ModelNone

class EthTSynCrcFlags(complexbase.GroupBase):
	"""Defines the fields of the message which shall be taken into account for CRC calculation and verification."""
	def __init__(self):
		super().__init__()
		self._crcCorrectionField_child=ModelNone
		self._crcDomainNumber_child=ModelNone
		self._crcMessageLength_child=ModelNone
		self._crcPreciseOriginTimestamp_child=ModelNone
		self._crcSequenceId_child=ModelNone
		self._crcSourcePortIdentity_child=ModelNone

class EthTSynSubTlvConfig(complexbase.GroupBase):
	"""Defines the subTLV fields which shall be included in the time sync message."""
	def __init__(self):
		super().__init__()
		self._ofsSubTlv_child=ModelNone
		self._statusSubTlv_child=ModelNone
		self._timeSubTlv_child=ModelNone
		self._userDataSubTlv_child=ModelNone

class EthTcpIpIcmpProps(complexbase.GroupBase):
	"""'''begin restrict to CP'''
This meta-class is used to configure the EcuInstance specific ICMP (Internet Control Message Protocol) attributes
'''end restrict to CP'''
'''begin restrict to AP'''
This meta-class is used to configure the Machine specific ICMP (Internet Control Message Protocol) attributes
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._icmpV4Props_child=ModelNone
		self._icmpV6Props_child=ModelNone

class EthTcpIpProps(complexbase.GroupBase):
	"""'''begin restrict to CP'''
This meta-class is used to configure the EcuInstance specific TcpIp Stack attributes.
'''end restrict to CP'''
'''begin restrict to AP'''
This meta-class is used to configure the Machine specific TcpIp Stack attributes.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._tcpProps_child=ModelNone
		self._udpProps_child=ModelNone

class EthTpConfig(complexbase.GroupBase):
	"""This element defines which PduTriggerings shall be handled using \"TP\" semantics."""
	def __init__(self):
		super().__init__()
		self._tpConnection_children=[]

class EthTpConnection(complexbase.GroupBase):
	"""A connection identifies which PduTriggerings shall be handled using the \"TP\" semantics."""
	def __init__(self):
		super().__init__()
		self._tpSdu_children=[]

class EthernetCluster(complexbase.GroupBase):
	"""Ethernet-specific cluster attributes."""
	def __init__(self):
		super().__init__()
		self._ethernetClusterVariant_children=[]
		self._couplingPortConnection_children=[]
		self._couplingPortStartupActiveTime_child=ModelNone
		self._couplingPortSwitchoffDelay_child=ModelNone
		self._macMulticastGroup_children=[]

class EthernetClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EthernetClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EthernetCommunicationConnector(complexbase.GroupBase):
	"""Ethernet specific attributes to the CommunicationConnector."""
	def __init__(self):
		super().__init__()
		self._apApplicationEndpoint_children=[]
		self._canXlProps_children=[]
		self._ethIpProps_child=ModelNone
		self._ipV6PathMtuEnabled_child=ModelNone
		self._ipV6PathMtuTimeout_child=ModelNone
		self._maximumTransmissionUnit_child=ModelNone
		self._neighborCacheSize_child=ModelNone
		self._networkEndpoint_children=[]
		self._pathMtuEnabled_child=ModelNone
		self._pathMtuTimeout_child=ModelNone
		self._pncFilterDataMask_child=ModelNone
		self._unicastNetworkEndpoint_children=[]

class EthernetCommunicationController(complexbase.GroupBase):
	"""Ethernet specific communication port attributes."""
	def __init__(self):
		super().__init__()
		self._ethernetCommunicationControllerVariant_children=[]
		self._canXlConfig_child=ModelNone
		self._couplingPort_children=[]
		self._macLayerType_child=ModelNone
		self._macUnicastAddress_child=ModelNone
		self._maximumReceiveBufferLength_child=ModelNone
		self._maximumTransmissionUnit_child=ModelNone
		self._maximumTransmitBufferLength_child=ModelNone
		self._slaveActAsPassiveCommunicationSlave_child=ModelNone
		self._slaveQualifiedUnexpectedLinkDownTime_child=ModelNone

class EthernetCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class EthernetCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class EthernetFrame(complexbase.GroupBase):
	"""Ethernet specific attributes to the Frame.

This element is removed and shall no longer be used. It is replaced by AbstractEthernetFrame."""

class EthernetFrameTriggering(complexbase.GroupBase):
	"""Ethernet specific Frame element."""

class EthernetPhysicalChannel(complexbase.GroupBase):
	"""The EthernetPhysicalChannel represents a VLAN or an untagged channel. 
An untagged channel is modeled as an EthernetPhysicalChannel without an aggregated VLAN."""
	def __init__(self):
		super().__init__()
		self._networkEndpoint_children=[]
		self._soAdConfig_child=ModelNone
		self._vlan_child=ModelNone

class EthernetPriorityRegeneration(complexbase.GroupBase):
	"""Defines a priority regeneration where the ingressPriority is replaced by regeneratedPriority.

The ethernetPriorityRegeneration is optional in case no priority regeneration shall be performed.

In case a ethernetPriorityRegeneration is defined it shall have 8 mappings, one for each priority."""
	def __init__(self):
		super().__init__()
		self._ingressPriority_child=ModelNone
		self._regeneratedPriority_child=ModelNone

class EthernetRawDataStreamClientMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a client PortPrototype to a Ethernet-based communication channel."""
	def __init__(self):
		super().__init__()
		self._remoteServerConfig_child=ModelNone

class EthernetRawDataStreamGrant(complexbase.GroupBase):
	"""This meta-class represents the ability to define the IAM configuration for a EthernetRawDataStream on deployment level."""
	def __init__(self):
		super().__init__()
		self._ethernetRawDataStreamMapping_child=ModelNone

class EthernetRawDataStreamLocalEndpointConfig(complexbase.GroupBase):
	"""This meta-class has the ability to act as a wrapper for the configuration of the remote endpoint in the context of a raw data stream mapping."""
	def __init__(self):
		super().__init__()
		self._localCommConnector_child=ModelNone
		self._localTcpPort_child=ModelNone
		self._localUdpPort_child=ModelNone

class EthernetRawDataStreamMapping(complexbase.GroupBase):
	"""This meta-class serves as the abstract bases class for the ability to map a PortPrototype to a Ethernet-based communication channel."""
	def __init__(self):
		super().__init__()
		self._localEndpointConfig_child=ModelNone
		self._socketOption_children=[]
		self._tlsSecureComProps_child=ModelNone

class EthernetRawDataStreamRemoteClientConfig(complexbase.GroupBase):
	"""This meta-class has the ability to act as a wrapper for the configuration of the remote server in the context of a raw data stream client mapping."""
	def __init__(self):
		super().__init__()
		self._multicastCredentials_child=ModelNone
		self._unicastUdpCredentials_child=ModelNone

class EthernetRawDataStreamRemoteServerConfig(complexbase.GroupBase):
	"""This meta-class has the ability to act as a wrapper for the configuration of the remote server in the context of a raw data stream client mapping."""
	def __init__(self):
		super().__init__()
		self._multicastCredentials_child=ModelNone
		self._unicastCredentials_child=ModelNone

class EthernetRawDataStreamServerMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a server PortPrototype to a Ethernet-based communication channel."""
	def __init__(self):
		super().__init__()
		self._remoteClientConfig_child=ModelNone

class EthernetWakeupSleepOnDatalineConfig(complexbase.GroupBase):
	"""EthernetWakeupSleepOnDatalineConfigSet is the main element that aggregates different config set regarding the wakeup and sleep on data line.

An EthernetWakeupSleepOnDatalineConfigSet could aggregate multiple different configurations regarding the wakeup and sleep on dataline (EthernetWakeupSleepOnDatalineConfig)."""
	def __init__(self):
		super().__init__()
		self._sleepModeExecutionDelay_child=ModelNone
		self._sleepRepetitionDelayOfSleepRequest_child=ModelNone
		self._sleepRepetitionsOfSleepRequest_child=ModelNone
		self._wakeupForwardLocalEnabled_child=ModelNone
		self._wakeupForwardRemoteEnabled_child=ModelNone
		self._wakeupLocalDetectionTime_child=ModelNone
		self._wakeupLocalDurationTime_child=ModelNone
		self._wakeupLocalEnabled_child=ModelNone
		self._wakeupRemoteEnabled_child=ModelNone
		self._wakeupRepetitionDelayOfWakeupRequest_child=ModelNone
		self._wakeupRepetitionsOfWakeupRequest_child=ModelNone

class EthernetWakeupSleepOnDatalineConfigSet(complexbase.GroupBase):
	"""This meta-class is the main element that aggregates different config set regarding the ethernet wakeup and sleep on data line."""
	def __init__(self):
		super().__init__()
		self._ethernetWakeupSleepOnDatalineConfig_children=[]

class EvaluatedVariantSet(complexbase.GroupBase):
	"""This meta class represents the ability to express if a set of ARElements is able to support one or more particular variants.

In other words, for a given set of evaluatedElements this meta class represents a table of evaluated variants, where each PredefinedVariant represents one column. In this column each descendant swSystemconstantValue resp. postbuildVariantCriterionValue represents one entry.

In a graphical representation each swSystemconstantValueSet / postBuildVariantCriterionValueSet could be used as an intermediate headline in the table column.

If the approvalStatus is \"APPROVED\" it expresses that the collection of CollectableElements is known be valid for the given evaluatedVariants.

Note that the EvaluatedVariantSet is a CollectableElement. This allows to establish a hierarchy of EvaluatedVariantSets."""
	def __init__(self):
		super().__init__()
		self._approvalStatus_child=ModelNone
		self._evaluatedElement_children=[]
		self._evaluatedVariant_children=[]

class EventControlledTiming(complexbase.GroupBase):
	"""Specification of a event driven sending behavior. The PDU is sent n
(numberOfRepeat + 1) times separated by the repetitionPeriod. If numberOfRepeats
= 0, then the Pdu is sent just once."""
	def __init__(self):
		super().__init__()
		self._numberOfRepetitions_child=ModelNone
		self._repetitionPeriod_child=ModelNone

class EventHandler(complexbase.GroupBase):
	"""This element represents an event group as part of the Provided Service Instance."""
	def __init__(self):
		super().__init__()
		self._applicationEndpoint_child=ModelNone
		self._consumedEventGroup_children=[]
		self._eventGroupIdentifier_child=ModelNone
		self._eventMulticastAddress_children=[]
		self._multicastThreshold_child=ModelNone
		self._pduActivationRoutingGroup_children=[]
		self._routingGroup_children=[]
		self._sdServerConfig_child=ModelNone
		self._sdServerEgTimingConfig_children=[]
		self._variationPoint_child=ModelNone

class EventInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._contextRPortPrototype_child=ModelNone
		self._targetEvent_child=ModelNone

class EventMapping(complexbase.GroupBase):
	"""Mapping of a VariableDataPrototype that is located in a SenderReceiverInterface to an Event that is located in a ServiceInterface."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._event_child=ModelNone

class EventObdReadinessGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to define the value of attribute eventObdReadinessGroup. It is only introduced to allow for a variant modeling of this attribute."""
	def __init__(self):
		super().__init__()
		self._eventObdReadinessGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class EventTriggeringConstraint(complexbase.GroupBase):
	"""Describes the occurrence behavior of the referenced timing event.

The occurrence behavior can only be determined when a mapping from the timing events to the implementation can be obtained. However, such an occurrence behavior can also be described by the modeler as an assumption or as a requirement about the occurrence of the event."""
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone

class ExclusiveArea(complexbase.GroupBase):
	"""Prevents an executable entity running in the area from being preempted."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class ExclusiveAreaNestingOrder(complexbase.GroupBase):
	"""This meta-class represents the ability to define a nesting order of ExclusiveAreas. A nesting order (that may occur in the executable code) is formally defined to be able to analyze the resource locking behavior."""
	def __init__(self):
		super().__init__()
		self._exclusiveArea_children=[]
		self._variationPoint_child=ModelNone

class ExclusiveAreaRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ExclusiveArea_child=ModelNone
		self._variationPoint_child=ModelNone

class Executable(complexbase.GroupBase):
	"""This meta-class represents an executable program."""
	def __init__(self):
		super().__init__()
		self._buildType_child=ModelNone
		self._implementationProps_children=[]
		self._minimumTimerGranularity_child=ModelNone
		self._reportingBehavior_child=ModelNone
		self._rootSwComponentPrototype_child=ModelNone
		self._traceSwitchConfiguration_children=[]
		self._version_child=ModelNone

class ExecutableEntity(complexbase.GroupBase):
	"""Abstraction of executable code."""
	def __init__(self):
		super().__init__()
		self._activationReason_children=[]
		self._canEnter_children=[]
		self._canEnterExclusiveArea_children=[]
		self._exclusiveAreaNestingOrder_children=[]
		self._minimumStartInterval_child=ModelNone
		self._reentrancyLevel_child=ModelNone
		self._runsInside_children=[]
		self._runsInsideExclusiveArea_children=[]
		self._swAddrMethod_child=ModelNone

class ExecutableEntityActivationReason(complexbase.GroupBase):
	"""This meta-class represents the ability to define the reason for the activation of the enclosing ExecutableEntity."""
	def __init__(self):
		super().__init__()
		self._bitPosition_child=ModelNone

class ExecutableImplementationProps(complexbase.GroupBase):
	"""This abstract class has the ability to act as a base class for classes that detail out the implementation-specific properties of an Executable."""

class ExecutableLoggingImplementationProps(complexbase.GroupBase):
	"""This meta-class contains configuration relevant for the implementation of an Executable used in the context of the LogAndTraceInstantiation."""
	def __init__(self):
		super().__init__()
		self._usesTimeBaseResource_child=ModelNone

class ExecutableTiming(complexbase.GroupBase):
	"""This meta-class represents the timing view for one or more executables."""
	def __init__(self):
		super().__init__()
		self._executable_children=[]

class ExecutionDependency(complexbase.GroupBase):
	"""This element defines a ProcessState in which a dependent process needs to be before the process that aggregates the ExecutionDependency element can be started."""
	def __init__(self):
		super().__init__()
		self._processState_child=ModelNone

class ExecutionOrderConstraint(complexbase.GroupBase):
	"""This constraint is used to restrict the order of execution for a set of [ARMetaClass{ExecutableEntity}]s. The ExecutionOrderConstraint can be used in any timing view.

The various scopes for ExecutionOrderConstraint are described below. Generally, each ExecutionOrderConstraint has a scope of software components and can reference all [ARMetaClass{ExecutableEntity}]s available in the corresponding internal behavior (RunnableEntity and BswModuleEntity) either directly or by the events activating respectively starting them (RteEvent and BswEvent).

On VFB level an ExecutionOrderConstraint can be specified for RunnableEntities part of the composition hierarchy referenced by the VfbTiming.

On SW-C level an ExecutionOrderConstraint can be specified for RunnableEntities part of the InternalBehavior referenced by the SwcTiming. 

On System level an ExecutionOrderConstraint can be specified for RunnableEntities part of the composition hierarchy of the system referenced by the SystemTiming.

On BSW Module level, an ExectionOrderConstraint can be specified for BswModuleEntities part of an BswInternalBehavior referenced by the BswModuleTiming.

On ECU level an ExecutionOrderConstraint can be specified for all [ARMetaClass{ExecutableEntity}]s and Events available via the EcucValueCollection, covering ECU Extract and BSW Module Configuration, referenced by the EcuTiming."""
	def __init__(self):
		super().__init__()
		self._baseComposition_child=ModelNone
		self._executionOrderConstraintType_child=ModelNone
		self._ignoreOrderAllowed_child=ModelNone
		self._isEvent_child=ModelNone
		self._orderedElement_children=[]
		self._permitMultipleReferencesToEE_child=ModelNone

class ExecutionTime(complexbase.GroupBase):
	"""Base class for several means how to describe the ExecutionTime of software. The required context information is provided through this class."""
	def __init__(self):
		super().__init__()
		self._exclusiveArea_child=ModelNone
		self._executableEntity_child=ModelNone
		self._hardwareConfiguration_child=ModelNone
		self._hwElement_child=ModelNone
		self._includedLibrary_children=[]
		self._memorySectionLocation_children=[]
		self._softwareContext_child=ModelNone
		self._variationPoint_child=ModelNone

class ExecutionTimeConstraint(complexbase.GroupBase):
	"""Constrains the execution time of the referenced [ARMetaClassRole{executable}{ExecutionTimeConstraint}] in [ARMetaClassRole{component}{ExecutionTimeConstraint}] between a [ARMetaClassRole{minimum}{ExecutionTimeConstraint}] and [ARMetaClassRole{maximum}{ExecutionTimeConstraint}] interval.

The time to execute the [ARMetaClassRole{executable}{ExecutionTimeConstraint}] including interruptions by other entities and including external calls is commonly called \"response time\". The TimingExtensions provide the concept of event chains and latency constraints for that purpose. An event chain from the start of the entity to the termination of the entity with according latency constraint represents a response time constraint for that executable entity."""
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone
		self._executable_child=ModelNone
		self._executionTimeType_child=ModelNone
		self._maximum_child=ModelNone
		self._minimum_child=ModelNone

class ExternalTriggerOccurredEvent(complexbase.GroupBase):
	"""This event is raised when the referenced Trigger has occurred."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class ExternalTriggeringPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns an ExternalTriggeringPoint it is entitled to raise an ExternalTriggerOccurredEvent."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._trigger_child=[]
		self._variationPoint_child=ModelNone

class ExternalTriggeringPointIdent(complexbase.GroupBase):
	"""This meta-class has been created to introduce the ability to become referenced into the meta-class ExternalTriggeringPoint without breaking backwards compatibility."""

class FibexElement(complexbase.GroupBase):
	"""ASAM FIBEX elements specifying Communication and Topology."""

class FibexElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._FibexElement_child=ModelNone
		self._variationPoint_child=ModelNone

class Field(complexbase.GroupBase):
	"""This meta-class represents the ability to define a piece of data that can be accessed with read and/or write semantics. It is also possible to generate a notification if the value of the data changes."""
	def __init__(self):
		super().__init__()
		self._hasGetter_child=ModelNone
		self._hasNotifier_child=ModelNone
		self._hasSetter_child=ModelNone
		self._variationPoint_child=ModelNone

class FieldInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._contextRPortPrototype_child=ModelNone
		self._targetField_child=ModelNone

class FieldMapping(complexbase.GroupBase):
	"""Mapping of a Field that is located in a ServiceInterface to ClientServerOperations that represent the getter and setter methods and to a VariableDataPrototype that represents the notifier in the Field."""
	def __init__(self):
		super().__init__()
		self._field_child=ModelNone
		self._getterOperation_child=ModelNone
		self._notifierDataElement_child=ModelNone
		self._setterOperation_child=ModelNone

class FieldSenderComSpec(complexbase.GroupBase):
	"""Port specific communication attributes for a Field that is defined in a ServiceInterface."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]

class FileInfoComment(complexbase.GroupBase):
	"""This class supports StructuredComment to provide auxiliary information with the goal to create a comment."""
	def __init__(self):
		super().__init__()
		self._sdg_children=[]

class FireAndForgetMethodMapping(complexbase.GroupBase):
	"""Mapping of a Fire&Forget Method that is located in a ServiceInterface to a VariableDataPrototype in a SenderReceiverInterface or to a Trigger in a TriggerInterface."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._method_child=ModelNone
		self._trigger_child=ModelNone

class FirewallRule(complexbase.GroupBase):
	"""Firewall Rule that defines the control information in individual packets."""
	def __init__(self):
		super().__init__()
		self._bucketSize_child=ModelNone
		self._dataLinkLayerRule_child=ModelNone
		self._ddsRule_child=ModelNone
		self._doIpRule_child=ModelNone
		self._networkLayerRule_child=[]
		self._payloadBytePatternRule_children=[]
		self._refillAmount_child=ModelNone
		self._someipRule_child=ModelNone
		self._someipSdRule_child=ModelNone
		self._transportLayerRule_child=[]

class FirewallRuleProps(complexbase.GroupBase):
	"""Firewall rule that is defined by an action that is performed if the referenced pattern matches."""
	def __init__(self):
		super().__init__()
		self._action_child=ModelNone
		self._matchingEgressRule_children=[]
		self._matchingIngressRule_children=[]
		self._matchingRule_children=[]

class FirewallStateInFirwallStateSwitchInterfaceInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class FirewallStateSwitchInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a PortInterface for interaction with the Firewall mode."""
	def __init__(self):
		super().__init__()
		self._firewallStateMachine_children=[]

class FlatInstanceDescriptor(complexbase.GroupBase):
	"""Represents exactly one node (e.g. a component instance or data element) of the instance tree of a software system. The purpose of this element is to map the various nested representations of this instance to a flat representation and assign a unique name (shortName) to it.

Use cases: 
* Specify unique names of measurable data to be used by MCD tools
* Specify unique names of calibration data to be used by MCD tool
* Specify a unique name for an instance of a component prototype in the ECU extract of the system description

Note that in addition it is possible to assign alias names via AliasNameAssignment."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._rtePluginProps_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._upstreamReference_child=ModelNone
		self._ecuExtractReference_child=ModelNone
		self._variationPoint_child=ModelNone

class FlatMap(complexbase.GroupBase):
	"""Contains a flat list of references to software objects. This list is used to identify instances and to resolve name conflicts. The scope is given by the RootSwCompositionPrototype for which it is used, i.e. it can be applied to a system, system extract or ECU-extract.

An instance of FlatMap may also be used in a preliminary context, e.g. in the scope of a software component before integration into a system. In this case it is not referred by a RootSwCompositionPrototype."""
	def __init__(self):
		super().__init__()
		self._instance_children=[]

class FlexrayAbsolutelyScheduledTiming(complexbase.GroupBase):
	"""Each frame in FlexRay is identified by its slot id and communication cycle. A description is provided by the usage of AbsolutelyScheduledTiming. 

In the static segment a frame can be sent multiple times within one communication cycle. For describing this case multiple AbsolutelyScheduledTimings have to be used. The main use case would be that a frame is sent twice within one communication cycle."""
	def __init__(self):
		super().__init__()
		self._communicationCycle_child=[]
		self._slotID_child=ModelNone

class FlexrayArTpChannel(complexbase.GroupBase):
	"""A channel is a group of connections sharing several properties.

The FlexRay AutosarTransport Layer supports several channels. These channels can work concurrently, thus each of them requires its own state machine and management data structures and its own PDU-IDs."""
	def __init__(self):
		super().__init__()
		self._ackType_child=ModelNone
		self._cancellation_child=ModelNone
		self._extendedAddressing_child=ModelNone
		self._flowControlPdu_child=ModelNone
		self._maxAr_child=ModelNone
		self._maxAs_child=ModelNone
		self._maxBs_child=ModelNone
		self._maxBufferRequest_child=ModelNone
		self._maxFcWait_child=ModelNone
		self._maxFrIf_child=ModelNone
		self._maxRetries_child=ModelNone
		self._maximumMessageLength_child=ModelNone
		self._minimumMulticastSeperationTime_child=ModelNone
		self._minimumSeparationTime_child=ModelNone
		self._multicastSegmentation_child=ModelNone
		self._nPdu_children=[]
		self._pduPool_children=[]
		self._timeBr_child=ModelNone
		self._timeBuffer_child=ModelNone
		self._timeCs_child=ModelNone
		self._timeFrIf_child=ModelNone
		self._timeoutAr_child=ModelNone
		self._timeoutAs_child=ModelNone
		self._timeoutBs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._tpConnection_children=[]
		self._transmitCancellation_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayArTpConfig(complexbase.GroupBase):
	"""This element defines exactly one FlexRay Autosar TP Configuration. 

One FlexrayArTpConfig element shall be created for each FlexRay Network in the System that uses FlexRay Autosar TP."""
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpChannel_children=[]
		self._tpNode_children=[]

class FlexrayArTpConnection(complexbase.GroupBase):
	"""A connection within a channel identifies the sender and the receiver of this particular communication.

The FlexRay Autosar Tp module routes a Pdu through this connection."""
	def __init__(self):
		super().__init__()
		self._connectionPrioPdus_child=ModelNone
		self._directTpSdu_child=ModelNone
		self._flowControlPdu_child=ModelNone
		self._multicast_child=ModelNone
		self._reversedTpSdu_child=ModelNone
		self._source_child=ModelNone
		self._target_children=[]
		self._transmitPdu_children=[]

class FlexrayArTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	def __init__(self):
		super().__init__()
		self._connector_children=[]
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayCluster(complexbase.GroupBase):
	"""FlexRay specific attributes to the physicalCluster"""
	def __init__(self):
		super().__init__()
		self._flexrayClusterVariant_children=[]
		self._actionPointOffset_child=ModelNone
		self._bit_child=ModelNone
		self._casRxLowMax_child=ModelNone
		self._coldStartAttempts_child=ModelNone
		self._cycle_child=ModelNone
		self._cycleCountMax_child=ModelNone
		self._detectNitError_child=ModelNone
		self._dynamicSlotIdlePhase_child=ModelNone
		self._ignoreAfterTx_child=ModelNone
		self._listenNoise_child=ModelNone
		self._macroPerCycle_child=ModelNone
		self._macrotickDuration_child=ModelNone
		self._maxWithoutClockCorrectionFatal_child=ModelNone
		self._maxWithoutClockCorrectionPassive_child=ModelNone
		self._minislotActionPointOffset_child=ModelNone
		self._minislotDuration_child=ModelNone
		self._networkIdleTime_child=ModelNone
		self._networkManagementVectorLength_child=ModelNone
		self._numberOfMinislots_child=ModelNone
		self._numberOfStaticSlots_child=ModelNone
		self._offsetCorrectionStart_child=ModelNone
		self._payloadLengthStatic_child=ModelNone
		self._safetyMargin_child=ModelNone
		self._sampleClockPeriod_child=ModelNone
		self._staticSlotDuration_child=ModelNone
		self._symbolWindow_child=ModelNone
		self._symbolWindowActionPointOffset_child=ModelNone
		self._syncFrameIdCountMax_child=ModelNone
		self._tranceiverStandbyDelay_child=ModelNone
		self._transmissionStartSequenceDuration_child=ModelNone
		self._wakeupRxIdle_child=ModelNone
		self._wakeupRxLow_child=ModelNone
		self._wakeupRxWindow_child=ModelNone
		self._wakeupTxActive_child=ModelNone
		self._wakeupTxIdle_child=ModelNone

class FlexrayClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class FlexrayClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class FlexrayCommunicationConnector(complexbase.GroupBase):
	"""FlexRay specific attributes to the CommunicationConnector"""
	def __init__(self):
		super().__init__()
		self._nmReadySleepTime_child=ModelNone
		self._pncFilterDataMask_child=ModelNone
		self._wakeUpChannel_child=ModelNone

class FlexrayCommunicationController(complexbase.GroupBase):
	"""FlexRay bus specific communication port attributes."""
	def __init__(self):
		super().__init__()
		self._flexrayCommunicationControllerVariant_children=[]
		self._acceptedStartupRange_child=ModelNone
		self._allowHaltDueToClock_child=ModelNone
		self._allowPassiveToActive_child=ModelNone
		self._clusterDriftDamping_child=ModelNone
		self._decodingCorrection_child=ModelNone
		self._delayCompensationA_child=ModelNone
		self._delayCompensationB_child=ModelNone
		self._externOffsetCorrection_child=ModelNone
		self._externRateCorrection_child=ModelNone
		self._externalSync_child=ModelNone
		self._fallBackInternal_child=ModelNone
		self._flexrayFifo_children=[]
		self._keySlotID_child=ModelNone
		self._keySlotOnlyEnabled_child=ModelNone
		self._keySlotUsedForStartUp_child=ModelNone
		self._keySlotUsedForSync_child=ModelNone
		self._latestTX_child=ModelNone
		self._listenTimeout_child=ModelNone
		self._macroInitialOffsetA_child=ModelNone
		self._macroInitialOffsetB_child=ModelNone
		self._maximumDynamicPayloadLength_child=ModelNone
		self._microInitialOffsetA_child=ModelNone
		self._microInitialOffsetB_child=ModelNone
		self._microPerCycle_child=ModelNone
		self._microtickDuration_child=ModelNone
		self._nmVectorEarlyUpdate_child=ModelNone
		self._offsetCorrectionOut_child=ModelNone
		self._rateCorrectionOut_child=ModelNone
		self._samplesPerMicrotick_child=ModelNone
		self._secondKeySlotId_child=ModelNone
		self._twoKeySlotMode_child=ModelNone
		self._wakeUpPattern_child=ModelNone

class FlexrayCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class FlexrayCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class FlexrayFifoConfiguration(complexbase.GroupBase):
	"""One First In First Out (FIFO) queued receive structure, defining the admittance criteria to the FIFO, and mandating the ability to admit messages into the FIFO based on Message Id filtering criteria."""
	def __init__(self):
		super().__init__()
		self._admitWithoutMessageId_child=ModelNone
		self._baseCycle_child=ModelNone
		self._channel_child=ModelNone
		self._cycleRepetition_child=ModelNone
		self._fifoDepth_child=ModelNone
		self._fifoRange_children=[]
		self._msgIdMask_child=ModelNone
		self._msgIdMatch_child=ModelNone

class FlexrayFifoRange(complexbase.GroupBase):
	"""FIFO Frame Id range acceptance criteria."""
	def __init__(self):
		super().__init__()
		self._rangeMax_child=ModelNone
		self._rangeMin_child=ModelNone

class FlexrayFrame(complexbase.GroupBase):
	"""FlexRay specific Frame element."""

class FlexrayFrameTriggering(complexbase.GroupBase):
	"""FlexRay specific attributes to the FrameTriggering"""
	def __init__(self):
		super().__init__()
		self._absolutelyScheduledTiming_children=[]
		self._allowDynamicLSduLength_child=ModelNone
		self._messageId_child=ModelNone
		self._payloadPreambleIndicator_child=ModelNone

class FlexrayNmCluster(complexbase.GroupBase):
	"""FlexRay specific NM cluster attributes."""
	def __init__(self):
		super().__init__()
		self._nmCarWakeUpBitPosition_child=ModelNone
		self._nmCarWakeUpFilterEnabled_child=ModelNone
		self._nmCarWakeUpFilterNodeId_child=ModelNone
		self._nmCarWakeUpRxEnabled_child=ModelNone
		self._nmControlBitVectorActive_child=ModelNone
		self._nmDataCycle_child=ModelNone
		self._nmDataEnabled_child=ModelNone
		self._nmDetectionLock_child=ModelNone
		self._nmMainFunctionPeriod_child=ModelNone
		self._nmMessageTimeoutTime_child=ModelNone
		self._nmReadySleepCount_child=ModelNone
		self._nmRemoteSleepIndicationTime_child=ModelNone
		self._nmRepeatMessageBitActive_child=ModelNone
		self._nmRepeatMessageTime_child=ModelNone
		self._nmRepetitionCycle_child=ModelNone
		self._nmVotingCycle_child=ModelNone

class FlexrayNmClusterCoupling(complexbase.GroupBase):
	"""FlexRay attributes that are valid for each of the referenced (coupled) FlexRay clusters."""
	def __init__(self):
		super().__init__()
		self._coupledCluster_children=[]
		self._nmControlBitVectorEnabled_child=ModelNone
		self._nmDataDisabled_child=ModelNone
		self._nmScheduleVariant_child=ModelNone

class FlexrayNmEcu(complexbase.GroupBase):
	"""FlexRay specific attributes."""
	def __init__(self):
		super().__init__()
		self._nmHwVoteEnabled_child=ModelNone
		self._nmMainFunctionAcrossFrCycle_child=ModelNone
		self._nmRepeatMessageBitEnable_child=ModelNone

class FlexrayNmNode(complexbase.GroupBase):
	"""FlexRay specific NM Node attributes."""
	def __init__(self):
		super().__init__()
		self._nmInstanceId_child=ModelNone

class FlexrayPhysicalChannel(complexbase.GroupBase):
	"""FlexRay specific attributes to the physicalChannel"""
	def __init__(self):
		super().__init__()
		self._channelName_child=ModelNone

class FlexrayTpConfig(complexbase.GroupBase):
	"""This element defines exactly one FlexRay ISO TP Configuration. 

One FlexRayTpConfig element shall be created for each FlexRay Network in the System that uses FlexRay Iso Tp."""
	def __init__(self):
		super().__init__()
		self._pduPool_children=[]
		self._tpAddress_children=[]
		self._tpConnection_children=[]
		self._tpConnectionControl_children=[]
		self._tpEcu_children=[]
		self._tpNode_children=[]

class FlexrayTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The FlexRayTp module routes a Pdu through this connection. 

In a System Description the references to the PduPools are mandatory. In an ECU Extract these references can be optional:
On unicast connections these references are always mandatory.
On multicast the txPduPool is mandatory on the sender side. The rxPduPool is mandatory on the receiver side. On Gateway ECUs both references are mandatory."""
	def __init__(self):
		super().__init__()
		self._bandwidthLimitation_child=ModelNone
		self._directTpSdu_child=ModelNone
		self._multicast_child=ModelNone
		self._receiver_children=[]
		self._reversedTpSdu_child=ModelNone
		self._rxPduPool_child=ModelNone
		self._tpConnectionControl_child=ModelNone
		self._transmitter_child=ModelNone
		self._txPduPool_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpConnectionControl(complexbase.GroupBase):
	"""Configuration parameters to control a FlexRay TP connection."""
	def __init__(self):
		super().__init__()
		self._ackType_child=ModelNone
		self._maxAr_child=ModelNone
		self._maxAs_child=ModelNone
		self._maxBufferSize_child=ModelNone
		self._maxFcWait_child=ModelNone
		self._maxFrIf_child=ModelNone
		self._maxNumberOfNpduPerCycle_child=ModelNone
		self._maxRetries_child=ModelNone
		self._separationCycleExponent_child=ModelNone
		self._timeBr_child=ModelNone
		self._timeBuffer_child=ModelNone
		self._timeCs_child=ModelNone
		self._timeFrIf_child=ModelNone
		self._timeoutAr_child=ModelNone
		self._timeoutAs_child=ModelNone
		self._timeoutBr_child=ModelNone
		self._timeoutBs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._timeoutCs_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpEcu(complexbase.GroupBase):
	"""ECU specific TP configuration parameters. Each TpEcu element has a reference to exactly one ECUInstance in the topology."""
	def __init__(self):
		super().__init__()
		self._cancellation_child=ModelNone
		self._cycleTimeMainFunction_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._fullDuplexEnabled_child=ModelNone
		self._transmitCancellation_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	def __init__(self):
		super().__init__()
		self._connector_children=[]
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class FlexrayTpPduPool(complexbase.GroupBase):
	"""FlexrayTpPduPool is a set of N-PDUs which are defined for FrTp sending or receiving purpose."""
	def __init__(self):
		super().__init__()
		self._nPdu_children=[]
		self._variationPoint_child=ModelNone

class FloatValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Float attributes.

Note that this class might be used in the extended meta-model only"""

class FMAttributeDef(complexbase.GroupBase):
	"""This metaclass represents the ability to define attributes for a feature."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._max_child=ModelNone
		self._min_child=ModelNone

class FMAttributeValue(complexbase.GroupBase):
	"""This defines a value for the attribute that is referred to in the role definition."""
	def __init__(self):
		super().__init__()
		self._definition_child=ModelNone
		self._value_child=ModelNone

class FMConditionByFeaturesAndAttributes(complexbase.GroupBase):
	"""A boolean expression that has the syntax of the AUTOSAR formula language but uses only references to features or feature attributes (not system constants) as operands."""

class FMConditionByFeaturesAndSwSystemconsts(complexbase.GroupBase):
	"""A boolean expression that has the syntax of the AUTOSAR formula language and may use references to features or system constants as operands."""

class FMFeature(complexbase.GroupBase):
	"""A FMFeature describes an essential characteristic of a product. Each FMFeature is contained in exactly one FMFeatureModel."""
	def __init__(self):
		super().__init__()
		self._attributeDef_children=[]
		self._decomposition_children=[]
		self._maximumIntendedBindingTime_child=ModelNone
		self._minimumIntendedBindingTime_child=ModelNone
		self._relation_children=[]
		self._restriction_children=[]

class FMFeatureDecomposition(complexbase.GroupBase):
	"""A FMFeatureDecomposition describes dependencies between a list of
features and their parent feature (i.e., the FMFeature that
aggregates the FMFeatureDecomposition). The kind of dependency is
defined by the attribute category."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._feature_children=[]
		self._max_child=ModelNone
		self._min_child=ModelNone

class FMFeatureMap(complexbase.GroupBase):
	"""A FMFeatureMap associates FMFeatures with variation points in the
AUTOSAR model. To do this, it defines value sets for system
constants and postbuild variant criterions that shall be chosen 
whenever a certain combination of features (and system constants) is encountered."""
	def __init__(self):
		super().__init__()
		self._mapping_children=[]

class FMFeatureMapAssertion(complexbase.GroupBase):
	"""Defines a boolean expression which shall evaluate to true for this mapping to become active. The expression is a formula that is based on features and system constants, and is defined by fmSyscond."""
	def __init__(self):
		super().__init__()
		self._fmSyscond_child=ModelNone

class FMFeatureMapCondition(complexbase.GroupBase):
	"""Defines a condition which needs to be fulfilled for this mapping to become active. The condition is implemented as formula that is based on features and attributes and is defined by fmCond."""
	def __init__(self):
		super().__init__()
		self._fmCond_child=ModelNone

class FMFeatureMapElement(complexbase.GroupBase):
	"""Defines value sets for system constants and postbuild variant
criterions that shall be chosen whenever a certain combination
of features (and system constants) is encountered."""
	def __init__(self):
		super().__init__()
		self._assertion_children=[]
		self._condition_children=[]
		self._postBuildVariantCriterionValueSet_children=[]
		self._swSystemconstantValueSet_children=[]

class FMFeatureModel(complexbase.GroupBase):
	"""A Feature model describes the features of a product line and their dependencies. Feature models are an optional part of an AUTOSAR model."""
	def __init__(self):
		super().__init__()
		self._feature_children=[]
		self._root_child=ModelNone

class FMFeatureRelation(complexbase.GroupBase):
	"""Defines relations for FMFeatures, for example dependencies on
other FMFeatures, or conflicts with other FMFeatures. A FMFeature
can only be part of a FMFeatureSelectionSet if all its
relations are fulfilled."""
	def __init__(self):
		super().__init__()
		self._feature_children=[]
		self._restriction_child=ModelNone

class FMFeatureRestriction(complexbase.GroupBase):
	"""Defines restrictions for FMFeatures. A FMFeature can only be part
of a FMFeatureSelectionSet if at least one of its restrictions evaluate to true."""
	def __init__(self):
		super().__init__()
		self._restriction_child=ModelNone

class FMFeatureSelection(complexbase.GroupBase):
	"""A FMFeatureSelection represents the state of a particular
FMFeature within a FMFeatureSelectionSet."""
	def __init__(self):
		super().__init__()
		self._feature_child=ModelNone
		self._state_child=ModelNone
		self._minimumSelectedBindingTime_child=ModelNone
		self._maximumSelectedBindingTime_child=ModelNone
		self._attributeValue_children=[]

class FMFeatureSelectionSet(complexbase.GroupBase):
	"""A FMFeatureSelectionSet is a set of FMFeatures that describes a specific product."""
	def __init__(self):
		super().__init__()
		self._featureModel_children=[]
		self._include_children=[]
		self._selection_children=[]

class FMFormulaByFeaturesAndAttributes(complexbase.GroupBase):
	"""An expression that has the syntax of the AUTOSAR formula language but uses only references to features or feature attributes (not system constants) as operands."""
	def __init__(self):
		super().__init__()
		self._attribute_children=[]
		self._feature_children=[]

class FMFormulaByFeaturesAndSwSystemconsts(complexbase.GroupBase):
	"""An expression that has the syntax of the AUTOSAR formula language and may use references to features or system constants as operands."""
	def __init__(self):
		super().__init__()
		self._feature_children=[]

class ForbiddenSignalPath(complexbase.GroupBase):
	"""The ForbiddenSignalPath describes the physical channels which an element shall not take in the topology. Such a signal path can be a constraint for the communication matrix,  because such a path has an effect on the frame generation and the frame path."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._physicalChannel_children=[]
		self._signal_children=[]

class FormulaExpression(complexbase.GroupBase):
	"""This class represents the syntax of the formula language. The class is modeled as an abstract class in order to be specialized into particular use cases. For each use case the referable objects might be specified in the specialization."""

class FrGlobalTimeDomainProps(complexbase.GroupBase):
	"""Enables the definition of Flexray GlobalTime specific properties."""
	def __init__(self):
		super().__init__()
		self._ofsDataIDList_children=[]
		self._syncDataIDList_children=[]

class Frame(complexbase.GroupBase):
	"""Data frame which is sent over a communication medium. This element describes the pure Layout of a frame sent on a channel."""
	def __init__(self):
		super().__init__()
		self._frameLength_child=ModelNone
		self._pduToFrameMapping_children=[]

class FrameMapping(complexbase.GroupBase):
	"""The entire source frame is mapped as it is onto the target frame (what in general is only possible inside of a common platform). In this case source and target frame should be the identical object.

Each pair consists in a SOURCE and a TARGET referencing to a FrameTriggering.

The Frame Mapping is not supported by the Autosar BSW. The existence is optional and has been incorporated into the System Template mainly for compatibility in order to allow interchange between FIBEX and AUTOSAR descriptions."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._sourceFrame_child=ModelNone
		self._targetFrame_child=ModelNone
		self._variationPoint_child=ModelNone

class FramePid(complexbase.GroupBase):
	"""Frame_PIDs that are included in the request. The \"pid\" attribute describes the value and the \"index\" attribute the position of the frame_PID in the request."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone
		self._pid_child=ModelNone

class FramePort(complexbase.GroupBase):
	"""Connectors reception or send port on the referenced channel referenced by a FrameTriggering."""

class FrameTriggering(complexbase.GroupBase):
	"""The FrameTriggering describes the instance of a frame sent on a channel and defines the manner of triggering (timing information) and identification of a frame on the channel, on which it is sent.

For the same frame, if FrameTriggerings exist on more than one channel of the same cluster the fan-out/in is handled by the Bus interface."""
	def __init__(self):
		super().__init__()
		self._framePort_children=[]
		self._frame_child=ModelNone
		self._pduTriggering_children=[]
		self._variationPoint_child=ModelNone

class FreeFormat(complexbase.GroupBase):
	"""Representing freely defined data."""
	def __init__(self):
		super().__init__()
		self._byteValue_children=[]

class FreeFormatEntry(complexbase.GroupBase):
	"""FreeFormat transmits a fixed master request frame with the eight data bytes provided. This may for instance be used to issue user specific fixed frames."""

class FunctionGroupPhmStateReference(complexbase.GroupBase):
	"""Function Group state dependency."""
	def __init__(self):
		super().__init__()
		self._functionGroupState_child=ModelNone

class FunctionGroupSet(complexbase.GroupBase):
	"""This meta-class provides the ability to create arbitrary collections of function groups."""
	def __init__(self):
		super().__init__()
		self._functionGroup_children=[]

class FunctionGroupStateInFunctionGroupSetInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class FunctionInhibitionAvailabilityNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Function Inhibition Manager to provide the control function for one Function Identifier (FID)."""
	def __init__(self):
		super().__init__()
		self._controlledFid_child=ModelNone

class FunctionInhibitionNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Function Inhibition Manager for one Function Identifier (FID). This class currently contains no attributes. Its name can be regarded as a symbol identifying the FID  from the viewpoint of the component or module which owns this class."""

class FunctionalClusterInteractsWithFunctionalClusterMapping(complexbase.GroupBase):
	"""This meta-class identifies a relation between functional clusters on the adaptive platform such one functional cluster can call APIs of the other functional cluster."""

class FunctionalClusterInteractsWithPersistencyDeploymentMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between any functional cluster modeled as a subclass of NonOsModuleInstantiation and a PersistencyDeployment."""
	def __init__(self):
		super().__init__()
		self._contractVersion_child=ModelNone
		self._functionalCluster_child=ModelNone
		self._maxNumberOfFiles_child=ModelNone
		self._persistencyAccess_child=ModelNone
		self._persistencyDeployment_child=ModelNone
		self._process_child=ModelNone

class FunctionalClusterToSecurityEventDefinitionMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between the SecurityEventDefinition and the Module Instantiation that reports the security event."""
	def __init__(self):
		super().__init__()
		self._moduleInstantiation_child=ModelNone
		self._securityEvent_child=ModelNone

class FurtherActionByteNeeds(complexbase.GroupBase):
	"""The FurtherActionByteNeeds indicates that the software-component is able to provide the \"further action byte\" to the DoIp Service Component."""

class Gateway(complexbase.GroupBase):
	"""A gateway is an ECU that is connected to two or more clusters (channels, but not redundant), and performs a frame, Pdu or signal mapping between them."""
	def __init__(self):
		super().__init__()
		self._ecu_child=ModelNone
		self._frameMapping_children=[]
		self._iPduMapping_children=[]
		self._signalMapping_children=[]

class GeneralAnnotation(complexbase.GroupBase):
	"""This class represents textual comments (called annotations) which relate to the object in which it is aggregated. These annotations are intended for use during the development process for transferring information from one step of the development process to the next one. 

The approach is similar to the \"yellow pads\" ...

This abstract class can be specialized in order to add some further formal properties."""
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._annotationOrigin_child=ModelNone
		self._annotationText_child=ModelNone

class GeneralParameter(complexbase.GroupBase):
	"""This represents a parameter in general e.g. an entry in a data sheet."""
	def __init__(self):
		super().__init__()
		self._prmChar_children=[]

class GeneralPurposeConnection(complexbase.GroupBase):
	"""This meta-class allows to describe the relationship between several PduTriggerings that are defined on the same PhysicalChannel, e.g. to create a link between Rx and Tx Pdu that are used for request/response."""
	def __init__(self):
		super().__init__()
		self._pduTriggering_children=[]

class GeneralPurposeIPdu(complexbase.GroupBase):
	"""This element is used for AUTOSAR Pdus without attributes that are routed by the PduR. Please note that the category name of such Pdus is standardized in the AUTOSAR System Template."""

class GeneralPurposePdu(complexbase.GroupBase):
	"""This element is used for AUTOSAR Pdus without additional attributes that are routed by a bus interface. Please note that the category name of such Pdus is standardized in the AUTOSAR System Template."""

class GenericEthernetFrame(complexbase.GroupBase):
	"""This element is used for EthernetFrames without additional attributes that are routed by the EthIf."""

class GenericModelReference(complexbase.GroupBase):
	"""This meta-class represents the ability to express a late binding reference to a model element. The model element can be from every model. Even if it is modeled according to the association representation, it is not limited to refer to AUTOSAR model elements."""
	base=complexbase.Attribute("base",SimpleTypes.NameToken,'BASE',False,"""This establishes the reference base.""")
	dest=complexbase.Attribute("dest",SimpleTypes.NameToken,'DEST',False,"""This attribute represents the class of the referenced model element. It is a String, since the model element can be in any model. Therefore we cannot have any assumption here.""")
	def __init__(self):
		super().__init__()
		self._ref_child=ModelNone

class GenericModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the generic module configuration on a specific machine. Different modules are distinguishable by the category attribute. 
This element can also be used to describe modules that are not standardized by AUTOSAR."""

class GenericTp(complexbase.GroupBase):
	"""Content Model for a generic transport protocol."""
	def __init__(self):
		super().__init__()
		self._tpAddress_child=ModelNone
		self._tpTechnology_child=ModelNone

class GlobalSupervision(complexbase.GroupBase):
	"""This element defines a collection of AliveSupervisions, DeadlineSupervisions, and LogicalSupervisions in order to provide an aggregated supervision state."""
	def __init__(self):
		super().__init__()
		self._aliveSupervision_children=[]
		self._deadlineSupervision_children=[]
		self._logicalSupervision_children=[]
		self._noCheckpointSupervision_children=[]
		self._noSupervision_children=[]
		self._supervisionMode_children=[]
		self._transition_children=[]

class GlobalSupervisionNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Watchdog Manager to get access on the Global Supervision control and status interface."""

class GlobalTimeCanMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for the CAN communication."""
	def __init__(self):
		super().__init__()
		self._crcSecured_child=ModelNone
		self._followUpOffset_child=ModelNone
		self._syncConfirmationTimeout_child=ModelNone

class GlobalTimeCanSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for the CAN communication."""
	def __init__(self):
		super().__init__()
		self._crcValidated_child=ModelNone
		self._sequenceCounterJumpWidth_child=ModelNone

class GlobalTimeCorrectionProps(complexbase.GroupBase):
	"""This meta-class defines the attributes for rate and offset correction."""
	def __init__(self):
		super().__init__()
		self._offsetCorrectionAdaptionInterval_child=ModelNone
		self._offsetCorrectionJumpThreshold_child=ModelNone
		self._rateCorrectionMeasurementDuration_child=ModelNone
		self._rateCorrectionsPerMeasurementDuration_child=ModelNone

class GlobalTimeCouplingPortProps(complexbase.GroupBase):
	"""Defines properties for the usage of the CouplingPort in the scope of Global Time Sync."""
	def __init__(self):
		super().__init__()
		self._propagationDelay_child=ModelNone

class GlobalTimeDomain(complexbase.GroupBase):
	"""This represents the ability to define a global time domain."""
	def __init__(self):
		super().__init__()
		self._communicationCluster_children=[]
		self._debounceTime_child=ModelNone
		self._domainId_child=ModelNone
		self._followUpTimeoutValue_child=ModelNone
		self._gateway_children=[]
		self._globalTimeCorrectionProps_child=ModelNone
		self._globalTimeDomainProperty_children=[]
		self._globalTimeDomainProps_child=[]
		self._globalTimeMaster_children=[]
		self._globalTimePdu_child=ModelNone
		self._globalTimePduTriggering_child=ModelNone
		self._globalTimeSubDomain_children=[]
		self._master_child=[]
		self._networkSegmentId_child=ModelNone
		self._offsetTimeDomain_child=ModelNone
		self._pduTriggering_children=[]
		self._slave_children=[]
		self._subDomain_children=[]
		self._syncLossThreshold_child=ModelNone
		self._syncLossTimeout_child=ModelNone

class GlobalTimeDomainRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._GlobalTimeDomain_child=ModelNone
		self._variationPoint_child=ModelNone

class GlobalTimeEthMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for Ethernet communication."""
	def __init__(self):
		super().__init__()
		self._crcSecured_child=ModelNone
		self._holdOverTime_child=ModelNone
		self._subTlvConfig_child=ModelNone

class GlobalTimeEthSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for Ethernet communication."""
	def __init__(self):
		super().__init__()
		self._crcValidated_child=ModelNone
		self._timeHardwareCorrectionThreshold_child=ModelNone

class GlobalTimeFrMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for Flexray communication."""
	def __init__(self):
		super().__init__()
		self._crcSecured_child=ModelNone

class GlobalTimeFrSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for Flexray communication."""
	def __init__(self):
		super().__init__()
		self._crcValidated_child=ModelNone
		self._sequenceCounterJumpWidth_child=ModelNone

class GlobalTimeGateway(complexbase.GroupBase):
	"""This represents the ability to define a time gateway for establishing a global time domain over several communication clusters."""
	def __init__(self):
		super().__init__()
		self._host_child=ModelNone
		self._master_child=ModelNone
		self._slave_child=ModelNone
		self._variationPoint_child=ModelNone

class GlobalTimeMaster(complexbase.GroupBase):
	"""This represents the generic concept of a global time master."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._icvSecured_child=ModelNone
		self._immediateResumeTime_child=ModelNone
		self._isSystemWideGlobalTimeMaster_child=ModelNone
		self._syncPeriod_child=ModelNone
		self._variationPoint_child=ModelNone

class GlobalTimeSlave(complexbase.GroupBase):
	"""This represents the generic concept of a global time slave."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._followUpTimeoutValue_child=ModelNone
		self._icvVerification_child=ModelNone
		self._timeLeapFutureThreshold_child=ModelNone
		self._timeLeapHealingCounter_child=ModelNone
		self._timeLeapPastThreshold_child=ModelNone
		self._variationPoint_child=ModelNone

class Grant(complexbase.GroupBase):
	"""This meta-class serves as the abstract base class for defining specific Grants"""

class GrantDesign(complexbase.GroupBase):
	"""This meta-class serves as an abstract base class for the description of grants on design level."""
	def __init__(self):
		super().__init__()
		self._processDesign_child=ModelNone

class Graphic(complexbase.GroupBase):
	"""This class represents an artifact containing the image to be inserted in the document"""
	editHeight=complexbase.Attribute("editHeight",SimpleTypes.String,'EDIT-HEIGHT',False,"""Specifies the height of the graphic when it is displayed in an editor. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	editWidth=complexbase.Attribute("editWidth",SimpleTypes.String,'EDIT-WIDTH',False,"""Specifies the width of the graphic when it is displayed in an editor. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	editfit=complexbase.Attribute("editfit",SimpleTypes.GraphicFitEnum,'EDITFIT',False,"""Specifies how the graphic shall be displayed in an editor. If the attribute is missing,""")
	editscale=complexbase.Attribute("editscale",SimpleTypes.String,'EDITSCALE',False,"""Set the proportional scale when displayed in an editor.""")
	filename=complexbase.Attribute("filename",SimpleTypes.String,'FILENAME',False,"""Name of the file that should be displayed. This attribute is supported in ASAM FSX and kept in AUTOSAR in order to support cut and paste.""")
	fit=complexbase.Attribute("fit",SimpleTypes.GraphicFitEnum,'FIT',False,"""It determines the way in which the graphic should be inserted.

Enter the attribute value \"AS-IS\" , to insert a graphic in its original dimensions.

The graphic is adapted, if it is too big for the space for which it was intended. 
Default is \"AS-IS\"""")
	generator=complexbase.Attribute("generator",SimpleTypes.NameToken,'GENERATOR',False,"""This attribute specifies the generator which is used to generate the image. 

Use case is that when editing a documentation, a figure (to be delivered by the  modeling tool) is inserted by the authoring tool as reference (this is the role  of graphic). But the real figure maybe injected during document processing. To  be able to recognize this situation, this attribute can be applied.""")
	height=complexbase.Attribute("height",SimpleTypes.String,'HEIGHT',False,"""Define the displayed height of the figure. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	htmlFit=complexbase.Attribute("htmlFit",SimpleTypes.GraphicFitEnum,'HTML-FIT',False,"""How to fit the graphic in an online media. Default is AS-IS.""")
	htmlHeight=complexbase.Attribute("htmlHeight",SimpleTypes.String,'HTML-HEIGHT',False,"""Specifies the height of the graphic when it is displayed online. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	htmlScale=complexbase.Attribute("htmlScale",SimpleTypes.String,'HTML-SCALE',False,"""Set the proportional scale when displayed online.""")
	htmlWidth=complexbase.Attribute("htmlWidth",SimpleTypes.String,'HTML-WIDTH',False,"""Specifies the width of the graphic when it is displayed online. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")
	notation=complexbase.Attribute("notation",SimpleTypes.GraphicNotationEnum,'NOTATION',False,"""This attribute captures the format used to represent the graphic.""")
	scale=complexbase.Attribute("scale",SimpleTypes.String,'SCALE',False,"""In this element the dimensions of the graphic can be altered proportionally.""")
	width=complexbase.Attribute("width",SimpleTypes.String,'WIDTH',False,"""Define the displayed width of the figure. The unit can be added to the number in the string. Possible units are:
cm, mm, px, pt. The default unit is px.""")

class HardwareConfiguration(complexbase.GroupBase):
	"""Describes in which mode the hardware is operating while needing this resource consumption."""
	def __init__(self):
		super().__init__()
		self._additionalInformation_child=ModelNone
		self._processorMode_child=ModelNone
		self._processorSpeed_child=ModelNone

class HardwareTestNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to indicate that a software-component is interested in the results of the hardware test and will establish a PortPrototype to query the hardware test manager."""

class HealthChannel(complexbase.GroupBase):
	"""This element defines the source of a health channel."""
	def __init__(self):
		super().__init__()
		self._recoveryNotification_children=[]

class HealthChannelExternalReportedStatus(complexbase.GroupBase):
	"""This element defines a health channel representing the status of an external health channel."""
	def __init__(self):
		super().__init__()
		self._statusId_child=ModelNone
		self._status_child=ModelNone

class HealthChannelExternalStatus(complexbase.GroupBase):
	"""This element defines a health channel representing the status of an external health channel."""
	def __init__(self):
		super().__init__()
		self._healthChannel_child=ModelNone
		self._notifiedStatus_children=[]
		self._process_child=ModelNone

class HealthChannelSupervision(complexbase.GroupBase):
	"""This element defines a health channel representing the status of a PhmSupervision."""
	def __init__(self):
		super().__init__()
		self._supervision_child=ModelNone

class HeapUsage(complexbase.GroupBase):
	"""Describes the heap memory usage of a SW-Component."""
	def __init__(self):
		super().__init__()
		self._hardwareConfiguration_child=ModelNone
		self._hwElement_child=ModelNone
		self._softwareContext_child=ModelNone
		self._variationPoint_child=ModelNone

class HttpTp(complexbase.GroupBase):
	"""Http over TCP as transport protocol."""
	def __init__(self):
		super().__init__()
		self._contentType_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._requestMethod_child=ModelNone
		self._tcpTpConfig_child=ModelNone
		self._uri_child=ModelNone

class HwAttributeDef(complexbase.GroupBase):
	"""This metaclass represents the ability to define a particular hardware attribute.

The category of this element defines the type of the attributeValue. If the category is Enumeration the hwAttributeEnumerationLiterals specify the available literals."""
	def __init__(self):
		super().__init__()
		self._hwAttributeLiteral_children=[]
		self._isRequired_child=ModelNone
		self._unit_child=ModelNone

class HwAttributeLiteralDef(complexbase.GroupBase):
	"""One available EnumerationLiteral of the Enumeration definition. Only applicable if the category of the HwAttributeDef equals Enumeration."""

class HwAttributeValue(complexbase.GroupBase):
	"""This metaclass represents the ability to assign a hardware attribute value. Note that v and vt are mutually exclusive."""
	def __init__(self):
		super().__init__()
		self._annotation_child=ModelNone
		self._hwAttributeDef_child=ModelNone
		self._v_child=ModelNone
		self._vt_child=ModelNone
		self._variationPoint_child=ModelNone

class HwCategory(complexbase.GroupBase):
	"""This metaclass represents the ability to declare hardware categories and its particular attributes."""
	def __init__(self):
		super().__init__()
		self._hwAttributeDef_children=[]

class HwDescriptionEntity(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a hardware entity."""
	def __init__(self):
		super().__init__()
		self._hwType_child=ModelNone
		self._hwCategory_children=[]
		self._hwAttributeValue_children=[]

class HwElement(complexbase.GroupBase):
	"""This represents the ability to describe Hardware Elements on an instance level. The particular types of hardware are distinguished by the category. This category determines the applicable attributes. The possible categories and attributes are defined in HwCategory."""
	def __init__(self):
		super().__init__()
		self._nestedElement_children=[]
		self._hwPinGroup_children=[]
		self._hwElementConnection_children=[]

class HwElementConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to connect two hardware elements. 
The details of the connection can be refined by hwPinGroupConnection."""
	def __init__(self):
		super().__init__()
		self._hwElement_children=[]
		self._hwPinGroupConnection_children=[]
		self._hwPinConnection_children=[]
		self._variationPoint_child=ModelNone

class HwElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._HwElement_child=ModelNone
		self._variationPoint_child=ModelNone

class HwPin(complexbase.GroupBase):
	"""This meta-class represents the possibility to describe a hardware pin."""
	def __init__(self):
		super().__init__()
		self._functionName_children=[]
		self._packagingPinName_child=ModelNone
		self._pinNumber_child=ModelNone
		self._variationPoint_child=ModelNone

class HwPinConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to connect two pins."""
	def __init__(self):
		super().__init__()
		self._hwPin_children=[]
		self._variationPoint_child=ModelNone

class HwPinGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to describe groups of pins which are used to connect hardware elements. This group acts as a bundle of pins. Thereby they allow to describe high level connections. Pin groups can even be nested."""
	def __init__(self):
		super().__init__()
		self._hwPinGroupContent_child=ModelNone
		self._variationPoint_child=ModelNone

class HwPinGroupConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to connect two pin groups."""
	def __init__(self):
		super().__init__()
		self._hwPinConnection_children=[]
		self._hwPinGroup_children=[]
		self._variationPoint_child=ModelNone

class HwPinGroupContent(complexbase.GroupBase):
	"""This meta-class specifies a mixture of hwPins and hwPinGroups."""
	def __init__(self):
		super().__init__()
		self._hwPin_children=[]
		self._hwPinGroup_children=[]

class HwPortMapping(complexbase.GroupBase):
	"""HWPortMapping specifies the hwCommunicationPort (defined in the ECU Resource Template) to realize the specified CommunicationConnector in a physical topology."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._hwCommunicationPort_child=ModelNone

class HwType(complexbase.GroupBase):
	"""This represents the ability to describe Hardware types on an abstract level. The particular types of hardware are distinguished by the category. This category determines the applicable attributes. The possible categories and attributes are defined in HwCategory."""

class IPdu(complexbase.GroupBase):
	"""The IPdu (Interaction Layer Protocol Data Unit) element is used to sum up all Pdus that are routed by the PduR."""
	def __init__(self):
		super().__init__()
		self._containedIPduProps_child=ModelNone

class IPduMapping(complexbase.GroupBase):
	"""Arranges those IPdus that are transferred by the gateway from one channel to the other in pairs and defines the mapping between them."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._pduMaxLength_child=ModelNone
		self._pdurTpChunkSize_child=ModelNone
		self._sourceIPdu_child=ModelNone
		self._targetIPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class IPduPort(complexbase.GroupBase):
	"""Connectors reception or send port on the referenced channel referenced by a PduTriggering."""
	def __init__(self):
		super().__init__()
		self._iPduSignalProcessing_child=ModelNone
		self._keyId_child=ModelNone
		self._rxSecurityVerification_child=ModelNone
		self._timestampRxAcceptanceWindow_child=ModelNone
		self._useAuthDataFreshness_child=ModelNone

class IPduTiming(complexbase.GroupBase):
	"""AUTOSAR COM provides the possibility to define two different TRANSMISSION MODES for each IPdu.

The Transmission Mode of an IPdu that is valid at a specific point in time is selected using the values of the signals that are mapped to this IPdu. For each IPdu a Transmission Mode Selector is defined. The Transmission Mode Selector is calculated by evaluating the conditions for a subset of signals  (class TransmissionModeCondition in the System Template).

The Transmission Mode Selector is defined to be true, if at least one Condition evaluates to true and is defined to be false, if all Conditions evaluate to false."""
	def __init__(self):
		super().__init__()
		self._minimumDelay_child=ModelNone
		self._transmissionModeDeclaration_child=ModelNone
		self._variationPoint_child=ModelNone

class IPv6ExtHeaderFilterList(complexbase.GroupBase):
	"""Permitted list for the filtering of IPv6 extension headers."""
	def __init__(self):
		super().__init__()
		self._allowedIPv6ExtHeader_children=[]

class IPv6ExtHeaderFilterSet(complexbase.GroupBase):
	"""Set of IPv6 Extension Header Filters."""
	def __init__(self):
		super().__init__()
		self._extHeaderFilterList_children=[]

class ISignal(complexbase.GroupBase):
	"""Signal of the Interaction Layer. The RTE supports a \"signal fan-out\" where the same System Signal is sent in different SignalIPdus to multiple receivers. 

To support the RTE \"signal fan-out\" each SignalIPdu  contains ISignals. If the same System Signal is to be mapped into several SignalIPdus there is one ISignal needed for each ISignalToIPduMapping. 

ISignals describe the Interface between the Precompile configured RTE and the potentially Postbuild configured Com Stack (see ECUC Parameter Mapping). 

In case of the SystemSignalGroup an ISignal shall be created for each SystemSignal contained in the SystemSignalGroup."""
	def __init__(self):
		super().__init__()
		self._dataTransformation_children=[]
		self._dataTypePolicy_child=ModelNone
		self._iSignalProps_child=ModelNone
		self._iSignalType_child=ModelNone
		self._initValue_child=[]
		self._length_child=ModelNone
		self._networkRepresentationProps_child=ModelNone
		self._systemSignal_child=ModelNone
		self._timeoutSubstitutionValue_child=[]
		self._transformationISignalProps_children=[]

class ISignalGroup(complexbase.GroupBase):
	"""SignalGroup of the Interaction Layer. The RTE supports a \"signal fan-out\" where the same System Signal Group is sent in different SignalIPdus to multiple receivers.   

An ISignalGroup refers to a set of ISignals that shall always be kept together. A ISignalGroup represents a COM Signal Group.  

Therefore it is recommended to put the ISignalGroup in the same Package as ISignals (see atp.recommendedPackage)"""
	def __init__(self):
		super().__init__()
		self._comBasedSignalGroupTransformation_children=[]
		self._iSignal_children=[]
		self._systemSignalGroup_child=ModelNone
		self._transformationISignalProps_children=[]

class ISignalIPdu(complexbase.GroupBase):
	"""Represents the IPdus handled by Com. The ISignalIPdu assembled and disassembled in AUTOSAR COM consists of one or more signals.
In case no multiplexing is performed this IPdu is routed to/from the Interface Layer.

A maximum of one dynamic length signal per IPdu is allowed."""
	def __init__(self):
		super().__init__()
		self._iPduTimingSpecification_children=[]
		self._iSignalToPduMapping_children=[]
		self._pduCounter_children=[]
		self._pduReplication_children=[]
		self._unusedBitPattern_child=ModelNone

class ISignalIPduGroup(complexbase.GroupBase):
	"""The AUTOSAR COM Layer is able to start and to stop sending and receiving  configurable groups of I-Pdus during runtime. An ISignalIPduGroup contains either ISignalIPdus or ISignalIPduGroups."""
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._communicationMode_child=ModelNone
		self._containedISignalIPduGroup_children=[]
		self._iSignalIPdu_children=[]
		self._nmPdu_children=[]

class ISignalIPduRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ISignalIPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalMapping(complexbase.GroupBase):
	"""Arranges those signals (or SignalGroups) that are transferred by the gateway from one channel to the other in pairs and defines the mapping between them.
Each pair consists in a source and a target referencing to a ISignalTriggering."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._sourceSignal_child=ModelNone
		self._targetSignal_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalPort(complexbase.GroupBase):
	"""Connectors reception or send port on the referenced channel referenced by an ISignalTriggering. 
If different timeouts or DataFilters for ISignals need to be specified several ISignalPorts may be created."""
	def __init__(self):
		super().__init__()
		self._dataFilter_child=ModelNone
		self._ddsQosProfile_child=ModelNone
		self._firstTimeout_child=ModelNone
		self._handleInvalid_child=ModelNone
		self._timeout_child=ModelNone

class ISignalProps(complexbase.GroupBase):
	"""Additional ISignal properties that may be stored in different files."""
	def __init__(self):
		super().__init__()
		self._handleOutOfRange_child=ModelNone

class ISignalToIPduMapping(complexbase.GroupBase):
	"""An ISignalToIPduMapping describes the mapping of ISignals to ISignalIPdus and defines the position of the ISignal within an ISignalIPdu."""
	def __init__(self):
		super().__init__()
		self._iSignalGroup_child=ModelNone
		self._iSignal_child=ModelNone
		self._packingByteOrder_child=ModelNone
		self._startPosition_child=ModelNone
		self._transferProperty_child=ModelNone
		self._updateIndicationBitPosition_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalTriggering(complexbase.GroupBase):
	"""A ISignalTriggering allows an assignment of ISignals to physical channels."""
	def __init__(self):
		super().__init__()
		self._iSignalGroup_child=ModelNone
		self._iSignalPort_children=[]
		self._iSignal_child=ModelNone
		self._variationPoint_child=ModelNone

class ISignalTriggeringRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ISignalTriggering_child=ModelNone
		self._variationPoint_child=ModelNone

class IamModuleInstantiation(complexbase.GroupBase):
	"""This meta-class represents the ability to define a definition of an IAM instantiation."""
	def __init__(self):
		super().__init__()
		self._grant_children=[]
		self._localComAccessControlEnabled_child=ModelNone
		self._remoteAccessControlEnabled_child=ModelNone

class IcmpRule(complexbase.GroupBase):
	"""Configuration of filter rules for ICMP (Internet Control Message Protocol)."""
	def __init__(self):
		super().__init__()
		self._checksumVerification_child=ModelNone
		self._code_child=ModelNone
		self._type_child=ModelNone

class IdentCaption(complexbase.GroupBase):
	"""This meta-class represents the caption. This allows having some meta-classes optionally identifiable."""

class Identifiable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (within the namespace borders). In addition to this, Identifiables are objects  which contribute significantly to the overall structure of an AUTOSAR description. In particular, Identifiables might contain Identifiables."""
	uuid=complexbase.Attribute("uuid",SimpleTypes.String,'UUID',False,"""The purpose of this attribute is to provide a globally unique identifier for an instance of a meta-class. The values of this attribute should be globally unique strings prefixed by the type of identifier.  For example, to include a
DCE UUID as defined by The Open Group, the UUID would be preceded by \"DCE:\". The values of this attribute may be used to support merging of different AUTOSAR models. 
The form of the UUID (Universally Unique Identifier) is taken from a standard defined by the Open Group (was Open Software Foundation). This standard is widely used, including by Microsoft for COM (GUIDs) and by many companies for DCE, which is based on CORBA. The method for generating these 128-bit IDs is published in the standard and the effectiveness and uniqueness of the IDs is not in practice disputed.
If the id namespace is omitted, DCE is assumed. 
An example is \"DCE:2fac1234-31f8-11b4-a222-08002b34c003\".
The uuid attribute has no semantic meaning for an AUTOSAR model and there is no requirement for AUTOSAR tools to manage the timestamp.""")
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone
		self._category_child=ModelNone
		self._adminData_child=ModelNone
		self._introduction_child=ModelNone
		self._annotation_children=[]

class IdsCommonElement(complexbase.GroupBase):
	"""This meta-class represents a common base class for IDS related elements of the Security Extract. It does not contribute any specific functionality other than the ability to become the target of a reference."""

class IdsCommonElementRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._IdsCommonElement_child=ModelNone
		self._variationPoint_child=ModelNone

class IdsDesign(complexbase.GroupBase):
	"""This meta-class represents the root element of a SecurityExtract file for IDS development. It defines the scope of an IDS to be designed and implemented by referencing all SecurityExtract meta-classes that need to be included into the IDS development process."""
	def __init__(self):
		super().__init__()
		self._element_children=[]

class IdsMapping(complexbase.GroupBase):
	"""This meta-class serves as abstract base class for mappings related to an IDS design."""

class IdsMgrCustomTimestampNeeds(complexbase.GroupBase):
	"""This meta-class is used to indicate that the enclosing SwcServiceDependency represents a service use case for the retrieval of a custom timestamp by the Intrusion Detection System Manager."""

class IdsMgrNeeds(complexbase.GroupBase):
	"""This meta-class is used to indicate that the enclosing SwcServiceDependency represents a service use case for the Intrusion Detection System Manager."""
	def __init__(self):
		super().__init__()
		self._useSmartSensorApi_child=ModelNone

class IdsPlatformInstantiation(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for platform modules that implement the intrusion detection system."""
	def __init__(self):
		super().__init__()
		self._networkInterface_children=[]
		self._timeBase_children=[]

class IdsmAbstractPortInterface(complexbase.GroupBase):
	"""This abstract meta-class acts as a base class for all kinds of PortInterfaces related to security event handling."""

class IdsmContextProviderInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for providing a Context for security events in the context of the intrusion detection system."""

class IdsmContextProviderMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between an IdsMInstance and a Process on deployment level to a given PortPrototype that is typed by a IdsmContextProviderInterface."""
	def __init__(self):
		super().__init__()
		self._idsPlatformInstantiation_child=ModelNone
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class IdsmInstance(complexbase.GroupBase):
	"""This meta-class provides the ability to create a relation between an EcuInstance and a specific class of filters for security events that apply for all security events reported on the referenced EcuInstance."""
	def __init__(self):
		super().__init__()
		self._blockState_children=[]
		self._ecuInstance_children=[]
		self._idsmInstanceId_child=ModelNone
		self._idsmModuleInstantiation_child=ModelNone
		self._rateLimitationFilter_children=[]
		self._signatureSupportAp_child=ModelNone
		self._signatureSupportCp_child=ModelNone
		self._timestampFormat_child=ModelNone
		self._trafficLimitationFilter_children=[]

class IdsmInstanceRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._IdsmInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class IdsmModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the IdsM configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._reportableSecurityEvent_children=[]

class IdsmProperties(complexbase.GroupBase):
	"""This meta-class provides the ability to aggregate filters for security events."""
	def __init__(self):
		super().__init__()
		self._rateLimitationFilter_children=[]
		self._trafficLimitationFilter_children=[]

class IdsmRateLimitation(complexbase.GroupBase):
	"""This meta-class represents the configuration of a rate limitation filter for security events. This means that security events are dropped if the number of events (of any type) processed within a configurable time window is greater than a configurable threshold."""
	def __init__(self):
		super().__init__()
		self._maxEventsInInterval_child=ModelNone
		self._timeInterval_child=ModelNone

class IdsmRateLimitationRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._IdsmRateLimitation_child=ModelNone
		self._variationPoint_child=ModelNone

class IdsmSignatureSupportAp(complexbase.GroupBase):
	"""This meta-class defines, for the Adaptive Platform, the cryptographic algorithm and key to be used by the IdsM instance for providing signature information in QSEv messages."""
	def __init__(self):
		super().__init__()
		self._cryptoPrimitive_child=ModelNone
		self._keySlot_child=ModelNone

class IdsmSignatureSupportCp(complexbase.GroupBase):
	"""This meta-class defines, for the Classic Platform, the cryptographic algorithm and key to be used by the IdsM instance for providing signature information in QSEv messages."""
	def __init__(self):
		super().__init__()
		self._authentication_child=ModelNone
		self._cryptoServiceKey_child=ModelNone

class IdsmTimestampProviderInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for providing a timestamp for security events in the context of the intrusion detection system."""

class IdsmTimestampProviderMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between an IdsMInstance and a Process on deployment level to a given PortPrototype that is typed by a IdsmTimestampProviderInterface."""
	def __init__(self):
		super().__init__()
		self._idsPlatformInstantiation_child=ModelNone
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class IdsmTrafficLimitation(complexbase.GroupBase):
	"""This meta-class represents the configuration of a traffic limitation filter for Security Events. This means that security events are dropped if the size (in terms of bandwidth) of security events (of any type) processed within a configurable time window is greater than a configurable threshold."""
	def __init__(self):
		super().__init__()
		self._maxBytesInInterval_child=ModelNone
		self._timeInterval_child=ModelNone

class IdsmTrafficLimitationRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._IdsmTrafficLimitation_child=ModelNone
		self._variationPoint_child=ModelNone

class Ieee1722Tp(complexbase.GroupBase):
	"""Content Model for IEEE 1722 configuration."""
	def __init__(self):
		super().__init__()
		self._relativeRepresentationTime_child=ModelNone
		self._streamIdentifier_child=ModelNone
		self._subType_child=ModelNone
		self._version_child=ModelNone

class IEEE1722TpAafConnection(complexbase.GroupBase):
	"""AV IEEE1722Tp AAF connection."""
	def __init__(self):
		super().__init__()
		self._aafAes3DataType_child=ModelNone
		self._aafFormat_child=ModelNone
		self._aafNominalRate_child=ModelNone
		self._aes3DataTypeH_child=ModelNone
		self._aes3DataTypeL_child=ModelNone
		self._channelsPerFrame_child=ModelNone
		self._eventDefaultValue_child=ModelNone
		self._pcmBitDepth_child=ModelNone
		self._sparseTimestampEnabled_child=ModelNone
		self._streamsPerFrame_child=ModelNone

class IEEE1722TpAcfBus(complexbase.GroupBase):
	"""Abstract class to define various busses to be transported over a IEEE1722TP ACF connection."""
	def __init__(self):
		super().__init__()
		self._acfPart_children=[]
		self._busId_child=ModelNone
		self._variationPoint_child=ModelNone

class IEEE1722TpAcfBusPart(complexbase.GroupBase):
	"""Definition of one IEEE1722Tp ACF part transported over the IEEE1722Tp channel."""
	def __init__(self):
		super().__init__()
		self._collectionTrigger_child=ModelNone
		self._variationPoint_child=ModelNone

class IEEE1722TpAcfCan(complexbase.GroupBase):
	"""ACF IEEE1722Tp bus used for CAN transport."""
	def __init__(self):
		super().__init__()
		self._messageType_child=ModelNone

class IEEE1722TpAcfCanPart(complexbase.GroupBase):
	"""Definition of one CAN part (frame or frame range) transported over the IEEE1722Tp channel."""
	def __init__(self):
		super().__init__()
		self._canAddressingMode_child=ModelNone
		self._canBitRateSwitch_child=ModelNone
		self._canFrameTxBehavior_child=ModelNone
		self._canIdentifier_child=ModelNone
		self._canIdentifierMask_child=ModelNone
		self._canIdentifierRange_child=ModelNone
		self._sdu_child=ModelNone

class IEEE1722TpAcfConnection(complexbase.GroupBase):
	"""ACF IEEE1722Tp connection."""
	def __init__(self):
		super().__init__()
		self._acfTransportedBus_children=[]
		self._collectionThreshold_child=ModelNone
		self._collectionTimeout_child=ModelNone
		self._mixedBusTypeCollection_child=ModelNone

class IEEE1722TpAcfLin(complexbase.GroupBase):
	"""ACF IEEE1722Tp bus used for LIN transport."""
	def __init__(self):
		super().__init__()
		self._baseFrequency_child=ModelNone
		self._frameSyncEnabled_child=ModelNone
		self._timestampInterval_child=ModelNone

class IEEE1722TpAcfLinPart(complexbase.GroupBase):
	"""Definition of one LIN part transported over the IEEE1722Tp channel."""
	def __init__(self):
		super().__init__()
		self._linIdentifier_child=ModelNone
		self._sdu_child=ModelNone

class IEEE1722TpAvConnection(complexbase.GroupBase):
	"""AV IEEE1722Tp connection."""
	def __init__(self):
		super().__init__()
		self._maxTransitTime_child=ModelNone
		self._sdu_children=[]

class IEEE1722TpConfig(complexbase.GroupBase):
	"""Definition of the IEEE1722Tp protocol."""
	def __init__(self):
		super().__init__()
		self._tpConnection_children=[]

class IEEE1722TpConnection(complexbase.GroupBase):
	"""Definition of the IEEE1722Tp protocol."""
	def __init__(self):
		super().__init__()
		self._communicationDirection_child=ModelNone
		self._destinationMacAddress_child=ModelNone
		self._macAddressStreamId_child=ModelNone
		self._pdu_child=ModelNone
		self._uniqueStreamId_child=ModelNone
		self._version_child=ModelNone
		self._vlanPriority_child=ModelNone

class IEEE1722TpConnectionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._IEEE1722TpConnection_child=ModelNone
		self._variationPoint_child=ModelNone

class IEEE1722TpCrfConnection(complexbase.GroupBase):
	"""AV IEEE1722Tp CRF connection."""
	def __init__(self):
		super().__init__()
		self._baseFrequency_child=ModelNone
		self._crfPull_child=ModelNone
		self._crfType_child=ModelNone
		self._frameSyncEnabled_child=ModelNone
		self._timestampInterval_child=ModelNone

class Ieee1722TpEthernetFrame(complexbase.GroupBase):
	"""Ieee1722Tp Ethernet Frame"""
	def __init__(self):
		super().__init__()
		self._relativeRepresentationTime_child=ModelNone
		self._streamIdentifier_child=ModelNone
		self._subType_child=ModelNone
		self._version_child=ModelNone

class IEEE1722TpIidcConnection(complexbase.GroupBase):
	"""AV IEEE1722Tp IIDC connection."""
	def __init__(self):
		super().__init__()
		self._iidcChannel_child=ModelNone
		self._iidcDataBlockSize_child=ModelNone
		self._iidcFractionNumber_child=ModelNone
		self._iidcSourcePacketHeader_child=ModelNone
		self._iidcStreamFormat_child=ModelNone
		self._iidcSy_child=ModelNone
		self._iidcTCode_child=ModelNone
		self._iidcTag_child=ModelNone

class IEEE1722TpRvfConnection(complexbase.GroupBase):
	"""AV IEEE1722Tp RVF connection."""
	def __init__(self):
		super().__init__()
		self._rvfActivePixels_child=ModelNone
		self._rvfColorSpace_child=ModelNone
		self._rvfEventDefault_child=ModelNone
		self._rvfFrameRate_child=ModelNone
		self._rvfInterlaced_child=ModelNone
		self._rvfPixelDepth_child=ModelNone
		self._rvfPixelFormat_child=ModelNone
		self._rvfTotalLines_child=ModelNone

class Implementation(complexbase.GroupBase):
	"""Description of an implementation a single software component or module."""
	def __init__(self):
		super().__init__()
		self._buildActionManifest_children=[]
		self._codeDescriptor_children=[]
		self._compiler_children=[]
		self._generatedArtifact_children=[]
		self._hwElement_children=[]
		self._linker_children=[]
		self._mcSupport_child=ModelNone
		self._programmingLanguage_child=ModelNone
		self._requiredArtifact_children=[]
		self._requiredGeneratorTool_children=[]
		self._resourceConsumption_child=ModelNone
		self._swVersion_child=ModelNone
		self._swcBswMapping_child=ModelNone
		self._usedCodeGenerator_child=ModelNone
		self._vendorId_child=ModelNone

class ImplementationDataType(complexbase.GroupBase):
	"""Describes a reusable data type on the implementation level. This will typically correspond to a typedef in C-code."""
	def __init__(self):
		super().__init__()
		self._dynamicArraySizeProfile_child=ModelNone
		self._isStructWithOptionalElement_child=ModelNone
		self._subElement_children=[]
		self._symbolProps_child=ModelNone
		self._typeEmitter_child=ModelNone

class ImplementationDataTypeElement(complexbase.GroupBase):
	"""Declares a data object which is locally aggregated. Such an element can only be used within the scope where it is aggregated. 

This element either consists of further subElements or it is further defined via its swDataDefProps.

There are several use cases within the system of ImplementationDataTypes fur such a local declaration:
* It can represent the elements of an array, defining the element type and array size
* It can represent an element of a struct, defining its type
* It can be the local declaration of a debug element."""
	def __init__(self):
		super().__init__()
		self._arrayImplPolicy_child=ModelNone
		self._arraySize_child=ModelNone
		self._arraySizeHandling_child=ModelNone
		self._arraySizeSemantics_child=ModelNone
		self._isOptional_child=ModelNone
		self._subElement_children=[]
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class ImplementationDataTypeElementInPortInterfaceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to the internal structure of an AutosarDataPrototype which is typed by an ImplementationDatatype in the context of a PortInterface.

In other words, this meta-class shall not be used to model a reference to the AutosarDataPrototype as a target itself, even if the AutosarDataPrototype is typed by an ImplementationDataType and even if that ImplementationDataType represents a composite data type."""
	def __init__(self):
		super().__init__()
		self._rootDataPrototype_child=ModelNone
		self._contextImplementationDataElement_children=[]
		self._targetImplementationDataTypeElement_child=ModelNone

class ImplementationDataTypeElementInSystemRef(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to the internal structure of an AutosarDataPrototype which is typed by an ImplementationDatatype in the context of a CompositionSwComponentType.
In other words, this meta-class shall not be used to model a reference to the AutosarDataPrototype as a target itself, even if the AutosarDataPrototype is typed by an ImplementationDataType and even if that ImplementationDataType represents a composite data type."""
	def __init__(self):
		super().__init__()
		self._base_child=ModelNone
		self._contextSwcPrototype_children=[]
		self._contextPortPrototype_child=ModelNone
		self._rootDataPrototype_child=ModelNone
		self._contextImplementationDataElement_children=[]
		self._targetImplementationDataTypeElement_child=ModelNone

class ImplementationDataTypeSubElementRef(complexbase.GroupBase):
	"""This meta-class represents the specialization of SubElementMapping with respect to ImplementationDataTypes."""
	def __init__(self):
		super().__init__()
		self._implementationDataTypeElement_child=ModelNone
		self._parameterImplementationDataTypeElement_child=ModelNone

class ImplementationElementInParameterInstanceRef(complexbase.GroupBase):
	"""Describes a reference to a particular ImplementationDataTypeElement instance in the context of a given ParameterDataPrototype.
Thus it refers to a particular element in the implementation description of a software data structure.

Use Case:
The RTE generator publishes its generated structure of calibration parameters in its BSW module description using the \"constantMemory\" role of ParameterDataPrototypes. Each ParameterDataPrototype describes a group of single calibration parameters. In order to point to these single parameters, this \"instance ref\" is needed.

Note that this class follows the pattern of an InstanceRef but is not implemented based on the abstract classes because the ImplementationDataType isn't either, especially because ImplementationDataTypeElement isn't derived from AtpPrototype."""
	def __init__(self):
		super().__init__()
		self._context_child=ModelNone
		self._target_child=ModelNone

class ImplementationProps(complexbase.GroupBase):
	"""Defines a symbol to be used as (depending on the concrete case) either a complete replacement or a prefix when generating code artifacts."""
	def __init__(self):
		super().__init__()
		self._symbol_child=ModelNone

class ImpositionTime(complexbase.GroupBase):
	"""This meta class represents one particular imposition time."""

class ImpositionTimeDefinitionGroup(complexbase.GroupBase):
	"""This meta class represents the ability to define the imposition times and properties of one particular imposition time."""
	def __init__(self):
		super().__init__()
		self._impositionTime_children=[]

class IncludedDataTypeSet(complexbase.GroupBase):
	"""An includedDataTypeSet declares that a set of AutosarDataType is used by a basic software module or a software component for its implementation and the AutosarDataType becomes part of the contract. 

This information is required if the AutosarDataType is not used for any DataPrototype owned by this software component or if the enumeration literals, lowerLimit and upperLimit constants shall be generated with a literalPrefix.

The optional literalPrefix is used to add a common prefix on enumeration literals, lowerLimit and upperLimit constants created by the RTE."""
	def __init__(self):
		super().__init__()
		self._dataType_children=[]
		self._literalPrefix_child=ModelNone

class IncludedModeDeclarationGroupSet(complexbase.GroupBase):
	"""An IncludedModeDeclarationGroupSet declares that a set of ModeDeclarationGroups used by the software component for its implementation and consequently these ModeDeclarationGroups become part of the contract."""
	def __init__(self):
		super().__init__()
		self._modeDeclarationGroup_children=[]
		self._prefix_child=ModelNone

class IndentSample(complexbase.GroupBase):
	"""This represents the ability to specify indentation of a labeled list by providing a sample content. This content can be measured by the rendering system in order to determine the width of indentation."""
	itemLabelPos=complexbase.Attribute("itemLabelPos",SimpleTypes.ItemLabelPosEnum,'ITEM-LABEL-POS',False,"""The position of the label in case the label is too long. The default is  \"NO-NEWLINE\"""")
	def __init__(self):
		super().__init__()
		self._l2_children=[]

class IndexEntry(complexbase.GroupBase):
	"""This class represents an index entry."""
	def __init__(self):
		super().__init__()
		self._sup_children=[]
		self._sub_children=[]

class IndexedArrayElement(complexbase.GroupBase):
	"""This element represents exactly one indexed element in the array.  Either the applicationArrayElement or implementationArrayElement reference shall be used."""
	def __init__(self):
		super().__init__()
		self._applicationArrayElement_child=ModelNone
		self._implementationArrayElement_child=ModelNone
		self._index_child=ModelNone

class IndicatorStatusNeeds(complexbase.GroupBase):
	"""This meta-class shall be taken to signal a service use case that affects the indicator status."""
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone

class InfrastructureServices(complexbase.GroupBase):
	"""Defines the network infrastructure services provided or consumed."""
	def __init__(self):
		super().__init__()
		self._dhcpServerConfiguration_child=ModelNone
		self._doIpEntity_child=ModelNone
		self._timeSynchronization_child=ModelNone

class InitEvent(complexbase.GroupBase):
	"""This RTEEvent is supposed to be used for initialization purposes, i.e. for starting and restarting a partition. It is not guaranteed that all RunnableEntities referenced by this InitEvent are executed before the 'regular' RunnableEntities are executed for the first time. The execution order depends on the task mapping."""

class InitialSdDelayConfig(complexbase.GroupBase):
	"""This element is used to configure the offer behavior of the server and the find behavior on the client."""
	def __init__(self):
		super().__init__()
		self._initialDelayMaxValue_child=ModelNone
		self._initialDelayMinValue_child=ModelNone
		self._initialRepetitionsBaseDelay_child=ModelNone
		self._initialRepetitionsMax_child=ModelNone

class InnerDataPrototypeGroupInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a nested DataPrototypeGroup"""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._targetDataPrototypeGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class InnerPortGroupInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._context_children=[]
		self._target_child=ModelNone

class InnerRunnableEntityGroupInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a nested RunnableEntityGroup."""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._targetRunnableEntityGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class InstanceEventInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponentPrototype_children=[]
		self._targetEvent_child=ModelNone

class InstantiationDataDefProps(complexbase.GroupBase):
	"""This is a general class allowing to apply additional SwDataDefProps to particular instantiations of a DataPrototype.

Typically the accessibility and further information like alias names for a  particular data is modeled on the level of DataPrototypes (especially  VariableDataPrototypes, ParameterDataPrototypes). But due to the recursive  structure of the meta-model concerning data types (a composite (data) type consists  out of data prototypes) a part of the MCD information is described in the data  type (in case of ApplicationCompositeDataType).

This is a strong restriction in the reuse of data typed because the data type should be re-used  for different VariableDataPrototypes and ParameterDataPrototypes to guarantee type  compatibility on C-implementation level (e.g. data of a Port is stored in PIM  or a ParameterDataPrototype used as ROM Block and shall be typed by the same data type as NVRAM Block).

This class overcomes such a restriction if applied properly."""
	def __init__(self):
		super().__init__()
		self._parameterInstance_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variableInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class InstantiationRTEEventProps(complexbase.GroupBase):
	"""This meta-class represents the ability to refine the properties of RTEEvents for particular instances of a software component."""
	def __init__(self):
		super().__init__()
		self._refinedEvent_child=ModelNone
		self._shortLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class InstantiationTimingEventProps(complexbase.GroupBase):
	"""This meta-class represents the ability to refine a timing event for particular instances of a software component. This approach supports an instance specific timing."""
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone

class IntegerValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Integer attributes.

Note that this class might be used in the extended meta-model only."""

class InterfaceMapping(complexbase.GroupBase):
	"""This meta-class collects the mappings of elements of a single ServiceInterface to PortInterface elements of the AUTOSAR Classic Platform."""
	def __init__(self):
		super().__init__()
		self._eventMapping_children=[]
		self._fieldMapping_children=[]
		self._fireAndForgetMethodMapping_children=[]
		self._methodMapping_children=[]

class InternalBehavior(complexbase.GroupBase):
	"""Common base class (abstract) for the internal behavior of both software components and basic software modules/clusters."""
	def __init__(self):
		super().__init__()
		self._constantMemory_children=[]
		self._constantValueMapping_children=[]
		self._dataTypeMapping_children=[]
		self._exclusiveArea_children=[]
		self._exclusiveAreaNestingOrder_children=[]
		self._staticMemory_children=[]

class InternalConstrs(complexbase.GroupBase):
	"""This meta-class represents the ability to express internal constraints."""
	def __init__(self):
		super().__init__()
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone
		self._scaleConstr_children=[]
		self._maxGradient_child=ModelNone
		self._maxDiff_child=ModelNone
		self._monotony_child=ModelNone

class InternalTriggerOccurredEvent(complexbase.GroupBase):
	"""This event is raised when the referenced InternalTriggeringPoint has occurred."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class InternalTriggeringPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns an InternalTriggeringPoint it is entitled to trigger the execution of RunnableEntities of the corresponding software-component."""
	def __init__(self):
		super().__init__()
		self._swImplPolicy_child=ModelNone
		self._variationPoint_child=ModelNone

class InterpolationRoutine(complexbase.GroupBase):
	"""This represents an interpolation routine taken to evaluate the contents of a curve or map against a specific input value."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._isDefault_child=ModelNone
		self._interpolationRoutine_child=ModelNone

class InterpolationRoutineMapping(complexbase.GroupBase):
	"""This meta-class provides a mapping between one record layout and its matching interpolation routines. This allows to formally specify the semantics of the interpolation routines. 

The use case is such that the curves/Maps define an interpolation method. This mapping table specifies which interpolation routine implements methods for a particular record layout. Using this information, the implementer of a software-component can select the appropriate interpolation routine."""
	def __init__(self):
		super().__init__()
		self._interpolationRoutine_children=[]
		self._swRecordLayout_child=ModelNone

class InterpolationRoutineMappingSet(complexbase.GroupBase):
	"""This meta-class specifies a set of interpolation routine mappings."""
	def __init__(self):
		super().__init__()
		self._interpolationRoutineMapping_children=[]

class InvalidationPolicy(complexbase.GroupBase):
	"""Specifies whether the component can actively invalidate a particular dataElement.

If no invalidationPolicy points to a dataElement this is considered to yield the identical result as if the handleInvalid attribute was set to dontInvalidate."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._handleInvalid_child=ModelNone

class InvertCondition(complexbase.GroupBase):
	"""inverts the nested condition"""
	def __init__(self):
		super().__init__()
		self._condition_child=[]

class IoHwAbstractionServerAnnotation(complexbase.GroupBase):
	"""The IoHwAbstractionServerAnnotation will only be used from a sensor- or an actuator component while interacting with the IoHwAbstraction layer.

Note that the \"server\" in the name of this meta-class is not meant to restrict the usage to ClientServerInterfaces."""
	def __init__(self):
		super().__init__()
		self._age_child=ModelNone
		self._argument_child=ModelNone
		self._bswResolution_child=ModelNone
		self._dataElement_child=ModelNone
		self._failureMonitoring_child=ModelNone
		self._filteringDebouncing_child=ModelNone
		self._pulseTest_child=ModelNone
		self._trigger_child=ModelNone

class IpIamAuthenticConnectionProps(complexbase.GroupBase):
	"""This meta-class defines a set of properties for IP connections in the context of IAM configuration."""
	def __init__(self):
		super().__init__()
		self._ipProtocol_child=ModelNone
		self._localNetworkEndpoint_child=ModelNone
		self._localPortRangeEnd_child=ModelNone
		self._localPortRangeStart_child=ModelNone
		self._remoteNetworkEndpoint_child=ModelNone
		self._remotePortRangeEnd_child=ModelNone
		self._remotePortRangeStart_child=ModelNone

class IpIamRemoteSubject(complexbase.GroupBase):
	"""This meta-class defines the proxy information about the remote node in case of general IP communication."""
	def __init__(self):
		super().__init__()
		self._authenticConnectionProps_children=[]

class IPSecConfig(complexbase.GroupBase):
	"""IPsec is a protocol that is designed to provide \"end-to-end\" cryptographically-based security for IP network connections."""
	def __init__(self):
		super().__init__()
		self._ipSecConfigProps_child=ModelNone
		self._ipSecRule_children=[]

class IPSecConfigProps(complexbase.GroupBase):
	"""This element holds all the attributes for configuration of IPsec that are independent of specific IPsec rules."""
	def __init__(self):
		super().__init__()
		self._ahCipherSuiteName_children=[]
		self._dpdAction_child=ModelNone
		self._dpdDelay_child=ModelNone
		self._espCipherSuiteName_children=[]
		self._ikeCipherSuiteName_child=ModelNone
		self._ikeOverTime_child=ModelNone
		self._ikeRandTime_child=ModelNone
		self._ikeReauthTime_child=ModelNone
		self._ikeRekeyTime_child=ModelNone
		self._saOverTime_child=ModelNone
		self._saRandTime_child=ModelNone
		self._saRekeyTime_child=ModelNone

class IPSecIamRemoteSubject(complexbase.GroupBase):
	"""This meta-class defines the proxy information about the remote node in case of IPsec."""
	def __init__(self):
		super().__init__()
		self._localIpSecRule_children=[]

class IPSecRule(complexbase.GroupBase):
	"""This element defines an IPsec rule that describes communication traffic that is monitored, protected and filtered."""
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._headerType_child=ModelNone
		self._ikeAuthenticationMethod_child=ModelNone
		self._ipProtocol_child=ModelNone
		self._localCertificate_children=[]
		self._localId_child=ModelNone
		self._localPortRangeEnd_child=ModelNone
		self._localPortRangeStart_child=ModelNone
		self._mode_child=ModelNone
		self._policy_child=ModelNone
		self._preSharedKey_child=ModelNone
		self._priority_child=ModelNone
		self._remoteCertificate_children=[]
		self._remoteId_child=ModelNone
		self._remoteIpAddress_children=[]
		self._remotePortRangeEnd_child=ModelNone
		self._remotePortRangeStart_child=ModelNone

class Ipv4ArpProps(complexbase.GroupBase):
	"""Specifies the configuration options for the ARP (Address Resolution Protocol)."""
	def __init__(self):
		super().__init__()
		self._tcpIpArpNumGratuitousArpOnStartup_child=ModelNone
		self._tcpIpArpPacketQueueEnabled_child=ModelNone
		self._tcpIpArpRequestTimeout_child=ModelNone
		self._tcpIpArpTableEntryTimeout_child=ModelNone

class Ipv4AutoIpProps(complexbase.GroupBase):
	"""Specifies the configuration options for Auto-IP (automatic private IP addressing)."""
	def __init__(self):
		super().__init__()
		self._tcpIpAutoIpInitTimeout_child=ModelNone

class Ipv4Configuration(complexbase.GroupBase):
	"""Internet Protocol version 4 (IPv4) configuration."""
	def __init__(self):
		super().__init__()
		self._assignmentPriority_child=ModelNone
		self._defaultGateway_child=ModelNone
		self._dnsServerAddress_children=[]
		self._ipAddressKeepBehavior_child=ModelNone
		self._ipv4Address_child=ModelNone
		self._ipv4AddressSource_child=ModelNone
		self._networkMask_child=ModelNone
		self._ttl_child=ModelNone

class Ipv4DhcpServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of a IPv4 DHCP server that runs on the network endpoint."""
	def __init__(self):
		super().__init__()
		self._addressRangeLowerBound_child=ModelNone
		self._addressRangeUpperBound_child=ModelNone
		self._defaultGateway_child=ModelNone
		self._defaultLeaseTime_child=ModelNone
		self._dnsServerAddress_children=[]
		self._networkMask_child=ModelNone

class Ipv4FragmentationProps(complexbase.GroupBase):
	"""Specifies the configuration options for IPv4 packet fragmentation/reassembly."""
	def __init__(self):
		super().__init__()
		self._tcpIpIpFragmentationRxEnabled_child=ModelNone
		self._tcpIpIpNumFragments_child=ModelNone
		self._tcpIpIpNumReassDgrams_child=ModelNone
		self._tcpIpIpReassTimeout_child=ModelNone

class Ipv4Props(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for IPv4."""
	def __init__(self):
		super().__init__()
		self._arpProps_child=ModelNone
		self._autoIpProps_child=ModelNone
		self._fragmentationProps_child=ModelNone

class Ipv4Rule(complexbase.GroupBase):
	"""Configuration of filter rules on IPv4 level."""
	def __init__(self):
		super().__init__()
		self._checksumVerification_child=ModelNone
		self._destinationIpAddress_child=ModelNone
		self._destinationNetworkMask_child=ModelNone
		self._differentiatedServiceCodePoint_child=ModelNone
		self._doNotFragment_child=ModelNone
		self._explicitCongestionNotification_child=ModelNone
		self._icmpRule_child=ModelNone
		self._internetHeaderLength_child=ModelNone
		self._moreFragments_child=ModelNone
		self._protocol_child=ModelNone
		self._sourceIpAddress_child=ModelNone
		self._sourceNetworkMask_child=ModelNone
		self._ttlMax_child=ModelNone
		self._ttlMin_child=ModelNone

class Ipv6Configuration(complexbase.GroupBase):
	"""Internet Protocol version 6 (IPv6) configuration."""
	def __init__(self):
		super().__init__()
		self._assignmentPriority_child=ModelNone
		self._defaultRouter_child=ModelNone
		self._dnsServerAddress_children=[]
		self._enableAnycast_child=ModelNone
		self._hopCount_child=ModelNone
		self._ipAddressKeepBehavior_child=ModelNone
		self._ipAddressPrefixLength_child=ModelNone
		self._ipv6Address_child=ModelNone
		self._ipv6AddressSource_child=ModelNone

class Ipv6DhcpServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of a IPv6 DHCP server that runs on the network endpoint."""
	def __init__(self):
		super().__init__()
		self._addressRangeLowerBound_child=ModelNone
		self._addressRangeUpperBound_child=ModelNone
		self._defaultGateway_child=ModelNone
		self._defaultLeaseTime_child=ModelNone
		self._dnsServerAddress_children=[]
		self._networkMask_child=ModelNone

class Ipv6FragmentationProps(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for IPv6 packet fragmentation/reassembly."""
	def __init__(self):
		super().__init__()
		self._tcpIpIpReassemblyBufferCount_child=ModelNone
		self._tcpIpIpReassemblyBufferSize_child=ModelNone
		self._tcpIpIpReassemblySegmentCount_child=ModelNone
		self._tcpIpIpReassemblyTimeout_child=ModelNone
		self._tcpIpIpTxFragmentBufferCount_child=ModelNone
		self._tcpIpIpTxFragmentBufferSize_child=ModelNone

class Ipv6NdpProps(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for the Neighbor Discovery Protocol for IPv6."""
	def __init__(self):
		super().__init__()
		self._tcpIpNdpDefaultReachableTime_child=ModelNone
		self._tcpIpNdpDefaultRetransTimer_child=ModelNone
		self._tcpIpNdpDefaultRouterListSize_child=ModelNone
		self._tcpIpNdpDefensiveProcessing_child=ModelNone
		self._tcpIpNdpDelayFirstProbeTime_child=ModelNone
		self._tcpIpNdpDelayFirstProbeTimeValue_child=ModelNone
		self._tcpIpNdpDestinationCacheSize_child=ModelNone
		self._tcpIpNdpDynamicHopLimitEnabled_child=ModelNone
		self._tcpIpNdpDynamicMtuEnabled_child=ModelNone
		self._tcpIpNdpDynamicReachableTimeEnabled_child=ModelNone
		self._tcpIpNdpDynamicRetransTimeEnabled_child=ModelNone
		self._tcpIpNdpMaxRandomFactor_child=ModelNone
		self._tcpIpNdpMaxRtrSolicitationDelay_child=ModelNone
		self._tcpIpNdpMaxRtrSolicitations_child=ModelNone
		self._tcpIpNdpMinRandomFactor_child=ModelNone
		self._tcpIpNdpNeighborUnreachabilityDetectionEnabled_child=ModelNone
		self._tcpIpNdpNumMulticastSolicitations_child=ModelNone
		self._tcpIpNdpNumUnicastSolicitations_child=ModelNone
		self._tcpIpNdpPacketQueueEnabled_child=ModelNone
		self._tcpIpNdpPrefixListSize_child=ModelNone
		self._tcpIpNdpRandomReachableTimeEnabled_child=ModelNone
		self._tcpIpNdpRndRtrSolicitationDelayEnabled_child=ModelNone
		self._tcpIpNdpRtrSolicitationInterval_child=ModelNone
		self._tcpIpNdpSlaacDadNumberOfTransmissions_child=ModelNone
		self._tcpIpNdpSlaacDadRetransmissionDelay_child=ModelNone
		self._tcpIpNdpSlaacDelayEnabled_child=ModelNone
		self._tcpIpNdpSlaacOptimisticDadEnabled_child=ModelNone

class Ipv6Props(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for IPv6."""
	def __init__(self):
		super().__init__()
		self._dhcpProps_child=ModelNone
		self._fragmentationProps_child=ModelNone
		self._ndpProps_child=ModelNone

class Ipv6Rule(complexbase.GroupBase):
	"""Configuration of filter rules on IPv6 level."""
	def __init__(self):
		super().__init__()
		self._destinationIpAddress_child=ModelNone
		self._destinationNetworkMask_child=ModelNone
		self._flowLabel_child=ModelNone
		self._hopLimit_child=ModelNone
		self._icmpRule_child=ModelNone
		self._nextHeader_child=ModelNone
		self._sourceIpAddress_child=ModelNone
		self._sourceNetworkMask_child=ModelNone
		self._trafficClass_child=ModelNone

class Item(complexbase.GroupBase):
	"""This meta-class represents one particular item in a list."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class J1939Cluster(complexbase.GroupBase):
	"""J1939 specific cluster attributes."""
	def __init__(self):
		super().__init__()
		self._j1939ClusterVariant_children=[]
		self._networkId_child=ModelNone
		self._request2Support_child=ModelNone
		self._usesAddressArbitration_child=ModelNone

class J1939ClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class J1939ClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class J1939ControllerApplication(complexbase.GroupBase):
	"""This element represents a J1939 controller application."""
	def __init__(self):
		super().__init__()
		self._functionId_child=ModelNone
		self._swComponentPrototype_child=ModelNone

class J1939ControllerApplicationToJ1939NmNodeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a J1939ControllerApplication to a J1939NmNode. Note that this is similar but not identical to the mapping of SwComponentPrototypes to EcuInstances; for J1939 the semantics of an EcuInstance itself is basically replaced by a J1939NmNode."""
	def __init__(self):
		super().__init__()
		self._j1939ControllerApplication_child=ModelNone
		self._j1939NmNode_child=ModelNone

class J1939DcmDm19Support(complexbase.GroupBase):
	"""The software-component provides information about calibration verification numbers for inclusion in DM19"""

class J1939DcmIPdu(complexbase.GroupBase):
	"""Represents the IPdus handled by J1939Dcm."""
	def __init__(self):
		super().__init__()
		self._diagnosticMessageType_child=ModelNone

class J1939NmCluster(complexbase.GroupBase):
	"""J1939 specific NmCluster attributes"""
	def __init__(self):
		super().__init__()
		self._addressClaimEnabled_child=ModelNone
		self._usesDynamicAddressing_child=ModelNone

class J1939NmEcu(complexbase.GroupBase):
	"""J1939 NmEcu specific attributes."""

class J1939NmNode(complexbase.GroupBase):
	"""J1939 specific NM Node attributes."""
	def __init__(self):
		super().__init__()
		self._addressConfigurationCapability_child=ModelNone
		self._nodeName_child=ModelNone

class J1939NodeName(complexbase.GroupBase):
	"""This element contains attributes to configure the J1939NmNode NAME."""
	def __init__(self):
		super().__init__()
		self._arbitraryAddressCapable_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._function_child=ModelNone
		self._functionInstance_child=ModelNone
		self._identitiyNumber_child=ModelNone
		self._industryGroup_child=ModelNone
		self._manufacturerCode_child=ModelNone
		self._vehicleSystem_child=ModelNone
		self._vehicleSystemInstance_child=ModelNone

class J1939RmIncomingRequestServiceNeeds(complexbase.GroupBase):
	"""\"This meta-class shall be used to specify needs with respect to the configuration of the J1939Rm, in particular for the case where an ApplicationSwComponentType needs to accept a request from another J1939 node."""

class J1939RmOutgoingRequestServiceNeeds(complexbase.GroupBase):
	"""This meta-class shall be used to specify needs with respect to the configuration of the J1939Rm, in particular for the case where an ApplicationSwComponentType needs to send a request to another J1939 node."""

class J1939SharedAddressCluster(complexbase.GroupBase):
	"""This meta-class represents the ability to identify several J1939Clusters that share a common address space for the routing of messages"""
	def __init__(self):
		super().__init__()
		self._participatingJ1939Cluster_children=[]
		self._variationPoint_child=ModelNone

class J1939TpConfig(complexbase.GroupBase):
	"""This element defines exactly one J1939 TP Configuration. 

One J1939TpConfig element shall be created for each J1939 Network in the System."""
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpConnection_children=[]
		self._tpNode_children=[]

class J1939TpConnection(complexbase.GroupBase):
	"""A J1939TpConnection represents an internal path for the transmission or reception of a Pdu via J1939Tp and describes the sender and the receiver of this particular communication. The J1939Tp module routes a Pdu (J1939 PGN) through the connection."""
	def __init__(self):
		super().__init__()
		self._broadcast_child=ModelNone
		self._bufferRatio_child=ModelNone
		self._cancellation_child=ModelNone
		self._dataPdu_child=ModelNone
		self._directPdu_child=ModelNone
		self._dynamicBs_child=ModelNone
		self._flowControlPdu_children=[]
		self._maxBs_child=ModelNone
		self._maxExpBs_child=ModelNone
		self._receiver_children=[]
		self._retry_child=ModelNone
		self._tpPg_children=[]
		self._tpSdu_children=[]
		self._transmitter_child=ModelNone
		self._variationPoint_child=ModelNone

class J1939TpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	def __init__(self):
		super().__init__()
		self._connector_child=ModelNone
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class J1939TpPg(complexbase.GroupBase):
	"""A J1939TpPg represents one J1939 message (parameter group, PG) identified by the PGN (parameter group number) that can be received or transmitted via J1939Tp."""
	def __init__(self):
		super().__init__()
		self._directPdu_child=ModelNone
		self._pgn_child=ModelNone
		self._requestable_child=ModelNone
		self._sdu_children=[]
		self._tpSdu_child=ModelNone

class Keyword(complexbase.GroupBase):
	"""This meta-class represents the ability to predefine keywords which may subsequently be used to construct names following a given naming convention, e.g. the AUTOSAR naming conventions.

Note that such names is not only shortName. It could be symbol, or even longName. Application of keywords is not limited to particular names."""
	def __init__(self):
		super().__init__()
		self._abbrName_child=ModelNone
		self._classification_children=[]

class KeywordSet(complexbase.GroupBase):
	"""This meta--class represents the ability to collect a set of predefined keywords."""
	def __init__(self):
		super().__init__()
		self._keyword_children=[]

class LGraphic(complexbase.GroupBase):
	"""This meta-class represents the figure in one particular language."""
	def __init__(self):
		super().__init__()
		self._graphic_child=ModelNone
		self._map_child=ModelNone

class LLongName(complexbase.GroupBase):
	"""MixedContentForLongNames  in one particular language. The language is denoted in the attribute l."""
	blueprintValue=complexbase.Attribute("blueprintValue",SimpleTypes.String,'BLUEPRINT-VALUE',False,"""This represents a description that documents how the value shall be defined when deriving objects from the blueprint.""")

class LOverviewParagraph(complexbase.GroupBase):
	"""MixedContentForOverviewParagraph in one particular language. The language is denoted in the attribute l."""
	blueprintValue=complexbase.Attribute("blueprintValue",SimpleTypes.String,'BLUEPRINT-VALUE',False,"""This represents a description that documents how the value shall be defined when deriving objects from the blueprint.""")

class LParagraph(complexbase.GroupBase):
	"""This is the text for a paragraph in one particular language. The language is denoted in the attribute l."""

class LPlainText(complexbase.GroupBase):
	"""This represents plain string in one particular language. The language is denoted in the attribute l."""

class LVerbatim(complexbase.GroupBase):
	"""MixedContentForVerbatim in one particular language. The language is denoted in the attribute l."""

class LabeledItem(complexbase.GroupBase):
	"""this represents an item of a labeled list."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	def __init__(self):
		super().__init__()
		self._itemLabel_child=ModelNone
		self._variationPoint_child=ModelNone
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class LabeledList(complexbase.GroupBase):
	"""This meta-class represents a labeled list, in which items have a label and a content. The policy how to render such items is specified in the labeled list."""
	def __init__(self):
		super().__init__()
		self._indentSample_child=ModelNone
		self._labeledItem_children=[]
		self._variationPoint_child=ModelNone

class LanguageSpecific(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a particular language for which an object is applicable."""
	l=complexbase.Attribute("l",SimpleTypes.LEnum,'L',True,"""'This attribute denotes the language in which the language specific document entity is given. Note that \"FOR-ALL\" means, that the entity is applicable to all languages. It is language neutral.

It follows ISO 639-1:2002 and is specified in upper case.""")

class LatencyTimingConstraint(complexbase.GroupBase):
	"""Constrains the time duration between the occurrence of the [ARMetaClassRole{stimulus}{TimingDescriptionEventChain}] and the occurrence of the corresponding [ARMetaClassRole{response}{TimingDescriptionEventChain}] of that [ARMetaClassRole{scope}{LatencyTimingConstraint}].

In contrast to [ARMetaClassRole{scope}{OffsetTimingConstraint}], a causal dependency between the [ARMetaClassRole{stimulus}{TimingDescriptionEventChain}] and the corresponding [ARMetaClassRole{response}{TimingDescriptionEventChain}] of the [ARMetaClassRole{scope}{LatencyTimingConstraint}] is required."""
	def __init__(self):
		super().__init__()
		self._latencyConstraintType_child=ModelNone
		self._scope_child=ModelNone
		self._minimum_child=ModelNone
		self._maximum_child=ModelNone
		self._nominal_child=ModelNone

class LifeCycleInfo(complexbase.GroupBase):
	"""LifeCycleInfo describes the life cycle state of an element together with additional information like what to use instead"""
	def __init__(self):
		super().__init__()
		self._lcObject_child=ModelNone
		self._lcState_child=ModelNone
		self._periodBegin_child=ModelNone
		self._periodEnd_child=ModelNone
		self._remark_child=ModelNone
		self._useInstead_children=[]

class LifeCycleInfoSet(complexbase.GroupBase):
	"""This meta class represents the ability to attach a life cycle  information to a particular set of elements.

The information can be defined for a particular period. This supports the definition of transition plans.

If no period is specified, the life cycle state applies forever."""
	def __init__(self):
		super().__init__()
		self._defaultLcState_child=ModelNone
		self._defaultPeriodBegin_child=ModelNone
		self._defaultPeriodEnd_child=ModelNone
		self._lifeCycleInfo_children=[]
		self._usedLifeCycleStateDefinitionGroup_child=ModelNone

class LifeCyclePeriod(complexbase.GroupBase):
	"""This meta class represents the ability to specify a point of time within a specified period, e.g. the starting or end point, in which a specific life cycle state is valid/applies to."""
	def __init__(self):
		super().__init__()
		self._date_child=ModelNone
		self._arReleaseVersion_child=ModelNone
		self._productRelease_child=ModelNone

class LifeCycleState(complexbase.GroupBase):
	"""This meta class represents one particular state in the LifeCycle."""

class LifeCycleStateDefinitionGroup(complexbase.GroupBase):
	"""This meta class represents the ability to define the states and properties of one particular life cycle."""
	def __init__(self):
		super().__init__()
		self._lcState_children=[]

class LimitValueVariationPoint(complexbase.GroupBase):
	"""This class represents the ability to express a numerical limit.  Note that this is in fact a NumericalValuationPoint but has the additional attribute intervalType.

Note that the xml.name is \"LIMIT\" for backward compatibility reasons."""
	intervalType=complexbase.Attribute("intervalType",SimpleTypes.IntervalTypeEnum,'INTERVAL-TYPE',False,"""This specifies the type of the interval. If the attribute is missing the interval shall be considered as \"CLOSED\".""")

class LinCluster(complexbase.GroupBase):
	"""LIN specific attributes"""
	def __init__(self):
		super().__init__()
		self._linClusterVariant_children=[]

class LinClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class LinClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinCommunicationConnector(complexbase.GroupBase):
	"""LIN bus specific communication connector attributes."""
	def __init__(self):
		super().__init__()
		self._initialNad_child=ModelNone
		self._linConfigurableFrame_children=[]
		self._linOrderedConfigurableFrame_children=[]
		self._scheduleChangeNextTimeBase_child=ModelNone

class LinCommunicationController(complexbase.GroupBase):
	"""LIN bus specific communication controller attributes."""
	def __init__(self):
		super().__init__()
		self._protocolVersion_child=ModelNone

class LinCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinConfigurableFrame(complexbase.GroupBase):
	"""Assignment of messageIds to Frames. 
This element shall be used for the LIN 2.0 Assign-Frame command."""
	def __init__(self):
		super().__init__()
		self._frame_child=ModelNone
		self._messageId_child=ModelNone

class LinConfigurationEntry(complexbase.GroupBase):
	"""A ScheduleTableEntry which contains LIN specific assignments."""
	def __init__(self):
		super().__init__()
		self._assignedController_child=ModelNone
		self._assignedLinSlaveConfig_child=ModelNone

class LinErrorResponse(complexbase.GroupBase):
	"""Each slave node shall publish a one bit signal, named response_error, to the master node in one of its transmitted unconditional frames. The response_error signal shall be set whenever a frame (except for event triggered frame responses) that is transmitted or received by the slave node contains an error in the frame response. The response_error signal shall be cleared when the unconditional frame containing the response_error signal is successfully transmitted."""
	def __init__(self):
		super().__init__()
		self._frameTriggering_child=ModelNone
		self._responseErrorPosition_child=ModelNone
		self._responseError_child=ModelNone

class LinEventTriggeredFrame(complexbase.GroupBase):
	"""An event triggered frame is used as a placeholder to allow multiple slave nodes to provide its response.

The header of an event triggered frame is transmitted when a frame slot allocated to the event triggered frame is processed. The publisher of an associated unconditional frame shall only transmit the response if at least one of the signals carried in its unconditional frame is updated. The LIN Master discovers and purges collisions with the collisionResolvingScheduleTable. 

The event controlled frame shall not contain any Pdus."""
	def __init__(self):
		super().__init__()
		self._collisionResolvingSchedule_child=ModelNone
		self._linUnconditionalFrame_children=[]

class LinFrame(complexbase.GroupBase):
	"""Lin specific Frame element."""

class LinFrameTriggering(complexbase.GroupBase):
	"""LIN specific attributes to the FrameTriggering"""
	def __init__(self):
		super().__init__()
		self._identifier_child=ModelNone
		self._linChecksum_child=ModelNone

class LinMaster(complexbase.GroupBase):
	"""Describing the properties of the refering ecu as a LIN master."""
	def __init__(self):
		super().__init__()
		self._linMasterVariant_children=[]
		self._linSlave_children=[]
		self._timeBase_child=ModelNone
		self._timeBaseJitter_child=ModelNone

class LinMasterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class LinMasterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinNmCluster(complexbase.GroupBase):
	"""Lin specific NmCluster attributes."""

class LinOrderedConfigurableFrame(complexbase.GroupBase):
	"""With the assignment of the index to a frame a mapping of Pids to Frames is possible. This element shall be used for the LIN 2.1 Assign-Frame-PID-Range command."""
	def __init__(self):
		super().__init__()
		self._frame_child=ModelNone
		self._index_child=ModelNone

class LinPhysicalChannel(complexbase.GroupBase):
	"""LIN specific attributes to the physicalChannel"""
	def __init__(self):
		super().__init__()
		self._busIdleTimeoutPeriod_child=ModelNone
		self._scheduleTable_children=[]

class LinScheduleTable(complexbase.GroupBase):
	"""The master task (in the master node) transmits frame headers based on a schedule table. The schedule table specifies the identifiers for each header and the interval between the start of a frame and the start of the following frame."""
	def __init__(self):
		super().__init__()
		self._resumePosition_child=ModelNone
		self._runMode_child=ModelNone
		self._tableEntry_children=[]
		self._variationPoint_child=ModelNone

class LinSlave(complexbase.GroupBase):
	"""Describing the properties of the referring ecu as a LIN slave."""
	def __init__(self):
		super().__init__()
		self._linSlaveVariant_children=[]
		self._assignNad_child=ModelNone
		self._configuredNad_child=ModelNone
		self._functionId_child=ModelNone
		self._initialNad_child=ModelNone
		self._linErrorResponse_child=ModelNone
		self._nasTimeout_child=ModelNone
		self._saveConfiguration_child=ModelNone
		self._supplierId_child=ModelNone
		self._variantId_child=ModelNone

class LinSlaveConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class LinSlaveConfig(complexbase.GroupBase):
	"""Node attributes of LIN slaves that are handled by the LinMaster. 

In the System Description LIN slaves may be described in the context of the Lin Master.

In an ECU Extract of the LinMaster the LinSlave Ecus shall not be available.

The information that is described here is necessary in the ECU Extract for the configuration of the LinMaster. 

The values of attributes of LinSlaveConfig and the corresponding LinSlave shall be identical (if both are defined in a System Description)."""
	def __init__(self):
		super().__init__()
		self._configuredNad_child=ModelNone
		self._functionId_child=ModelNone
		self._ident_child=ModelNone
		self._initialNad_child=ModelNone
		self._linConfigurableFrame_children=[]
		self._linErrorResponse_child=ModelNone
		self._linOrderedConfigurableFrame_children=[]
		self._linSlaveEcu_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._supplierId_child=ModelNone
		self._variantId_child=ModelNone

class LinSlaveConfigIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable LinSlaveConfig."""

class LinSlaveContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class LinSporadicFrame(complexbase.GroupBase):
	"""A sporadic frame is a group of unconditional frames that share the same frame slot. The sporadic frame shall not contain any Pdus."""
	def __init__(self):
		super().__init__()
		self._substitutedFrame_children=[]

class LinTpConfig(complexbase.GroupBase):
	"""This element defines exactly one Lin TP Configuration. 

One LinTpConfig element shall be created for each Lin Network in the System."""
	def __init__(self):
		super().__init__()
		self._tpAddress_children=[]
		self._tpConnection_children=[]
		self._tpNode_children=[]

class LinTpConnection(complexbase.GroupBase):
	"""A LinTP channel represents an internal path for the transmission or reception of a Pdu via LinTp and describes the sender and the receiver of this particular communication. 

LinTp supports (per Lin Cluster) the configuration of one Rx Tp-SDU and one Tx Tp-SDU per NAD the LinMaster uses to address one or more of its Lin Slaves. To support this an arbitrary number of LinTpConnections shall be described."""
	def __init__(self):
		super().__init__()
		self._dataPdu_child=ModelNone
		self._dropNotRequestedNad_child=ModelNone
		self._flowControl_child=ModelNone
		self._linTpNSdu_child=ModelNone
		self._maxNumberOfRespPendingFrames_child=ModelNone
		self._multicast_child=ModelNone
		self._p2Max_child=ModelNone
		self._p2Timing_child=ModelNone
		self._receiver_children=[]
		self._timeoutAs_child=ModelNone
		self._timeoutCr_child=ModelNone
		self._timeoutCs_child=ModelNone
		self._transmitter_child=ModelNone
		self._variationPoint_child=ModelNone

class LinTpNode(complexbase.GroupBase):
	"""TP Node (Sender or Receiver) provides the TP Address and the connection to the Topology description."""
	def __init__(self):
		super().__init__()
		self._connector_child=ModelNone
		self._dropNotRequestedNad_child=ModelNone
		self._maxNumberOfRespPendingFrames_child=ModelNone
		self._p2Max_child=ModelNone
		self._p2Timing_child=ModelNone
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class LinUnconditionalFrame(complexbase.GroupBase):
	"""Unconditional frames carry signals. The master sends a frame header in a scheduled frame slot and the designated slave node fills the frame with data."""

class Linker(complexbase.GroupBase):
	"""Specifies the linker attributes used to describe how the linker shall be invoked."""
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._options_child=ModelNone
		self._vendor_child=ModelNone
		self._version_child=ModelNone

class List(complexbase.GroupBase):
	"""This meta-class represents the ability to express a list. The kind of list is specified in the attribute."""
	type=complexbase.Attribute("type",SimpleTypes.ListEnum,'TYPE',False,"""The type of the list. Default is \"UNNUMBER\"""")
	def __init__(self):
		super().__init__()
		self._item_children=[]
		self._variationPoint_child=ModelNone

class LogAndTraceInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the Log&Trace configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._dltEcu_child=ModelNone
		self._logSink_children=[]
		self._sessionIdSupport_child=ModelNone
		self._timeBaseResource_children=[]

class LogAndTraceInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a PortInterface for support of Logging or Tracing."""

class LogAndTraceMessageCollectionSet(complexbase.GroupBase):
	"""Collection of DltMessages"""
	def __init__(self):
		super().__init__()
		self._dltMessage_children=[]

class LogicAddress(complexbase.GroupBase):
	"""The logical DoIP address. This element shall only be used for DoIP (Diagnosis over IP)."""
	def __init__(self):
		super().__init__()
		self._address_child=ModelNone
		self._variationPoint_child=ModelNone

class LogicalSupervision(complexbase.GroupBase):
	"""Defines a LogicalSupervision graph consisting of transitions, initial- and final checkpoints."""
	def __init__(self):
		super().__init__()
		self._initialCheckpoint_children=[]
		self._finalCheckpoint_children=[]
		self._transition_children=[]

class LTMessageCollectionToPortPrototypeMapping(complexbase.GroupBase):
	"""This mapping element assigns a collection of Log or Trace messages to a PortPrototype of an application."""
	def __init__(self):
		super().__init__()
		self._logAndTraceMessageCollectionSet_child=ModelNone
		self._rPortPrototype_child=ModelNone

class MacMulticastConfiguration(complexbase.GroupBase):
	"""References a per cluster globally defined MAC-Multicast-Group."""
	def __init__(self):
		super().__init__()
		self._macMulticastGroup_child=ModelNone

class MacMulticastGroup(complexbase.GroupBase):
	"""Per EthernetCluster globally defined MacMulticastGroup. One sender can handle many receivers simultaneously if the receivers have all the same macMulticastAddress. The addresses need to be unique for the particular EthernetCluster."""
	def __init__(self):
		super().__init__()
		self._macMulticastAddress_child=ModelNone

class MacSecCipherSuiteConfig(complexbase.GroupBase):
	"""This meta-class defines the cipher suite configuration to use with MACsec.
cipherSuitePriority is present in case the MKA instance acts as a Key Server to select the cipher suite to use for MACsec."""
	def __init__(self):
		super().__init__()
		self._cipherSuite_child=ModelNone
		self._cipherSuitePriority_child=ModelNone

class MacSecCryptoAlgoConfig(complexbase.GroupBase):
	"""This meta-class defines the cryptography configuration for MACsec."""
	def __init__(self):
		super().__init__()
		self._capability_child=ModelNone
		self._cipherSuiteConfig_children=[]
		self._confidentialityOffset_child=ModelNone
		self._replayProtection_child=ModelNone
		self._replayProtectionWindow_child=ModelNone

class MacSecGlobalKayProps(complexbase.GroupBase):
	"""Configuration of the MAC Security Key Agreement Entity properties that are shared by different KaY configurations."""
	def __init__(self):
		super().__init__()
		self._bypassEtherType_children=[]
		self._bypassVlan_children=[]

class MacSecKayParticipant(complexbase.GroupBase):
	"""This meta-class configures a MKA participant."""
	def __init__(self):
		super().__init__()
		self._ckn_child=ModelNone
		self._cryptoAlgoConfig_child=ModelNone
		self._sak_child=ModelNone

class MacSecLocalKayProps(complexbase.GroupBase):
	"""Configuration of the MAC Security Key Agreement Entity (KaY)."""
	def __init__(self):
		super().__init__()
		self._destinationMacAddress_child=ModelNone
		self._globalKayProps_child=ModelNone
		self._keyServerPriority_child=ModelNone
		self._mkaParticipant_children=[]
		self._role_child=ModelNone
		self._sourceMacAddress_child=ModelNone

class MacSecParticipantSet(complexbase.GroupBase):
	"""Collection of MACsec Kay Participants on an Ethernet Link."""
	def __init__(self):
		super().__init__()
		self._ethernetCluster_child=ModelNone
		self._mkaParticipant_children=[]

class MacSecProps(complexbase.GroupBase):
	"""This meta-class allows to configure MACsec (Media access control security) and the MKA (MACsec Key Agreement) for the CouplingPort (PHY)."""
	def __init__(self):
		super().__init__()
		self._autoStart_child=ModelNone
		self._macSecKayConfig_child=ModelNone
		self._onFailPermissiveMode_child=ModelNone
		self._onFailPermissiveModeTimeout_child=ModelNone
		self._sakRekeyTimeSpan_child=ModelNone

class Machine(complexbase.GroupBase):
	"""Machine that represents an Adaptive Autosar Software Stack."""
	def __init__(self):
		super().__init__()
		self._defaultApplicationTimeout_child=ModelNone
		self._environmentVariable_children=[]
		self._machineDesign_child=ModelNone
		self._moduleInstantiation_children=[]
		self._processor_children=[]
		self._secureCommunicationDeployment_children=[]
		self._trustedPlatformExecutableLaunchBehavior_child=ModelNone

class MachineDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define requirements on a Machine in the context of designing a system."""
	def __init__(self):
		super().__init__()
		self._accessControl_child=ModelNone
		self._communicationConnector_children=[]
		self._communicationController_children=[]
		self._ethIpProps_children=[]
		self._pnResetTimer_child=ModelNone
		self._pncPrepareSleepTimer_child=ModelNone
		self._serviceDiscoveryConfig_children=[]
		self._tcpIpIcmpProps_children=[]
		self._tcpIpProps_children=[]

class MachineTiming(complexbase.GroupBase):
	"""This meta-class represents the timing view for a machine."""
	def __init__(self):
		super().__init__()
		self._machine_child=ModelNone

class Map(complexbase.GroupBase):
	"""Image maps enable authors to specify regions of an image or object and assign a specific action to each region (e.g., retrieve a document, run a program, etc.) When the region is activated by the user, the action is executed.

The class follows the html approach and is intended to support interactive documents."""
	class_=complexbase.Attribute("class_",SimpleTypes.String,'CLASS',False,"""This attribute assigns a class name or set of class names to an element. Any number of elements may be assigned the same class name or set of class names. Multiple class names shall be separated by white space characters. Class names are typically used to apply CSS formatting rules to an element.""")
	name=complexbase.Attribute("name",SimpleTypes.NameToken,'NAME',False,"""This attribute assigns a name to the image map in the MAP element. This name can be used to be referenced in an HTML image through the attribute USEMAP. Although this is not actually necessary in the
MSR model, it was inserted in order to support the MAPs which were created for HTML.""")
	onclick=complexbase.Attribute("onclick",SimpleTypes.String,'ONCLICK',False,"""The ONCLICK-Event occurs, if the current element is clicked on. A script can be stored in
this attribute to be performed in the Event.""")
	ondblclick=complexbase.Attribute("ondblclick",SimpleTypes.String,'ONDBLCLICK',False,"""The ONDBLCLICK-Event occurs, if the current Event is \"double\" clicked-on. 
A script can be stored in this attribute to be performed in the Event.""")
	onkeydown=complexbase.Attribute("onkeydown",SimpleTypes.String,'ONKEYDOWN',False,"""The ONKEYDOWN-Event occurs, if a button on the current element is pressed down. 

A script can be stored in this attribute to be performed in the event.""")
	onkeypress=complexbase.Attribute("onkeypress",SimpleTypes.String,'ONKEYPRESS',False,"""The ONKEYPRESS-Event occurs, if a button on the current element is pressed down and released. 

A script can be stored in this attribute to be performed in the Event.""")
	onkeyup=complexbase.Attribute("onkeyup",SimpleTypes.String,'ONKEYUP',False,"""The ONKEYUP-Event occurs, if a button on the current element is released. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousedown=complexbase.Attribute("onmousedown",SimpleTypes.String,'ONMOUSEDOWN',False,"""The ONMOUSEDOWN-Event occurs, if the mouse button used for clicking is held down on the current element. 

A script can be stored in this attribute to be performed in the Event.""")
	onmousemove=complexbase.Attribute("onmousemove",SimpleTypes.String,'ONMOUSEMOVE',False,"""The ONMOUSEMOVE-Event occurs, if the mouse pointer is moved on the current 
element (i.e. it is located on the current element). 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseout=complexbase.Attribute("onmouseout",SimpleTypes.String,'ONMOUSEOUT',False,"""The ONMOUSEOUT-Event occurs, if the mouse pointer is moved from the current element.

A script can be stored in this attribute to be performed in the Event.""")
	onmouseover=complexbase.Attribute("onmouseover",SimpleTypes.String,'ONMOUSEOVER',False,"""The ONMOUSEOVER-Event occurs, if the mouse pointer is moved to the current element
from another location outside it. 

A script can be stored in this attribute to be performed in the Event.""")
	onmouseup=complexbase.Attribute("onmouseup",SimpleTypes.String,'ONMOUSEUP',False,"""The ONMOUSEUP-Event occurs if the mouse button used for clicking is released on the
current element. 

A script can be stored in this attribute to be performed in the Event.""")
	style=complexbase.Attribute("style",SimpleTypes.String,'STYLE',False,"""This attribute specifies formatting style information for the current element. The content of this attribute is called inline CSS. The style attribute is deprecated (considered outdated), because it fuses together content and formatting.""")
	title=complexbase.Attribute("title",SimpleTypes.String,'TITLE',False,"""This attribute offers advisory information. Some Web browsers will display this information as tooltips. Authoring tools may make this information available to users as additional information about the element.""")
	def __init__(self):
		super().__init__()
		self._area_children=[]

class MappingConstraint(complexbase.GroupBase):
	"""Different constraints that may be used to limit the mapping of SW components to applicable ECUs, Partitions or Cores depending on the mappingScope attribute."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._variationPoint_child=ModelNone

class McDataAccessDetails(complexbase.GroupBase):
	"""This meta-class allows to attach detailed information about the usage of a data buffer by the RTE to a corresponding McDataInstance.

Use Case:  Direct memory access to RTE internal buffers for rapid prototyping. In case of implicit communication, the various task local buffers need to be identified in relation to RTE events and variable access points.

Note that the SwComponentPrototype, the RunnableEntity and the VariableDataPrototype are implicitly given be the referred instances of RTEEvent and VariableAccess."""
	def __init__(self):
		super().__init__()
		self._rteEvent_children=[]
		self._variableAccess_children=[]

class McDataInstance(complexbase.GroupBase):
	"""Describes the specific properties of one data instance in order to support measurement and/or calibration of this data instance.

The most important attributes are:
* Its shortName is copied from the ECU Flat map (if applicable) and will be used as identifier and for display by the MC system.
* The category is copied from the corresponding data type (ApplicationDataType if defined, otherwise ImplementationDataType) as far as applicable.
* The symbol is the one used in the programming language. It will be used to find out the actual memory address by the final generation tool with the help of linker generated information.

It is assumed that in the M1 model this part and all the aggregated and referred elements (with the exception of the Flat Map and the references from ImplementationElementInParameterInstanceRef and McAccessDetails) are completely generated from \"upstream\" information. This means, that even if an element like e.g. a CompuMethod is only used via reference here, it will be copied into the M1 artifact which holds the complete McSupportData for a given Implementation."""
	def __init__(self):
		super().__init__()
		self._arraySize_child=ModelNone
		self._displayIdentifier_child=ModelNone
		self._flatMapEntry_child=ModelNone
		self._instanceInMemory_child=ModelNone
		self._mcDataAccessDetails_child=ModelNone
		self._mcDataAssignment_children=[]
		self._resultingProperties_child=ModelNone
		self._resultingRptSwPrototypingAccess_child=ModelNone
		self._role_child=ModelNone
		self._rptImplPolicy_child=ModelNone
		self._subElement_children=[]
		self._symbol_child=ModelNone
		self._variationPoint_child=ModelNone

class McFunction(complexbase.GroupBase):
	"""Represents a functional element to be used as input to support measurement and calibration. It is used to
* assign calibration parameters to a logical function
* assign measurement variables to a logical function
* structure functions hierarchically"""
	def __init__(self):
		super().__init__()
		self._defCalprmSet_child=ModelNone
		self._refCalprmSet_child=ModelNone
		self._inMeasurementSet_child=ModelNone
		self._outMeasurmentSet_child=ModelNone
		self._locMeasurementSet_child=ModelNone
		self._outMeasurementSet_child=ModelNone
		self._subFunction_children=[]

class McFunctionDataRefSet(complexbase.GroupBase):
	"""Refers to a set of data assigned to an McFunction in a particular role. The data are given
* either by entries in a FlatMap
* or by data instances that are part of MC support data.
These two possibilities are exclusive within a given McFunctionDataRefSet. Which one to use depends on the process and tool environment. 

The set is subject to variability because the same functional model may be used with various representation of the data."""
	def __init__(self):
		super().__init__()
		self._mcFunctionDataRefSetVariant_children=[]
		self._flatMapEntry_children=[]
		self._mcDataInstance_children=[]

class McFunctionDataRefSetConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class McFunctionDataRefSetContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class McGroup(complexbase.GroupBase):
	"""Represents a group element to be used as input to support measurement and calibration. It is used to provide selection lists (groups) of calibration parameters, measurement variables, and functions in a hierarchical manner (subGroups)."""
	def __init__(self):
		super().__init__()
		self._subGroup_children=[]
		self._refCalprmSet_child=ModelNone
		self._refMeasurementSet_child=ModelNone
		self._mcFunction_children=[]

class McGroupDataRefSet(complexbase.GroupBase):
	"""Refers to a set of data assigned to an McGroup in a particular role. The data are given
* either by entries in a FlatMap
* or by data instances that are part of MC support data.
These two possibilities can be mixed within a given McGroupDataRefSet.
Which one to use depends on the process and tool environment. 

The set is subject to variability because the same functional model may be used with various representation of the data."""
	def __init__(self):
		super().__init__()
		self._mcGroupDataRefSetVariant_children=[]
		self._flatMapEntry_children=[]
		self._mcDataInstance_children=[]

class McGroupDataRefSetConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class McGroupDataRefSetContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class McParameterElementGroup(complexbase.GroupBase):
	"""Denotes a group of calibration parameters which are handled by the RTE as one data structure."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._ramLocation_child=ModelNone
		self._romLocation_child=ModelNone

class McSupportData(complexbase.GroupBase):
	"""Root element for all measurement and calibration support data related to one Implementation artifact on an ECU. There shall be one such element related to the RTE implementation (if it owns MC data) and a separate one for each module or component, which owns private MC data."""
	def __init__(self):
		super().__init__()
		self._emulationSupport_children=[]
		self._mcParameterInstance_children=[]
		self._mcVariableInstance_children=[]
		self._measurableSystemConstantValues_children=[]
		self._rptSupportData_child=ModelNone

class McSwEmulationMethodSupport(complexbase.GroupBase):
	"""This denotes the method used by the RTE to handle the calibration data. It is published by the RTE generator and can be used e.g. to generate the corresponding emulation method in a Complex Driver.

According to the actual method given by the category attribute,  not all attributes are always needed:

* double pointered method: only baseReference is mandatory
* single pointered method: only referenceTable is mandatory
* initRam method: only elementGroup(s) are mandatory

Note: For single/double pointered method the group locations are implicitly accessed via the reference table and their location can be found from the initial values in the M1 model of the respective pointers. Therefore, the description of elementGroups is not needed in these cases.  Likewise, for double pointered method the reference table description can be accessed via the M1 model under baseReference."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._baseReference_child=ModelNone
		self._elementGroup_children=[]
		self._referenceTable_child=ModelNone
		self._variationPoint_child=ModelNone

class MeasuredExecutionTime(complexbase.GroupBase):
	"""Specifies the ExecutionTime which has been gathered using measurement means."""
	def __init__(self):
		super().__init__()
		self._maximumExecutionTime_child=ModelNone
		self._minimumExecutionTime_child=ModelNone
		self._nominalExecutionTime_child=ModelNone

class MeasuredHeapUsage(complexbase.GroupBase):
	"""The heap usage has been measured."""
	def __init__(self):
		super().__init__()
		self._averageMemoryConsumption_child=ModelNone
		self._maximumMemoryConsumption_child=ModelNone
		self._minimumMemoryConsumption_child=ModelNone
		self._testPattern_child=ModelNone

class MeasuredStackUsage(complexbase.GroupBase):
	"""The stack usage has been measured."""
	def __init__(self):
		super().__init__()
		self._averageMemoryConsumption_child=ModelNone
		self._maximumMemoryConsumption_child=ModelNone
		self._minimumMemoryConsumption_child=ModelNone
		self._testPattern_child=ModelNone

class MemorySection(complexbase.GroupBase):
	"""Provides a description of an abstract memory section used in the Implementation for code or data. It shall be declared by the Implementation Description of the module or component, which actually allocates the memory in its code. This means in case of data prototypes which are allocated by the RTE, that the generated Implementation Description of the RTE shall contain the corresponding MemorySections.

The attribute \"symbol\" (if symbol is missing: \"shortName\") defines the module or component specific section name used in the code. For details see the document \"Specification of Memory Mapping\".
Typically the section name is build according the pattern:

<SwAddrMethod shortName>[_<further specialization nominator>][_<alignment>] 

where

* '''[<SwAddrMethod shortName>]''' is the shortName of the referenced SwAddrMethod
* '''[_<further specialization nominator>]''' is an optional infix to indicate the specialization in the case that several MemorySections for different purpose of the same Implementation Description referring to the same or equally named SwAddrMethods. 
* '''[_<alignment>]''' is the alignment attributes value and is only applicable in the case that the memoryAllocationKeywordPolicy value of the referenced SwAddrMethod is set to addrMethodShortNameAndAlignment 

MemorySection used to Implement the code of RunnableEntitys and BswSchedulableEntitys shall have a symbol (if missing: shortName) identical to the referred SwAddrMethod to conform to the generated RTE header files.   

In addition to the section name described above, a prefix is used in the corresponding macro code in order to define a name space. This prefix is by default given by the shortName of the BswModuleDescription resp. the SwComponentType. It can be superseded by  the prefix attribute."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._executableEntity_children=[]
		self._memClassSymbol_child=ModelNone
		self._option_children=[]
		self._prefix_child=ModelNone
		self._size_child=ModelNone
		self._swAddrmethod_child=ModelNone
		self._symbol_child=ModelNone
		self._variationPoint_child=ModelNone

class MemorySectionLocation(complexbase.GroupBase):
	"""Specifies in which hardware ProvidedMemorySegment the softwareMemorySection is located."""
	def __init__(self):
		super().__init__()
		self._providedMemory_child=ModelNone
		self._softwareMemorySection_child=ModelNone

class MemoryUsage(complexbase.GroupBase):
	"""This meta-class is used to describe the memory  consumption."""
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class MetaDataItem(complexbase.GroupBase):
	"""This meta-class represents a single meta-data item."""
	def __init__(self):
		super().__init__()
		self._length_child=ModelNone
		self._metaDataItemType_child=ModelNone

class MetaDataItemSet(complexbase.GroupBase):
	"""This meta-class represents the ability to define a set of meta-data items to be used in SenderReceiverInterfaces."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._metaDataItem_children=[]

class MethodMapping(complexbase.GroupBase):
	"""Mapping of a ClientServerOperation that is located in a ClientServerInterface to a Method that is located in a ServiceInterface."""
	def __init__(self):
		super().__init__()
		self._clientServerOperation_child=ModelNone
		self._method_child=ModelNone

class MixedContentForLongName(complexbase.GroupBase):
	"""This is the model for titles and long-names. It allows some emphasis and index entries but no reference target (which is provided by the identifiable in question). It is intended that the content model can also be rendered as plain text.

The abstract class can be used for single language as well as for multi language elements."""
	def __init__(self):
		super().__init__()
		self._tt_children=[]
		self._e_children=[]
		self._sup_children=[]
		self._sub_children=[]
		self._ie_children=[]

class MixedContentForOverviewParagraph(complexbase.GroupBase):
	"""This is the text model of a restricted paragraph item within a documentation. Such restricted paragraphs are used mainly for overview items, e.g. desc."""
	def __init__(self):
		super().__init__()
		self._br_children=[]
		self._ft_children=[]
		self._trace_children=[]
		self._tt_children=[]
		self._xref_children=[]
		self._xrefTarget_children=[]
		self._e_children=[]
		self._sup_children=[]
		self._sub_children=[]
		self._ie_children=[]

class MixedContentForParagraph(complexbase.GroupBase):
	"""This mainly represents the text model of a full blown paragraph within a documentation."""
	def __init__(self):
		super().__init__()
		self._ft_children=[]
		self._trace_children=[]
		self._tt_children=[]
		self._br_children=[]
		self._xref_children=[]
		self._xrefTarget_children=[]
		self._e_children=[]
		self._sup_children=[]
		self._sub_children=[]
		self._ie_children=[]
		self._std_children=[]
		self._xdoc_children=[]
		self._xfile_children=[]

class MixedContentForPlainText(complexbase.GroupBase):
	"""This represents a plain text which conceptually is handled as mixed contents. It is modeled as such for symmetry reasons."""

class MixedContentForUnitNames(complexbase.GroupBase):
	"""This is the text model for items with subscript and superscripts such as measurement unit designations. It is intended, that such models can easily be transcribed to a plain text model either by using appropriate characters or by transcribing like m^2."""
	def __init__(self):
		super().__init__()
		self._sup_children=[]
		self._sub_children=[]

class MixedContentForVerbatim(complexbase.GroupBase):
	"""This is the text model for preformatted (verbatim) text. It mainly consists of attributes which do not change the length on rendering.

This class represents multilingual verbatim. Verbatim, sometimes called preformatted text, means that white-space is maintained. When verbatim is rendered in PDF or Online media, it is rendered using a monospaced font while white-space is obeyed. Blanks are rendered as well as newline characters.

Even if there are inline elements, the length of the data shall not be influenced by formatting."""
	def __init__(self):
		super().__init__()
		self._tt_children=[]
		self._e_children=[]
		self._xref_children=[]
		self._br_children=[]

class MlFigure(complexbase.GroupBase):
	"""This metaclass represents the ability to embed a figure."""
	frame=complexbase.Attribute("frame",SimpleTypes.FrameEnum,'FRAME',False,"""Used to defined the frame line around a figure. It can assume the following values:

* TOP - Border at the top of the figure
* BOTTOM - Border at the bottom of the figure
* TOPBOT - Borders at the top and bottom of  the figure
* ALL - Borders all around the figure
* SIDES - Borders at the sides of the figure
* NONE - No borders around the figure""")
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	pgwide=complexbase.Attribute("pgwide",SimpleTypes.PgwideEnum,'PGWIDE',False,"""Used to indicate wether the figure should take the complete page width (value = \"pgwide\") or not (value = \"noPgwide\").""")
	def __init__(self):
		super().__init__()
		self._figureCaption_child=ModelNone
		self._lGraphic_children=[]
		self._verbatim_children=[]
		self._variationPoint_children=[]

class MlFormula(complexbase.GroupBase):
	"""This meta-class represents the ability to express a formula in a documentation. The formula can be expressed by various means. If more than one representation is available, they need to be consistent. The rendering system can use the representation which is most appropriate."""
	def __init__(self):
		super().__init__()
		self._formulaCaption_child=ModelNone
		self._lGraphic_children=[]
		self._verbatim_children=[]
		self._texMath_children=[]
		self._genericMath_children=[]
		self._variationPoint_children=[]

class ModeAccessPoint(complexbase.GroupBase):
	"""A ModeAccessPoint is required by a RunnableEntity owned by a Mode Manager or Mode User.
Its semantics implies the ability to access the current mode (provided by the RTE)  of a ModeDeclarationGroupPrototype's ModeDeclarationGroup."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone
		self._modeGroup_child=[]
		self._variationPoint_child=ModelNone

class ModeAccessPointIdent(complexbase.GroupBase):
	"""This meta-class has been created to introduce the ability to become referenced into the meta-class ModeAccessPoint without breaking backwards compatibility."""

class ModeDeclaration(complexbase.GroupBase):
	"""Declaration of one Mode. The name and semantics of a specific mode is not defined in the meta-model."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeDeclarationGroup(complexbase.GroupBase):
	"""A collection of Mode Declarations. Also, the initial mode is explicitly identified."""
	def __init__(self):
		super().__init__()
		self._initialMode_child=ModelNone
		self._modeDeclaration_children=[]
		self._modeManagerErrorBehavior_child=ModelNone
		self._modeTransition_children=[]
		self._modeUserErrorBehavior_child=ModelNone
		self._onTransitionValue_child=ModelNone

class ModeDeclarationGroupPrototype(complexbase.GroupBase):
	"""The ModeDeclarationGroupPrototype specifies a set of Modes (ModeDeclarationGroup) which is provided or required in the given context."""
	def __init__(self):
		super().__init__()
		self._swCalibrationAccess_child=ModelNone
		self._type_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeDeclarationGroupPrototypeInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._swComponentPrototype_children=[]
		self._contextPortPrototype_child=ModelNone
		self._targetModeDeclarationGroupPrototype_child=ModelNone

class ModeDeclarationGroupPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._targetModeDeclarationGroupPrototype_child=ModelNone

class ModeDeclarationGroupPrototypeMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular ModeDeclarationGroupPrototypes (in the given context) that are unequally named and/or require a reference to a ModeDeclarationMappingSet in order to become compatible by definition of ModeDeclarationMappings."""
	def __init__(self):
		super().__init__()
		self._firstModeGroup_child=ModelNone
		self._modeDeclarationMappingSet_child=ModelNone
		self._secondModeGroup_child=ModelNone

class ModeDeclarationGroupPrototypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ModeDeclarationGroupPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeDeclarationInStateManagementStateNotificationInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class ModeDeclarationMapping(complexbase.GroupBase):
	"""This meta-class implements a concrete mapping of two ModeDeclarations."""
	def __init__(self):
		super().__init__()
		self._firstMode_children=[]
		self._secondMode_child=ModelNone

class ModeDeclarationMappingSet(complexbase.GroupBase):
	"""This meta-class implements a container for ModeDeclarationGroupMappings"""
	def __init__(self):
		super().__init__()
		self._modeDeclarationMapping_children=[]

class ModeDrivenTransmissionModeCondition(complexbase.GroupBase):
	"""The condition defined by this class evaluates to true if one of the referenced modeDeclarations (OR associated) is active. All referenced modeDeclarations shall be from the same ModeDeclarationGroup.

The condition is used to define which TransmissionMode shall be activated using Com_SwitchIpduTxMode."""
	def __init__(self):
		super().__init__()
		self._modeDeclaration_children=[]

class ModeErrorBehavior(complexbase.GroupBase):
	"""This represents the ability to define the error behavior in the context of mode handling."""
	def __init__(self):
		super().__init__()
		self._defaultMode_child=ModelNone
		self._errorReactionPolicy_child=ModelNone

class ModeGroupInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class ModeInBswInstanceRef(complexbase.GroupBase):
	"""Instance reference to be capable of referencing a specific ModeDeclaration of a ModeDeclarationGroupPrototype utilized in a BSW module."""
	def __init__(self):
		super().__init__()
		self._contextBswImplementation_child=ModelNone
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class ModeInBswModuleDescriptionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextModeDeclarationGroup_child=ModelNone
		self._targetMode_child=ModelNone

class ModeInProcessInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class ModeInSwcBswInstanceRef(complexbase.GroupBase):
	"""Abstract class representing an instance reference to be capable of referencing a specific ModeDeclaration utilized by a SW-C or BSW module."""

class ModeInSwcInstanceRef(complexbase.GroupBase):
	"""Instance reference to be capable of referencing a ModeDeclaration at a specific Mode Switch Port of a SW-C."""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class ModeInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of ModeDeclarationGroupPrototypes in context of two different ModeInterfaces."""
	def __init__(self):
		super().__init__()
		self._modeMapping_child=ModelNone

class ModePortAnnotation(complexbase.GroupBase):
	"""Annotation to a port used for calibration regarding a certain ModeDeclarationGroupPrototype."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class ModeRequestTypeMap(complexbase.GroupBase):
	"""Specifies a mapping between a ModeDeclarationGroup and an ImplementationDataType. This ImplementationDataType shall be used to implement the ModeDeclarationGroup."""
	def __init__(self):
		super().__init__()
		self._implementationDataType_child=ModelNone
		self._modeGroup_child=ModelNone

class ModeSwitchEventTriggeredActivity(complexbase.GroupBase):
	"""This meta-class defines an activity of the NvBlockSwComponentType for a specific NvBlock which is triggered by a ModeSwitchEvent."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._swcModeSwitchEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeSwitchInterface(complexbase.GroupBase):
	"""A mode switch interface declares a ModeDeclarationGroupPrototype to be sent and received."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class ModeSwitchPoint(complexbase.GroupBase):
	"""A ModeSwitchPoint is required by a RunnableEntity owned a Mode Manager. Its semantics implies the ability to initiate a mode switch."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class ModeSwitchReceiverComSpec(complexbase.GroupBase):
	"""Communication attributes of RPortPrototypes with respect to mode communication"""
	def __init__(self):
		super().__init__()
		self._enhancedModeApi_child=ModelNone
		self._modeGroup_child=ModelNone
		self._supportsAsynchronousModeSwitch_child=ModelNone

class ModeSwitchSenderComSpec(complexbase.GroupBase):
	"""Communication attributes of PPortPrototypes with respect to mode communication"""
	def __init__(self):
		super().__init__()
		self._enhancedModeApi_child=ModelNone
		self._modeGroup_child=ModelNone
		self._modeSwitchedAck_child=ModelNone
		self._queueLength_child=ModelNone

class ModeSwitchedAckEvent(complexbase.GroupBase):
	"""This event is raised when the referenced ModeSwitchPoint has been processed or an error occurred."""
	def __init__(self):
		super().__init__()
		self._eventSource_child=ModelNone

class ModeSwitchedAckRequest(complexbase.GroupBase):
	"""Requests acknowledgements that a mode switch has been proceeded successfully"""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone

class ModeTransition(complexbase.GroupBase):
	"""This meta-class represents the ability to describe possible ModeTransitions in the context of a ModeDeclarationGroup."""
	def __init__(self):
		super().__init__()
		self._enteredMode_child=ModelNone
		self._exitedMode_child=ModelNone

class Modification(complexbase.GroupBase):
	"""This meta-class represents the ability  to record what has changed in a document in comparison to its predecessor."""
	def __init__(self):
		super().__init__()
		self._change_child=ModelNone
		self._reason_child=ModelNone

class MsrQueryChapter(complexbase.GroupBase):
	"""This meta-class represents the ability to express a query which yields a set of chapters as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultChapter_child=ModelNone

class MsrQueryP1(complexbase.GroupBase):
	"""This meta-class represents the ability to express a query which yields the content of a topic as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultP1_child=ModelNone

class MsrQueryP2(complexbase.GroupBase):
	"""This meta-class represents the ability to express a query which yields the content of a DocumentationBlock as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultP2_child=ModelNone

class MsrQueryProps(complexbase.GroupBase):
	"""This metaclass represents the ability to specificy a query which yields some documentation text. The qualities of the result are determined by the context in which the query is used."""
	def __init__(self):
		super().__init__()
		self._msrQueryName_child=ModelNone
		self._msrQueryArg_children=[]
		self._comment_child=ModelNone

class MsrQueryResultChapter(complexbase.GroupBase):
	"""This metaclass represents the result of an msrquery which is a set of chapters."""
	def __init__(self):
		super().__init__()
		self._chapter_children=[]

class MsrQueryResultTopic1(complexbase.GroupBase):
	"""This metaclass represents the ability to express the result of a query which is a set of topics."""
	def __init__(self):
		super().__init__()
		self._topic1_children=[]

class MsrQueryTopic1(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a query which yields a set of topics as a result."""
	def __init__(self):
		super().__init__()
		self._msrQueryProps_child=ModelNone
		self._msrQueryResultTopic1_child=ModelNone

class MultiLanguageOverviewParagraph(complexbase.GroupBase):
	"""This is the content of a multilingual paragraph in an overview item."""
	def __init__(self):
		super().__init__()
		self._l2_children=[]

class MultiLanguageParagraph(complexbase.GroupBase):
	"""This is the content model of a multilingual paragraph in a documentation."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	def __init__(self):
		super().__init__()
		self._l1_children=[]
		self._variationPoint_child=ModelNone

class MultiLanguagePlainText(complexbase.GroupBase):
	"""This is a multilingual plaint Text.It is intended to be rendered as a paragraph."""
	def __init__(self):
		super().__init__()
		self._l10_children=[]

class MultiLanguageVerbatim(complexbase.GroupBase):
	"""This class represents multilingual Verbatim. Verbatim means, that white-space is maintained. When Verbatim is rendered in PDF or Online media, white-space is obeyed. Blanks are rendered as well as newline characters."""
	allowBreak=complexbase.Attribute("allowBreak",SimpleTypes.NameToken,'ALLOW-BREAK',False,"""This indicates if the verbatim text might be split on multiple pages. Default is \"1\".""")
	float=complexbase.Attribute("float",SimpleTypes.FloatEnum,'FLOAT',False,"""Indicate whether it is allowed to break the element. The following values are allowed:""")
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	pgwide=complexbase.Attribute("pgwide",SimpleTypes.PgwideEnum,'PGWIDE',False,"""Used to indicate wether the figure should take the complete page width (value = \"pgwide\") or not (value = \"noPgwide\").""")
	def __init__(self):
		super().__init__()
		self._l5_children=[]
		self._variationPoint_child=ModelNone

class MultidimensionalTime(complexbase.GroupBase):
	"""Specifies a time value based on [cite{ASAM-MCD-2MC-ASAP2}] see [TPS_GST_00354]."""
	def __init__(self):
		super().__init__()
		self._cseCode_child=ModelNone
		self._cseCodeFactor_child=ModelNone

class MultilanguageLongName(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a long name which acts in the role of a headline. It is intended for human readers. Per language it should be around max 80 characters."""
	def __init__(self):
		super().__init__()
		self._l4_children=[]

class MultilanguageReferrable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (while adhering to namespace borders). They also may have a longName. But they are not considered to contribute substantially to the overall structure of an AUTOSAR description. In particular it does not contain other Referrables."""
	def __init__(self):
		super().__init__()
		self._longName_child=ModelNone

class MultiplexedIPdu(complexbase.GroupBase):
	"""A MultiplexedPdu (i.e. NOT a COM I-PDU) contains a DynamicPart, an optional StaticPart and a selectorField. In case of multiplexing this IPdu is routed between the Pdu Multiplexer and the Interface Layer. 

A multiplexer is used to define variable parts within an IPdu that may carry different signals. The receivers of such a IPdu can determine which signalPdus are transmitted by evaluating the selector field, which carries a unique selector code for each sub-part."""
	def __init__(self):
		super().__init__()
		self._dynamicPart_children=[]
		self._selectorFieldByteOrder_child=ModelNone
		self._selectorFieldLength_child=ModelNone
		self._selectorFieldStartPosition_child=ModelNone
		self._staticPart_children=[]
		self._triggerMode_child=ModelNone
		self._unusedBitPattern_child=ModelNone

class MultiplexedPart(complexbase.GroupBase):
	"""The StaticPart and the DynamicPart have common properties. Both can be separated in multiple segments within the multiplexed PDU."""
	def __init__(self):
		super().__init__()
		self._segmentPosition_children=[]

class MultiplicityRestrictionWithSeverity(complexbase.GroupBase):
	"""Restriction that specifies the valid number of occurrences of an element in the current context."""

class NPdu(complexbase.GroupBase):
	"""This is a Pdu of the Transport Layer.
The main purpose of the TP Layer is to segment and reassemble IPdus."""

class NameTokenValueVariationPoint(complexbase.GroupBase):
	"""This class represents the ability to express a formula for a name token."""

class NetworkEndpoint(complexbase.GroupBase):
	"""The network endpoint defines the network addressing (e.g. IP-Address or MAC multicast address)."""
	def __init__(self):
		super().__init__()
		self._fullyQualifiedDomainName_child=ModelNone
		self._infrastructureServices_child=ModelNone
		self._ipSecConfig_child=ModelNone
		self._networkEndpointAddress_children=[]
		self._priority_child=ModelNone

class NetworkEndpointAddress(complexbase.GroupBase):
	"""To build a valid network endpoint address there has to be either one MAC multicast group reference or an ipv4 configuration or an ipv6 configuration."""

class NetworkEndpointRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._NetworkEndpoint_child=ModelNone
		self._variationPoint_child=ModelNone

class NetworkHandlePortMapping(complexbase.GroupBase):
	"""This class is used to associate a PortPrototype with a network handle in order to control the network handle from the PortPrototype"""
	def __init__(self):
		super().__init__()
		self._networkHandle_child=ModelNone
		self._pPortPrototypeInExecutable_child=ModelNone
		self._process_child=ModelNone

class NetworkLayerRule(complexbase.GroupBase):
	"""Configuration of filter rules on the Network layer"""

class NetworkManagementPortInterface(complexbase.GroupBase):
	"""This PortInterface shall be used to submit triggers to the state management"""

class NetworkSegmentIdentification(complexbase.GroupBase):
	"""This meta-class represents the ability to identify the PhysicalChannel on a system scope in a numerical way. One possible application of this approach is the Time Validation."""
	def __init__(self):
		super().__init__()
		self._networkSegmentId_child=ModelNone

class NmCluster(complexbase.GroupBase):
	"""Set of NM nodes coordinated with use of the NM algorithm."""
	def __init__(self):
		super().__init__()
		self._communicationCluster_child=ModelNone
		self._nmChannelId_child=ModelNone
		self._nmChannelSleepMaster_child=ModelNone
		self._nmNode_children=[]
		self._nmNodeDetectionEnabled_child=ModelNone
		self._nmNodeIdEnabled_child=ModelNone
		self._nmPncParticipation_child=ModelNone
		self._nmRepeatMsgIndEnabled_child=ModelNone
		self._nmSynchronizingNetwork_child=ModelNone
		self._pncClusterVectorLength_child=ModelNone
		self._variationPoint_child=ModelNone

class NmClusterCoupling(complexbase.GroupBase):
	"""Attributes that are valid for each of the referenced (coupled) clusters."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class NmConfig(complexbase.GroupBase):
	"""Contains the all configuration elements for AUTOSAR Nm."""
	def __init__(self):
		super().__init__()
		self._nmCluster_children=[]
		self._nmClusterCoupling_children=[]
		self._nmIfEcu_children=[]

class NmCoordinator(complexbase.GroupBase):
	"""A NM coordinator is an ECU, which is connected to at least two busses, and where the requirement exists that shutdown of NM of at least two of these busses (also referred to as coordinated busses) has to be performed synchronously."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone
		self._nmActiveCoordinator_child=ModelNone
		self._nmCoordSyncSupport_child=ModelNone
		self._nmGlobalCoordinatorTime_child=ModelNone
		self._nmNode_children=[]
		self._nmShutdownDelayTimer_child=ModelNone

class NmEcu(complexbase.GroupBase):
	"""ECU on which NM is running."""
	def __init__(self):
		super().__init__()
		self._busDependentNmEcu_children=[]
		self._busSpecificNmEcu_child=[]
		self._ecuInstance_child=ModelNone
		self._nmBusSynchronizationEnabled_child=ModelNone
		self._nmComControlEnabled_child=ModelNone
		self._nmCoordinator_child=ModelNone
		self._nmCycletimeMainFunction_child=ModelNone
		self._nmMultipleChannelsEnabled_child=ModelNone
		self._nmNodeDetectionEnabled_child=ModelNone
		self._nmNodeIdEnabled_child=ModelNone
		self._nmPassiveModeEnabled_child=ModelNone
		self._nmPduRxIndicationEnabled_child=ModelNone
		self._nmRemoteSleepIndEnabled_child=ModelNone
		self._nmRepeatMsgIndEnabled_child=ModelNone
		self._nmStateChangeIndEnabled_child=ModelNone
		self._nmUserDataEnabled_child=ModelNone
		self._variationPoint_child=ModelNone

class NmHandleToFunctionGroupStateMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to create a mapping between an NmNetworkHandle and a collection of function group states. This way, the impact of function groups on the network management can be specified."""
	def __init__(self):
		super().__init__()
		self._functionGroupState_children=[]
		self._mappingDirection_child=ModelNone
		self._nmHandle_child=ModelNone

class NmInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the Nm configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._networkHandle_children=[]

class NmInteractsWithSmMapping(complexbase.GroupBase):
	"""This mapping represents an interaction from network management to state management."""
	def __init__(self):
		super().__init__()
		self._nmNetworkHandle_child=ModelNone
		self._stateRequest_child=ModelNone

class NmNetworkHandle(complexbase.GroupBase):
	"""Group of partialNetworks and/or VLANs that can be controlled collectively."""
	def __init__(self):
		super().__init__()
		self._partialNetwork_children=[]
		self._vlan_children=[]

class NmNode(complexbase.GroupBase):
	"""The linking of NmEcus to NmClusters is realized via the NmNodes."""
	def __init__(self):
		super().__init__()
		self._controller_child=ModelNone
		self._machine_child=ModelNone
		self._nmCoordCluster_child=ModelNone
		self._nmCoordinatorRole_child=ModelNone
		self._nmIfEcu_child=ModelNone
		self._nmNodeId_child=ModelNone
		self._nmPassiveModeEnabled_child=ModelNone
		self._rxNmPdu_children=[]
		self._txNmPdu_children=[]
		self._variationPoint_child=ModelNone

class NmPdu(complexbase.GroupBase):
	"""Network Management Pdu"""
	def __init__(self):
		super().__init__()
		self._iSignalToIPduMapping_children=[]
		self._nmDataInformation_child=ModelNone
		self._nmVoteInformation_child=ModelNone
		self._unusedBitPattern_child=ModelNone

class NmPduRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._NmPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class NoCheckpointSupervision(complexbase.GroupBase):
	"""Defines explicitly that NO supervision shall be applied for a set of SupervisionCheckpoints."""
	def __init__(self):
		super().__init__()
		self._checkpoint_children=[]

class NoSupervision(complexbase.GroupBase):
	"""Defines explicitly that NO supervision shall be applied for a specific Supervised Entity instance."""
	def __init__(self):
		super().__init__()
		self._process_child=ModelNone
		self._targetPhmSupervisedEntity_child=ModelNone

class NonOsModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the abstract attributes for the configuration of an adaptive autosar module other than the OS module."""

class NonqueuedReceiverComSpec(complexbase.GroupBase):
	"""Communication attributes specific to non-queued receiving."""
	def __init__(self):
		super().__init__()
		self._aliveTimeout_child=ModelNone
		self._enableUpdate_child=ModelNone
		self._filter_child=ModelNone
		self._handleDataStatus_child=ModelNone
		self._handleNeverReceived_child=ModelNone
		self._handleTimeoutType_child=ModelNone
		self._initValue_child=[]
		self._timeoutSubstitutionValue_child=[]

class NonqueuedSenderComSpec(complexbase.GroupBase):
	"""Communication attributes for non-queued sender/receiver communication (sender side)"""
	def __init__(self):
		super().__init__()
		self._dataFilter_child=ModelNone
		self._initValue_child=[]

class NotAvailableValueSpecification(complexbase.GroupBase):
	"""This meta-class provides the ability to specify a ValueSpecification to state that the respective element is not available. This ability is needed to support the existence of ApplicationRecordElements where attribute isOptional ist set to the value true."""
	def __init__(self):
		super().__init__()
		self._defaultPattern_child=ModelNone

class Note(complexbase.GroupBase):
	"""This represents a note in a documentation, which may be used to highlight specific issues such as hints or caution notes.

N.B., Documentation notes can be nested recursively, even if this is not really intended. In case of nested notes e.g. the note icon of inner notes might be omitted while rendering the note."""
	noteType=complexbase.Attribute("noteType",SimpleTypes.NoteTypeEnum,'NOTE-TYPE',False,"""Type of the Note. Default is \"HINT\"""")
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._variationPoint_child=ModelNone
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class NumericalOrText(complexbase.GroupBase):
	"""This meta-class represents the ability to yield either a numerical or a string. A typical use case is that two or more instances of this meta-class are aggregated with a VariationPoint where some instances yield strings while other instances yield numerical depending on the resolution of the binding expression."""
	def __init__(self):
		super().__init__()
		self._vf_child=ModelNone
		self._vt_child=ModelNone
		self._variationPoint_child=ModelNone

class NumericalRuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class is used to support a rule-based initialization approach for data types with an array-nature (ImplementationDataType of category ARRAY)."""
	def __init__(self):
		super().__init__()
		self._ruleBasedValues_child=ModelNone

class NumericalValueSpecification(complexbase.GroupBase):
	"""A numerical  ValueSpecification which is intended to be assigned to a Primitive data element.
Note that the numerical value is a variant, it can be computed by a formula."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class NumericalValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for Numerical attributes.

Note that this class might be used in the extended meta-model only."""

class NvBlockDataMapping(complexbase.GroupBase):
	"""Defines the mapping between the VariableDataPrototypes in the NvBlockComponents ports and the VariableDataPrototypes of the RAM Block.

The data types of the referenced VariableDataPrototypes in the ports and the referenced sub-element (inside a CompositeDataType) of the VariableDataPrototype representing the RAM Block shall be compatible."""
	def __init__(self):
		super().__init__()
		self._bitfieldTextTableMaskNvBlockDescriptor_child=ModelNone
		self._bitfieldTextTableMaskPortPrototype_child=ModelNone
		self._nvRamBlockElement_child=ModelNone
		self._readNvData_child=ModelNone
		self._writtenNvData_child=ModelNone
		self._writtenReadNvData_child=ModelNone
		self._variationPoint_child=ModelNone

class NvBlockDescriptor(complexbase.GroupBase):
	"""Specifies the properties of exactly on NVRAM Block."""
	def __init__(self):
		super().__init__()
		self._clientServerPort_children=[]
		self._constantValueMapping_children=[]
		self._dataTypeMapping_children=[]
		self._instantiationDataDefProps_children=[]
		self._modeSwitchEventTriggeredActivity_children=[]
		self._nvBlockDataMapping_children=[]
		self._nvBlockNeeds_child=ModelNone
		self._ramBlock_child=ModelNone
		self._romBlock_child=ModelNone
		self._supportDirtyFlag_child=ModelNone
		self._timingEvent_child=ModelNone
		self._writingStrategy_children=[]
		self._writingStrategyRole_child=ModelNone
		self._variationPoint_child=ModelNone

class NvBlockNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of a single NVRAM Block."""
	def __init__(self):
		super().__init__()
		self._calcRamBlockCrc_child=ModelNone
		self._checkStaticBlockId_child=ModelNone
		self._cyclicWritingPeriod_child=ModelNone
		self._nDataSets_child=ModelNone
		self._nRomBlocks_child=ModelNone
		self._ramBlockStatusControl_child=ModelNone
		self._readonly_child=ModelNone
		self._reliability_child=ModelNone
		self._resistantToChangedSw_child=ModelNone
		self._restoreAtStart_child=ModelNone
		self._selectBlockForFirstInitAll_child=ModelNone
		self._storeAtShutdown_child=ModelNone
		self._storeCyclic_child=ModelNone
		self._storeEmergency_child=ModelNone
		self._storeImmediate_child=ModelNone
		self._storeOnChange_child=ModelNone
		self._useAutoValidationAtShutDown_child=ModelNone
		self._useCRCCompMechanism_child=ModelNone
		self._writeOnlyOnce_child=ModelNone
		self._writeVerification_child=ModelNone
		self._writingFrequency_child=ModelNone
		self._writingPriority_child=ModelNone

class NvBlockSwComponentType(complexbase.GroupBase):
	"""The NvBlockSwComponentType defines non volatile data which data can be shared between SwComponentPrototypes. The non volatile data of the NvBlockSwComponentType are accessible via provided and required ports."""
	def __init__(self):
		super().__init__()
		self._bulkNvDataDescriptor_children=[]
		self._nvBlockDescriptor_children=[]

class NvDataInterface(complexbase.GroupBase):
	"""A non volatile data interface declares a number of VariableDataPrototypes to be exchanged between non volatile block components and atomic software components."""
	def __init__(self):
		super().__init__()
		self._nvData_children=[]

class NvDataPortAnnotation(complexbase.GroupBase):
	"""Annotation to a port regarding a certain VariableDataPrototype."""
	def __init__(self):
		super().__init__()
		self._variable_child=ModelNone

class NvProvideComSpec(complexbase.GroupBase):
	"""Communication attributes of PPortPrototypes with respect to Nv data communication on the provided side."""
	def __init__(self):
		super().__init__()
		self._ramBlockInitValue_child=[]
		self._romBlockInitValue_child=[]
		self._variable_child=ModelNone

class NvRequireComSpec(complexbase.GroupBase):
	"""Communication attributes of RPortPrototypes with respect to Nv data communication on the required side."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._variable_child=ModelNone

class ObdControlServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Service 08 (request control of on-board system) in relation to a particular test-Identifier (TID) supported by this component or module."""
	def __init__(self):
		super().__init__()
		self._testId_child=ModelNone

class ObdInfoServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a given InfoType (OBD Service 09) which is supported by this component or module."""
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._infoType_child=ModelNone

class ObdMonitorServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a particular on-board monitoring test supported by this component or module. (OBD Service 06)."""
	def __init__(self):
		super().__init__()
		self._applicationDataType_child=ModelNone
		self._eventNeeds_child=ModelNone
		self._onBoardMonitorId_child=ModelNone
		self._testId_child=ModelNone
		self._unitAndScalingId_child=ModelNone
		self._updateKind_child=ModelNone

class ObdPidServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a particular PID (parameter identifier) which is supported by this component or module.

In case of using a client/server communicated value, the related value shall be communicated via the port referenced by assignedPort. The details of this communication (e.g. appropriate naming conventions) are specified in the related software specifications (SWS)."""
	def __init__(self):
		super().__init__()
		self._dataLength_child=ModelNone
		self._parameterId_child=ModelNone
		self._standard_child=ModelNone

class ObdRatioDenominatorNeeds(complexbase.GroupBase):
	"""This meta-class shall be used to indicate that a software-component wants to access the in-use-monitoring performance ration denominator."""
	def __init__(self):
		super().__init__()
		self._denominatorCondition_child=ModelNone

class ObdRatioServiceNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs of a component or module on the configuration of OBD Services in relation to a particular \"ratio monitoring\" which is supported by this component or module."""
	def __init__(self):
		super().__init__()
		self._connectionType_child=ModelNone
		self._denominatorGroup_child=ModelNone
		self._iumprGroup_child=ModelNone
		self._rateBasedMonitoredEvent_child=ModelNone
		self._usedFid_child=ModelNone
		self._usedSecondaryFid_children=[]

class OffsetTimingConstraint(complexbase.GroupBase):
	"""Bounds the time offset between the occurrence of two timing events, without requiring a direct functional dependency between the [ARMetaClassRole{source}{OffsetTimingConstraint}] and the [ARMetaClassRole{target}{OffsetTimingConstraint}].

If the [ARMetaClassRole{target}{OffsetTimingConstraint}] event occurs, it is expected to occur earliest with the [ARMetaClassRole{minimum}{OffsetTimingConstraint}], and latest with the [ARMetaClassRole{maximum}{OffsetTimingConstraint}] offset relatively after the occurrence of the [ARMetaClassRole{source}{OffsetTimingConstraint}] event.

Note: not every [ARMetaClassRole{source}{OffsetTimingConstraint}] event occurrence shall be followed by a [ARMetaClassRole{target}{OffsetTimingConstraint}] event occurrence.

In contrast to [ARMetaClass{LatencyTimingConstraint}], there shall not necessarily be a causal dependency between the [ARMetaClassRole{source}{OffsetTimingConstraint}] and [ARMetaClassRole{target}{OffsetTimingConstraint}] event."""
	def __init__(self):
		super().__init__()
		self._source_child=ModelNone
		self._target_child=ModelNone
		self._minimum_child=ModelNone
		self._maximum_child=ModelNone

class OperationArgumentInComponentInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._base_child=ModelNone
		self._contextComponent_children=[]
		self._contextPortPrototype_child=ModelNone
		self._contextOperation_child=ModelNone
		self._rootArgumentDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class OperationInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class OperationInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._targetOperation_child=ModelNone

class OperationInvokedEvent(complexbase.GroupBase):
	"""This event is raised when the ClientServerOperation referenced in OperationInvokedEvent.operation shall be invoked."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone

class OrderedMaster(complexbase.GroupBase):
	"""Element in the network endpoint list."""
	def __init__(self):
		super().__init__()
		self._index_child=ModelNone
		self._timeSyncServer_child=ModelNone

class OsModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the OS configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._osArtiAdapterLaunchBehavior_child=ModelNone
		self._resourceGroup_children=[]
		self._supportedTimerGranularity_child=ModelNone

class OsTaskExecutionEvent(complexbase.GroupBase):
	"""This RTEEvent is supposed to execute RunnableEntities which have to react on the execution of specific OsTasks. Therefore, this event is unconditionally raised whenever the OsTask on which it is mapped is executed.
The main use case for this event is scheduling of Runnables of Complex Drivers which have to react on task executions."""

class OsTaskProxy(complexbase.GroupBase):
	"""This meta-class represents a proxy for an OsTask in the System Description."""
	def __init__(self):
		super().__init__()
		self._period_child=ModelNone
		self._preemptability_child=ModelNone
		self._priority_child=ModelNone

class PModeGroupInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPPort_child=ModelNone
		self._targetModeGroup_child=ModelNone

class PModeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPPort_child=ModelNone
		self._contextModeDeclarationGroup_child=ModelNone
		self._targetMode_child=ModelNone

class POperationInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPPort_child=ModelNone
		self._targetProvidedOperation_child=ModelNone

class PPortComSpec(complexbase.GroupBase):
	"""Communication attributes of a provided PortPrototype. This class will contain attributes that are valid for all kinds of provide ports, independent of client-server or sender-receiver communication patterns."""

class PPortInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_child=ModelNone
		self._targetPPort_child=ModelNone

class PPortPrototype(complexbase.GroupBase):
	"""Component port providing a certain port interface."""
	def __init__(self):
		super().__init__()
		self._providedInterface_child=ModelNone

class PPortPrototypeInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextComponentPrototype_children=[]
		self._targetPPortPrototype_child=ModelNone

class PPortPrototypeInSoftwareClusterDesignInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwClusterDesignComponentPrototype_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._targetPPortPrototype_child=ModelNone

class PTriggerInAtomicSwcTypeInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPPort_child=ModelNone
		self._targetTrigger_child=ModelNone

class PackageableElement(complexbase.GroupBase):
	"""This meta-class specifies the ability to be a member of an AUTOSAR package."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class Paginateable(complexbase.GroupBase):
	"""This meta-class represents the ability to control the pagination policy when creating documents."""
	break_=complexbase.Attribute("break_",SimpleTypes.ChapterEnumBreak,'BREAK',False,"""This attributes allows to specify a forced page break.""")
	keepWithPrevious=complexbase.Attribute("keepWithPrevious",SimpleTypes.KeepWithPreviousEnum,'KEEP-WITH-PREVIOUS',False,"""This attribute denotes the pagination policy. In particular it defines if the containing text block shall be kept together with the previous block.""")

class ParameterAccess(complexbase.GroupBase):
	"""The presence of a ParameterAccess implies that a RunnableEntity needs access to a ParameterDataPrototype."""
	def __init__(self):
		super().__init__()
		self._accessedParameter_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class ParameterDataPrototype(complexbase.GroupBase):
	"""A ParameterDataPrototype represents a formalized generic piece of information that is typically immutable by the application software layer, but mutable by measurement and calibration tools. ParameterDataPrototype is used in various contexts and the specific context gives the otherwise generic ParameterDataPrototype a dedicated semantics."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._variationPoint_child=ModelNone

class ParameterDataPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._targetParameterDataPrototype_child=ModelNone

class ParameterInAtomicSWCTypeInstanceRef(complexbase.GroupBase):
	"""This class implements an instance reference which can be applied for variables as well as for parameters."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._rootParameterDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class ParameterInterface(complexbase.GroupBase):
	"""A parameter interface declares a number of parameter and characteristic values to be exchanged between parameter components and software components."""
	def __init__(self):
		super().__init__()
		self._parameter_children=[]

class ParameterPortAnnotation(complexbase.GroupBase):
	"""Annotation to a port used for calibration regarding a certain ParameterDataPrototype."""
	def __init__(self):
		super().__init__()
		self._parameter_child=ModelNone

class ParameterProvideComSpec(complexbase.GroupBase):
	"""\"Communication\" specification that applies to parameters on the provided side of a connection."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._parameter_child=ModelNone

class ParameterRequireComSpec(complexbase.GroupBase):
	"""\"Communication\" specification that applies to parameters on the required side of a connection."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._parameter_child=ModelNone

class ParameterSwComponentType(complexbase.GroupBase):
	"""The ParameterSwComponentType defines parameters and characteristic values accessible via provided Ports. The provided values are the same for all connected SwComponentPrototypes"""
	def __init__(self):
		super().__init__()
		self._constantMapping_children=[]
		self._dataTypeMapping_children=[]
		self._instantiationDataDefProps_children=[]

class PassThroughSwConnector(complexbase.GroupBase):
	"""This kind of SwConnector can be used inside a CompositionSwComponentType to connect two delegation PortPrototypes."""
	def __init__(self):
		super().__init__()
		self._providedOuterPort_child=ModelNone
		self._requiredOuterPort_child=ModelNone
		self._serviceInterfaceElementMapping_children=[]

class PayloadBytePatternRule(complexbase.GroupBase):
	"""Configuration of a generic firewall rule that defines the individual bytes of a message that shall match."""
	def __init__(self):
		super().__init__()
		self._payloadBytePatternRulePart_children=[]

class PayloadBytePatternRulePart(complexbase.GroupBase):
	"""Configuration of one byte in the datagram,"""
	def __init__(self):
		super().__init__()
		self._offset_child=ModelNone
		self._value_child=ModelNone

class Pdu(complexbase.GroupBase):
	"""Collection of all Pdus that can be routed through a bus interface."""
	def __init__(self):
		super().__init__()
		self._hasDynamicLength_child=ModelNone
		self._length_child=ModelNone
		self._metaDataLength_child=ModelNone

class PduActivationRoutingGroup(complexbase.GroupBase):
	"""Group of Pdus that can be activated or deactivated for transmission over a socket connection."""
	def __init__(self):
		super().__init__()
		self._eventGroupControlType_child=ModelNone
		self._iPduIdentifierTcp_children=[]
		self._iPduIdentifierUdp_children=[]
		self._variationPoint_child=ModelNone

class PduMappingDefaultValue(complexbase.GroupBase):
	"""Default Value which will be distributed if no I-Pdu has been received since last sending."""
	def __init__(self):
		super().__init__()
		self._defaultValueElement_children=[]

class PduToFrameMapping(complexbase.GroupBase):
	"""A PduToFrameMapping defines the composition of Pdus in each frame."""
	def __init__(self):
		super().__init__()
		self._packingByteOrder_child=ModelNone
		self._pdu_child=ModelNone
		self._startPosition_child=ModelNone
		self._updateIndicationBitPosition_child=ModelNone
		self._variationPoint_child=ModelNone

class PduTriggering(complexbase.GroupBase):
	"""The PduTriggering describes on which channel the IPdu is transmitted.
The Pdu routing by the PduR is only allowed for subclasses of IPdu.

Depending on its relation to entities such channels and clusters it can be unambiguously deduced whether a fan-out is handled by the Pdu router or the Bus Interface. 

If the fan-out is specified between different clusters it shall be handled by the Pdu Router. If the fan-out is specified between different channels of the same cluster it shall be handled by the Bus Interface."""
	def __init__(self):
		super().__init__()
		self._iPduPort_children=[]
		self._iPdu_child=ModelNone
		self._iSignalTriggering_children=[]
		self._secOcCryptoMapping_child=ModelNone
		self._triggerIPduSendCondition_children=[]
		self._variationPoint_child=ModelNone

class PduTriggeringRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._PduTriggering_child=ModelNone
		self._variationPoint_child=ModelNone

class PdurIPduGroup(complexbase.GroupBase):
	"""The AUTOSAR PduR will enable and disable the sending of configurable groups of IPdus during runtime according to the AUTOSAR PduR specification."""
	def __init__(self):
		super().__init__()
		self._communicationMode_child=ModelNone
		self._iPdu_children=[]

class PerInstanceMemory(complexbase.GroupBase):
	"""Defines a 'C' typed memory-block that needs to be available for each instance of the SW-component.  This is typically only useful if supportsMultipleInstantiation is set to \"true\" or if the software-component defines NVRAM access via permanent blocks."""
	def __init__(self):
		super().__init__()
		self._initValue_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._type_child=ModelNone
		self._typeDefinition_child=ModelNone
		self._variationPoint_child=ModelNone

class PerInstanceMemorySize(complexbase.GroupBase):
	"""Resources needed by the allocation of PerInstanceMemory for each SWC instance. Note that these resources are not covered by an ObjectFileSection, because they are supposed to be allocated by the RTE."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._perInstanceMemory_child=ModelNone
		self._size_child=ModelNone
		self._variationPoint_child=ModelNone

class PeriodicEventTriggering(complexbase.GroupBase):
	"""Describes the behavior of an event with a strict periodic occurrence pattern, given by [ARMetaClassRole{period}{PeriodicEventTriggering}].

Additionally, it is possible to soften the strictness of the periodic occurrence behavior by specifying a [ARMetaClassRole{jitter}{PeriodicEventTriggering}], so that there can be a deviation from the [ARMetaClassRole{period}{PeriodicEventTriggering}] up to the size of the [ARMetaClassRole{jitter}{PeriodicEventTriggering}]."""
	def __init__(self):
		super().__init__()
		self._minimumInterArrivalTime_child=ModelNone
		self._jitter_child=ModelNone
		self._period_child=ModelNone

class PermissibleSignalPath(complexbase.GroupBase):
	"""The PermissibleSignalPath describes the way a data element shall take in the topology. The path is described by ordered references to PhysicalChannels. 

If more than one PermissibleSignalPath is defined for the same
signal/operation attributes, any of them can be chosen. Such a signal path can be a constraint for the communication matrix . This path describes that one data element should take path A (e.g. 1. CAN channel, 2. LIN channel) 
and not path B (1. CAN channel, FlexRay channel A). 

This has an effect on the frame generation and the frame path."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._physicalChannel_children=[]
		self._signal_children=[]

class PersistencyDataElement(complexbase.GroupBase):
	"""This meta-class represents the ability to formally specify a piece of data that is subject to persistency in the context of the enclosing PersistencyKeyValueStorageInterface.

PersistencyDataElement represents also a key-value pair of the deployed PersistencyKeyValueStorage and provides an initial value."""

class PersistencyDataRequiredComSpec(complexbase.GroupBase):
	"""This meta-class represents the ability to define port-specific attributes for supporting use cases of data persistency on the required side."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._initValue_child=[]

class PersistencyDeployment(complexbase.GroupBase):
	"""This abstract meta-class serves as a base class for concrete classes representing different aspects of persistency."""
	def __init__(self):
		super().__init__()
		self._deploymentUri_children=[]
		self._maximumAllowedSize_child=ModelNone
		self._minimumSustainedSize_child=ModelNone
		self._redundancyHandling_children=[]
		self._updateStrategy_child=ModelNone
		self._version_child=ModelNone

class PersistencyDeploymentElement(complexbase.GroupBase):
	"""This abstract meta-class serves as a base class for concrete classes representing different aspects of elements of a [ARMetaClass{PersistencyDeployment}]."""
	def __init__(self):
		super().__init__()
		self._updateStrategy_child=ModelNone

class PersistencyDeploymentElementToCryptoKeySlotMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between the PersistencyDeploymentElement and a CryptoKeySlot."""
	def __init__(self):
		super().__init__()
		self._cryptoAlgorithmString_child=ModelNone
		self._cryptoKeySlot_child=ModelNone
		self._keySlotUsage_child=ModelNone
		self._persistencyDeploymentElement_child=ModelNone
		self._verificationHash_child=ModelNone

class PersistencyDeploymentToCryptoKeySlotMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between the PersistencyDeployment and a CryptoKeySlot."""
	def __init__(self):
		super().__init__()
		self._cryptoAlgorithmString_child=ModelNone
		self._cryptoKeySlot_child=ModelNone
		self._keySlotUsage_child=ModelNone
		self._persistencyDeployment_child=ModelNone
		self._verificationHash_child=ModelNone

class PersistencyDeploymentToDltLogSinkMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between the PersistencyDeployment and a DltLogSink."""
	def __init__(self):
		super().__init__()
		self._dltContext_child=ModelNone
		self._logSink_child=ModelNone
		self._persistencyDeployment_child=ModelNone

class PersistencyDeploymentUri(complexbase.GroupBase):
	"""This meta-class represents the ability to contain URIs relevant for the persistency deployment."""
	def __init__(self):
		super().__init__()
		self._uri_child=ModelNone

class PersistencyFile(complexbase.GroupBase):
	"""This meta-class represents the model of a file as part of the persistency on deployment level."""
	def __init__(self):
		super().__init__()
		self._contentUri_child=ModelNone
		self._fileName_child=ModelNone

class PersistencyFileElement(complexbase.GroupBase):
	"""This meta-class has the ability to represent a file at design time such that it is possible to configure the behavior for accessing the represented file at run-time."""
	def __init__(self):
		super().__init__()
		self._contentUri_child=ModelNone
		self._fileName_child=ModelNone

class PersistencyFileStorage(complexbase.GroupBase):
	"""This meta-class comes with the ability to define a collection of single files (directory) that creates the deployment-side counterpart to a [ARMetaClass{PortPrototype}] typed by a [ARMetaClass{PersistencyFileStorageInterface}]."""
	def __init__(self):
		super().__init__()
		self._file_children=[]

class PersistencyFileStorageInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a PortInterface for supporting persistency use cases for files."""
	def __init__(self):
		super().__init__()
		self._fileElement_children=[]
		self._maxNumberOfFiles_child=ModelNone

class PersistencyInterface(complexbase.GroupBase):
	"""This meta-class provides the abstract ability to define a PortInterface for the support of persistency use cases."""
	def __init__(self):
		super().__init__()
		self._contractVersion_child=ModelNone
		self._minimumSustainedSize_child=ModelNone
		self._redundancy_child=ModelNone
		self._redundancyHandling_children=[]
		self._updateStrategy_child=ModelNone

class PersistencyInterfaceElement(complexbase.GroupBase):
	"""This meta-class provides the abstract ability to define an element of a PortInterface for the support of persistency use cases."""
	def __init__(self):
		super().__init__()
		self._updateStrategy_child=ModelNone

class PersistencyKeyValueDataTypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between an existing data type in a key-value-storage stored by a previous version to a new data type used on application software level in the current version."""
	def __init__(self):
		super().__init__()
		self._currentDataType_child=ModelNone
		self._previousContractVersion_child=ModelNone
		self._previousDataType_child=ModelNone
		self._previousExecutableVersion_child=ModelNone

class PersistencyKeyValuePair(complexbase.GroupBase):
	"""This meta-class represents the ability to formally model a key-value pair in the context of the deployment of persistency."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._valueDataType_child=ModelNone

class PersistencyKeyValueStorage(complexbase.GroupBase):
	"""This meta-class represents the ability to model a key-value storage  on deployment level."""
	def __init__(self):
		super().__init__()
		self._keyValuePair_children=[]

class PersistencyKeyValueStorageInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a [ARMetaClass{PortInterface}] for supporting persistency use cases for data."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._dataTypeForSerialization_children=[]
		self._dataTypeMapping_child=ModelNone

class PersistencyPortPrototypeToDeploymentMapping(complexbase.GroupBase):
	"""This abstract bas class implements the shared functionality of all mapping between a [ARMetaClass{PortPrototype}], a [ARMetaClass{Process}], and a specific subclass of [ARMetaClass{PersistencyDeployment}]."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._process_child=ModelNone

class PersistencyPortPrototypeToFileStorageMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a collection of files on deployment level to a given [ARMetaClass{PortPrototype}]."""
	def __init__(self):
		super().__init__()
		self._fileStorage_child=ModelNone

class PersistencyPortPrototypeToKeyValueStorageMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a PortPrototype and a key-value storage."""
	def __init__(self):
		super().__init__()
		self._keyValueStorage_child=ModelNone

class PersistencyRedundancyChecksum(complexbase.GroupBase):
	"""Abstract class that defines the common attributes for implementations of redundancy."""
	def __init__(self):
		super().__init__()
		self._algorithmFamily_child=ModelNone
		self._length_child=ModelNone

class PersistencyRedundancyCrc(complexbase.GroupBase):
	"""This meta-class formally describes the usage of a CRC for the implementation of redundancy."""

class PersistencyRedundancyHandling(complexbase.GroupBase):
	"""This abstract base class represents a formal description of redundancy."""
	def __init__(self):
		super().__init__()
		self._scope_child=ModelNone

class PersistencyRedundancyHash(complexbase.GroupBase):
	"""This meta-class formally describes the usage of a Hash for the implementation of redundancy."""
	def __init__(self):
		super().__init__()
		self._initializationVectorLength_child=ModelNone

class PersistencyRedundancyMOutOfN(complexbase.GroupBase):
	"""This meta-class provides the ability to describe redundancy via an \"M out of N\" approach. In this case N is the number of copies created and M is the minimum number of identical copies to justify a reliable read access to the data."""
	def __init__(self):
		super().__init__()
		self._m_child=ModelNone
		self._n_child=ModelNone

class PhmAbstractRecoveryNotificationInterface(complexbase.GroupBase):
	"""This abstract meta-class provides the abstract ability to define a PortInterface for the Recovery Notification by Platform Health Management."""

class PhmCheckpoint(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a checkpoint for interaction with the Platform Health Management Supervised Entity."""
	def __init__(self):
		super().__init__()
		self._checkpointId_child=ModelNone

class PhmCheckpointInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextComponentPrototype_children=[]
		self._contextRPortPrototype_child=ModelNone
		self._targetPhmCheckpoint_child=ModelNone

class PhmContributionToMachineMapping(complexbase.GroupBase):
	"""This element associates one or more PlatformHealthManagementContributions with a Machine."""
	def __init__(self):
		super().__init__()
		self._machine_child=ModelNone
		self._phmContribution_children=[]

class PhmHealthChannelInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a PortInterface for interaction with the Platform Health Management Health Channel."""
	def __init__(self):
		super().__init__()
		self._status_children=[]

class PhmHealthChannelRecoveryNotificationInterface(complexbase.GroupBase):
	"""This meta-class represents a PortInterface that can be taken for implementing a PHM HealthChannel notification."""

class PhmHealthChannelStatus(complexbase.GroupBase):
	"""The PhmHealthChannelStatus specifies one possible status of the health channel."""
	def __init__(self):
		super().__init__()
		self._statusId_child=ModelNone
		self._triggersRecoveryNotification_child=ModelNone

class PhmRecoveryActionInterface(complexbase.GroupBase):
	"""This meta-class represents a PortInterface that can be taken for implementing a PHM recovery action on application software level."""
	def __init__(self):
		super().__init__()
		self._recovery_child=ModelNone

class PhmStateReference(complexbase.GroupBase):
	"""Definition of state dependency."""

class PhmSupervisedEntityInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to implement a PortInterface for interaction with the Platform Health Management Supervised Entity."""
	def __init__(self):
		super().__init__()
		self._checkpoint_children=[]

class PhmSupervision(complexbase.GroupBase):
	"""Defines explicitly that NO supervision shall be applied for a set of SupervisionCheckpoints."""

class PhmSupervisionRecoveryNotificationInterface(complexbase.GroupBase):
	"""This meta-class represents a PortInterface that can be taken for implementing a PHM Supervision notification."""

class PhysConstrs(complexbase.GroupBase):
	"""This meta-class represents the ability to express physical constraints. Therefore it has (in opposite to InternalConstrs) a reference to a Unit."""
	def __init__(self):
		super().__init__()
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone
		self._scaleConstr_children=[]
		self._maxGradient_child=ModelNone
		self._maxDiff_child=ModelNone
		self._monotony_child=ModelNone
		self._unit_child=ModelNone

class PhysicalChannel(complexbase.GroupBase):
	"""'''begin restrict to CP'''
A physical channel is the transmission medium that is used to send and receive information between communicating ECUs. Each CommunicationCluster has at least one physical channel. Bus systems like CAN and LIN only have exactly one PhysicalChannel. A FlexRay cluster may have more than one PhysicalChannels that may be used in parallel for redundant communication.

An ECU is part of a cluster if it contains at least one controller that is connected to at least one channel of the cluster.#
'''end restrict to CP'''
'''begin restrict to AP'''
This element represents a physical connection (in case of CAN, FlexRay, LIN) or a logical connection (VLAN in case of Ethernet) between communicating devices.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._commConnector_children=[]
		self._frameTriggering_children=[]
		self._iSignalTriggering_children=[]
		self._managedPhysicalChannel_children=[]
		self._pduTriggering_children=[]
		self._variationPoint_child=ModelNone

class PhysicalChannelRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._PhysicalChannel_child=ModelNone
		self._variationPoint_child=ModelNone

class PhysicalDimension(complexbase.GroupBase):
	"""This class represents a physical dimension.
If the physical dimension of two units is identical, then a conversion between them is possible. The conversion between units is related to the definition of the physical dimension.

Note that the equivalence of the exponents does not per se define the convertibility. For example Energy and Torque share the same exponents (Nm).

Please note further the value of an exponent does not necessarily have to be an integer number. It is also possible that the value yields a rational number, e.g. to compute the square root of a given physical quantity. In this case the exponent value would be a rational number where the numerator value is 1 and the denominator value is 2."""
	def __init__(self):
		super().__init__()
		self._lengthExp_child=ModelNone
		self._massExp_child=ModelNone
		self._timeExp_child=ModelNone
		self._currentExp_child=ModelNone
		self._temperatureExp_child=ModelNone
		self._molarAmountExp_child=ModelNone
		self._luminousIntensityExp_child=ModelNone

class PhysicalDimensionMapping(complexbase.GroupBase):
	"""This class represents a specific mapping between two PhysicalDimensions."""
	def __init__(self):
		super().__init__()
		self._firstPhysicalDimension_child=ModelNone
		self._secondPhysicalDimension_child=ModelNone

class PhysicalDimensionMappingSet(complexbase.GroupBase):
	"""This class represents a container for a list of mappings between PhysicalDimensions."""
	def __init__(self):
		super().__init__()
		self._physicalDimensionMapping_children=[]

class PlatformHealthManagementContribution(complexbase.GroupBase):
	"""This element defines a contribution to the Platform Health Management."""
	def __init__(self):
		super().__init__()
		self._checkpoint_children=[]
		self._supervisionModeCondition_children=[]
		self._globalSupervision_children=[]
		self._healthChannel_children=[]

class PlatformHealthManagementInterface(complexbase.GroupBase):
	"""This meta-class provides the abstract ability to define a PortInterface for the interaction with Platform Health Management."""

class PlatformModuleEndpointConfiguration(complexbase.GroupBase):
	"""This meta-class defines the abstract attributes for the configuration of a network for a specific CommunicationConnector."""

class PlatformModuleEthernetEndpointConfiguration(complexbase.GroupBase):
	"""This meta-class defines the attributes for the configuration of a port, protocol type and IP address of the communication on a VLAN."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._ipv4MulticastIpAddress_child=ModelNone
		self._ipv6MulticastIpAddress_child=ModelNone
		self._secureComPropsForTcp_child=ModelNone
		self._secureComPropsForUdp_child=ModelNone
		self._tcpPort_child=ModelNone
		self._udpPort_child=ModelNone

class PlcaProps(complexbase.GroupBase):
	"""This meta-class allows to configure the PLCA (Physical Layer Collision Avoidance) in case 10-BASE-T1S Ethernet is used and PLCA is enabled on the CouplingPort (PHY)."""
	def __init__(self):
		super().__init__()
		self._plcaLocalNodeId_child=ModelNone
		self._plcaMaxBurstCount_child=ModelNone
		self._plcaMaxBurstTimer_child=ModelNone

class PncMapping(complexbase.GroupBase):
	"""'''begin restrict to CP'''
Describes a mapping between one or several Virtual Function Clusters onto Partial Network Clusters. A Virtual Function Cluster is realized by a PortGroup. A Partial Network Cluster is realized by one or more IPduGroups.  
'''end restrict to CP'''
'''begin restrict to AP'''
Describes a mapping between one or several Virtual Function Clusters onto Partial Network Clusters. A Virtual Function Cluster is realized by a PortGroup. A Partial Network Cluster is realized by one or more ServiceInstances.  
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._dynamicPncMappingPduGroup_children=[]
		self._ident_child=ModelNone
		self._physicalChannel_children=[]
		self._pncConsumedProvidedServiceInstanceGroup_children=[]
		self._pncGroup_children=[]
		self._pncIdentifier_child=ModelNone
		self._pncPdurGroup_children=[]
		self._pncWakeupEnable_child=ModelNone
		self._relevantForDynamicPncMapping_children=[]
		self._serviceInstance_children=[]
		self._shortLabel_child=ModelNone
		self._vfc_children=[]
		self._wakeupFrame_children=[]
		self._variationPoint_child=ModelNone

class PncMappingIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable PncMapping."""

class PortAPIOption(complexbase.GroupBase):
	"""Options how to generate the signatures of calls for an AtomicSwComponentType in order to communicate over a PortPrototype (for calls into a RunnableEntity as well as for calls from a RunnableEntity to the PortPrototype)."""
	def __init__(self):
		super().__init__()
		self._enableTakeAddress_child=ModelNone
		self._errorHandling_child=ModelNone
		self._indirectAPI_child=ModelNone
		self._portArgValue_children=[]
		self._port_child=ModelNone
		self._supportedFeature_children=[]
		self._transformerStatusForwarding_child=ModelNone
		self._variationPoint_child=ModelNone

class PortDefinedArgumentBlueprint(complexbase.GroupBase):
	"""This class describes a Blueprint of a PortDefinedArgument which needs to be defined in the SwcInternalBehavior of the component implementing the server of the ClientServerInterface."""
	def __init__(self):
		super().__init__()
		self._blueprintMappingGuide_child=ModelNone
		self._valueType_child=ModelNone
		self._variationPoint_child=ModelNone

class PortDefinedArgumentValue(complexbase.GroupBase):
	"""A PortDefinedArgumentValue is passed to a RunnableEntity dealing with the ClientServerOperations provided by a given PortPrototype. Note that this is restricted to PPortPrototypes of a ClientServerInterface."""
	def __init__(self):
		super().__init__()
		self._value_child=[]
		self._valueType_child=ModelNone

class PortElementToCommunicationResourceMapping(complexbase.GroupBase):
	"""This meta class maps a communication resource to CP Software Clusters. In this case the kind of PortPrototype specified whether the Software Cluster has to provide or to require the resource."""
	def __init__(self):
		super().__init__()
		self._clientServerOperation_child=ModelNone
		self._communicationResource_child=ModelNone
		self._modeDeclarationGroupPrototype_child=ModelNone
		self._parameterDataPrototype_child=ModelNone
		self._trigger_child=ModelNone
		self._variableDataPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class PortGroup(complexbase.GroupBase):
	"""Group of ports which share a common functionality

'''begin restrict to CP AP'''
, e.g. need specific network resources. This information shall be available on the VFB level in order to delegate it properly via compositions. When propagated into the ECU extract, this information is used as input for the configuration of Services like the Communication Manager. 
'''end restrict to CP AP'''
'''begin restrict to FO'''
. 
'''end restrict to FO'''
A PortGroup is defined locally in a component (which can be a composition) and refers to the \"outer\" ports belonging to the group as well as to the \"inner\" groups which propagate  this group into the components which are part of a composition. A PortGroup within an atomic SWC cannot be linked to inner groups."""
	def __init__(self):
		super().__init__()
		self._innerGroup_children=[]
		self._outerPort_children=[]
		self._variationPoint_child=ModelNone

class PortGroupInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._target_child=ModelNone

class PortInCompositionTypeInstanceRef(complexbase.GroupBase):
	""

class PortInterface(complexbase.GroupBase):
	"""Abstract base class for an interface that is either provided or required by a port of a  software component."""
	def __init__(self):
		super().__init__()
		self._isService_child=ModelNone
		self._namespace_children=[]
		self._serviceKind_child=ModelNone

class PortInterfaceBlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map two PortInterfaces of which one acts as the blueprint for the other."""
	def __init__(self):
		super().__init__()
		self._portInterfaceBlueprint_child=ModelNone
		self._derivedPortInterface_child=ModelNone

class PortInterfaceElementInImplementationDatatypeRef(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to the internal structure of an AutosarDataPrototype  which is typed by an implementationDatatype in the context of a PortInterface.
In other words, this meta-class shall not be used to model a reference to the '''AutosarDataPrototype as a target itself''', '''even''' if the AutosarDataPrototype is typed by an ImplementationDataType '''and even''' if that ImplementationDataType represents a composite data type."""
	def __init__(self):
		super().__init__()
		self._contextDataPrototype_children=[]
		self._portInterface_child=ModelNone
		self._rootDataPrototype_child=ModelNone
		self._targetDataPrototype_child=ModelNone

class PortInterfaceMapping(complexbase.GroupBase):
	"""Specifies one PortInterfaceMapping to support the connection of Ports typed by two different PortInterfaces with PortInterface elements having unequal names and/or unequal semantic (resolution or range)."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class PortInterfaceMappingSet(complexbase.GroupBase):
	"""Specifies a set of (one or more) PortInterfaceMappings."""
	def __init__(self):
		super().__init__()
		self._portInterfaceMapping_children=[]

class PortInterfaceToDataTypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a PortInterface with a DataTypeMappingSet. This association is needed for the generation of header files in the scope of a single PortInterface.

The association is intentionally made outside the scope of the PortInterface itself because the designers of a PortInterface most likely will not want to add details about the level of ImplementationDataType."""
	def __init__(self):
		super().__init__()
		self._dataTypeMappingSet_children=[]
		self._portInterface_child=ModelNone

class PortPrototype(complexbase.GroupBase):
	"""Base class for the ports of an AUTOSAR software component.

The aggregation of PortPrototypes is subject to variability with the purpose to support the conditional existence of ports."""
	def __init__(self):
		super().__init__()
		self._clientServerAnnotation_children=[]
		self._delegatedPortAnnotation_child=ModelNone
		self._ioHwAbstractionServerAnnotation_children=[]
		self._modePortAnnotation_children=[]
		self._nvDataPortAnnotation_children=[]
		self._parameterPortAnnotation_children=[]
		self._portPrototypeProps_child=[]
		self._senderReceiverAnnotation_children=[]
		self._triggerPortAnnotation_children=[]
		self._variationPoint_child=ModelNone

class PortPrototypeBlueprint(complexbase.GroupBase):
	"""This meta-class represents the ability to express a blueprint of a PortPrototype by referring to a particular PortInterface. This blueprint can then be used as a guidance to create particular PortPrototypes which are defined according to this blueprint. By this it is possible to standardize application interfaces without the need to also standardize software-components with PortPrototypes typed by the standardized PortInterfaces."""
	def __init__(self):
		super().__init__()
		self._initValue_children=[]
		self._interface_child=ModelNone
		self._providedComSpec_children=[]
		self._requiredComSpec_children=[]

class PortPrototypeBlueprintInitValue(complexbase.GroupBase):
	"""This meta-class represents the ability to express init values in PortPrototypeBlueprints. These init values act as a kind of blueprint from which for example proper ComSpecs can be derived."""
	def __init__(self):
		super().__init__()
		self._dataPrototype_child=ModelNone
		self._value_child=[]

class PortPrototypeBlueprintMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a PortPrototypeBlueprint to a PortProtoype of which one acts as the blueprint for the other."""
	def __init__(self):
		super().__init__()
		self._portPrototypeBlueprint_child=ModelNone
		self._derivedPortPrototype_child=ModelNone

class PortPrototypeInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextComponentPrototype_children=[]
		self._targetPortPrototype_child=ModelNone

class PortPrototypeProps(complexbase.GroupBase):
	"""This meta-class represents the ability to define a further qualification of semantics of sub-classes of PortPrototype."""

class PortPrototypeRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._PortPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class PositiveIntegerValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for positive Integer attributes.

Note that this class might be used in the extended meta-model only."""

class PossibleErrorReaction(complexbase.GroupBase):
	"""Describes a possible error reaction code for the transient fault handler."""
	def __init__(self):
		super().__init__()
		self._reactionCode_child=ModelNone

class PostBuildVariantCondition(complexbase.GroupBase):
	"""This class specifies the value which shall be assigned to a particular variant criterion in order to bind the variation point. If multiple criterion/value pairs are specified, they shall all match to bind the variation point.

In other words binding can be represented by

  (criterion1 == value1) && (condition2 == value2) ..."""
	def __init__(self):
		super().__init__()
		self._matchingCriterion_child=ModelNone
		self._value_child=ModelNone

class PostBuildVariantCriterion(complexbase.GroupBase):
	"""This class specifies one particular PostBuildVariantSelector."""
	def __init__(self):
		super().__init__()
		self._compuMethod_child=ModelNone

class PostBuildVariantCriterionValue(complexbase.GroupBase):
	"""This class specifies the value which shall be assigned to a particular variant criterion in order to bind the variation point. If multiple criterion/value pairs are specified, they all shall match to bind the variation point."""
	def __init__(self):
		super().__init__()
		self._variantCriterion_child=ModelNone
		self._value_child=ModelNone
		self._annotation_children=[]

class PostBuildVariantCriterionValueSet(complexbase.GroupBase):
	"""This meta-class represents the ability to denote one set of postBuildVariantCriterionValues."""
	def __init__(self):
		super().__init__()
		self._postBuildVariantCriterionValue_children=[]

class PRPortPrototype(complexbase.GroupBase):
	"""This kind of PortPrototype can take the role of both a required and a provided PortPrototype."""
	def __init__(self):
		super().__init__()
		self._providedRequiredInterface_child=ModelNone

class PredefinedChapter(complexbase.GroupBase):
	"""This represents a predefined chapter."""
	def __init__(self):
		super().__init__()
		self._msrQueryChapter_children=[]
		self._chapter_children=[]
		self._msrQueryTopic1_children=[]
		self._topic1_children=[]
		self._prms_children=[]
		self._traceableTable_children=[]
		self._table_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]
		self._msrQueryP1_children=[]

class PredefinedVariant(complexbase.GroupBase):
	"""This specifies one predefined variant. It is characterized by the union of all system constant values and post-build variant criterion values aggregated within all referenced system constant value sets and post build variant criterion value sets plus the value sets of the included variants."""
	def __init__(self):
		super().__init__()
		self._includedVariant_children=[]
		self._postBuildVariantCriterionValueSet_children=[]
		self._swSystemconstantValueSet_children=[]

class PrimitiveAttributeCondition(complexbase.GroupBase):
	"""The PrimitiveAttributeCondition evaluates to true, if the referenced primitive attribute is accepted by all rules of this condition."""
	def __init__(self):
		super().__init__()
		self._attribute_child=ModelNone

class PrimitiveAttributeTailoring(complexbase.GroupBase):
	"""Tailoring of primitive attributes.
Primitive attributes are attributes that have a type which is marked by the stereotype <<primitive>> or <<enumeration>>"""
	def __init__(self):
		super().__init__()
		self._defaultValueHandling_child=ModelNone
		self._subAttributeTailoring_children=[]
		self._valueRestriction_child=ModelNone

class PrivacyLevel(complexbase.GroupBase):
	"""This meta-class defines the Privacy Level for a Log and Trace content."""
	def __init__(self):
		super().__init__()
		self._compuMethod_child=ModelNone
		self._privacyLevel_child=ModelNone

class PrmChar(complexbase.GroupBase):
	"""This metaclass represents the ability to express the characteristics of one particular parameter. It can be exressed as numerical or as text parameter (provided as subclasses of PrmCharContents)"""
	def __init__(self):
		super().__init__()
		self._cond_child=ModelNone
		self._remark_child=ModelNone
		self._text_child=ModelNone
		self._prmUnit_child=ModelNone
		self._max_child=ModelNone
		self._typ_child=ModelNone
		self._min_child=ModelNone
		self._tol_child=ModelNone
		self._abs_child=ModelNone

class PrmCharAbsTol(complexbase.GroupBase):
	"""The parameter is specified as ablolute value with a tolerance."""
	def __init__(self):
		super().__init__()
		self._abs_child=ModelNone
		self._tol_child=ModelNone

class PrmCharContents(complexbase.GroupBase):
	"""This is the contents of the parameter."""

class PrmCharMinTypMax(complexbase.GroupBase):
	"""This metaclass represents the characteristics of a parameter as minimal, typical maximum value."""
	def __init__(self):
		super().__init__()
		self._min_child=ModelNone
		self._typ_child=ModelNone
		self._max_child=ModelNone

class PrmCharNumericalContents(complexbase.GroupBase):
	"""This metaclass represents the fact that it is a numerical parameter."""
	def __init__(self):
		super().__init__()
		self._prmUnit_child=ModelNone
		self._max_child=ModelNone
		self._typ_child=ModelNone
		self._min_child=ModelNone
		self._tol_child=ModelNone
		self._abs_child=ModelNone

class PrmCharNumericalValue(complexbase.GroupBase):
	"""This metaclass represents a numercial parameter characteristics."""

class PrmCharTextualContents(complexbase.GroupBase):
	"""This metaclass represents the fact that it is a textual parameter."""
	def __init__(self):
		super().__init__()
		self._text_child=ModelNone

class Prms(complexbase.GroupBase):
	"""This metaclass represents the ability to specify a parameter table. It can be used e.g. to specify parameter tables in a data sheet."""
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._prm_children=[]

class Process(complexbase.GroupBase):
	"""This meta-class provides information required to execute the referenced [ARMetaClass{Executable}]."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._executable_children=[]
		self._functionClusterAffiliation_child=ModelNone
		self._numberOfRestartAttempts_child=ModelNone
		self._preMapping_child=ModelNone
		self._processStateMachine_child=ModelNone
		self._securityEvent_children=[]
		self._stateDependentStartupConfig_children=[]

class ProcessArgument(complexbase.GroupBase):
	"""This meta-class has the ability to define command line arguments for processing by the Main function."""
	def __init__(self):
		super().__init__()
		self._argument_child=ModelNone

class ProcessDesign(complexbase.GroupBase):
	"""This meta-class has the ability to stand in for a Process at the time when the Process does not yet exist. But its future existence already needs to be considered during design phase and for that a dedicated model element is required.."""
	def __init__(self):
		super().__init__()
		self._executable_children=[]

class ProcessDesignToMachineDesignMapping(complexbase.GroupBase):
	"""This element is used in the design phase to predefine a mapping of a process to a machine. Such a mapping may be overruled in the deployment phase."""
	def __init__(self):
		super().__init__()
		self._machineDesign_child=ModelNone
		self._processDesign_child=ModelNone

class ProcessExecutionError(complexbase.GroupBase):
	"""This meta-class has the ability to describe the value of a execution error along with a documentation of its semantics."""
	def __init__(self):
		super().__init__()
		self._executionError_child=ModelNone

class ProcessToMachineMapping(complexbase.GroupBase):
	"""This meta-class has the ability to associate a Process with a Machine. This relation involves the definition of further properties, e.g. timeouts."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone
		self._machine_child=ModelNone
		self._nonOsModuleInstantiation_child=ModelNone
		self._persistencyCentralStorageURI_child=ModelNone
		self._process_child=ModelNone
		self._shallNotRunOn_children=[]
		self._shallRunOn_children=[]

class ProcessToMachineMappingSet(complexbase.GroupBase):
	"""This meta-class acts as a bucket for collecting ProcessToMachineMappings."""
	def __init__(self):
		super().__init__()
		self._processToMachineMapping_children=[]

class Processor(complexbase.GroupBase):
	"""This represents a processor for the execution of an AUTOSAR adaptive platform"""
	def __init__(self):
		super().__init__()
		self._core_children=[]

class ProcessorCore(complexbase.GroupBase):
	"""This meta-class represents the ability to model a processor core for the execution of an AUTOSAR adaptive platform."""
	def __init__(self):
		super().__init__()
		self._coreId_child=ModelNone

class ProvidedApServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a provided service instance in an abstract way."""

class ProvidedServiceInstance(complexbase.GroupBase):
	"""Service instances that are provided by the ECU that is connected via the ApplicationEndpoint to a CommunicationConnector."""
	def __init__(self):
		super().__init__()
		self._allowedServiceConsumer_children=[]
		self._autoAvailable_child=ModelNone
		self._eventHandler_children=[]
		self._instanceIdentifier_child=ModelNone
		self._loadBalancingPriority_child=ModelNone
		self._loadBalancingWeight_child=ModelNone
		self._localUnicastAddress_children=[]
		self._minorVersion_child=ModelNone
		self._priority_child=ModelNone
		self._remoteMulticastSubscriptionAddress_children=[]
		self._remoteUnicastAddress_children=[]
		self._sdServerConfig_child=ModelNone
		self._sdServerTimerConfig_children=[]
		self._serviceIdentifier_child=ModelNone

class ProvidedServiceInstanceRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._ProvidedServiceInstance_child=ModelNone
		self._variationPoint_child=ModelNone

class ProvidedServiceInstanceToSwClusterDesignPPortPrototypeMapping(complexbase.GroupBase):
	"""This concrete meta-class represents the ability to assign a transport-layer-dependent ProvidedServiceInstance to a PPortPrototype in the context of the SoftwareClusterDesign. With this mapping it is possible to define the list of provided and required AdaptivePlatformServiceInstances in the scope of the SoftwareClusterDesign."""
	def __init__(self):
		super().__init__()
		self._providedPortPrototype_child=ModelNone
		self._providedServiceInstance_child=ModelNone

class ProvidedSomeipServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a provided service instance in a concrete implementation on top of SOME/IP."""
	def __init__(self):
		super().__init__()
		self._capabilityRecord_children=[]
		self._eventProps_children=[]
		self._loadBalancingPriority_child=ModelNone
		self._loadBalancingWeight_child=ModelNone
		self._methodResponseProps_children=[]
		self._priority_child=ModelNone
		self._providedEventGroup_children=[]
		self._sdServerConfig_child=ModelNone
		self._serviceInstanceId_child=ModelNone

class ProvidedUserDefinedServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a provided service instance in a concrete implementation that is not standardized by AUTOSAR."""

class PskIdentityToKeySlotMapping(complexbase.GroupBase):
	"""This meta-class allows to map a PresharedKeyIdentity to a concrete key that will be used for a crypto operation."""

class QueuedReceiverComSpec(complexbase.GroupBase):
	"""Communication attributes specific to queued receiving."""
	def __init__(self):
		super().__init__()
		self._queueLength_child=ModelNone

class QueuedSenderComSpec(complexbase.GroupBase):
	"""Communication attributes specific to distribution of  events (PPortPrototype, SenderReceiverInterface and dataElement carries an \"event\")."""

class RModeGroupInAtomicSWCInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetModeGroup_child=ModelNone

class RModeInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextPort_child=ModelNone
		self._contextModeDeclarationGroupPrototype_child=ModelNone
		self._targetModeDeclaration_child=ModelNone

class ROperationInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetRequiredOperation_child=ModelNone

class RPortComSpec(complexbase.GroupBase):
	"""Communication attributes of a required PortPrototype. This class will contain attributes that are valid for all kinds of require-ports, independent of client-server or sender-receiver communication patterns."""

class RPortInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_child=ModelNone
		self._targetRPort_child=ModelNone

class RPortPrototype(complexbase.GroupBase):
	"""Component port requiring a certain port interface."""
	def __init__(self):
		super().__init__()
		self._mayBeUnconnected_child=ModelNone
		self._requiredInterface_child=ModelNone

class RPortPrototypeInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextComponentPrototype_children=[]
		self._targetRPortPrototype_child=ModelNone

class RPortPrototypeInSoftwareClusterDesignInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwClusterDesignComponentPrototype_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._targetRPortPrototype_child=ModelNone

class RPortPrototypeProps(complexbase.GroupBase):
	"""PortPrototypeProps for a RPort."""
	def __init__(self):
		super().__init__()
		self._searchIntention_child=ModelNone

class RTriggerInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetTrigger_child=ModelNone

class RVariableInAtomicSwcInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRPort_child=ModelNone
		self._targetDataElement_child=ModelNone

class RapidPrototypingScenario(complexbase.GroupBase):
	"""This meta-class provides the ability to describe a Rapid Prototyping Scenario. Such a Rapid Prototyping Scenario consist out of two main aspects, the description of the byPassPoints and the relation to an rptHook."""
	def __init__(self):
		super().__init__()
		self._hostSystem_child=ModelNone
		self._rptContainer_children=[]
		self._rptProfile_children=[]
		self._rptSystem_child=ModelNone

class RawDataStreamClientInterface(complexbase.GroupBase):
	"""This meta-class represents the necessary capabilities for raw data streaming on the client side, i.e. the streaming of data that do not undergo any serialization.
Each RawDataStreamClientInterface supports the following capabilities without further modeling:

* connect: set up the communication channel
* shutdown: close the communication channel
* write: send data down the communication channel
* read: access incoming data on the communication channel"""

class RawDataStreamDeployment(complexbase.GroupBase):
	"""This meta-class represents the ability to model deployment-level information for a raw data stream"""
	def __init__(self):
		super().__init__()
		self._rawDataStreamInterface_child=ModelNone

class RawDataStreamEthernetTcpUdpCredentials(complexbase.GroupBase):
	"""This-meta-class represents the ability to create a configuration of network credentials for a raw data stream connection over TCP and UDP (inherited from base class)."""
	def __init__(self):
		super().__init__()
		self._tcpPort_child=ModelNone

class RawDataStreamEthernetUdpCredentials(complexbase.GroupBase):
	"""This-meta-class represents the ability to create a configuration of network credentials for a raw data stream connection over UDP."""

class RawDataStreamGrant(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to define the IAM configuration for a RawDataStream on deployment level."""
	def __init__(self):
		super().__init__()
		self._design_child=ModelNone

class RawDataStreamGrantDesign(complexbase.GroupBase):
	"""This meta-class represents the ability to define the IAM configuration for a RawDataStream on design level."""
	def __init__(self):
		super().__init__()
		self._rawDataStream_child=ModelNone

class RawDataStreamMapping(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for mapping raw data streams to the application software."""
	def __init__(self):
		super().__init__()
		self._deployment_child=ModelNone
		self._portPrototype_child=ModelNone
		self._process_child=ModelNone

class RawDataStreamServerInterface(complexbase.GroupBase):
	"""This meta-class represents the necessary capabilities for raw data streaming on the server side, i.e. the streaming of data
that do not undergo any serialization.

Each RawDataStreamServerInterface supports the following capabilities without further modeling:

* waitForConnection: wait until a communication channel is set up.
* shutdown: close the communication channel
* write: send data down the communication channel
* read: access incoming data on the communication channel"""

class ReceiverAnnotation(complexbase.GroupBase):
	"""Annotation of a receiver port, specifying properties of data elements that don't affect communication or generation of the RTE. The given attributes are requirements on the required data."""
	def __init__(self):
		super().__init__()
		self._signalAge_child=ModelNone

class ReceiverComSpec(complexbase.GroupBase):
	"""'''begin restrict to CP'''
Receiver-specific communication attributes (RPortPrototype typed by SenderReceiverInterface).
'''end restrict to CP'''
'''begin restrict to AP'''
Receiver-specific communication attributes (RPortPrototype typed by ServiceInterface) that are relevant for events and field notifiers.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._compositeNetworkRepresentation_children=[]
		self._dataElement_child=ModelNone
		self._dataUpdatePeriod_child=ModelNone
		self._externalReplacement_child=ModelNone
		self._handleOutOfRange_child=ModelNone
		self._handleOutOfRangeStatus_child=ModelNone
		self._maxDeltaCounterInit_child=ModelNone
		self._maxNoNewOrRepeatedData_child=ModelNone
		self._networkRepresentation_child=ModelNone
		self._receiverIntent_child=ModelNone
		self._receptionProps_child=ModelNone
		self._replaceWith_child=ModelNone
		self._syncCounterInit_child=ModelNone
		self._transformationComSpecProps_children=[]
		self._usesEndToEndProtection_child=ModelNone

class ReceptionComSpecProps(complexbase.GroupBase):
	"""This meta-class defines a set of reception attributes which the application software is assumed to implement."""
	def __init__(self):
		super().__init__()
		self._dataUpdatePeriod_child=ModelNone
		self._timeout_child=ModelNone

class RecordValueSpecification(complexbase.GroupBase):
	"""Specifies the values for a record."""
	def __init__(self):
		super().__init__()
		self._field_children=[]

class RecoveryNotification(complexbase.GroupBase):
	"""This meta-class represents a PHM action that can trigger a recovery operation inside a piece of StateManagement software."""
	def __init__(self):
		super().__init__()
		self._recoveryNotificationTimeout_child=ModelNone

class RecoveryNotificationToPPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a RecoveryNotification to a PPortPrototype while also being able to identify the respective Process in which the actual recovery executes."""
	def __init__(self):
		super().__init__()
		self._process_child=ModelNone
		self._recoveryAction_child=ModelNone
		self._recoveryNotification_child=ModelNone

class ReferenceBase(complexbase.GroupBase):
	"""This meta-class establishes a basis for relative references. Reference bases are identified by the shortLabel which shall be unique in the current package."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._isDefault_child=ModelNone
		self._isGlobal_child=ModelNone
		self._baseIsThisPackage_child=ModelNone
		self._globalInPackage_children=[]
		self._globalElement_children=[]
		self._package_child=ModelNone

class ReferenceCondition(complexbase.GroupBase):
	"""The ReferenceCondition evaluates to true, if the referenced reference is accepted by all rules of this condition."""
	def __init__(self):
		super().__init__()
		self._reference_child=ModelNone

class ReferenceTailoring(complexbase.GroupBase):
	"""Tailoring of Non-Containment References."""
	def __init__(self):
		super().__init__()
		self._typeTailoring_children=[]
		self._unresolvedReferenceRestriction_child=ModelNone

class ReferenceValueSpecification(complexbase.GroupBase):
	"""Specifies a reference to a data prototype to be used as an initial value for a pointer in the software."""
	def __init__(self):
		super().__init__()
		self._referenceValue_child=ModelNone

class Referrable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (while adhering to namespace borders)."""
	def __init__(self):
		super().__init__()
		self._shortName_child=ModelNone
		self._shortNameFragment_children=[]
		self._references=[]

class ReferrableRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._Referrable_child=ModelNone
		self._variationPoint_child=ModelNone

class RelativeTolerance(complexbase.GroupBase):
	"""Maximum allowable deviation"""
	def __init__(self):
		super().__init__()
		self._relative_child=ModelNone

class RemotingTechnology(complexbase.GroupBase):
	"""This element is deprecated and will be removed in future. 

Old description: Defines the used remoting Technology."""
	def __init__(self):
		super().__init__()
		self._name_child=ModelNone
		self._version_child=ModelNone

class RequestResponseDelay(complexbase.GroupBase):
	"""Time to wait before answering the query."""
	def __init__(self):
		super().__init__()
		self._maxValue_child=ModelNone
		self._minValue_child=ModelNone

class RequiredApServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a required service instance in an abstract way."""

class RequiredMethodInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextComponentPrototype_children=[]
		self._contextRPortPrototype_child=ModelNone
		self._targetMethod_child=ModelNone

class RequiredServiceInstanceToSwClusterDesignRPortPrototypeMapping(complexbase.GroupBase):
	"""This concrete meta-class represents the ability to assign a transport-layer-dependent RequiredServiceInstance to an RPortPrototype in the context of the SoftwareClusterDesign. With this mapping it is possible to define the list of provided and required AdaptivePlatformServiceInstances in the scope of the SoftwareClusterDesign."""
	def __init__(self):
		super().__init__()
		self._requiredPortPrototype_child=ModelNone
		self._requiredServiceInstance_child=ModelNone

class RequiredSomeipServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a required service instance in a concrete implementation on top of SOME/IP."""
	def __init__(self):
		super().__init__()
		self._blocklistedVersion_children=[]
		self._capabilityRecord_children=[]
		self._methodRequestProps_children=[]
		self._requiredEventGroup_children=[]
		self._requiredMinorVersion_child=ModelNone
		self._requiredServiceInstanceId_child=ModelNone
		self._sdClientConfig_child=ModelNone
		self._versionDrivenFindBehavior_child=ModelNone

class RequiredUserDefinedServiceInstance(complexbase.GroupBase):
	"""This meta-class represents the ability to describe the existence and configuration of a required service instance in a concrete implementation that is not standardized by AUTOSAR."""

class ResourceConsumption(complexbase.GroupBase):
	"""Description of consumed resources by one implementation of a software."""
	def __init__(self):
		super().__init__()
		self._accessCountSet_children=[]
		self._executionTime_children=[]
		self._heapUsage_children=[]
		self._memorySection_children=[]
		self._memoryUsage_children=[]
		self._sectionNamePrefix_children=[]
		self._stackUsage_children=[]

class ResourceGroup(complexbase.GroupBase):
	"""This meta-class represents a resource group that limits the resource usage of a collection of processes."""
	def __init__(self):
		super().__init__()
		self._cpuUsage_child=ModelNone
		self._memUsage_child=ModelNone

class RestrictionWithSeverity(complexbase.GroupBase):
	"""A restriction that has a severity. The severity describes the severity level that is reported in case the restriction is violated."""
	def __init__(self):
		super().__init__()
		self._severity_child=ModelNone

class RoleBasedBswModuleEntryAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular BswModuleEntry (usually a configurable callback). 

With this assignment, the role of the callback is mapped to a specific ServiceNeeds element, so that a tool is able to create appropriate configuration values for the module that implements the AUTOSAR Service."""
	def __init__(self):
		super().__init__()
		self._assignedEntry_child=ModelNone
		self._role_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedDataAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular data object in either

* the SwcInternalBehavior of a software component (or in the BswInternalBehavior of a BSW module or BSW cluster) in the context of an AUTOSAR Service or
* an NvBlockDescriptor to sort out the assignment of event-based writing strategies to data elements in a PortPrototype.

With this assignment, the role of the data can be mapped to a DataPrototype that is used in the context of the definition of a specific ServiceNeeds or NvBlockDescriptor, so that a tool is able to create the correct access or writing strategy."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._usedDataElement_child=ModelNone
		self._usedParameterElement_child=ModelNone
		self._usedPim_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedDataTypeAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular data type of a software component (or in the BswModuleBehavior of a module or cluster) in the context of an AUTOSAR Service.

With this assignment, the role of the data type can be mapped to a specific ServiceNeeds element, so that a tool is able to create the correct access."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._usedImplementationDataType_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedMcDataAssignment(complexbase.GroupBase):
	"""This meta-class allows to define links that specify logical relationships between single McDataInstances. The details on the existence and semantics of such links are not standardized.

Possible Use Case: 
Rapid Prototyping solutions in which additional communication buffers and switches are implemented  in the RTE that allow to switch between the usage of the original and the bypass buffers. The different buffers and the switch can be represented by McDataInstances (in order to be accessed by MC tools) which have relationships to each other."""
	def __init__(self):
		super().__init__()
		self._executionContext_children=[]
		self._mcDataInstance_children=[]
		self._role_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedPortAssignment(complexbase.GroupBase):
	"""This class specifies an assignment of a role to a particular service port (RPortPrototype or PPortPrototype) of an AtomicSwComponentType. With this assignment, the role of the service port can be mapped to a specific ServiceNeeds element, so that a tool is able to create the correct connector."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._role_child=ModelNone
		self._variationPoint_child=ModelNone

class RoleBasedResourceDependency(complexbase.GroupBase):
	"""This class specifies a dependency between CpSoftwareClusterResources."""
	def __init__(self):
		super().__init__()
		self._resource_child=ModelNone
		self._role_child=ModelNone

class RootSwClusterDesignComponentPrototype(complexbase.GroupBase):
	"""This meta-class represents the ability to define the service endpoints in the scope of a SwClusterDesign."""
	def __init__(self):
		super().__init__()
		self._applicationType_child=ModelNone

class RootSwComponentPrototype(complexbase.GroupBase):
	"""The RootSwCompositionPrototype represents the top-level-composition of software components within an Executable.

The contained SwComponentPrototypes are fully specified by their SwComponentTypes (including PortPrototypes, PortInterfaces, VariableDataPrototypes, etc.)."""
	def __init__(self):
		super().__init__()
		self._applicationType_child=ModelNone

class RootSwCompositionPrototype(complexbase.GroupBase):
	"""The RootSwCompositionPrototype represents the top-level-composition of software components within a given System.

'''begin restrict to CP AP'''
According to the use case of the System, this may for example be a more or less complete VFB description, the software of a System Extract or the software of a flat ECU Extract with only atomic SWCs. 
'''begin restrict to FO'''
This may for example be a more or less complete VFB++ description.
'''end restrict to FO'''
Therefore the RootSwComposition will only occasionally contain all atomic software components that are used in a complete VFB System. The OEM is primarily interested in the required functionality and the interfaces defining the integration of the Software Component into the System. The internal structure of such a component contains often substantial intellectual property of a supplier. Therefore a top-level software composition will often contain empty compositions which represent subsystems. 

'''begin restrict to CP AP'''
The contained SwComponentPrototypes are fully specified by their SwComponentTypes (including PortPrototypes, PortInterfaces, VariableDataPrototypes, SwcInternalBehavior etc.), and their ports are interconnected using SwConnectorPrototypes.
'''begin restrict to FO'''
The contained SwComponentPrototypes are fully specified by their SwComponentTypes (including PortPrototypes, PortInterfaces, VariableDataPrototypes, etc.).
'''end restrict to FO'''"""
	def __init__(self):
		super().__init__()
		self._calibrationParameterValueSet_children=[]
		self._flatMap_child=ModelNone
		self._softwareComposition_child=ModelNone
		self._variationPoint_child=ModelNone

class RoughEstimateHeapUsage(complexbase.GroupBase):
	"""Rough estimation of the heap usage."""
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class RoughEstimateOfExecutionTime(complexbase.GroupBase):
	"""Provides a description of a rough estimate on the ExecutionTime."""
	def __init__(self):
		super().__init__()
		self._additionalInformation_child=ModelNone
		self._estimatedExecutionTime_child=ModelNone

class RoughEstimateStackUsage(complexbase.GroupBase):
	"""Rough estimation of the stack usage."""
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class Row(complexbase.GroupBase):
	"""This meta-class represents the ability to express one row in a table."""
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates if by default a line should be displayed below the row.""")
	valign=complexbase.Attribute("valign",SimpleTypes.ValignEnum,'VALIGN',False,"""Indicates how the cells in the rows shall be aligned. Default is inherited from tbody, otherwise it is \"TOP\"""")
	def __init__(self):
		super().__init__()
		self._entry_children=[]
		self._variationPoint_child=ModelNone

class RptComponent(complexbase.GroupBase):
	"""Description of component instance for which rapid prototyping support is implemented."""
	def __init__(self):
		super().__init__()
		self._mcDataAssignment_children=[]
		self._rpImplPolicy_child=ModelNone
		self._rptExecutableEntity_children=[]
		self._variationPoint_child=ModelNone

class RptContainer(complexbase.GroupBase):
	"""This meta-class defines a byPassPoint and the relation to a rptHook.

Additionally it may contain further rptContainers if the byPassPoint is not atomic. For example a byPassPoint referencing to a RunnableEntity may contain rptContainers referring to the data access points of the RunnableEntity. 

The RptContainer structure on M1 shall follow the M1 structure of the Software Component Descriptions. The category attribute denotes which level of the Software Component Description is annotated."""
	def __init__(self):
		super().__init__()
		self._byPassPoint_children=[]
		self._explicitRptProfileSelection_children=[]
		self._rptContainer_children=[]
		self._rptExecutableEntityProperties_child=ModelNone
		self._rptHook_children=[]
		self._rptImplPolicy_child=ModelNone
		self._rptSwPrototypingAccess_child=ModelNone
		self._variationPoint_child=ModelNone

class RptExecutableEntity(complexbase.GroupBase):
	"""This describes a ExecutableEntity instance which can be bypassed."""
	def __init__(self):
		super().__init__()
		self._rptExecutableEntityEvent_children=[]
		self._rptRead_children=[]
		self._rptWrite_children=[]
		self._symbol_child=ModelNone
		self._variationPoint_child=ModelNone

class RptExecutableEntityEvent(complexbase.GroupBase):
	"""This describes an ExecutableEntity event instance which can be bypassed."""
	def __init__(self):
		super().__init__()
		self._executionContext_children=[]
		self._mcDataAssignment_children=[]
		self._rptEventId_child=ModelNone
		self._rptExecutableEntityProperties_child=ModelNone
		self._rptImplPolicy_child=ModelNone
		self._rptServicePointPost_children=[]
		self._rptServicePointPre_children=[]
		self._variationPoint_child=ModelNone

class RptExecutableEntityProperties(complexbase.GroupBase):
	"""Describes the code preparation for rapid prototyping at ExecutableEntity invocation."""
	def __init__(self):
		super().__init__()
		self._maxRptEventId_child=ModelNone
		self._minRptEventId_child=ModelNone
		self._rptExecutionControl_child=ModelNone
		self._rptServicePoint_child=ModelNone

class RptExecutionContext(complexbase.GroupBase):
	"""Defines an environment for the execution of ExecutableEntites which is qualified by

* OSTask
* communication buffer usage"""

class RptHook(complexbase.GroupBase):
	"""This meta-class provide the ability to describe a rapid prototyping hook. This can either be described by an other AUTOSAR system with the category RPT_SYSTEM or as a non AUTOSAR software."""
	def __init__(self):
		super().__init__()
		self._codeLabel_child=ModelNone
		self._mcdIdentifier_child=ModelNone
		self._rptArHook_child=ModelNone
		self._sdg_children=[]
		self._variationPoint_child=ModelNone

class RptImplPolicy(complexbase.GroupBase):
	"""Describes the code preparation for rapid prototyping at data accesses."""
	def __init__(self):
		super().__init__()
		self._rptEnablerImplType_child=ModelNone
		self._rptPreparationLevel_child=ModelNone

class RptProfile(complexbase.GroupBase):
	"""The RptProfile describes the common properties of a Rapid Prototyping method."""
	def __init__(self):
		super().__init__()
		self._maxServicePointId_child=ModelNone
		self._minServicePointId_child=ModelNone
		self._servicePointSymbolPost_child=ModelNone
		self._servicePointSymbolPre_child=ModelNone
		self._stimEnabler_child=ModelNone

class RptServicePoint(complexbase.GroupBase):
	"""Description of a Service Point implemented for rapid prototyping."""
	def __init__(self):
		super().__init__()
		self._serviceId_child=ModelNone
		self._symbol_child=ModelNone
		self._variationPoint_child=ModelNone

class RptSupportData(complexbase.GroupBase):
	"""Root element for rapid prototyping support data related to one Implementation artifact on an ECU, in particular the RTE. 
The rapid prototyping support data may reference to elements provided for McSupportData."""
	def __init__(self):
		super().__init__()
		self._executionContext_children=[]
		self._rptComponent_children=[]
		self._rptServicePoint_children=[]

class RptSwPrototypingAccess(complexbase.GroupBase):
	"""Describes the accessibility of data and modes by the rapid prototyping tooling."""
	def __init__(self):
		super().__init__()
		self._rptHookAccess_child=ModelNone
		self._rptReadAccess_child=ModelNone
		self._rptWriteAccess_child=ModelNone

class RTEEvent(complexbase.GroupBase):
	"""Abstract base class for all RTE-related events"""
	def __init__(self):
		super().__init__()
		self._disabledMode_children=[]
		self._startOnEvent_child=ModelNone
		self._variationPoint_child=ModelNone

class RteEventInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextSwComponent_children=[]
		self._targetRteEvent_child=ModelNone

class RteEventInCompositionSeparation(complexbase.GroupBase):
	"""This meta-class is used to define a separation constraint in the context of a SwComposition. The referenced RteEvents are not allowed to be mapped into the same OsTask."""
	def __init__(self):
		super().__init__()
		self._rteEvent_children=[]

class RteEventInCompositionToOsTaskProxyMapping(complexbase.GroupBase):
	"""This meta-class is used to map an RteEvent to an OsTaskProxy in the context of a SwComposition.
Several RteEventInCompositionToOsTaskProxyMappings can be used to define a pairing constraint that describes which RteEvents shall be mapped together into an OsTask. 
Optionally the relative position of the RteEvents in the OsTask can be defined in the mapping."""
	def __init__(self):
		super().__init__()
		self._offset_child=ModelNone
		self._osTaskProxy_child=ModelNone
		self._rteEvent_child=ModelNone

class RteEventInEcuInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextAtomicComponent_child=ModelNone
		self._targetRteEvent_child=ModelNone

class RteEventInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextSwComponent_children=[]
		self._targetRteEvent_child=ModelNone

class RteEventInSystemSeparation(complexbase.GroupBase):
	"""This meta-class is used to define a separation constraint in the context of the System. The referenced RteEvents are not allowed to be mapped into the same OsTask."""
	def __init__(self):
		super().__init__()
		self._rteEvent_children=[]

class RteEventInSystemToOsTaskProxyMapping(complexbase.GroupBase):
	"""This meta-class is used to map an RteEvent to an OsTaskProxy in the context of the System.
Several RteEventToOsTaskProxyMappings can be used to define a pairing constraint that describes which RteEvents shall be mapped together into an OsTask. 
Optionally the position of the RteEvents in the OsTask can be defined."""
	def __init__(self):
		super().__init__()
		self._offset_child=ModelNone
		self._osTaskProxy_child=ModelNone
		self._rteEvent_child=ModelNone

class RtePluginProps(complexbase.GroupBase):
	"""The properties of a communication graph with respect to the utilization of RTE Implementation Plug-in."""
	def __init__(self):
		super().__init__()
		self._associatedCrossSwClusterComRtePlugin_child=ModelNone
		self._associatedRtePlugin_child=ModelNone

class RtpTp(complexbase.GroupBase):
	"""RTP over UDP or over TCP as transport protocol."""
	def __init__(self):
		super().__init__()
		self._ssrc_child=ModelNone
		self._tcpUdpConfig_child=[]

class RuleArguments(complexbase.GroupBase):
	"""This represents the arguments for a rule-based value specification."""
	def __init__(self):
		super().__init__()
		self._v_children=[]
		self._vf_children=[]
		self._vt_children=[]
		self._vtf_children=[]
		self._variationPoint_children=[]

class RuleBasedAxisCont(complexbase.GroupBase):
	"""This represents the values for the axis of a compound primitive (curve, map).

For standard and fix axes,  SwAxisCont contains the values of the axis directly. 

The axis values of SwAxisCont with the category COM_AXIS, RES_AXIS are for display only. For editing and processing, only the values in the related GroupAxis are binding."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._unit_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swAxisIndex_child=ModelNone
		self._ruleBasedValues_child=ModelNone

class RuleBasedValueCont(complexbase.GroupBase):
	"""This represents the values of a compound primitive (CURVE, MAP, CUBOID, CUBE_4, CUBE_5, VAL_BLK) or an array."""
	def __init__(self):
		super().__init__()
		self._unit_child=ModelNone
		self._swArraysize_child=ModelNone
		self._ruleBasedValues_child=ModelNone

class RuleBasedValueSpecification(complexbase.GroupBase):
	"""This meta-class is used to support a rule-based initialization approach for data types with an array-nature (ApplicationArrayDataType and ImplementationDataType of category ARRAY) or a compound ApplicationPrimitiveDataType (which also boils down to an array-nature)."""
	def __init__(self):
		super().__init__()
		self._rule_child=ModelNone
		self._arguments_children=[]
		self._maxSizeToFill_child=ModelNone

class RunnableEntity(complexbase.GroupBase):
	"""A RunnableEntity represents the smallest code-fragment that is provided by an AtomicSwComponentType and are executed under control of the RTE. RunnableEntities are for instance set up to respond to data reception or operation invocation on a server."""
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._asynchronousServerCallResultPoint_children=[]
		self._canBeInvokedConcurrently_child=ModelNone
		self._dataReadAccess_children=[]
		self._dataReceivePointByArgument_children=[]
		self._dataReceivePointByValue_children=[]
		self._dataSendPoint_children=[]
		self._dataWriteAccess_children=[]
		self._externalTriggeringPoint_children=[]
		self._internalTriggeringPoint_children=[]
		self._modeAccessPoint_children=[]
		self._modeSwitchPoint_children=[]
		self._parameterAccess_children=[]
		self._readLocalVariable_children=[]
		self._serverCallPoint_children=[]
		self._symbol_child=ModelNone
		self._waitPoint_children=[]
		self._writtenLocalVariable_children=[]
		self._variationPoint_child=ModelNone

class RunnableEntityArgument(complexbase.GroupBase):
	"""This meta-class represents the ability to provide specific information regarding the arguments to a RunnableEntity."""
	def __init__(self):
		super().__init__()
		self._symbol_child=ModelNone

class RunnableEntityGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to define a collection of RunnableEntities. The collection can be nested."""
	def __init__(self):
		super().__init__()
		self._runnableEntityGroup_children=[]
		self._runnableEntity_children=[]
		self._variationPoint_child=ModelNone

class RunnableEntityInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a RunnableEntity in the context of a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._targetRunnableEntity_child=ModelNone
		self._variationPoint_child=ModelNone

class RuntimeError(complexbase.GroupBase):
	"""The reported failure is classified as runtime error."""

class RxIdentifierRange(complexbase.GroupBase):
	"""Optional definition of a CanId range to reduce the effort of specifying every possible FrameTriggering within the defined Id range during reception. All frames received within a range are mapped to the same Pdu that is passed to a upper layer module (e.g. Nm, CDD, PduR)."""
	def __init__(self):
		super().__init__()
		self._lowerCanId_child=ModelNone
		self._upperCanId_child=ModelNone

class SaveConfigurationEntry(complexbase.GroupBase):
	"""This service is used to notify a slave node to store its configuration."""

class ScaleConstr(complexbase.GroupBase):
	"""This meta-class represents the ability to specify constraints as a list of intervals (called scales)."""
	validity=complexbase.Attribute("validity",SimpleTypes.ScaleConstrValidityEnum,'VALIDITY',False,"""Specifies if the values defined by the scales are considered to be valid. If the attribute is missing then the default value is \"VALID\".""")
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._desc_child=ModelNone
		self._lowerLimit_child=ModelNone
		self._upperLimit_child=ModelNone

class ScheduleTableEntry(complexbase.GroupBase):
	"""Table entry in a LinScheduleTable. Specifies what will be done in the frame slot."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._delay_child=ModelNone
		self._positionInTable_child=ModelNone

class SdClientConfig(complexbase.GroupBase):
	"""Client configuration for Service-Discovery."""
	def __init__(self):
		super().__init__()
		self._capabilityRecord_children=[]
		self._clientServiceMajorVersion_child=ModelNone
		self._clientServiceMinorVersion_child=ModelNone
		self._initialFindBehavior_child=ModelNone
		self._requestResponseDelay_child=ModelNone
		self._ttl_child=ModelNone

class SdServerConfig(complexbase.GroupBase):
	"""Server configuration for Service-Discovery."""
	def __init__(self):
		super().__init__()
		self._capabilityRecord_children=[]
		self._initialOfferBehavior_child=ModelNone
		self._offerCyclicDelay_child=ModelNone
		self._requestResponseDelay_child=ModelNone
		self._serverServiceMajorVersion_child=ModelNone
		self._serverServiceMinorVersion_child=ModelNone
		self._ttl_child=ModelNone

class Sdf(complexbase.GroupBase):
	"""This class represents a numerical value in a special data group which may be subject to variability."""
	gid=complexbase.Attribute("gid",SimpleTypes.NameToken,'GID',False,"""This attributes specifies an identifier. Gid comes from the SGML/XML-Term \"Generic Identifier\" which is the element name in XML. The role of this attribute is the same as the name of an XML - element.""")
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class Sdg(complexbase.GroupBase):
	"""Sdg (SpecialDataGroup) is a generic model which can be used to keep arbitrary information which is not explicitly modeled in the meta-model. 

Sdg can have various contents as defined by sdgContentsType. Special Data should only be used moderately since all elements should be defined in the meta-model. 

Thereby SDG should be considered as a temporary solution when no explicit model is available. If an sdgCaption is available, it is possible to establish a reference to the sdg structure."""
	gid=complexbase.Attribute("gid",SimpleTypes.NameToken,'GID',False,"""This attributes specifies an identifier. Gid comes from the SGML/XML-Term \"Generic Identifier\" which is the element name in XML. The role of this attribute is the same as the name of an XML - element.""")
	def __init__(self):
		super().__init__()
		self._sdgCaption_child=ModelNone
		self._sdgCaptionRef_child=ModelNone
		self._variationPoint_child=ModelNone
		self._sdf_children=[]
		self._sdg_children=[]
		self._sd_children=[]
		self._sdxf_children=[]
		self._sdx_children=[]

class SdgAbstractForeignReference(complexbase.GroupBase):
	"""An abstract reference that can point to any referrable object in an AUTOSAR Model."""
	def __init__(self):
		super().__init__()
		self._destMetaClass_child=ModelNone

class SdgAbstractPrimitiveAttribute(complexbase.GroupBase):
	"""Describes primitive attributes of a special data group."""

class SdgAggregationWithVariation(complexbase.GroupBase):
	"""Describes that the Sdg may contain another Sdg. The gid of the nested Sdg is defined by subSdg. 

Represents 'sdg'."""
	def __init__(self):
		super().__init__()
		self._subSdg_child=ModelNone

class SdgAttribute(complexbase.GroupBase):
	"""Describes the attributes of an Sdg."""

class SdgCaption(complexbase.GroupBase):
	"""This meta-class represents the caption of a special data group. This allows to have some parts of special data as identifiable."""
	def __init__(self):
		super().__init__()
		self._desc_child=ModelNone

class SdgClass(complexbase.GroupBase):
	"""An SdgClass specifies the name and structure of the SDG that may be used to store proprietary data in an AUTOSAR model. 

The SdgClass is similar to an UML stereotype."""
	def __init__(self):
		super().__init__()
		self._extendsMetaClass_child=ModelNone
		self._caption_child=ModelNone
		self._attribute_children=[]
		self._sdgConstraint_children=[]

class SdgContents(complexbase.GroupBase):
	"""This meta-class represents the possible contents of a special data group. It can be an arbitrary mix of references, of primitive special data and nested special data groups."""
	def __init__(self):
		super().__init__()
		self._sdx_children=[]
		self._sdxf_children=[]
		self._sd_children=[]
		self._sdg_children=[]
		self._sdf_children=[]

class SdgDef(complexbase.GroupBase):
	"""A SdgDef groups several SdgClasses which belong to the same extension.

The concept of an SdgDef is similiar to an UML Profile."""
	def __init__(self):
		super().__init__()
		self._sdgClass_children=[]

class SdgElementWithGid(complexbase.GroupBase):
	"""A special data group element with gid is an abstract element that shall have a name (gid, \"Generic Identifier\")."""
	def __init__(self):
		super().__init__()
		self._gid_child=ModelNone

class SdgForeignReference(complexbase.GroupBase):
	"""A reference without variation support that can point to any referrable object in an AUTOSAR Model. This class accepts the special data \"Sdx\" reference."""

class SdgForeignReferenceWithVariation(complexbase.GroupBase):
	"""A reference with variation support that can point to any referrable object in an AUTOSAR Model. This class accepts the special data \"Sdxf\" reference."""

class SdgPrimitiveAttribute(complexbase.GroupBase):
	"""Describes primitive special data attributes without variation.

This class accepts a special data \"sd\" attribute."""

class SdgPrimitiveAttributeWithVariation(complexbase.GroupBase):
	"""Describes a primitive numerical special data attribute with variation.

This class accepts a special data \"sdf\" element."""

class SdgReference(complexbase.GroupBase):
	"""Describes an attribute of a SdgClass which is used on the definition side to model a reference from one Sdg to another Sdg on the value side."""
	def __init__(self):
		super().__init__()
		self._destSdg_child=ModelNone

class SdgTailoring(complexbase.GroupBase):
	"""Describes if the referenced Sdg may be attached to the current class."""
	def __init__(self):
		super().__init__()
		self._sdgClass_child=ModelNone

class SecOcCryptoServiceMapping(complexbase.GroupBase):
	"""This meta-class has the ability to represent a crypto service mapping for the Pdu-based communication via SecOC."""
	def __init__(self):
		super().__init__()
		self._authentication_child=ModelNone
		self._cryptoServiceKey_child=ModelNone
		self._cryptoServiceQueue_child=ModelNone

class SecOcDeployment(complexbase.GroupBase):
	"""The meta-class represents the ability to define a deployment of the SecOc communication protocol configuration settings to crypto module entities."""
	def __init__(self):
		super().__init__()
		self._secOcJobMapping_children=[]

class SecOcJobMapping(complexbase.GroupBase):
	"""This meta-class allows to map a SecOcJobRequirement to a concrete crypto job that will fulfill the JobRequirement.

The crypto job represents a call to a specific routine that implements a crypto function and that uses a specific key and refers to a specific primitive as a formal representation of the crypto algorithm."""
	def __init__(self):
		super().__init__()
		self._secOcJobRequirement_child=ModelNone

class SecOcJobRequirement(complexbase.GroupBase):
	"""Requirements for the cryptographic job that need to be executed."""
	def __init__(self):
		super().__init__()
		self._secOcJobSemantic_child=ModelNone

class SecOcSecureComProps(complexbase.GroupBase):
	"""Configuration of AUTOSAR SecOC."""
	def __init__(self):
		super().__init__()
		self._authInfoTxLength_child=ModelNone
		self._authentication_child=ModelNone
		self._authenticationVerifyAttempts_child=ModelNone
		self._freshnessValueLength_child=ModelNone
		self._freshnessValueTxLength_child=ModelNone
		self._jobRequirement_children=[]

class SectionNamePrefix(complexbase.GroupBase):
	"""A prefix to be used for generated code artifacts defining a memory section name in the source code of the using module or SWC."""
	def __init__(self):
		super().__init__()
		self._implementedIn_child=ModelNone
		self._variationPoint_child=ModelNone

class SecureComProps(complexbase.GroupBase):
	"""This meta-class defines a communication security protocol and its configuration settings."""

class SecureCommunicationAuthenticationProps(complexbase.GroupBase):
	"""Authentication properties used to configure SecuredIPdus."""
	def __init__(self):
		super().__init__()
		self._authAlgorithm_child=ModelNone
		self._authInfoTxLength_child=ModelNone

class SecureCommunicationDeployment(complexbase.GroupBase):
	"""The meta-class represents the ability to define a deployment of secure communication protocol configuration settings to crypto module entities."""

class SecureCommunicationFreshnessProps(complexbase.GroupBase):
	"""Freshness properties used to configure SecuredIPdus."""
	def __init__(self):
		super().__init__()
		self._freshnessCounterSyncAttempts_child=ModelNone
		self._freshnessTimestampTimePeriodFactor_child=ModelNone
		self._freshnessValueLength_child=ModelNone
		self._freshnessValueTxLength_child=ModelNone
		self._useFreshnessTimestamp_child=ModelNone

class SecureCommunicationProps(complexbase.GroupBase):
	"""This meta-class contains configuration settings that are specific for an individual SecuredIPdu."""
	def __init__(self):
		super().__init__()
		self._authAlgorithm_child=ModelNone
		self._authDataFreshnessLength_child=ModelNone
		self._authDataFreshnessStartPosition_child=ModelNone
		self._authInfoTxLength_child=ModelNone
		self._authenticationBuildAttempts_child=ModelNone
		self._authenticationRetries_child=ModelNone
		self._dataId_child=ModelNone
		self._freshnessCounterSyncAttempts_child=ModelNone
		self._freshnessTimestampTimePeriodFactor_child=ModelNone
		self._freshnessValueId_child=ModelNone
		self._freshnessValueLength_child=ModelNone
		self._freshnessValueTxLength_child=ModelNone
		self._messageLinkLength_child=ModelNone
		self._messageLinkPosition_child=ModelNone
		self._secondaryFreshnessValueId_child=ModelNone
		self._securedAreaLength_child=ModelNone
		self._securedAreaOffset_child=ModelNone
		self._useFreshnessTimestamp_child=ModelNone

class SecureCommunicationPropsSet(complexbase.GroupBase):
	"""Collection of properties used to configure SecuredIPdus."""
	def __init__(self):
		super().__init__()
		self._authenticationProps_children=[]
		self._freshnessProps_children=[]

class SecureOnBoardCommunicationNeeds(complexbase.GroupBase):
	"""Specifies the need for the existence of the SecOc module on the respective ECU. This class currently contains no attributes. An instance of this class is used to find out which ports of a software-component deal with the administration of secure communication in order to group the request and response ports."""
	def __init__(self):
		super().__init__()
		self._verificationStatusIndicationMode_child=ModelNone

class SecuredIPdu(complexbase.GroupBase):
	"""If useAsCryptographicPdu is not set or set to false this IPdu contains the payload of an Authentic IPdu supplemented by additional Authentication Information (Freshness Counter and an Authenticator).

If useAsCryptographicPdu is set to true this IPdu contains the Authenticator for a payload that is transported in a separate message. The separate Authentic IPdu is described by the Pdu that is referenced with the payload reference from this SecuredIPdu."""
	def __init__(self):
		super().__init__()
		self._authenticationProps_child=ModelNone
		self._dynamicRuntimeLengthHandling_child=ModelNone
		self._freshnessProps_child=ModelNone
		self._payload_child=ModelNone
		self._secureCommunicationProps_child=ModelNone
		self._useAsCryptographicIPdu_child=ModelNone
		self._useSecuredPduHeader_child=ModelNone

class SecurityEventAggregationFilter(complexbase.GroupBase):
	"""This meta-class represents the aggregation filter that aggregates all security events occurring within a configured time frame into one (i.e. the last reported) security event."""
	def __init__(self):
		super().__init__()
		self._contextDataSource_child=ModelNone
		self._minimumIntervalLength_child=ModelNone

class SecurityEventContextData(complexbase.GroupBase):
	"""This meta-class represents the possibility that context data can be attached to the aggregating SecurityEventDefinition. If this meta-class does not exist for a SecurityEventDefinition, then no context data shall be provided for this SecurityEventDefinition."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SecurityEventContextMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to create an association between a collection of security events, an IdsM instance which handles the security events and the filter chains applicable to the security events."""
	def __init__(self):
		super().__init__()
		self._filterChain_children=[]
		self._idsmInstance_children=[]
		self._mappedSecurityEvent_children=[]

class SecurityEventContextMappingApplication(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a collection of security events with an IdsM instance and with the executional context of an application (e.g. name of SWC on CP or name of SWCL on AP)  in which this IdsM instance can receive reports for these security events."""
	def __init__(self):
		super().__init__()
		self._affectedApplication_child=ModelNone

class SecurityEventContextMappingBswModule(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a collection of security events with an IdsM instance and with the executional context of a BSW module in which this IdsM instance can receive reports for these security events."""
	def __init__(self):
		super().__init__()
		self._affectedBswModule_child=ModelNone

class SecurityEventContextMappingCommConnector(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a collection of security events with an IdsM instance and with the executional context related to a CommunicationConnector in which this IdsM instance can receive reports for these security events."""
	def __init__(self):
		super().__init__()
		self._commConnector_children=[]

class SecurityEventContextMappingFunctionalCluster(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a collection of security events with an IdsM instance and with the executional context of a functional cluster in which this IdsM instance can receive reports for these security events."""
	def __init__(self):
		super().__init__()
		self._affectedFunctionalCluster_child=ModelNone

class SecurityEventContextProps(complexbase.GroupBase):
	"""This meta-class specifies the SecurityEventDefinition to be mapped to an IdsmInstance and adds mapping-dependent properties of this security event valid only for this specific mapping."""
	def __init__(self):
		super().__init__()
		self._contextData_children=[]
		self._defaultReportingMode_child=ModelNone
		self._persistentStorage_child=ModelNone
		self._securityEvent_children=[]
		self._sensorInstanceId_child=ModelNone
		self._severity_child=ModelNone
		self._variationPoint_child=ModelNone

class SecurityEventDefinition(complexbase.GroupBase):
	"""This meta-class defines a security-related event as part of the intrusion detection system."""
	def __init__(self):
		super().__init__()
		self._eventSymbolName_child=ModelNone
		self._id_child=ModelNone

class SecurityEventDefinitionRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SecurityEventDefinition_child=ModelNone
		self._variationPoint_child=ModelNone

class SecurityEventFilterChain(complexbase.GroupBase):
	"""This meta-class represents a configurable chain of filters used to qualify security events.
The different filters of this filter chain are applied in the follow order: SecurityEventStateFilter, SecurityEventOneEveryNFilter, SecurityEventAggregationFilter, SecurityEventThresholdFilter."""
	def __init__(self):
		super().__init__()
		self._aggregation_child=ModelNone
		self._oneEveryN_child=ModelNone
		self._state_child=ModelNone
		self._threshold_child=ModelNone

class SecurityEventFilterChainRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SecurityEventFilterChain_child=ModelNone
		self._variationPoint_child=ModelNone

class SecurityEventMapping(complexbase.GroupBase):
	"""This meta-class represents a reportable instance of a security event."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._process_child=ModelNone
		self._reportingPortPrototype_child=ModelNone

class SecurityEventOneEveryNFilter(complexbase.GroupBase):
	"""This meta-class represents the configuration of a sampling (i.e. every n-th event is sampled) filter for security events."""
	def __init__(self):
		super().__init__()
		self._n_child=ModelNone

class SecurityEventReportInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for the reporting of security events in the context of the intrusion detection system."""

class SecurityEventReportToSecurityEventDefinitionMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a PortPrototype for reporting a security event to the actual security event that shall be reported by this PortPrototype."""
	def __init__(self):
		super().__init__()
		self._reportedSecurityEvent_child=ModelNone
		self._securityEventDefinition_child=ModelNone

class SecurityEventStateFilter(complexbase.GroupBase):
	"""This meta-class represents the configuration of a state filter for security events. The referenced states represent a block list, i.e. the security events are dropped if the referenced state is the active state in the relevant state machine (which depends on whether the IdsM instance runs on the Classic or the Adaptive Platform)."""
	def __init__(self):
		super().__init__()
		self._blockIfStateActiveAp_children=[]
		self._blockIfStateActiveCp_children=[]

class SecurityEventThresholdFilter(complexbase.GroupBase):
	"""This meta-class represents the threshold filter that drops (repeatedly at each beginning of a configurable time interval) a configurable number of security events . All subsequently arriving security events (within the configured time interval) pass the filter."""
	def __init__(self):
		super().__init__()
		self._intervalLength_child=ModelNone
		self._thresholdNumber_child=ModelNone

class SegmentPosition(complexbase.GroupBase):
	"""The StaticPart and the DynamicPart can be
separated in multiple segments within the multiplexed PDU. 

The ISignalIPdus are copied bit by bit into the MultiplexedIPdu. If the space of the first segment is 5 bits large than the first 5 bits of the ISignalIPdu are copied into this first segment and so on."""
	def __init__(self):
		super().__init__()
		self._segmentByteOrder_child=ModelNone
		self._segmentLength_child=ModelNone
		self._segmentPosition_child=ModelNone

class SenderAnnotation(complexbase.GroupBase):
	"""Annotation of a sender port, specifying properties of data elements that don't affect communication or generation of the RTE."""

class SenderComSpec(complexbase.GroupBase):
	"""'''begin restrict to CP'''
Communication attributes for a sender port (PPortPrototype typed by SenderReceiverInterface).
'''end restrict to CP'''
'''begin restrict to AP'''
Communication attributes for a sender port (PPortPrototype typed by ServiceInterface) that are relevant for events and field notifiers.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._compositeNetworkRepresentation_children=[]
		self._dataElement_child=ModelNone
		self._dataUpdatePeriod_child=ModelNone
		self._handleOutOfRange_child=ModelNone
		self._networkRepresentation_child=ModelNone
		self._senderIntent_child=ModelNone
		self._transmissionAcknowledge_child=ModelNone
		self._transmissionProps_child=ModelNone
		self._usesEndToEndProtection_child=ModelNone

class SenderRecArrayElementMapping(complexbase.GroupBase):
	"""The SenderRecArrayElement may be a primitive one or a composite one. If the element is primitive, it will be mapped to the SystemSignal (multiplicity 1). If the VariableDataPrototype that is referenced by SenderReceiverToSignalGroupMapping is typed by an ApplicationDataType the reference to the ApplicationArrayElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference to the ImplementationArrayElement shall be used. 

If the element is composite, there will be no mapping to the SystemSignal (multiplicity 0). In this case the ArrayElementMapping element will aggregate the TypeMapping element. In that way also the composite datatypes can be mapped to SystemSignals. 

Regardless whether composite or primitive array element is mapped the indexed element always needs to be specified."""
	def __init__(self):
		super().__init__()
		self._complexTypeMapping_child=[]
		self._indexedArrayElement_child=ModelNone
		self._systemSignal_child=ModelNone

class SenderRecArrayTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is an Array, the \"ArrayTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._arrayElementMapping_children=[]
		self._senderToSignalTextTableMapping_child=ModelNone
		self._signalToReceiverTextTableMapping_child=ModelNone

class SenderRecCompositeTypeMapping(complexbase.GroupBase):
	"""Two mappings exist for the composite data types:
\"ArrayTypeMapping\" and \"RecordTypeMapping\".
In both, a primitive datatype will be mapped to a system signal. 

But it is also possible to combine the arrays and the records, so that an \"array\" could be an element of a \"record\" and in the same manner a \"record\" could be an element of an \"array\". Nesting these data types is also possible.

If an element of a composite data type is again a composite one, the \"CompositeTypeMapping\" element will be used one more time (aggregation between the ArrayElementMapping and CompositeTypeMapping or aggregation between the RecordElementMapping and CompositeTypeMapping)."""

class SenderRecRecordElementMapping(complexbase.GroupBase):
	"""Mapping of a primitive record element to a SystemSignal. If the VariableDataPrototype that is referenced by SenderReceiverToSignalGroupMapping is typed by an ApplicationDataType the reference applicationRecordElement shall be used. If the VariableDataPrototype is typed by the ImplementationDataType the reference implementationRecordElement shall be used. Either the implementationRecordElement or applicationRecordElement reference shall be used. 

If the element is composite, there will be no mapping to the SystemSignal (multiplicity 0). In this case the RecordElementMapping element will aggregate the complexTypeMapping element. In that way also the composite datatypes can be mapped to SystemSignals."""
	def __init__(self):
		super().__init__()
		self._applicationRecordElement_child=ModelNone
		self._complexTypeMapping_child=[]
		self._implementationRecordElement_child=ModelNone
		self._senderToSignalTextTableMapping_child=ModelNone
		self._signalToReceiverTextTableMapping_child=ModelNone
		self._systemSignal_child=ModelNone

class SenderRecRecordTypeMapping(complexbase.GroupBase):
	"""If the ApplicationCompositeDataType is a Record, the \"RecordTypeMapping\" will be used."""
	def __init__(self):
		super().__init__()
		self._recordElementMapping_children=[]

class SenderReceiverAnnotation(complexbase.GroupBase):
	"""Annotation of the data elements in a port that realizes a sender/receiver interface."""
	def __init__(self):
		super().__init__()
		self._computed_child=ModelNone
		self._dataElement_child=ModelNone
		self._limitKind_child=ModelNone
		self._processingKind_child=ModelNone

class SenderReceiverCompositeElementToSignalMapping(complexbase.GroupBase):
	"""Mapping of an Variable Data Prototype which is aggregated within a composite datatype to a SystemSignal (only one element of the composite data type is mapped)."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._systemSignal_child=ModelNone
		self._typeMapping_child=[]

class SenderReceiverInterface(complexbase.GroupBase):
	"""A sender/receiver interface declares a number of data elements to be sent and received."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]
		self._invalidationPolicy_children=[]
		self._metaDataItemSet_children=[]

class SenderReceiverToSignalGroupMapping(complexbase.GroupBase):
	"""Mapping of a sender receiver communication data element with a composite datatype to a signal group."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._signalGroup_child=ModelNone
		self._typeMapping_child=[]

class SenderReceiverToSignalMapping(complexbase.GroupBase):
	"""Mapping of a sender receiver communication data element to a signal."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._senderToSignalTextTableMapping_child=ModelNone
		self._signalToReceiverTextTableMapping_child=ModelNone
		self._systemSignal_child=ModelNone

class SensorActuatorSwComponentType(complexbase.GroupBase):
	"""The SensorActuatorSwComponentType introduces the possibility to link from the software representation of a sensor/actuator to its hardware description provided by the ECU Resource Template."""
	def __init__(self):
		super().__init__()
		self._sensorActuator_child=ModelNone

class SeparateSignalPath(complexbase.GroupBase):
	"""The SeparateSignalPath describes that two SwcToSwcSignals and/or SwcToSwcOperationArguments shall not take the same way (Signal Path) in the topology (e.g. Redundancy). 
This means that the signals are not allowed to share even a single physical channel in their path."""
	def __init__(self):
		super().__init__()
		self._operation_children=[]
		self._signal_children=[]

class SequenceCounterMapping(complexbase.GroupBase):
	"""The purpose of sequence counters is to map a response to the correct request of a known client.

The SequenceCounter is mapped to the requestGroup and to the responseGroup."""
	def __init__(self):
		super().__init__()
		self._systemSignal_child=ModelNone

class SerializationTechnology(complexbase.GroupBase):
	"""<font color=\"#0f0f0f\">This element is deprecated and will be removed in future. This information is replaced by the TransformationTechnology element.</font>
<font color=\"#0f0f0f\">
</font><font color=\"#0f0f0f\">Old description: Identifies the used serialization technology. The corresponding serialization plugin has to be provided on each affected ECU by the integrator. </font>"""
	def __init__(self):
		super().__init__()
		self._protocol_child=ModelNone
		self._version_child=ModelNone

class ServerCallPoint(complexbase.GroupBase):
	"""If a RunnableEntity owns a ServerCallPoint it is entitled to invoke a particular ClientServerOperation of a specific RPortPrototype of the corresponding AtomicSwComponentType"""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._timeout_child=ModelNone
		self._variationPoint_child=ModelNone

class ServerComSpec(complexbase.GroupBase):
	"""'''begin restrict to CP'''
Communication attributes for a server port (PPortPrototype and ClientServerInterface).
'''end restrict to CP'''
'''begin restrict to AP'''
Server-specific communication attributes (PPortPrototype typed by ServiceInterface) that are relevant for methods and field getters and setters.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._getter_child=ModelNone
		self._operation_child=ModelNone
		self._queueLength_child=ModelNone
		self._setter_child=ModelNone
		self._transformationComSpecProps_children=[]

class ServiceDependency(complexbase.GroupBase):
	"""Collects all dependencies of a software module or component on an  AUTOSAR Service related to a specific item (e.g. an NVRAM Block, a diagnostic event etc.). It defines the quality of service (ServiceNeeds) of this item as well as (optionally) references to additional elements.

This information is required for tools in order to generate the related basic software configuration and ServiceSwComponentTypes."""
	def __init__(self):
		super().__init__()
		self._assignedDataType_children=[]
		self._diagnosticRelevance_child=ModelNone
		self._symbolicNameProps_child=ModelNone

class ServiceDiscoveryConfiguration(complexbase.GroupBase):
	"""Service Discovery configuration settings for the middleware transport layer."""

class ServiceEventDeployment(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to specify a deployment of an Event to a middleware transport layer."""
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone
		self._trigger_child=ModelNone
		self._variationPoint_child=ModelNone

class ServiceFieldDeployment(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to specify a deployment of a Field to a middleware transport layer."""
	def __init__(self):
		super().__init__()
		self._field_child=ModelNone
		self._variationPoint_child=ModelNone

class ServiceInstanceCollectionSet(complexbase.GroupBase):
	"""Collection of ServiceInstances"""
	def __init__(self):
		super().__init__()
		self._serviceInstance_children=[]

class ServiceInstanceToMachineMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map one or several AdaptivePlatformServiceInstances to a CommunicationConnector of a Machine."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_child=ModelNone
		self._secOcComPropsForMulticast_children=[]
		self._secureComPropsForTcp_child=ModelNone
		self._secureComPropsForUdp_child=ModelNone
		self._serviceInstance_children=[]

class ServiceInstanceToPortPrototypeMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to assign a transport layer dependent ServiceInstance to a PortPrototype. 

With this mapping it is possible to define how specific PortPrototypes are represented in the middleware in terms of service configuration."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._processDesign_child=ModelNone
		self._process_child=ModelNone
		self._serviceInstance_child=ModelNone

class ServiceInstanceToSignalMapping(complexbase.GroupBase):
	"""This meta-class is defined for a specific ServiceInstance and contains the mappings of elements of a ServiceInterface for which the ServiceInstance is defined  to individual ISignalTriggerings."""
	def __init__(self):
		super().__init__()
		self._eventElementMapping_children=[]
		self._fieldMapping_children=[]
		self._fireAndForgetMethodMapping_children=[]
		self._methodMapping_children=[]
		self._serviceInstance_child=ModelNone
		self._triggerMapping_children=[]

class ServiceInstanceToSwClusterDesignPortPrototypeMapping(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to assign a transport-layer-dependent ServiceInstance to a PortPrototype in the context of the SoftwareClusterDesign. With this mapping it is possible to define the list of provided and required AdaptivePlatformServiceInstances in the scope of the SoftwareClusterDesign."""

class ServiceInterface(complexbase.GroupBase):
	"""This represents the ability to define a PortInterface that consists of a heterogeneous collection of methods, events and fields."""
	def __init__(self):
		super().__init__()
		self._majorVersion_child=ModelNone
		self._minorVersion_child=ModelNone
		self._event_children=[]
		self._field_children=[]
		self._method_children=[]
		self._trigger_children=[]

class ServiceInterfaceDeployment(complexbase.GroupBase):
	"""Middleware transport layer specific configuration settings for the ServiceInterface and all contained ServiceInterface elements."""
	def __init__(self):
		super().__init__()
		self._eventDeployment_children=[]
		self._fieldDeployment_children=[]
		self._methodDeployment_children=[]
		self._serviceInterface_child=ModelNone

class ServiceInterfaceElementMapping(complexbase.GroupBase):
	"""This abstract meta-class acts as base class for the mapping of specific elements of a ServiceInterface."""

class ServiceInterfaceElementSecureComConfig(complexbase.GroupBase):
	"""This element allows to secure the communication of the referenced ServiceInterface element."""
	def __init__(self):
		super().__init__()
		self._dataId_child=ModelNone
		self._event_child=ModelNone
		self._fieldNotifier_child=ModelNone
		self._freshnessValueId_child=ModelNone
		self._getterCall_child=ModelNone
		self._getterReturn_child=ModelNone
		self._methodCall_child=ModelNone
		self._methodReturn_child=ModelNone
		self._securedRxVerification_child=ModelNone
		self._setterCall_child=ModelNone
		self._setterReturn_child=ModelNone

class ServiceInterfaceEventMapping(complexbase.GroupBase):
	"""This meta-class allows to define a mapping between events of ServiceInterfaces that are mapped to each other by the ServiceInterfaceMapping."""
	def __init__(self):
		super().__init__()
		self._sourceEvent_child=ModelNone
		self._targetEvent_child=ModelNone

class ServiceInterfaceFieldMapping(complexbase.GroupBase):
	"""This meta-class allows to define a mapping between fields of ServiceInterfaces that are mapped to each other by the ServiceInterfaceMapping."""
	def __init__(self):
		super().__init__()
		self._sourceField_child=ModelNone
		self._targetField_child=ModelNone

class ServiceInterfaceMapping(complexbase.GroupBase):
	"""Specifies one ServiceInterfaceMapping that allows to define that a ServiceInterface is composite of several other ServiceInterfaces."""
	def __init__(self):
		super().__init__()
		self._compositeServiceInterface_child=ModelNone
		self._sourceServiceInterface_children=[]

class ServiceInterfaceMethodMapping(complexbase.GroupBase):
	"""This meta-class allows to define a mapping between methods of ServiceInterfaces that are mapped to each other by the ServiceInterfaceMapping."""
	def __init__(self):
		super().__init__()
		self._sourceMethod_child=ModelNone
		self._targetMethod_child=ModelNone

class ServiceInterfacePedigree(complexbase.GroupBase):
	"""Collection of ServiceInterfaces that belong to the same versioning."""
	def __init__(self):
		super().__init__()
		self._serviceInterface_children=[]

class ServiceInterfaceTriggerMapping(complexbase.GroupBase):
	"""This meta-class allows to define a mapping between triggers of ServiceInterfaces that are mapped to each other by the ServiceInterfaceMapping."""
	def __init__(self):
		super().__init__()
		self._sourceTrigger_child=ModelNone
		self._targetTrigger_child=ModelNone

class ServiceMethodDeployment(complexbase.GroupBase):
	"""This abstract meta-class represents the ability to specify a deployment of a Method to a middleware transport layer."""
	def __init__(self):
		super().__init__()
		self._method_child=ModelNone
		self._variationPoint_child=ModelNone

class ServiceNeeds(complexbase.GroupBase):
	"""This expresses the abstract needs that a Software Component or Basic Software Module has on the configuration of an AUTOSAR Service to which it will be connected. \"Abstract needs\" means that the model abstracts from the Configuration Parameters of the underlying Basic Software."""

class ServiceProxySwComponentType(complexbase.GroupBase):
	"""This class provides the ability to express a software-component which provides access to an internal service for remote ECUs. It acts as a proxy for the service providing access to the service.

An important use case is the request of vehicle mode switches: Such requests can be communicated via sender-receiver interfaces across ECU boundaries, but the mode manager being responsible to perform the mode switches is an AUTOSAR Service which is located in the Basic Software and is not visible in the VFB view. To handle this situation, a ServiceProxySwComponentType will act as proxy for the mode manager. It will have R-Ports to be connected with the mode requestors on VFB level and Service-Ports to be connected with the local mode manager at ECU integration time.

Apart from the semantics, a ServiceProxySwComponentType has these specific properties:
* A prototype of it can be mapped to more than one ECUs in the system description.
* Exactly one additional instance of it will be created in the ECU-Extract per ECU to which the prototype has been mapped.
* For remote communication, it can have only R-Ports with sender-receiver interfaces and 1:n semantics.
* There shall be no connectors between two prototypes of any ServiceProxySwComponentType."""

class ServiceSwComponentType(complexbase.GroupBase):
	"""ServiceSwComponentType is used for configuring services for a given ECU. Instances of this class are only to be created in ECU Configuration phase for the specific purpose of the service configuration."""

class ServiceTiming(complexbase.GroupBase):
	"""This meta-class represents the timing view for one or more service instances."""
	def __init__(self):
		super().__init__()
		self._serviceInstance_children=[]

class ShortNameFragment(complexbase.GroupBase):
	"""This class describes how the Referrable.shortName is composed of several shortNameFragments."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._fragment_child=ModelNone

class SignalBasedEventElementToISignalTriggeringMapping(complexbase.GroupBase):
	"""This meta-class defines the mapping of a ServiceInterface event or an element that is defined inside of the event in case that the datatype is composite to an ISignalTriggering."""
	def __init__(self):
		super().__init__()
		self._dataPrototypeInServiceInterfaceRef_child=ModelNone
		self._filter_child=ModelNone
		self._iSignalTriggering_child=ModelNone
		self._transmissionTrigger_child=ModelNone

class SignalBasedFieldToISignalTriggeringMapping(complexbase.GroupBase):
	"""This meta-class defines the mapping of a ServiceInterface field to ISignalTriggerings that represent the notifier elements on a signal-based communication channel."""
	def __init__(self):
		super().__init__()
		self._dataPrototypeInServiceInterfaceRef_child=ModelNone
		self._filter_child=ModelNone
		self._getterCallSignal_child=ModelNone
		self._getterReturnSignal_child=ModelNone
		self._notifierSignalTriggering_child=ModelNone
		self._setterCallSignal_child=ModelNone
		self._setterReturnSignal_child=ModelNone
		self._transmissionTrigger_child=ModelNone

class SignalBasedFireAndForgetMethodToISignalTriggeringMapping(complexbase.GroupBase):
	"""This meta-class defines the mapping of a ServiceInterface fire and forget method part to an ISignalTriggering."""
	def __init__(self):
		super().__init__()
		self._dataPrototypeInMethodArgumentInstanceRef_child=ModelNone
		self._iSignalTriggering_child=ModelNone

class SignalBasedMethodToISignalTriggeringMapping(complexbase.GroupBase):
	"""This meta-class defines the mapping of a ServiceInterface method to a ISignalTriggering."""
	def __init__(self):
		super().__init__()
		self._callSignalTriggering_child=ModelNone
		self._method_child=ModelNone
		self._returnSignalTriggering_child=ModelNone

class SignalBasedTriggerToISignalTriggeringMapping(complexbase.GroupBase):
	"""This meta-class defines the mapping of a ServiceInterface trigger  to an ISignalTriggering."""
	def __init__(self):
		super().__init__()
		self._iSignalTriggering_child=ModelNone
		self._trigger_child=ModelNone

class SignalIPduCounter(complexbase.GroupBase):
	"""A PduCounter is included in a predefined set of Pdus and used to ensure that a sequence of Pdus is maintained. The counter is incremented when a Pdu is transmitted. The receivers check if the received Pdu is the next one in sequence."""
	def __init__(self):
		super().__init__()
		self._pduCounterSize_child=ModelNone
		self._pduCounterStartPosition_child=ModelNone
		self._pduCounterThreshold_child=ModelNone
		self._variationPoint_child=ModelNone

class SignalIPduReplication(complexbase.GroupBase):
	"""PduReplication is a form of redundancy where the data content of one ISignalIPdu (source) is transmitted inside a set of replica ISignalIPdus. These ISignalIPdus (copies) have different Pdu IDs, identical PduCounters, identical data content and are transmitted with the same frequency."""
	def __init__(self):
		super().__init__()
		self._pduReplicationVoting_child=ModelNone
		self._replicaPdus_children=[]
		self._variationPoint_child=ModelNone

class SignalPathConstraint(complexbase.GroupBase):
	"""Additional guidelines for the System Generator, which specific way a signal between two Software Components should take in the network without defining in which frame and with which timing it is transmitted."""
	def __init__(self):
		super().__init__()
		self._introduction_child=ModelNone
		self._variationPoint_child=ModelNone

class SignalServiceTranslationElementProps(complexbase.GroupBase):
	"""Defined translation properties for individual mapped elements."""
	def __init__(self):
		super().__init__()
		self._element_child=[]
		self._filter_child=ModelNone
		self._transmissionTrigger_child=ModelNone

class SignalServiceTranslationEventProps(complexbase.GroupBase):
	"""This element allows to define the properties which are applicable for the signal/service translation event."""
	def __init__(self):
		super().__init__()
		self._elementProps_children=[]
		self._safeTranslation_child=ModelNone
		self._secureTranslation_child=ModelNone
		self._serviceElementMapping_children=[]
		self._translationTarget_child=ModelNone

class SignalServiceTranslationProps(complexbase.GroupBase):
	"""This element allows to define the properties which are applicable for the signal/service translation service."""
	def __init__(self):
		super().__init__()
		self._controlConsumedEventGroup_children=[]
		self._controlPnc_children=[]
		self._controlProvidedEventGroup_children=[]
		self._serviceControl_child=ModelNone
		self._signalServiceTranslationEventProps_children=[]

class SignalServiceTranslationPropsSet(complexbase.GroupBase):
	"""Collection of SignalServiceTranslationProps."""
	def __init__(self):
		super().__init__()
		self._signalServiceTranslationProps_children=[]

class SimulatedExecutionTime(complexbase.GroupBase):
	"""Specifies the ExecutionTime which has been gathered using simulation means."""
	def __init__(self):
		super().__init__()
		self._maximumExecutionTime_child=ModelNone
		self._minimumExecutionTime_child=ModelNone
		self._nominalExecutionTime_child=ModelNone

class SingleLanguageLongName(complexbase.GroupBase):
	"""SingleLanguageLongName"""

class SingleLanguageReferrable(complexbase.GroupBase):
	"""Instances of this class can be referred to by their identifier (while adhering to namespace borders). They also may have a longName but in one language only. 

Specializations of this class only occur as inline elements in one particular language. Therefore they aggregate

But they are not considered to contribute substantially to the overall structure of an AUTOSAR description. In particular it does not contain other Referrables."""
	def __init__(self):
		super().__init__()
		self._longName1_child=ModelNone

class SingleLanguageUnitNames(complexbase.GroupBase):
	"""This represents the ability to express a display name."""

class SlOverviewParagraph(complexbase.GroupBase):
	"""MixedContentForOverviewParagraph in one particular language.  The language is defined by the context. 
The attribute l is there only for backwards compatibility and shall be ignored."""
	L=complexbase.Attribute("L",SimpleTypes.LEnum,'L',False,"""The attribute l is there only for backwards compatibility and shall be ignored.""")

class SlParagraph(complexbase.GroupBase):
	"""This is the text for a paragraph in one particular language. The language is defined by the context. 
The attribute l is there only for backwards compatibility and shall be ignored."""
	L=complexbase.Attribute("L",SimpleTypes.LEnum,'L',False,"""The attribute l is there only for backwards compatibility and shall be ignored.""")

class SmInteractsWithNmMapping(complexbase.GroupBase):
	"""This mapping represents an interaction from state management to network management."""
	def __init__(self):
		super().__init__()
		self._actionItem_child=ModelNone
		self._nmNetworkHandle_child=ModelNone

class SoAdConfig(complexbase.GroupBase):
	"""SoAd Configuration for one specific Physical Channel."""
	def __init__(self):
		super().__init__()
		self._connection_children=[]
		self._connectionBundle_children=[]
		self._logicAddress_children=[]
		self._socketAddress_children=[]

class SoAdRoutingGroup(complexbase.GroupBase):
	"""Routing of Pdus in the SoAd can be activated or deactivated. The ShortName of this element shall contain the RoutingGroupId."""
	def __init__(self):
		super().__init__()
		self._eventGroupControlType_child=ModelNone

class SoConIPduIdentifier(complexbase.GroupBase):
	"""Identification of Pdu content on a socket connection. This Identifier is required in case that multiple Pdus are transmitted over the same socket connection."""
	def __init__(self):
		super().__init__()
		self._headerId_child=ModelNone
		self._pduCollectionPduTimeout_child=ModelNone
		self._pduCollectionSemantics_child=ModelNone
		self._pduCollectionTrigger_child=ModelNone
		self._pduTriggering_child=ModelNone

class SoConIPduIdentifierRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SoConIPduIdentifier_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketAddress(complexbase.GroupBase):
	"""This meta-class represents a socket address towards the rest of the meta-model. The actual semantics of the represented socket address, however, is contributed by aggregation of an ApplicationEndpoint."""
	def __init__(self):
		super().__init__()
		self._allowedIPv6ExtHeaders_child=ModelNone
		self._allowedTcpOptions_child=ModelNone
		self._applicationEndpoint_child=ModelNone
		self._connector_child=ModelNone
		self._differentiatedServiceField_child=ModelNone
		self._flowLabel_child=ModelNone
		self._ipAddress_child=ModelNone
		self._multicastConnector_children=[]
		self._pathMtuDiscoveryEnabled_child=ModelNone
		self._pduCollectionMaxBufferSize_child=ModelNone
		self._pduCollectionTimeout_child=ModelNone
		self._portAddress_child=ModelNone
		self._staticSocketConnection_children=[]
		self._udpChecksumHandling_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketAddressRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SocketAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketConnection(complexbase.GroupBase):
	"""The SoAd serves as a (De)Multiplexer between different PDU sources and the TCP/IP stack."""
	def __init__(self):
		super().__init__()
		self._allowedIPv6ExtHeaders_child=ModelNone
		self._allowedTcpOptions_child=ModelNone
		self._autosarConnector_child=ModelNone
		self._clientIpAddrFromConnectionRequest_child=ModelNone
		self._clientPortFromConnectionRequest_child=ModelNone
		self._clientPort_child=ModelNone
		self._doIpSourceAddress_child=ModelNone
		self._doIpTargetAddress_child=ModelNone
		self._ident_child=ModelNone
		self._localPort_child=ModelNone
		self._nPdu_child=ModelNone
		self._pdu_children=[]
		self._pduCollectionMaxBufferSize_child=ModelNone
		self._pduCollectionTimeout_child=ModelNone
		self._remotePort_child=ModelNone
		self._runtimeIpAddressConfiguration_child=ModelNone
		self._runtimePortConfiguration_child=ModelNone
		self._shortLabel_child=ModelNone
		self._socketProtocol_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketConnectionBundle(complexbase.GroupBase):
	"""This elements groups SocketConnections, i.e. specifies socket connections belonging to the bundle and describes properties which are common for all socket connections in the bundle."""
	def __init__(self):
		super().__init__()
		self._bundledConnection_children=[]
		self._differentiatedServiceField_child=ModelNone
		self._flowLabel_child=ModelNone
		self._pathMtuDiscoveryEnabled_child=ModelNone
		self._pdu_children=[]
		self._serverPort_child=ModelNone
		self._udpChecksumHandling_child=ModelNone
		self._variationPoint_child=ModelNone

class SocketConnectionIpduIdentifier(complexbase.GroupBase):
	"""An Identifier is required in case of one port per ECU communication where multiple Pdus are transmitted over the same connection. If only one IPdu is transmitted over the connetion this attribute can be ignored."""
	def __init__(self):
		super().__init__()
		self._headerId_child=ModelNone
		self._pduCollectionPduTimeout_child=ModelNone
		self._pduCollectionSemantics_child=ModelNone
		self._pduCollectionTrigger_child=ModelNone
		self._pdu_child=ModelNone
		self._pduTriggering_child=ModelNone
		self._routingGroup_children=[]

class SocketConnectionIpduIdentifierSet(complexbase.GroupBase):
	"""Collection of PduIdentifiers used for transmission over a Socket Connection with the header option."""
	def __init__(self):
		super().__init__()
		self._iPduIdentifier_children=[]

class SoftwareCluster(complexbase.GroupBase):
	"""This meta-class represents the ability to define an uploadable software-package, i.e. the SoftwareCluster shall contain all software and configuration for a given purpose."""
	def __init__(self):
		super().__init__()
		self._artifactChecksum_children=[]
		self._artifactLocator_children=[]
		self._claimedFunctionGroup_children=[]
		self._conflictsTo_child=ModelNone
		self._containedARElement_children=[]
		self._containedFibexElement_children=[]
		self._containedPackageElement_children=[]
		self._containedProcess_children=[]
		self._dependsOn_child=ModelNone
		self._design_children=[]
		self._diagnosticDeploymentProps_child=ModelNone
		self._installationBehavior_child=ModelNone
		self._license_children=[]
		self._moduleInstantiation_children=[]
		self._releaseNotes_child=ModelNone
		self._typeApproval_child=ModelNone
		self._vendorId_child=ModelNone
		self._vendorSignature_child=ModelNone
		self._version_child=ModelNone

class SoftwareClusterDependencyCompareCondition(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a concrete dependency condition in the context of a SoftwareClusterDependencyFormula."""
	def __init__(self):
		super().__init__()
		self._compareType_child=ModelNone
		self._considerBuildNumber_child=ModelNone
		self._softwareCluster_child=ModelNone
		self._version_child=ModelNone

class SoftwareClusterDependencyFormula(complexbase.GroupBase):
	"""This meta-class represents the ability to define a dependency among SoftwareClusters."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._operator_child=ModelNone
		self._part_children=[]

class SoftwareClusterDependencyFormulaPart(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for the definition of different formula parts of a SoftwareClusterDependencyFormula."""

class SoftwareClusterDesign(complexbase.GroupBase):
	"""This meta-class represents the ability for the OEM to design the grouping of software uploadable to a specific target Machine."""
	def __init__(self):
		super().__init__()
		self._containedProcess_children=[]
		self._diagnosticContribution_children=[]
		self._intendedTargetMachine_children=[]
		self._requiredARElement_children=[]
		self._requiredDesignElement_children=[]
		self._requiredFibexElement_children=[]
		self._requiredPackageElement_children=[]
		self._rootComposition_child=ModelNone

class SoftwareClusterDiagnosticAddress(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic address in an abstract form. Sub-classes are supposed to clarify how the diagnostic address shall be defined according to the applicable addressing scheme (DoIP vs. CAN TP vs. ...)."""
	def __init__(self):
		super().__init__()
		self._addressSemantics_child=ModelNone

class SoftwareClusterDiagnosticDeploymentProps(complexbase.GroupBase):
	"""This meta-class acts as the owner of all deployment-related diagnostic properties of a SoftwareCluster."""
	def __init__(self):
		super().__init__()
		self._diagnosticAddress_children=[]
		self._diagnosticExtract_child=ModelNone
		self._externalAuthentication_children=[]
		self._powerDownTime_child=ModelNone
		self._validationConfiguration_child=ModelNone

class SoftwareClusterDoipDiagnosticAddress(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic address specifically for the DoIP case."""
	def __init__(self):
		super().__init__()
		self._diagnosticAddress_child=ModelNone

class SoftwareClusterSovdAddress(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic address specifically for the SOVD case."""
	def __init__(self):
		super().__init__()
		self._componentQualifier_child=ModelNone

class SoftwareClusterUdsDiagnosticAddress(complexbase.GroupBase):
	"""This meta-class represents the ability to define a diagnostic address specifically for the UDS case"""
	def __init__(self):
		super().__init__()
		self._diagnosticAddress_child=ModelNone

class SoftwareContext(complexbase.GroupBase):
	"""Specifies the context of the software for this resource consumption."""
	def __init__(self):
		super().__init__()
		self._input_child=ModelNone
		self._state_child=ModelNone

class SoftwarePackage(complexbase.GroupBase):
	"""This meta-class represents the ability to formalize the content of a software package."""
	def __init__(self):
		super().__init__()
		self._actionType_child=ModelNone
		self._activationAction_child=ModelNone
		self._compressedSoftwarePackageSize_child=ModelNone
		self._deltaPackageApplicableVersion_child=ModelNone
		self._estimatedDurationOfOperation_child=ModelNone
		self._minimumSupportedUcmVersion_child=ModelNone
		self._packagerId_child=ModelNone
		self._packagerSignature_child=ModelNone
		self._purposeOfUpdate_child=ModelNone
		self._softwareCluster_child=ModelNone
		self._uncompressedSoftwareClusterSize_child=ModelNone

class SoftwarePackageStep(complexbase.GroupBase):
	"""This meta-class represents the configuration of an activation step in the context of software package activation."""
	def __init__(self):
		super().__init__()
		self._process_child=ModelNone
		self._transfer_children=[]

class SoftwarePackageStoring(complexbase.GroupBase):
	"""This meta-class provides the ability to specify whether and where the referenced SoftwarePackage is stored."""
	def __init__(self):
		super().__init__()
		self._storing_child=ModelNone
		self._transfer_children=[]

class SomeipCollectionProps(complexbase.GroupBase):
	"""Collection of attributes that are configurable for an event that is provided by a ServiceInstance or for a method that is provided or requested by a ServiceInstance."""
	def __init__(self):
		super().__init__()
		self._udpCollectionBufferTimeout_child=ModelNone
		self._udpCollectionTrigger_child=ModelNone

class SomeipDataPrototypeTransformationProps(complexbase.GroupBase):
	"""This meta-class represents the ability to define data transformation props specifically for a SOME/IP serialization for a given DataPrototype."""
	def __init__(self):
		super().__init__()
		self._dataPrototype_children=[]
		self._networkRepresentation_child=ModelNone
		self._someipTransformationProps_child=ModelNone

class SomeipEventDeployment(complexbase.GroupBase):
	"""SOME/IP configuration settings for an Event."""
	def __init__(self):
		super().__init__()
		self._burstSize_child=ModelNone
		self._eventId_child=ModelNone
		self._eventReceptionDefaultValue_child=[]
		self._maximumSegmentLength_child=ModelNone
		self._separationTime_child=ModelNone
		self._serializer_child=ModelNone
		self._transportProtocol_child=ModelNone

class SomeipEventGroup(complexbase.GroupBase):
	"""Grouping of events and notification events inside a ServiceInterface in order to allow subscriptions."""
	def __init__(self):
		super().__init__()
		self._eventGroupId_child=ModelNone
		self._event_children=[]

class SomeipEventProps(complexbase.GroupBase):
	"""This meta-class allows to set configuration options for an event in the provided service instance."""
	def __init__(self):
		super().__init__()
		self._collectionProps_child=ModelNone
		self._event_child=ModelNone

class SomeipFieldDeployment(complexbase.GroupBase):
	"""SOME/IP configuration settings for a Field."""
	def __init__(self):
		super().__init__()
		self._get_child=ModelNone
		self._notifier_child=ModelNone
		self._set_child=ModelNone

class SomeipMethodDeployment(complexbase.GroupBase):
	"""SOME/IP configuration settings for a Method."""
	def __init__(self):
		super().__init__()
		self._burstSizeRequest_child=ModelNone
		self._burstSizeResponse_child=ModelNone
		self._maximumSegmentLengthRequest_child=ModelNone
		self._maximumSegmentLengthResponse_child=ModelNone
		self._methodId_child=ModelNone
		self._separationTimeRequest_child=ModelNone
		self._separationTimeResponse_child=ModelNone
		self._transportProtocol_child=ModelNone

class SomeipMethodProps(complexbase.GroupBase):
	"""This meta-class allows to set configuration options for a method in the service instance."""
	def __init__(self):
		super().__init__()
		self._collectionProps_child=ModelNone
		self._method_child=ModelNone

class SomeipProtocolRule(complexbase.GroupBase):
	"""Configuration of SOME/IP firewall rules"""
	def __init__(self):
		super().__init__()
		self._clientId_child=ModelNone
		self._lengthVerification_child=ModelNone
		self._majorVersion_child=ModelNone
		self._messageType_child=ModelNone
		self._methodId_child=ModelNone
		self._protocolVersion_child=ModelNone
		self._returnCode_child=ModelNone
		self._serviceInterfaceId_child=ModelNone

class SomeipProvidedEventGroup(complexbase.GroupBase):
	"""The meta-class represents the ability to configure ServiceInstance related communication settings on the provided side for each EventGroup separately."""
	def __init__(self):
		super().__init__()
		self._eventGroup_child=ModelNone
		self._eventMulticastUdpPort_child=ModelNone
		self._ipv4MulticastIpAddress_child=ModelNone
		self._ipv6MulticastIpAddress_child=ModelNone
		self._multicastThreshold_child=ModelNone
		self._sdServerEventGroupTimingConfig_child=ModelNone

class SomeipRemoteMulticastConfig(complexbase.GroupBase):
	"""This  meta-class is used  to statically configure the remote peer's multicast address."""
	def __init__(self):
		super().__init__()
		self._eventGroup_children=[]
		self._ipv4Address_child=ModelNone
		self._ipv6Address_child=ModelNone
		self._udpPort_child=ModelNone

class SomeipRemoteUnicastConfig(complexbase.GroupBase):
	"""This meta-class is used  to statically configure the remote peer's unicast address in case that a static service connection is used and only a single remote peer exists."""
	def __init__(self):
		super().__init__()
		self._eventGroup_children=[]
		self._ipv4Address_child=ModelNone
		self._ipv6Address_child=ModelNone
		self._tcpPort_child=ModelNone
		self._udpPort_child=ModelNone

class SomeipRequiredEventGroup(complexbase.GroupBase):
	"""The meta-class represents the ability to configure ServiceInstance related communication settings on the required side for each EventGroup separately."""
	def __init__(self):
		super().__init__()
		self._eventGroup_child=ModelNone
		self._sdClientEventGroupTimingConfig_child=ModelNone

class SomeipSdClientEventGroupTimingConfig(complexbase.GroupBase):
	"""This meta-class is used to specify configuration related to service discovery in the context of an event group on SOME/IP."""
	def __init__(self):
		super().__init__()
		self._requestResponseDelay_child=ModelNone
		self._subscribeEventgroupRetryDelay_child=ModelNone
		self._subscribeEventgroupRetryMax_child=ModelNone
		self._timeToLive_child=ModelNone

class SomeipSdClientEventGroupTimingConfigRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SomeipSdClientEventGroupTimingConfig_child=ModelNone
		self._variationPoint_child=ModelNone

class SomeipSdClientServiceInstanceConfig(complexbase.GroupBase):
	"""Client specific settings that are relevant for the configuration of SOME/IP Service-Discovery."""
	def __init__(self):
		super().__init__()
		self._initialFindBehavior_child=ModelNone
		self._priority_child=ModelNone
		self._serviceFindTimeToLive_child=ModelNone

class SomeipSdClientServiceInstanceConfigRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SomeipSdClientServiceInstanceConfig_child=ModelNone
		self._variationPoint_child=ModelNone

class SomeipSdRule(complexbase.GroupBase):
	"""Configuration of SOME/IP Service Discovery firewall rules"""
	def __init__(self):
		super().__init__()
		self._entryType_child=ModelNone
		self._eventGroupId_child=ModelNone
		self._maxMajorVersion_child=ModelNone
		self._maxMinorVersion_child=ModelNone
		self._minMajorVersion_child=ModelNone
		self._minMinorVersion_child=ModelNone
		self._serviceInstanceId_child=ModelNone
		self._serviceInterfaceId_child=ModelNone

class SomeipSdServerEventGroupTimingConfig(complexbase.GroupBase):
	"""EventGroup specific timing configuration settings."""
	def __init__(self):
		super().__init__()
		self._requestResponseDelay_child=ModelNone

class SomeipSdServerEventGroupTimingConfigRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SomeipSdServerEventGroupTimingConfig_child=ModelNone
		self._variationPoint_child=ModelNone

class SomeipSdServerServiceInstanceConfig(complexbase.GroupBase):
	"""Server specific settings that are relevant for the configuration of SOME/IP Service-Discovery."""
	def __init__(self):
		super().__init__()
		self._initialOfferBehavior_child=ModelNone
		self._offerCyclicDelay_child=ModelNone
		self._priority_child=ModelNone
		self._requestResponseDelay_child=ModelNone
		self._serviceOfferTimeToLive_child=ModelNone

class SomeipSdServerServiceInstanceConfigRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SomeipSdServerServiceInstanceConfig_child=ModelNone
		self._variationPoint_child=ModelNone

class SomeipServiceDiscovery(complexbase.GroupBase):
	"""This meta-class represents a specialization of the generic service discovery for the SOME/IP case."""
	def __init__(self):
		super().__init__()
		self._multicastSdIpAddress_child=ModelNone
		self._multicastSecureComProps_child=ModelNone
		self._someipServiceDiscoveryPort_child=ModelNone
		self._unicastSecureComProps_children=[]

class SomeipServiceInstanceToMachineMapping(complexbase.GroupBase):
	"""This meta-class allows to map SomeipServiceInstances to a CommunicationConnector of a Machine. In this step the network configuration (IP Address, Transport Protocol, Port Number) for the ServiceInstance is defined."""
	def __init__(self):
		super().__init__()
		self._remoteMulticastConfig_children=[]
		self._remoteUnicastConfig_children=[]
		self._tcpPort_child=ModelNone
		self._udpCollectionBufferSizeThreshold_child=ModelNone
		self._udpPort_child=ModelNone

class SomeipServiceInterfaceDeployment(complexbase.GroupBase):
	"""SOME/IP configuration settings for a ServiceInterface."""
	def __init__(self):
		super().__init__()
		self._eventGroup_children=[]
		self._serviceInterfaceId_child=ModelNone
		self._serviceInterfaceVersion_child=ModelNone

class SomeipServiceVersion(complexbase.GroupBase):
	"""This meta-class represents the ability to describe a version of a SOME/IP Service."""
	def __init__(self):
		super().__init__()
		self._majorVersion_child=ModelNone
		self._minorVersion_child=ModelNone

class SomeipTpChannel(complexbase.GroupBase):
	"""This element is used to assign properties to SomeipTpConnections that are referencing this SomeipTpChannel."""
	def __init__(self):
		super().__init__()
		self._burstSize_child=ModelNone
		self._rxTimeoutTime_child=ModelNone
		self._separationTime_child=ModelNone

class SomeipTpConfig(complexbase.GroupBase):
	"""This element defines exactly one SOME/IP TP Configuration."""
	def __init__(self):
		super().__init__()
		self._tpChannel_children=[]
		self._tpConnection_children=[]

class SomeipTpConnection(complexbase.GroupBase):
	"""A connection identifies the sender and the receiver of this particular communication. The SOME/IP TP module routes a Pdu through this connection."""
	def __init__(self):
		super().__init__()
		self._separationTime_child=ModelNone
		self._tpChannel_child=ModelNone
		self._tpSdu_child=ModelNone
		self._transportPdu_child=ModelNone

class SOMEIPTransformationDescription(complexbase.GroupBase):
	"""The SOMEIPTransformationDescription is used to specify SOME/IP transformer specific attributes."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._byteOrder_child=ModelNone
		self._interfaceVersion_child=ModelNone

class SOMEIPTransformationISignalProps(complexbase.GroupBase):
	"""The class SOMEIPTransformationISignalProps specifies ISignal specific configuration properties for SOME/IP transformer attributes."""
	def __init__(self):
		super().__init__()
		self._someipTransformationISignalPropsVariant_children=[]
		self._implementsLegacyStringSerialization_child=ModelNone
		self._implementsSOMEIPStringHandling_child=ModelNone
		self._interfaceVersion_child=ModelNone
		self._isDynamicLengthFieldSize_child=ModelNone
		self._messageType_child=ModelNone
		self._sessionHandlingSR_child=ModelNone
		self._sizeOfArrayLengthFields_child=ModelNone
		self._sizeOfStringLengthFields_child=ModelNone
		self._sizeOfStructLengthFields_child=ModelNone
		self._sizeOfUnionLengthFields_child=ModelNone
		self._tlvDataId_children=[]
		self._tlvDataId0_children=[]
		self._tlvDataIdDefinition_children=[]

class SOMEIPTransformationISignalPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SOMEIPTransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class SOMEIPTransformationProps(complexbase.GroupBase):
	"""The class SOMEIPTransformationProps specifies SOME/IP specific configuration properties."""
	def __init__(self):
		super().__init__()
		self._alignment_child=ModelNone
		self._sizeOfArrayLengthField_child=ModelNone
		self._sizeOfStringLengthField_child=ModelNone
		self._sizeOfStructLengthField_child=ModelNone
		self._sizeOfUnionLengthField_child=ModelNone

class SovdGatewayEthernetCredentials(complexbase.GroupBase):
	"""This meta-class represents the ability to define Ethernet credentials for the purpose of connecting a client to an SOVD gateway."""
	def __init__(self):
		super().__init__()
		self._ipv4Address_child=ModelNone
		self._ipv6Address_child=ModelNone
		self._udpPort_child=ModelNone

class SovdGatewayInstantiation(complexbase.GroupBase):
	"""This meta-class represents the configuration of an SOVD gateway."""
	def __init__(self):
		super().__init__()
		self._securePropsForExternalComm_child=ModelNone
		self._unicastCredentials_child=ModelNone

class SovdGatewayLocalEndpointTcpConfig(complexbase.GroupBase):
	"""This meta-class provides the ability to define the TCP configuration of a local endpoint for external communication of an SOVD gateway."""
	def __init__(self):
		super().__init__()
		self._tcpPort_child=ModelNone

class SovdModuleInstantiation(complexbase.GroupBase):
	"""This abstract meta-class serves as the base class for meta-classes that describe the configuration of an SOVD module."""
	def __init__(self):
		super().__init__()
		self._communicationConnector_children=[]
		self._nodeIdentifier_child=ModelNone
		self._securePropsForTcp_child=ModelNone
		self._securePropsForUdp_child=ModelNone

class SovdServerInstantiation(complexbase.GroupBase):
	"""This meta-class represents the configuration of an SOVD server."""
	def __init__(self):
		super().__init__()
		self._componentQualifier_child=ModelNone

class SpecElementReference(complexbase.GroupBase):
	"""This is a reference to a specification element in the Autosar standard."""
	def __init__(self):
		super().__init__()
		self._alternativeName_child=ModelNone

class SpecElementScope(complexbase.GroupBase):
	"""This class defines if a specification element is relevant within the context of this data exchange point."""
	def __init__(self):
		super().__init__()
		self._inScope_child=ModelNone

class SpecificationDocumentScope(complexbase.GroupBase):
	"""Represents a standardized or custom specification document such as Software Component Template, Main Requirements, Specification of Communication, etc.

Autosar specifications are referenced via their title."""
	def __init__(self):
		super().__init__()
		self._customDocumentation_child=ModelNone
		self._documentElementScope_children=[]

class SpecificationScope(complexbase.GroupBase):
	"""Specification of the relevant subset of Autosar specifications."""
	def __init__(self):
		super().__init__()
		self._specificationDocumentScope_children=[]

class SporadicEventTriggering(complexbase.GroupBase):
	"""Describes the behavior of an event which occurs occasionally or singularly."""
	def __init__(self):
		super().__init__()
		self._minimumInterArrivalTime_child=ModelNone
		self._maximumInterArrivalTime_child=ModelNone
		self._jitter_child=ModelNone
		self._period_child=ModelNone

class StackUsage(complexbase.GroupBase):
	"""Describes the stack memory usage of a software."""
	def __init__(self):
		super().__init__()
		self._executableEntity_child=ModelNone
		self._hardwareConfiguration_child=ModelNone
		self._hwElement_child=ModelNone
		self._softwareContext_child=ModelNone
		self._variationPoint_child=ModelNone

class StartupConfig(complexbase.GroupBase):
	"""This meta-class represents a reusable startup configuration for processes.."""
	def __init__(self):
		super().__init__()
		self._environmentVariable_children=[]
		self._executionError_child=ModelNone
		self._permissionToCreateChildProcess_child=ModelNone
		self._processArgument_children=[]
		self._schedulingPolicy_child=ModelNone
		self._schedulingPriority_child=ModelNone
		self._terminationBehavior_child=ModelNone
		self._timeout_child=ModelNone

class StateDependentFirewall(complexbase.GroupBase):
	"""Firewall rules that are defined in a firewall state"""
	def __init__(self):
		super().__init__()
		self._defaultAction_child=ModelNone
		self._firewallRuleProps_children=[]
		self._firewallState_children=[]
		self._firewallStateModeDeclaration_children=[]

class StateDependentStartupConfig(complexbase.GroupBase):
	"""This meta-class defines the startup configuration for the process depending on a collection of machine states."""
	def __init__(self):
		super().__init__()
		self._executionDependency_children=[]
		self._functionGroupState_children=[]
		self._resourceConsumption_child=ModelNone
		self._resourceGroup_child=ModelNone
		self._startupConfig_child=ModelNone

class StateManagemenPhmErrorInterface(complexbase.GroupBase):
	"""This meta-class indicates that the PortPrototype that references this class is used for accepting a error submissions from the platform health management."""

class StateManagementActionItem(complexbase.GroupBase):
	"""This meta-class represents an action item that is executed in response to a state change."""

class StateManagementActionList(complexbase.GroupBase):
	"""This meta-class represents the ability to define an action list that is associated with a state of a state machine."""
	def __init__(self):
		super().__init__()
		self._actionItem_children=[]
		self._affectedState_child=ModelNone

class StateManagementCompareCondition(complexbase.GroupBase):
	"""StateManagementCompareConditions are atomic conditions. They are based on the idea of a comparison at runtime of some variable data with something constant. The type of the comparison (==, !=, <, <=, ...) is specified in StateManagementCompareCondition.compareType."""
	def __init__(self):
		super().__init__()
		self._compareType_child=ModelNone
		self._compareValue_child=[]

class StateManagementCompareFormula(complexbase.GroupBase):
	"""A StateManagementCompareFormula embodies the computation instruction that is to be evaluated at
runtime to determine if the aggregating request rule yields true or false. The formula itself consists of parts which are combined by the
logical operations specified by StateManagementCompareFormula .op."""
	def __init__(self):
		super().__init__()
		self._part_children=[]

class StateManagementCompareFormulaPart(complexbase.GroupBase):
	"""A StateManagementCompareFormulaPart can either be a atomic condition, e.g. a StateManagementTriggerCompareRule or StateManagementErrorCompareRule, or a StateManagementCompareFormula again, which allows arbitrary nesting."""

class StateManagementDiagTriggerInterface(complexbase.GroupBase):
	"""This meta-class indicates that the PortPrototype that references this class is used for accepting a state change trigger from the diagnostics management."""

class StateManagementEmErrorInterface(complexbase.GroupBase):
	"""This meta-class indicates that the PortPrototype that references this class is used for accepting a error submissions from the execution management."""

class StateManagementErrorCompareRule(complexbase.GroupBase):
	"""This meta-class represents the configuration of a compare rule for the processing of an error submission."""

class StateManagementErrorInterface(complexbase.GroupBase):
	"""The usage of this meta-class for typing a PortPrototype indicates that the PortPrototype is used for the error provision in the context of state management on the AUTOSAR adaptive platform."""

class StateManagementFunctionGroupSwitchNotificationInterface(complexbase.GroupBase):
	"""The usage of this meta-class for typing a PortPrototype indicates that the PortPrototype is used for sending out a notification of a function group state change in the context of state management on the AUTOSAR adaptive platform."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class StateManagementModuleInstantiation(complexbase.GroupBase):
	"""This meta-class represents the deployment-level configuration of the state management on the AUTOSAR adaptive platform."""
	def __init__(self):
		super().__init__()
		self._actionItemList_children=[]
		self._notification_children=[]
		self._request_children=[]

class StateManagementNmActionItem(complexbase.GroupBase):
	"""This meta-class represents a state management action item to interact with the network management."""
	def __init__(self):
		super().__init__()
		self._nmStateRequest_child=ModelNone

class StateManagementNotificationInterface(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for state management notification interfaces."""

class StateManagementPortInterface(complexbase.GroupBase):
	"""This abstract class acts as a base class for PortInterfaces that are used in the context of state management on the AUTOSAR adaptive platform."""

class StateManagementRequestError(complexbase.GroupBase):
	"""This meta-class has the ability to configure the submission of an error to the state management."""
	def __init__(self):
		super().__init__()
		self._rule_children=[]

class StateManagementRequestInterface(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for state management request interfaces."""

class StateManagementRequestRule(complexbase.GroupBase):
	"""This meta-class represents a rule for deciding about a state change."""
	def __init__(self):
		super().__init__()
		self._formula_child=ModelNone
		self._nextState_child=ModelNone

class StateManagementRequestTrigger(complexbase.GroupBase):
	"""This meta-class has the ability to configure a trigger request to the state management."""
	def __init__(self):
		super().__init__()
		self._rule_children=[]

class StateManagementSetFunctionGroupStateActionItem(complexbase.GroupBase):
	"""This meta-class represents a state management action item to set a specific state in a specific function group."""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._setFunctionGroupState_child=ModelNone

class StateManagementSleepActionItem(complexbase.GroupBase):
	"""This action item can be used to universally implement afterrun. One specific use case for afterrun comes up in the context of network management."""
	def __init__(self):
		super().__init__()
		self._sleepTime_child=ModelNone

class StateManagementStateMachineActionItem(complexbase.GroupBase):
	"""This meta-class represents a state management action item to start or stop a state machine."""
	def __init__(self):
		super().__init__()
		self._overrideInitialState_child=ModelNone
		self._startStateMachine_child=ModelNone
		self._stopStateMachine_child=ModelNone

class StateManagementStateNotification(complexbase.GroupBase):
	"""This meta-class represents the ability to formalize state notifications on the AUTOSAR adaptive platform."""
	def __init__(self):
		super().__init__()
		self._notificationPort_child=ModelNone
		self._stateMachine_child=ModelNone

class StateManagementStateRequest(complexbase.GroupBase):
	"""This abstract class serves as the base class for state requests on the AUTOSAR adaptive platform."""
	def __init__(self):
		super().__init__()
		self._stateRequestPort_child=ModelNone

class StateManagementSyncActionItem(complexbase.GroupBase):
	"""This meta-class represents a state management action item to synchronize state machines."""

class StateManagementTriggerCompareRule(complexbase.GroupBase):
	"""This meta-class represents the configuration of a compare rule for the processing of a trigger request."""
	def __init__(self):
		super().__init__()
		self._assumedCurrentState_child=ModelNone

class StateManagementTriggerInterface(complexbase.GroupBase):
	"""The usage of this meta-class for typing a PortPrototype indicates that the PortPrototype is used for the trigger provision in the context of state management on the AUTOSAR adaptive platform."""

class StaticPart(complexbase.GroupBase):
	"""Some parts/signals of the I-PDU may be the same regardless of the selector field. Such a part is called static part. The static part is optional."""
	def __init__(self):
		super().__init__()
		self._iPdu_child=ModelNone
		self._variationPoint_child=ModelNone

class StaticSocketConnection(complexbase.GroupBase):
	"""Definition of static SocketConnection between the Socket that is defined by the aggregating SocketAddress and the remoteAddress."""
	def __init__(self):
		super().__init__()
		self._iPduIdentifier_children=[]
		self._remoteAddress_children=[]
		self._tcpConnectTimeout_child=ModelNone
		self._tcpRole_child=ModelNone
		self._variationPoint_child=ModelNone

class Std(complexbase.GroupBase):
	"""This represents a reference to external standards."""
	def __init__(self):
		super().__init__()
		self._subtitle_child=ModelNone
		self._state_child=ModelNone
		self._date_child=ModelNone
		self._url_child=ModelNone
		self._position_child=ModelNone

class StdCppImplementationDataType(complexbase.GroupBase):
	"""This meta-class represents the way to specify a data type definition that is taken as the basis for a C++ language binding to a C++ Standard Library feature."""

class StreamFilterIEEE1722Tp(complexbase.GroupBase):
	"""Configuration of filter rules for IP and TP."""
	def __init__(self):
		super().__init__()
		self._streamId_child=ModelNone

class StreamFilterIpv4Address(complexbase.GroupBase):
	"""IPv4 address range definition."""
	def __init__(self):
		super().__init__()
		self._ipv4Address_child=ModelNone
		self._ipv4AddressMask_child=ModelNone

class StreamFilterIpv6Address(complexbase.GroupBase):
	"""IPv6 address range definition."""
	def __init__(self):
		super().__init__()
		self._ipv6Address_child=ModelNone
		self._ipv6AddressMask_child=ModelNone

class StreamFilterMACAddress(complexbase.GroupBase):
	"""Configuration of filter rules on the DataLink layer"""
	def __init__(self):
		super().__init__()
		self._macAddress_child=ModelNone
		self._macAddressMask_child=ModelNone

class StreamFilterPortRange(complexbase.GroupBase):
	"""Configuration of filter rules for IP and TP."""
	def __init__(self):
		super().__init__()
		self._max_child=ModelNone
		self._min_child=ModelNone

class StreamFilterRuleDataLinkLayer(complexbase.GroupBase):
	"""Configuration of filter rules on the DataLink layer"""
	def __init__(self):
		super().__init__()
		self._destinationMacAddress_child=ModelNone
		self._etherType_child=ModelNone
		self._sourceMacAddress_child=ModelNone
		self._vlanId_child=ModelNone
		self._vlanPriority_child=ModelNone

class StreamFilterRuleIpTp(complexbase.GroupBase):
	"""Configuration of filter rules for IP and TP."""
	def __init__(self):
		super().__init__()
		self._destinationIpv4Address_child=ModelNone
		self._destinationIpv6Address_child=ModelNone
		self._destinationPort_children=[]
		self._sourceIpv4Address_child=ModelNone
		self._sourceIpv6Address_child=ModelNone
		self._sourcePort_children=[]

class StructuredReq(complexbase.GroupBase):
	"""This represents a structured requirement. This is intended for a case where specific requirements for features are collected.

Note that this can be rendered as a labeled list."""
	def __init__(self):
		super().__init__()
		self._date_child=ModelNone
		self._issuedBy_child=ModelNone
		self._type_child=ModelNone
		self._importance_child=ModelNone
		self._description_child=ModelNone
		self._rationale_child=ModelNone
		self._appliesTo_children=[]
		self._dependencies_child=ModelNone
		self._useCase_child=ModelNone
		self._conflicts_child=ModelNone
		self._supportingMaterial_child=ModelNone
		self._remark_child=ModelNone
		self._testedItem_children=[]
		self._variationPoint_child=ModelNone

class SubElementMapping(complexbase.GroupBase):
	"""This meta-class allows for the definition of mappings of elements of a composite data type."""
	def __init__(self):
		super().__init__()
		self._firstElement_children=[]
		self._secondElement_children=[]
		self._textTableMapping_children=[]

class SubElementRef(complexbase.GroupBase):
	"""This meta-class provides the ability to reference elements of composite data type."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SupervisedEntityCheckpointNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Watchdog Manager to support a Checkpoint for a Supervised Entity."""

class SupervisedEntityCheckpointNeedsRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._SupervisedEntityCheckpointNeeds_child=ModelNone
		self._variationPoint_child=ModelNone

class SupervisedEntityNeeds(complexbase.GroupBase):
	"""Specifies the abstract needs on the configuration of the Watchdog Manager for one specific Supervised Entity."""
	def __init__(self):
		super().__init__()
		self._activateAtStart_child=ModelNone
		self._checkpoints_children=[]
		self._enableDeactivation_child=ModelNone
		self._expectedAliveCycle_child=ModelNone
		self._maxAliveCycle_child=ModelNone
		self._minAliveCycle_child=ModelNone
		self._toleratedFailedCycles_child=ModelNone

class SupervisionCheckpoint(complexbase.GroupBase):
	"""This element contains an instance reference to a RPortPrototype representing a checkpoint for Platform Health Management."""
	def __init__(self):
		super().__init__()
		self._checkpointId_child=ModelNone
		self._phmCheckpoint_child=ModelNone
		self._process_child=ModelNone

class SupervisionMode(complexbase.GroupBase):
	"""This element defines a SupervisionMode."""
	def __init__(self):
		super().__init__()
		self._activeSupervision_children=[]
		self._expiredSupervisionTolerance_child=ModelNone
		self._modeCondition_child=ModelNone

class SupervisionModeCondition(complexbase.GroupBase):
	"""This element defines a SupervisionModeCondition in the context of platform health management contribution."""
	def __init__(self):
		super().__init__()
		self._stateReference_children=[]

class SwAddrMethod(complexbase.GroupBase):
	"""Used to assign a common addressing method, e.g. common memory section, to data or code objects. These objects could actually live in different modules or components."""
	def __init__(self):
		super().__init__()
		self._memoryAllocationKeywordPolicy_child=ModelNone
		self._option_children=[]
		self._sectionInitializationPolicy_child=ModelNone
		self._sectionType_child=ModelNone

class SwAxisCont(complexbase.GroupBase):
	"""This represents the values for the axis of a compound primitive (curve, map).

For standard and fix axes,  SwAxisCont contains the values of the axis directly. 

The axis values of SwAxisCont with the category COM_AXIS, RES_AXIS are for display only. For editing and processing, only the values in the related GroupAxis are binding."""
	def __init__(self):
		super().__init__()
		self._category_child=ModelNone
		self._unit_child=ModelNone
		self._unitDisplayName_child=ModelNone
		self._swAxisIndex_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swValuesPhys_child=ModelNone

class SwAxisGeneric(complexbase.GroupBase):
	"""This meta-class defines a generic axis. In a generic axis the axispoints points are calculated in the ECU. 

The ECU is equipped with a fixed calculation algorithm. Parameters for the algorithm can be stored in the data component of the ECU. Therefore these parameters are specified in the data declaration, not in the calibration data."""
	def __init__(self):
		super().__init__()
		self._swAxisType_child=ModelNone
		self._swNumberOfAxisPoints_child=ModelNone
		self._swGenericAxisParam_children=[]

class SwAxisGrouped(complexbase.GroupBase):
	"""An SwAxisGrouped is an axis which is shared between multiple calibration parameters."""
	def __init__(self):
		super().__init__()
		self._sharedAxisType_child=ModelNone
		self._swAxisIndex_child=ModelNone
		self._mcDataInstance_child=ModelNone
		self._arParameter_child=ModelNone

class SwAxisIndividual(complexbase.GroupBase):
	"""This meta-class describes an axis integrated into a parameter (field etc.). The integration makes this individual to each parameter. The so-called grouped axis represents the counterpart to this. It is conceived as an independent parameter (see class SwAxisGrouped)."""
	def __init__(self):
		super().__init__()
		self._inputVariableType_child=ModelNone
		self._swVariableRef_children=[]
		self._compuMethod_child=ModelNone
		self._unit_child=ModelNone
		self._swMaxAxisPoints_child=ModelNone
		self._swMinAxisPoints_child=ModelNone
		self._dataConstr_child=ModelNone
		self._swAxisGeneric_child=ModelNone

class SwAxisType(complexbase.GroupBase):
	"""This meta-class represents a specific axis calculation strategy. No formal specification is given, due to the fact that it is possible to use arbitrary algorithms for calculating axis-points. 

Instead, the algorithm is described verbally but the parameters are specified formally with respect to their names and constraints. As a result, SwAxisType mainly reserves appropriate keywords."""
	def __init__(self):
		super().__init__()
		self._swGenericAxisDesc_child=ModelNone
		self._swGenericAxisParamType_children=[]

class SwBaseType(complexbase.GroupBase):
	"""This meta-class represents a base type used within ECU software."""

class SwBitRepresentation(complexbase.GroupBase):
	"""Description of the structure of a bit variable: Comprises of the bitPosition in a memory object (e.g. swHostVariable, which stands parallel to swBitRepresentation) and the numberOfBits . In this way, interrelated memory areas can be described. Non-related memory areas are not supported."""
	def __init__(self):
		super().__init__()
		self._bitPosition_child=ModelNone
		self._numberOfBits_child=ModelNone

class SwCalprmAxis(complexbase.GroupBase):
	"""This element specifies an individual input parameter axis (abscissa)."""
	def __init__(self):
		super().__init__()
		self._swAxisIndex_child=ModelNone
		self._category_child=ModelNone
		self._swAxisGrouped_child=ModelNone
		self._swAxisIndividual_child=ModelNone
		self._swCalibrationAccess_child=ModelNone
		self._displayFormat_child=ModelNone
		self._baseType_child=ModelNone

class SwCalprmAxisSet(complexbase.GroupBase):
	"""This element specifies the input parameter axes (abscissas) of parameters (and variables, if these are used adaptively)."""
	def __init__(self):
		super().__init__()
		self._swCalprmAxis_children=[]

class SwCalprmAxisTypeProps(complexbase.GroupBase):
	"""Base class for the type of the calibration axis. This provides the particular model of the specialization. If the specialization would be the directly from SwCalPrmAxis, the sequence of common properties and the specializes ones would be different."""
	def __init__(self):
		super().__init__()
		self._maxGradient_child=ModelNone
		self._monotony_child=ModelNone

class SwCalprmRefProxy(complexbase.GroupBase):
	"""Wrapper class for different kinds of references to a calibration parameter."""
	def __init__(self):
		super().__init__()
		self._arParameter_child=ModelNone
		self._mcDataInstance_child=ModelNone

class SwComponentDocumentation(complexbase.GroupBase):
	"""This class specifies the ability to write dedicated documentation to a component type according to ASAM FSX."""
	def __init__(self):
		super().__init__()
		self._swFeatureDef_child=ModelNone
		self._swFeatureDesc_child=ModelNone
		self._swTestDesc_child=ModelNone
		self._swCalibrationNotes_child=ModelNone
		self._swMaintenanceNotes_child=ModelNone
		self._swDiagnosticsNotes_child=ModelNone
		self._swCarbDoc_child=ModelNone
		self._chapter_children=[]
		self._variationPoint_child=ModelNone

class SwComponentMappingConstraints(complexbase.GroupBase):
	"""Collection of constraints that are valid for particular SwComponentTypes."""
	def __init__(self):
		super().__init__()
		self._rteEventSeparation_children=[]
		self._rteEventToOsTaskProxyMapping_children=[]
		self._swcToApplicationPartitionMapping_children=[]

class SwComponentPrototype(complexbase.GroupBase):
	"""Role of a software component within a composition."""
	def __init__(self):
		super().__init__()
		self._type_child=ModelNone
		self._variationPoint_child=ModelNone

class SwComponentPrototypeAssignment(complexbase.GroupBase):
	"""This meta-class is only required to allow for the variant modeling of an instanceRef."""
	def __init__(self):
		super().__init__()
		self._swComponent_child=ModelNone
		self._variationPoint_child=ModelNone

class SwComponentType(complexbase.GroupBase):
	"""Base class for AUTOSAR software components."""
	def __init__(self):
		super().__init__()
		self._swComponentDocumentation_children=[]
		self._consistencyNeeds_children=[]
		self._port_children=[]
		self._portGroup_children=[]
		self._swcMappingConstraint_children=[]
		self._unitGroup_children=[]

class SwConnector(complexbase.GroupBase):
	"""The base class for connectors between ports. Connectors have to be identifiable to allow references from the system constraint template."""
	def __init__(self):
		super().__init__()
		self._mapping_child=ModelNone
		self._variationPoint_child=ModelNone

class SwDataDefProps(complexbase.GroupBase):
	"""This class is a collection of properties relevant for data objects under various aspects. One could consider this class as a \"pattern of inheritance by aggregation\". The properties can be applied to all objects of all classes in which SwDataDefProps is aggregated.

'''begin restrict to CP AP'''
Note that not all of the attributes or associated elements are useful all of the time. Hence, the process definition (e.g. expressed with an OCL or a Document Control Instance MSR-DCI) has the task of implementing limitations.

SwDataDefProps covers various aspects:

* Structure of the data element for calibration use cases: is it a single value, a curve, or a map, but also the recordLayouts which specify how such elements are mapped/converted to the DataTypes in the programming language (or in AUTOSAR). This is mainly expressed by properties like swRecordLayout and swCalprmAxisSet 

* Implementation aspects, mainly expressed by swImplPolicy, swVariableAccessImplPolicy, swAddrMethod, swPointerTagetProps, baseType, implementationDataType and additionalNativeTypeQualifier

* Access policy for the MCD system, mainly expressed by swCalibrationAccess 

* Semantics of the data element, mainly expressed by compuMethod and/or unit, dataConstr, invalidValue

* Code generation policy provided by swRecordLayout
'''end restrict to CP AP'''"""
	def __init__(self):
		super().__init__()
		self._swDataDefPropsVariant_children=[]
		self._displayPresentation_child=ModelNone
		self._stepSize_child=ModelNone
		self._swValueBlockSizeMult_children=[]
		self._annotation_children=[]
		self._swAddrMethod_child=ModelNone
		self._swAlignment_child=ModelNone
		self._baseType_child=ModelNone
		self._swBitRepresentation_child=ModelNone
		self._swCalibrationAccess_child=ModelNone
		self._swValueBlockSize_child=ModelNone
		self._swCalprmAxisSet_child=ModelNone
		self._swTextProps_child=ModelNone
		self._swComparisonVariable_children=[]
		self._compuMethod_child=ModelNone
		self._dataConstr_child=ModelNone
		self._swDataDependency_child=ModelNone
		self._displayFormat_child=ModelNone
		self._implementationDataType_child=ModelNone
		self._swHostVariable_child=ModelNone
		self._swImplPolicy_child=ModelNone
		self._additionalNativeTypeQualifier_child=ModelNone
		self._swIntendedResolution_child=ModelNone
		self._swInterpolationMethod_child=ModelNone
		self._invalidValue_child=[]
		self._mcFunction_child=ModelNone
		self._swIsVirtual_child=ModelNone
		self._swPointerTargetProps_child=ModelNone
		self._swRecordLayout_child=ModelNone
		self._swRefreshTiming_child=ModelNone
		self._unit_child=ModelNone
		self._valueAxisDataType_child=ModelNone

class SwDataDefPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class SwDataDefPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class SwDataDependency(complexbase.GroupBase):
	"""This element describes the interdependencies of data objects, e.g. variables and parameters. 

Use cases:
* Calculate the value of a calibration parameter (by the MCD system) from the value(s) of other calibration parameters.
* Virtual data - that means the data object is not directly in the ecu and this property describes how the \"virtual variable\" can be computed from the real ones (by the MCD system)."""
	def __init__(self):
		super().__init__()
		self._swDataDependencyFormula_child=ModelNone
		self._swDataDependencyArgs_child=ModelNone

class SwDataDependencyArgs(complexbase.GroupBase):
	"""This element specifies the elements used in a SwDataDependency."""
	def __init__(self):
		super().__init__()
		self._mcDataInstanceVar_children=[]
		self._autosarVariable_children=[]
		self._mcDataInstance_children=[]
		self._arParameter_children=[]

class SwGenericAxisParam(complexbase.GroupBase):
	"""This meta-class describes a specific parameter of a generic axis. The name of the parameter is defined through a reference to a parameter type defined on a corresponding axis type.

The value of the parameter is given here in case that it is not changeable during calibration. Example is shift / offset in a fixed axis."""
	def __init__(self):
		super().__init__()
		self._swGenericAxisParamType_child=ModelNone
		self._vf_children=[]

class SwGenericAxisParamType(complexbase.GroupBase):
	"""This meta-class describes a generic axis parameter type, namely:

* Plausibility checks can be specified via dataConstr.

* Textual description (desc), as a formal description is not of any use, due to the large variety of possibilities.

* If this parameter contains structures, these can be simulated through the recursive use of SwGenericAxisParamTypes."""
	def __init__(self):
		super().__init__()
		self._dataConstr_child=ModelNone

class SwPointerTargetProps(complexbase.GroupBase):
	"""This element defines, that the data object (which is specified by the aggregating element) contains a reference to another data object or to a function in the CPU code. This corresponds to a pointer in the C-language.

The attributes of this element describe the category and the detailed properties of the target which is either a data description or a function signature."""
	def __init__(self):
		super().__init__()
		self._targetCategory_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._functionPointerSignature_child=ModelNone

class SwRecordLayout(complexbase.GroupBase):
	"""Defines how the data objects (variables, calibration parameters etc.) are to be stored in the ECU memory. As an example, this definition specifies the sequence of axis points in the ECU memory. Iterations through axis values are stored within the sub-elements swRecordLayoutGroup."""
	def __init__(self):
		super().__init__()
		self._swRecordLayoutGroup_child=ModelNone

class SwRecordLayoutGroup(complexbase.GroupBase):
	"""Specifies how a record layout is set up. Using SwRecordLayoutGroup it recursively models iterations through axis values. The subelement swRecordLayoutGroupContentType may reference other SwRecordLayouts, SwRecordLayoutVs and SwRecordLayoutGroups for the modeled record layout."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._desc_child=ModelNone
		self._swRecordLayoutGroupAxis_child=ModelNone
		self._swRecordLayoutGroupIndex_child=ModelNone
		self._swGenericAxisParamType_child=ModelNone
		self._swRecordLayoutGroupFrom_child=ModelNone
		self._swRecordLayoutGroupTo_child=ModelNone
		self._swRecordLayoutGroupStep_child=ModelNone
		self._swRecordLayoutComponent_child=ModelNone
		self._swRecordLayoutGroup_children=[]
		self._swRecordLayoutV_children=[]
		self._swRecordLayout_children=[]

class SwRecordLayoutGroupContent(complexbase.GroupBase):
	"""This is the contents of a RecordLayout which is inserted for every iteration. Note that since this is atpMixed, multiple properties can be inserted for each iteration."""
	def __init__(self):
		super().__init__()
		self._swRecordLayout_children=[]
		self._swRecordLayoutV_children=[]
		self._swRecordLayoutGroup_children=[]

class SwRecordLayoutV(complexbase.GroupBase):
	"""This element specifies which values are stored for the current SwRecordLayoutGroup. If no baseType is present, the SwBaseType referenced initially in the parent SwRecordLayoutGroup is valid. The specification of swRecordLayoutVAxis gives the axis of the values which shall be stored in accordance with the current record layout SwRecordLayoutGroup. In swRecordLayoutVProp one can specify the information which shall be stored."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._category_child=ModelNone
		self._desc_child=ModelNone
		self._baseType_child=ModelNone
		self._swRecordLayoutVAxis_child=ModelNone
		self._swRecordLayoutVProp_child=ModelNone
		self._swRecordLayoutVIndex_child=ModelNone
		self._swGenericAxisParamType_child=ModelNone
		self._swRecordLayoutVFixValue_child=ModelNone

class SwServiceArg(complexbase.GroupBase):
	"""Specifies the properties of a data object exchanged during the call of an SwService, e.g. an argument or a return value.

The SwServiceArg can also be used in the argument list of a C-macro. For this purpose the category shall be set to \"MACRO\". A reference to implementationDataType can optional be added if the actual argument has an implementationDataType."""
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swDataDefProps_child=ModelNone
		self._variationPoint_child=ModelNone

class SwSystemconst(complexbase.GroupBase):
	"""This element defines a system constant which serves an input to 
select a particular variation point. In particular a system constant serves as an operand of the binding function (swSyscond) in a Variation point.

Note that the binding process can only happen  if a value was assigned to to the referenced system constants."""
	def __init__(self):
		super().__init__()
		self._swDataDefProps_child=ModelNone

class SwSystemconstDependentFormula(complexbase.GroupBase):
	"""This class represents an expression depending on system constants."""
	def __init__(self):
		super().__init__()
		self._syscString_children=[]
		self._sysc_children=[]

class SwSystemconstValue(complexbase.GroupBase):
	"""This meta-class assigns a particular value to a system constant."""
	def __init__(self):
		super().__init__()
		self._swSystemconst_child=ModelNone
		self._value_child=ModelNone
		self._annotation_children=[]

class SwSystemconstantValueSet(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a set of system constant values."""
	def __init__(self):
		super().__init__()
		self._swSystemconstantValue_children=[]

class SwTextProps(complexbase.GroupBase):
	"""This meta-class expresses particular properties applicable to strings in variables or calibration parameters."""
	def __init__(self):
		super().__init__()
		self._arraySizeSemantics_child=ModelNone
		self._swMaxTextSize_child=ModelNone
		self._baseType_child=ModelNone
		self._swFillCharacter_child=ModelNone

class SwValueCont(complexbase.GroupBase):
	"""This metaclass represents the content of one particular SwInstance."""
	def __init__(self):
		super().__init__()
		self._unit_child=ModelNone
		self._unitDisplayName_child=ModelNone
		self._swArraysize_child=ModelNone
		self._swValuesPhys_child=ModelNone

class SwValues(complexbase.GroupBase):
	"""This meta-class represents a list of values. These values can either be the input values of a curve (abscissa values) or the associated values (ordinate values). 

For multidimensional structures, the values are ordered such that they follow the memory layout, see [TPS_SWCT_01882]

In particular for maps and cuboids etc. the resulting long value list can be subsectioned using ValueGroup. But the processing needs to be done as if vg is not there.

Note that numerical values and textual values should not be mixed."""
	def __init__(self):
		super().__init__()
		self._vtf_children=[]
		self._vf_children=[]
		self._vt_children=[]
		self._v_children=[]
		self._vg_children=[]

class SwVariableRefProxy(complexbase.GroupBase):
	"""Proxy class for several kinds of references to a variable."""
	def __init__(self):
		super().__init__()
		self._autosarVariable_child=ModelNone
		self._mcDataInstanceVar_child=ModelNone

class SwcBswMapping(complexbase.GroupBase):
	"""Maps an SwcInternalBehavior to an BswInternalBehavior. This is required to coordinate the API generation and the scheduling for AUTOSAR Service Components, ECU Abstraction Components and Complex Driver Components by the RTE and the BSW scheduling mechanisms."""
	def __init__(self):
		super().__init__()
		self._bswBehavior_child=ModelNone
		self._runnableMapping_children=[]
		self._swcBehavior_child=ModelNone
		self._synchronizedModeGroup_children=[]
		self._synchronizedTrigger_children=[]

class SwcBswRunnableMapping(complexbase.GroupBase):
	"""Maps a BswModuleEntity to a RunnableEntity if it is implemented as part of a BSW module (in the case of an AUTOSAR Service, a Complex Driver or an ECU Abstraction). The mapping can be used by a tool to find relevant information on the behavior, e.g. whether the bswEntity shall be running in interrupt context."""
	def __init__(self):
		super().__init__()
		self._bswEntity_child=ModelNone
		self._swcRunnable_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcBswSynchronizedModeGroupPrototype(complexbase.GroupBase):
	"""Synchronizes a mode group provided by a component via a port with a mode group provided by a BSW module or cluster."""
	def __init__(self):
		super().__init__()
		self._bswModeGroup_child=ModelNone
		self._swcModeGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcBswSynchronizedTrigger(complexbase.GroupBase):
	"""Synchronizes a Trigger provided by a component via a port with a Trigger provided by a BSW module or cluster."""
	def __init__(self):
		super().__init__()
		self._bswTrigger_child=ModelNone
		self._swcTrigger_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcExclusiveAreaPolicy(complexbase.GroupBase):
	"""Options how to generate the ExclusiveArea related APIs. If no SwcExclusiveAreaPolicy is specified for an ExclusiveArea the default values apply."""
	def __init__(self):
		super().__init__()
		self._apiPrinciple_child=ModelNone
		self._exclusiveArea_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcImplementation(complexbase.GroupBase):
	"""This meta-class represents a specialization of the general Implementation meta-class with respect to the usage in application software."""
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone
		self._perInstanceMemorySize_children=[]
		self._requiredRTEVendor_child=ModelNone

class SwcInternalBehavior(complexbase.GroupBase):
	"""The SwcInternalBehavior of an AtomicSwComponentType describes the relevant aspects of the software-component with respect to the RTE, i.e. the RunnableEntities and the RTEEvents they respond to."""
	def __init__(self):
		super().__init__()
		self._arTypedPerInstanceMemory_children=[]
		self._event_children=[]
		self._exclusiveAreaPolicy_children=[]
		self._explicitInterRunnableVariable_children=[]
		self._handleTerminationAndRestart_child=ModelNone
		self._implicitInterRunnableVariable_children=[]
		self._includedDataTypeSet_children=[]
		self._includedModeDeclarationGroupSet_children=[]
		self._instantiationDataDefProps_children=[]
		self._perInstanceMemory_children=[]
		self._perInstanceParameter_children=[]
		self._portAPIOption_children=[]
		self._runnable_children=[]
		self._serviceDependency_children=[]
		self._sharedParameter_children=[]
		self._supportsMultipleInstantiation_child=ModelNone
		self._variationPointProxy_children=[]
		self._variationPoint_child=ModelNone

class SwcModeManagerErrorEvent(complexbase.GroupBase):
	"""This event is raised when an error occurred during the handling of the referenced ModeDeclarationGroupPrototype."""
	def __init__(self):
		super().__init__()
		self._modeGroup_child=ModelNone

class SwcModeSwitchEvent(complexbase.GroupBase):
	"""This event is raised when the specified mode change occurs."""
	def __init__(self):
		super().__init__()
		self._activation_child=ModelNone
		self._mode_children=[]

class SwcServiceDependency(complexbase.GroupBase):
	"""Specialization of ServiceDependency in the context of an SwcInternalBehavior. It allows to associate ports, port groups and (in special cases) data defined for an atomic software component  to a given ServiceNeeds element."""
	def __init__(self):
		super().__init__()
		self._assignedData_children=[]
		self._assignedPort_children=[]
		self._representedPortGroup_child=ModelNone
		self._serviceNeeds_child=[]
		self._variationPoint_child=ModelNone

class SwcServiceDependencyInCompositionInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._base_child=ModelNone
		self._rootContext_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._targetSwcServiceDependency_child=ModelNone

class SwcServiceDependencyInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._base_child=ModelNone
		self._contextRootSwComposition_child=ModelNone
		self._contextSwComponentPrototype_children=[]
		self._targetSwcServiceDependency_child=ModelNone

class SwcSupportedFeature(complexbase.GroupBase):
	"""This meta-class represents a abstract base class for features that can be supported by a RunnableEntity."""

class SwcTiming(complexbase.GroupBase):
	"""The SwcTiming is used to describe the timing of an atomic software component.

TimingDescriptions aggregated by SwcTiming are restricted to event chains referring to events which are derived from the classes TDEventVfb and TDEventSwcInternalBehavior."""
	def __init__(self):
		super().__init__()
		self._behavior_child=ModelNone
		self._component_child=ModelNone

class SwcToApplicationPartitionMapping(complexbase.GroupBase):
	"""Allows to map a given SwComponentPrototype to a formally defined partition at a point in time when the corresponding EcuInstance is not yet known or defined."""
	def __init__(self):
		super().__init__()
		self._applicationPartition_child=ModelNone
		self._swComponentPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcToEcuMapping(complexbase.GroupBase):
	"""This meta-class is used:
* to map SwComponentPrototypes to a specific ECU Instance unit,
* optionally to map SwComponentPrototypes to a HwElement with category ProcessingUnit,
* optionally to map SwComponentPrototypes typed by SensorActuatorSwComponentType to a HwElement with category SensorActuator.

For each combination of ECUInstance and the optional ProcessingUnit and the optional SensorActuator only one SwcToEcuMapping shall be used."""
	def __init__(self):
		super().__init__()
		self._component_children=[]
		self._controlledHwElement_child=ModelNone
		self._ecuInstance_child=ModelNone
		self._partition_child=ModelNone
		self._processingUnit_child=ModelNone
		self._variationPoint_child=ModelNone

class SwcToEcuMappingConstraint(complexbase.GroupBase):
	"""The System Constraint Description has to describe dedicated and exclusive mapping of SW-Cs to one or more ECUs. Dedicated mapping means that the SW-C can only be mapped to the ECUs it is dedicated to. Exclusive Mapping means that the SW-C cannot be mapped to the ECUs it is excluded from."""
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone
		self._ecuInstance_children=[]
		self._swcToEcuMappingConstraintType_child=ModelNone

class SwcToImplMapping(complexbase.GroupBase):
	"""Map instances of an AtomicSwComponentType to a specific Implementation."""
	def __init__(self):
		super().__init__()
		self._componentImplementation_child=ModelNone
		self._component_children=[]
		self._variationPoint_child=ModelNone

class SwcToSwcOperationArguments(complexbase.GroupBase):
	"""The SwcToSwcOperationArguments describes the information (client server operation arguments, plus the operation identification, if required) that are exchanged between two SW Components from exactly one client to one server, or from one server back to one client. The direction attribute defines which direction is described. If direction == IN, all arguments sent from the client to the server are described by the SwcToSwcOperationArguments, in direction == OUT, it's the arguments sent back from server to client."""
	def __init__(self):
		super().__init__()
		self._direction_child=ModelNone
		self._operation_children=[]

class SwcToSwcSignal(complexbase.GroupBase):
	"""The SwcToSwcSignal describes the information (data element) that is exchanged between two SW Components. On the SWC Level it is possible that a SW Component sends one data element from one P-Port to two different SW Components (1:n Communication). The SwcToSwcSignal describes exactly the information which is exchanged between one P-Port of a SW Component and one R-Port of another SW Component."""
	def __init__(self):
		super().__init__()
		self._dataElement_children=[]

class SwitchAsynchronousTrafficShaperGroupEntry(complexbase.GroupBase):
	"""Defines an Asynchronous Traffic Shapter (ATS) Group for a switch."""
	def __init__(self):
		super().__init__()
		self._maximumResidenceTime_child=ModelNone

class SwitchFlowMeteringEntry(complexbase.GroupBase):
	"""Defines a Flow Metering Entry for a switch."""
	def __init__(self):
		super().__init__()
		self._colorMode_child=ModelNone
		self._committedBurstSize_child=ModelNone
		self._committedInformationRate_child=ModelNone
		self._couplingFlag_child=ModelNone
		self._excessBurstSize_child=ModelNone
		self._excessInformationRate_child=ModelNone

class SwitchStreamFilterActionDestPortModification(complexbase.GroupBase):
	"""Defines the action to modify the destination port(s) determined by the frame forwarding process for an particular Ethernet frame. Either the egress destination of an Ethernet frame is extended or overwritten."""
	def __init__(self):
		super().__init__()
		self._egressPort_children=[]
		self._modification_child=ModelNone

class SwitchStreamFilterEntry(complexbase.GroupBase):
	"""Defines a Stream Filter Entry."""
	def __init__(self):
		super().__init__()
		self._asynchronousTrafficShaper_child=ModelNone
		self._filterPriority_child=ModelNone
		self._flowMetering_child=ModelNone
		self._maxSduSize_child=ModelNone
		self._streamGate_child=ModelNone
		self._streamIdentificationHandle_children=[]
		self._streamIdentificationWildcard_child=ModelNone

class SwitchStreamFilterRule(complexbase.GroupBase):
	"""SwitchStreamIdentification"""
	def __init__(self):
		super().__init__()
		self._dataLinkLayerRule_child=ModelNone
		self._ieee1722TpRule_child=ModelNone
		self._ipTpRule_child=ModelNone

class SwitchStreamGateEntry(complexbase.GroupBase):
	"""Defines a Asynchronous Traffic Shapter (ATS) Group for a switch."""
	def __init__(self):
		super().__init__()
		self._internalPriorityValue_child=ModelNone

class SwitchStreamIdentification(complexbase.GroupBase):
	"""SwitchStreamIdentification"""
	def __init__(self):
		super().__init__()
		self._egressPort_children=[]
		self._filterActionBlockSource_child=ModelNone
		self._filterActionDestPortModification_child=ModelNone
		self._filterActionDropFrame_child=ModelNone
		self._filterActionVlanModification_child=ModelNone
		self._ingressPort_children=[]
		self._streamFilterRule_child=ModelNone

class SymbolProps(complexbase.GroupBase):
	"""'''begin restrict to CP'''
This meta-class represents the ability to attach with the symbol attribute a symbolic name that is conform to C language requirements to another meta-class, e.g. AtomicSwComponentType, that is a potential subject to a name clash on the level of RTE source code.
'''end restrict to CP'''
'''begin restrict to AP'''
This meta-class represents the ability to contribute a part of a namespace.
'''end restrict to AP'''
'''begin restrict to FO'''
If applied to Classic Platform:
This meta-class represents the ability to attach with the symbol attribute a symbolic name that is conform to C language requirements to another meta-class, e.g. AtomicSwComponentType, that is a potential subject to a name clash on the level of RTE source code.

If applied to Adaptive Platform:
This meta-class represents the ability to contribute a part of a namespace.
'''end restrict to FO'''"""

class SymbolicNameProps(complexbase.GroupBase):
	"""This meta-class can be taken to contribute to the creation of symbolic name values."""

class SyncTimeBaseMgrUserNeeds(complexbase.GroupBase):
	"""Specifies the needs on the configuration of the Synchronized Time-base Manager for one time-base. This class currently contains no attributes. An instance of this class is used to find out which ports of a software-component belong to this time-base in order to group the request and response ports of the same time-base. The actual time-base value is stored in the PortDefinedArgumentValue of the respective port specification."""

class SynchronizationPointConstraint(complexbase.GroupBase):
	"""Specifies a synchronization point either between groups of [ARMetaClass{ExecutableEntity}]s or individual [ARMetaClass{ExecutableEntity}]s referenced via their corresponding RTE or BSW events."""
	def __init__(self):
		super().__init__()
		self._sourceEec_children=[]
		self._sourceEvent_children=[]
		self._targetEec_children=[]
		self._targetEvent_children=[]

class SynchronizationTimingConstraint(complexbase.GroupBase):
	"""This constraint is used to restrict the timing behavior of different, but correlated events or event chains, with regard to synchronization.  Two scenarios are supported:

* If ([ARMetaClassRole{synchronizationConstraintType}{SynchronizationTimingConstraint}]==[ARMetaClassRole{responseSynchronization}{SynchronizationTypeEnum}])
** [ARMetaClass{TimingDescriptionEvent}]s: An arbitrary number of correlated events which play the role of responses shall occur synchronously with respect to a predefined tolerance. 
** [ARMetaClass{TimingDescriptionEventChain}]s: An arbitrary number of correlated event chains with a common stimulus, but different responses, where the responses shall occur synchronously with respect to a predefined tolerance.

* If ([ARMetaClassRole{synchronizationConstraintType}{SynchronizationTimingConstraint}]==[ARMetaClassRole{stimulusSynchronization}{SynchronizationTypeEnum}])
** [ARMetaClass{TimingDescriptionEvent}]s:An arbitrary number of correlated events which play the role of stimuli shall occur synchronously with respect to a predefined tolerance.
** [ARMetaClass{TimingDescriptionEventChain}]s: An arbitrary number of correlated event chains with a common response, but different stimuli, where the stimuli shall occur synchronously with respect to a predefined tolerance.

In case the constraint is imposed on events the following two scenarios are supported:

* If ([ARMetaClassRole{eventOccurrenceKind}{SynchronizationTimingConstraint}]==[AREnumLiteral{singleOccurrence}{EventOccurrenceKindEnum}]): any of the events shall occur only once in the given time interval.

* If ([ARMetaClassRole{eventOccurrenceKind}{SynchronizationTimingConstraint}]==[AREnumLiteral{multipleOccurrences}{EventOccurrenceKindEnum}]): any of the events may occur more than once in the given time interval. In other words multiple occurrences of an event within the given time interval are permitted."""
	def __init__(self):
		super().__init__()
		self._eventOccurrenceKind_child=ModelNone
		self._scopeEvent_children=[]
		self._scope_children=[]
		self._synchronizationConstraintType_child=ModelNone
		self._tolerance_child=ModelNone

class SynchronizedTimeBaseConsumer(complexbase.GroupBase):
	"""This meta-class represents a Synchronized Time Base Consumer."""
	def __init__(self):
		super().__init__()
		self._networkTimeConsumer_child=ModelNone

class SynchronizedTimeBaseConsumerInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for the interaction with a Time Synchronization Consumer."""

class SynchronizedTimeBaseProvider(complexbase.GroupBase):
	"""This meta-class represents a Synchronized Time Base Provider."""
	def __init__(self):
		super().__init__()
		self._networkTimeProvider_child=ModelNone
		self._timeSyncCorrection_child=ModelNone

class SynchronizedTimeBaseProviderInterface(complexbase.GroupBase):
	"""This meta-class provides the ability to define a PortInterface for the interaction with a Time Synchronization Provider."""
	def __init__(self):
		super().__init__()
		self._timeBaseKind_child=ModelNone

class SynchronousServerCallPoint(complexbase.GroupBase):
	"""This means that the RunnableEntity is supposed to perform a  blocking wait for a response from the server."""
	def __init__(self):
		super().__init__()
		self._calledFromWithinExclusiveArea_child=ModelNone

class System(complexbase.GroupBase):
	"""'''begin restrict to CP'''
The top level element of the System Description.
The System description defines five major elements: Topology, Software, Communication, Mapping and Mapping Constraints.

The System element directly aggregates the elements describing the Software, Mapping and Mapping Constraints; it contains a reference to an ASAM FIBEX description specifying Communication and Topology.
'''end restrict to CP'''
'''begin restrict to AP'''
The top level element of the System Description. 
'''end restrict to AP'''
'''begin restrict to FO'''
The top level element of the Abstract Platform System Description.
'''end restrict to FO'''"""
	def __init__(self):
		super().__init__()
		self._systemDocumentation_children=[]
		self._clientIdDefinitionSet_children=[]
		self._containerIPduHeaderByteOrder_child=ModelNone
		self._ecuExtractVersion_child=ModelNone
		self._fibexElement_children=[]
		self._interpolationRoutineMappingSet_children=[]
		self._j1939SharedAddressCluster_children=[]
		self._mapping_children=[]
		self._pncVectorLength_child=ModelNone
		self._pncVectorOffset_child=ModelNone
		self._rootSoftwareComposition_children=[]
		self._swCluster_children=[]
		self._systemVersion_child=ModelNone

class SystemMapping(complexbase.GroupBase):
	"""'''begin restrict to CP'''
The system mapping aggregates all mapping aspects (mapping of SW components to ECUs, mapping of data elements to signals, and mapping constraints).
'''end restrict to CP'''
'''begin restrict to AP'''
The system mapping aggregates all mapping aspects that are relevant in the System Description.
'''end restrict to AP'''"""
	def __init__(self):
		super().__init__()
		self._appOsTaskProxyToEcuTaskProxyMapping_children=[]
		self._applicationPartitionToEcuPartitionMapping_children=[]
		self._comManagementMapping_children=[]
		self._cryptoServiceMapping_children=[]
		self._dataMapping_children=[]
		self._ddsISignalToTopicMapping_children=[]
		self._ecuResourceMapping_children=[]
		self._j1939ControllerApplicationToJ1939NmNodeMapping_children=[]
		self._mappingConstraint_children=[]
		self._pncMapping_children=[]
		self._portElementToComResourceMapping_children=[]
		self._resourceEstimation_children=[]
		self._resourceToApplicationPartitionMapping_children=[]
		self._rteEventSeparation_children=[]
		self._rteEventToOsTaskProxyMapping_children=[]
		self._signalPathConstraint_children=[]
		self._softwareClusterToApplicationPartitionMapping_children=[]
		self._softwareClusterToResourceMapping_children=[]
		self._swClusterMapping_children=[]
		self._swImplMapping_children=[]
		self._swMapping_children=[]
		self._swcToApplicationPartitionMapping_children=[]
		self._systemSignalGroupToComResourceMapping_children=[]
		self._systemSignalToComResourceMapping_children=[]
		self._variationPoint_child=ModelNone

class SystemSignal(complexbase.GroupBase):
	"""The system signal represents the communication system's view of data exchanged between SW components which reside on different ECUs. The system signals allow to represent this communication in a flattened structure, with exactly one system signal defined for each data element prototype sent and received by connected SW component instances."""
	def __init__(self):
		super().__init__()
		self._dynamicLength_child=ModelNone
		self._physicalProps_child=ModelNone

class SystemSignalGroup(complexbase.GroupBase):
	"""A signal group refers to a set of signals that shall always be kept together. A signal group is used to guarantee the atomic transfer of AUTOSAR composite data types.  

The SystemSignalGroup defines a signal grouping on VFB level. On cluster level the Signal grouping is described by the ISignalGroup element."""
	def __init__(self):
		super().__init__()
		self._systemSignal_children=[]
		self._transformingSystemSignal_child=ModelNone

class SystemSignalGroupToCommunicationResourceMapping(complexbase.GroupBase):
	"""This meta class maps a communication resource to a SystemSignalGroup. This mapping can be used in an early process stage in which the DataMapping linking the Ports and mapped CpSoftwareClusterCommunicationResource(s) to SystemSignals of a SystemSignalGroup is not yet available."""
	def __init__(self):
		super().__init__()
		self._softwareClusterComResource_child=ModelNone
		self._systemSignalGroup_child=ModelNone
		self._variationPoint_child=ModelNone

class SystemSignalToCommunicationResourceMapping(complexbase.GroupBase):
	"""This meta class maps a communication resource to a SystemSignal. This mapping can be used in an early process stage in which the DataMapping linking the Ports and mapped CpSoftwareClusterCommunicationResource(s) to the SystemSignal is not yet available."""
	def __init__(self):
		super().__init__()
		self._softwareClusterComResource_child=ModelNone
		self._systemSignal_child=ModelNone
		self._variationPoint_child=ModelNone

class SystemTiming(complexbase.GroupBase):
	"""A model element used to refine timing descriptions and constraints (from a VfbTiming) at System level, utilizing information about topology, software deployment, and signal mapping described in the System Template.

TimingDescriptions aggregated by SystemTiming are restricted to events which are derived from the class TDEventVfb, TDEventSwcInternalBehavior and TDEventCom."""
	def __init__(self):
		super().__init__()
		self._system_child=ModelNone

class Table(complexbase.GroupBase):
	"""This class implements an exchange table according to OASIS Technical Resolution TR 9503:1995.

http://www.oasis-open.org/specs/a503.htm"""
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates if by default a line should be drawn between the columns of this table.""")
	float=complexbase.Attribute("float",SimpleTypes.FloatEnum,'FLOAT',False,"""Indicate whether it is allowed to break the element.""")
	frame=complexbase.Attribute("frame",SimpleTypes.FrameEnum,'FRAME',False,"""Used to defined the frame line around a table.""")
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	orient=complexbase.Attribute("orient",SimpleTypes.OrientEnum,'ORIENT',False,"""Indicate whether a table should be represented as landscape or portrait. 
* land : landscape
* port : portrait""")
	pgwide=complexbase.Attribute("pgwide",SimpleTypes.NameToken,'PGWIDE',False,"""Used to indicate whether the figure should take the complete page width (value = \"pgwide\") or not (value = \"noPgwide\").""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates if by default  a line should be drawn at the bottom of table rows.""")
	tabstyle=complexbase.Attribute("tabstyle",SimpleTypes.NameToken,'TABSTYLE',False,"""Indicates an external table style.""")
	def __init__(self):
		super().__init__()
		self._tableCaption_child=ModelNone
		self._tgroup_children=[]
		self._variationPoint_child=ModelNone

class TagWithOptionalValue(complexbase.GroupBase):
	"""A tagged value is a combination of a tag (key) and a value that gives supplementary information that is attached to a model element. Please note that keys without a value are allowed."""
	def __init__(self):
		super().__init__()
		self._key_child=ModelNone
		self._sequenceOffset_child=ModelNone
		self._value_child=ModelNone
		self._variationPoint_child=ModelNone

class TargetIPduRef(complexbase.GroupBase):
	"""Target destination of the referencing mapping."""
	def __init__(self):
		super().__init__()
		self._defaultValue_child=ModelNone
		self._targetIPdu_child=ModelNone

class Tbody(complexbase.GroupBase):
	"""This meta-class represents a part within a table group. Such a part can be the table head, the table body or the table foot."""
	valign=complexbase.Attribute("valign",SimpleTypes.ValignEnum,'VALIGN',False,"""Indicates how the cells in the rows shall be aligned. Default is inherited from tbody, otherwise it is \"TOP\"""")
	def __init__(self):
		super().__init__()
		self._row_children=[]

class TcpIpIcmpv4Props(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for ICMPv4 (Internet Control Message Protocol)."""
	def __init__(self):
		super().__init__()
		self._tcpIpIcmpV4EchoReplyEnabled_child=ModelNone
		self._tcpIpIcmpV4Ttl_child=ModelNone

class TcpIpIcmpv6Props(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for ICMPv6 (Internet Control Message Protocol)."""
	def __init__(self):
		super().__init__()
		self._tcpIpIcmpV6EchoReplyAvoidFragmentation_child=ModelNone
		self._tcpIpIcmpV6EchoReplyEnabled_child=ModelNone
		self._tcpIpIcmpV6HopLimit_child=ModelNone
		self._tcpIpIcmpV6MsgDestinationUnreachableEnabled_child=ModelNone
		self._tcpIpIcmpV6MsgParameterProblemEnabled_child=ModelNone

class TcpOptionFilterList(complexbase.GroupBase):
	"""Permitted list for the filtering of TCP options."""
	def __init__(self):
		super().__init__()
		self._allowedTcpOption_children=[]

class TcpOptionFilterSet(complexbase.GroupBase):
	"""Set of TcpOptionFilterLists."""
	def __init__(self):
		super().__init__()
		self._tcpOptionFilterList_children=[]

class TcpProps(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for TCP (Transmission Control Protocol)."""
	def __init__(self):
		super().__init__()
		self._tcpCongestionAvoidanceEnabled_child=ModelNone
		self._tcpDelayedAckTimeout_child=ModelNone
		self._tcpFastRecoveryEnabled_child=ModelNone
		self._tcpFastRetransmitEnabled_child=ModelNone
		self._tcpFinWait2Timeout_child=ModelNone
		self._tcpKeepAliveEnabled_child=ModelNone
		self._tcpKeepAliveInterval_child=ModelNone
		self._tcpKeepAliveProbesMax_child=ModelNone
		self._tcpKeepAliveTime_child=ModelNone
		self._tcpMaxRtx_child=ModelNone
		self._tcpMsl_child=ModelNone
		self._tcpNagleEnabled_child=ModelNone
		self._tcpReceiveWindowMax_child=ModelNone
		self._tcpRetransmissionTimeout_child=ModelNone
		self._tcpSlowStartEnabled_child=ModelNone
		self._tcpSynMaxRtx_child=ModelNone
		self._tcpSynReceivedTimeout_child=ModelNone
		self._tcpTtl_child=ModelNone

class TcpRule(complexbase.GroupBase):
	"""Configuration of TCP filter rules."""
	def __init__(self):
		super().__init__()
		self._numberOfParallelTcpSessions_child=ModelNone
		self._stateManagementBasedOnTcpFlags_child=ModelNone
		self._timeoutCheck_child=ModelNone

class TcpTp(complexbase.GroupBase):
	"""Content Model for TCP configuration."""
	def __init__(self):
		super().__init__()
		self._keepAliveInterval_child=ModelNone
		self._keepAliveProbesMax_child=ModelNone
		self._keepAliveTime_child=ModelNone
		self._keepAlives_child=ModelNone
		self._naglesAlgorithm_child=ModelNone
		self._receiveWindowMin_child=ModelNone
		self._tcpRetransmissionTimeout_child=ModelNone
		self._tcpTpPort_child=ModelNone

class TcpUdpConfig(complexbase.GroupBase):
	"""Tcp or Udp Transport Protocol Configuration."""

class TDCpSoftwareClusterMapping(complexbase.GroupBase):
	"""This is used to specify a mapping between a software cluster that provides temporal and dynamic resources and the software clusters that need these resources."""
	def __init__(self):
		super().__init__()
		self._provider_child=ModelNone
		self._requestor_children=[]
		self._timingDescription_child=ModelNone
		self._variationPoint_child=ModelNone

class TDCpSoftwareClusterMappingSet(complexbase.GroupBase):
	"""This is used to gather of classic platform software cluster mappings."""
	def __init__(self):
		super().__init__()
		self._tdCpSoftwareClusterResourceToTdMapping_children=[]
		self._tdCpSoftwareClusterToTdMapping_children=[]

class TDCpSoftwareClusterResourceMapping(complexbase.GroupBase):
	"""This is used to assign an unequivocal global resource identification to a temporal and dynamic resource."""
	def __init__(self):
		super().__init__()
		self._resource_child=ModelNone
		self._timingDescription_child=ModelNone
		self._variationPoint_child=ModelNone

class TDEventBsw(complexbase.GroupBase):
	"""This is used to describe timing events related to BSW modules."""
	def __init__(self):
		super().__init__()
		self._bswModuleDescription_child=ModelNone

class TDEventBswInternalBehavior(complexbase.GroupBase):
	"""This is used to describe timing events related to the BswInternalBehavior of a BSW module."""
	def __init__(self):
		super().__init__()
		self._bswModuleEntity_child=ModelNone
		self._tdEventBswInternalBehaviorType_child=ModelNone

class TDEventBswModeDeclaration(complexbase.GroupBase):
	"""This is used to describe timing events related to the mode communication on BSW level."""
	def __init__(self):
		super().__init__()
		self._entryModeDeclaration_child=ModelNone
		self._exitModeDeclaration_child=ModelNone
		self._modeDeclaration_child=ModelNone
		self._tdEventBswModeDeclarationType_child=ModelNone

class TDEventBswModule(complexbase.GroupBase):
	"""This is used to describe timing events related to the interaction between BSW modules."""
	def __init__(self):
		super().__init__()
		self._bswModuleEntry_child=ModelNone
		self._tdEventBswModuleType_child=ModelNone

class TDEventCom(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events related to communication including the physical layer."""
	def __init__(self):
		super().__init__()
		self._ecuInstance_child=ModelNone

class TDEventComplex(complexbase.GroupBase):
	"""This is used to describe complex timing events.

The context of a complex timing event either is described informally, e.g. using the documentation block, or is described formally by the associated TDEventOccurrenceExpression."""

class TDEventCycleStart(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events related to a point in time where a communication cycle starts.

Via the attribute \"cycleRepetition\", a filtered view to the cycle start can be defined."""
	def __init__(self):
		super().__init__()
		self._cycleRepetition_child=ModelNone

class TDEventFrClusterCycleStart(complexbase.GroupBase):
	"""This is used to describe the timing event related to a point in time where a communication cycle starts on a FlexRay cluster."""
	def __init__(self):
		super().__init__()
		self._frCluster_child=ModelNone

class TDEventFrame(complexbase.GroupBase):
	"""This is used to describe timing events related to the exchange of frames between the communication controller and the bus specific (FlexRay / CAN / LIN) Interface BSW module."""
	def __init__(self):
		super().__init__()
		self._frame_child=ModelNone
		self._physicalChannel_child=ModelNone
		self._tdEventType_child=ModelNone

class TDEventFrameEthernet(complexbase.GroupBase):
	"""This is used to describe timing description events related to the exchange of Ethernet frames between an Ethernet communication controller and the BSW Ethernet interface and driver module."""
	def __init__(self):
		super().__init__()
		self._socketConnectionBundle_child=ModelNone
		self._staticSocketConnection_child=ModelNone
		self._tdEventType_child=ModelNone
		self._tdHeaderIdFilter_children=[]
		self._tdPduTriggeringFilter_children=[]

class TDEventIPdu(complexbase.GroupBase):
	"""This is used to describe timing events related to the exchange of I-PDUs between the bus specific (FlexRay / CAN / LIN) Interface BSW module and COM."""
	def __init__(self):
		super().__init__()
		self._iPdu_child=ModelNone
		self._physicalChannel_child=ModelNone
		self._tdEventType_child=ModelNone

class TDEventISignal(complexbase.GroupBase):
	"""This is used to describe timing events related to the exchange of I-Signals between COM and RTE."""
	def __init__(self):
		super().__init__()
		self._iSignal_child=ModelNone
		self._physicalChannel_child=ModelNone
		self._tdEventType_child=ModelNone

class TDEventModeDeclaration(complexbase.GroupBase):
	"""This is used to describe timing events related to mode switch communication at VFB level."""
	def __init__(self):
		super().__init__()
		self._entryModeDeclaration_child=ModelNone
		self._exitModeDeclaration_child=ModelNone
		self._modeDeclaration_child=ModelNone
		self._tdEventModeDeclarationType_child=ModelNone

class TDEventOccurrenceExpression(complexbase.GroupBase):
	"""This is used to specify a filter on the occurrences of [ARMetaClass{TimingDescriptionEvent}]s by means of a TDEventOccurrenceExpressionFormula.
Filter criteria can be [ARMetaClassRole{variable}{TDEventOccurrenceExpression}]  and [ARMetaClassRole{argument}{TDEventOccurrenceExpression}] values, i.e. the timing event only occurs for specific values, as well as the temporal characteristics of the occurrences of arbitrary timing events."""
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._formula_child=ModelNone
		self._mode_children=[]
		self._variable_children=[]

class TDEventOccurrenceExpressionFormula(complexbase.GroupBase):
	"""This is an extension of the FormulaExpression for the AUTOSAR Timing Extensions.

A TDEventOccurrenceExpressionFormula provides the means to express the temporal characteristics of timing event occurrences in correlation with specific variable and argument values.

The formal definition of the extended functions (ExtUnaryFunctions) is described in detail in the AUTOSAR Timing Extensions."""
	def __init__(self):
		super().__init__()
		self._argument_children=[]
		self._event_children=[]
		self._mode_children=[]
		self._variable_children=[]

class TDEventOperation(complexbase.GroupBase):
	"""This is used to describe timing events related to client-server communication at VFB level."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._tdEventOperationType_child=ModelNone

class TDEventServiceInstance(complexbase.GroupBase):
	"""This is the abstract parent class to describe specific timing description event types for service-oriented communication."""
	def __init__(self):
		super().__init__()
		self._serviceInstanceToPortPrototypeMapping_child=ModelNone

class TDEventServiceInstanceDiscovery(complexbase.GroupBase):
	"""This is used to describe timing description events related to different phases of service discovery."""
	def __init__(self):
		super().__init__()
		self._tdEventServiceInstanceDiscoveryType_child=ModelNone

class TDEventServiceInstanceEvent(complexbase.GroupBase):
	"""This is used to describe timing description events related to events of a service."""
	def __init__(self):
		super().__init__()
		self._event_child=ModelNone
		self._tdEventServiceInstanceEventType_child=ModelNone

class TDEventServiceInstanceField(complexbase.GroupBase):
	"""This is used to describe timing description events related to fields of a service."""
	def __init__(self):
		super().__init__()
		self._field_child=ModelNone
		self._tdEventServiceInstanceFieldType_child=ModelNone

class TDEventServiceInstanceMethod(complexbase.GroupBase):
	"""This is used to describe timing description events related to methods of a service."""
	def __init__(self):
		super().__init__()
		self._method_child=ModelNone
		self._tdEventServiceInstanceMethodType_child=ModelNone

class TDEventSLLET(complexbase.GroupBase):
	"""Used to describe SL-LET (System-Level) timing events."""

class TDEventSLLETPort(complexbase.GroupBase):
	"""Used to describe SL-LET timing events on the level of a SWC port."""
	def __init__(self):
		super().__init__()
		self._port_child=ModelNone

class TDEventSwc(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events at Software Component (SW-C) level."""
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone

class TDEventSwcInternalBehavior(complexbase.GroupBase):
	"""This is used to describe timing events related to the SwcInternalBehavior of an AtomicSwComponentType."""
	def __init__(self):
		super().__init__()
		self._runnable_child=ModelNone
		self._tdEventSwcInternalBehaviorType_child=ModelNone
		self._variableAccess_child=ModelNone

class TDEventSwcInternalBehaviorReference(complexbase.GroupBase):
	"""This is used to reference timing description events related to the Software Component (SW-C) view which are specified in other timing views."""
	def __init__(self):
		super().__init__()
		self._referencedTDEventSwc_child=ModelNone

class TDEventTrigger(complexbase.GroupBase):
	"""This is used to describe timing events related to triggers at VFB level."""
	def __init__(self):
		super().__init__()
		self._tdEventTriggerType_child=ModelNone
		self._trigger_child=ModelNone

class TDEventTTCanCycleStart(complexbase.GroupBase):
	"""This is used to describe the timing event related to a point in time where a communication cycle starts on a TTCAN cluster."""
	def __init__(self):
		super().__init__()
		self._ttCanCluster_child=ModelNone

class TDEventVariableDataPrototype(complexbase.GroupBase):
	"""This is used to describe timing events related to sender-receiver communication at VFB level."""
	def __init__(self):
		super().__init__()
		self._dataElement_child=ModelNone
		self._tdEventVariableDataPrototypeType_child=ModelNone

class TDEventVfb(complexbase.GroupBase):
	"""This is the abstract parent class to describe timing events at Virtual Functional Bus (VFB) level."""
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone

class TDEventVfbPort(complexbase.GroupBase):
	"""This is the abstract parent class to describe specific timing event types at Virtual Functional Bus (VFB) level."""
	def __init__(self):
		super().__init__()
		self._isExternal_child=ModelNone
		self._portPrototypeBlueprint_child=ModelNone
		self._port_child=ModelNone

class TDEventVfbReference(complexbase.GroupBase):
	"""This is used to reference timing description events related to the Virtual Functional Bus (VFB) view which are specified in other timing views."""
	def __init__(self):
		super().__init__()
		self._referencedTDEventVfb_child=ModelNone

class TDHeaderIdRange(complexbase.GroupBase):
	"""Specifies a range of PDU header identifiers. This range is specified by a minimum and maximum header identifier; and the maximum header identifier shall be greater than or equal the minimum header identifier."""
	def __init__(self):
		super().__init__()
		self._maxHeaderId_child=ModelNone
		self._minHeaderId_child=ModelNone

class TDLETZoneClock(complexbase.GroupBase):
	"""Describes a LET zone clock."""
	def __init__(self):
		super().__init__()
		self._accuracyExt_child=ModelNone
		self._accuracyInt_child=ModelNone

class TextTableMapping(complexbase.GroupBase):
	"""Defines the mapping of two DataPrototypes typed by AutosarDataTypes that refer to CompuMethods of category TEXTTABLE, SCALE_LINEAR_AND_TEXTTABLE or BITFIELD_TEXTTABLE."""
	def __init__(self):
		super().__init__()
		self._bitfieldTextTableMaskFirst_child=ModelNone
		self._bitfieldTextTableMaskSecond_child=ModelNone
		self._identicalMapping_child=ModelNone
		self._mappingDirection_child=ModelNone
		self._valuePair_children=[]

class TextTableValuePair(complexbase.GroupBase):
	"""Defines a pair of text values which are translated into each other."""
	def __init__(self):
		super().__init__()
		self._firstValue_child=ModelNone
		self._secondValue_child=ModelNone

class TextValueSpecification(complexbase.GroupBase):
	"""The purpose of TextValueSpecification is to define the labels that correspond to enumeration values."""
	def __init__(self):
		super().__init__()
		self._value_child=ModelNone

class TextualCondition(complexbase.GroupBase):
	"""Specifies additional conditions for one or more model elements. The condition is described using human language."""
	def __init__(self):
		super().__init__()
		self._text_child=ModelNone

class Tgroup(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a table section."""
	align=complexbase.Attribute("align",SimpleTypes.AlignEnum,'ALIGN',False,"""Specifies how the cell entries shall be horizontally aligned within the specified TGROUP.
Default is \"LEFT\"""")
	cols=complexbase.Attribute("cols",SimpleTypes.Integer,'COLS',False,"""This attribute represents the number of columns in the table.""")
	colsep=complexbase.Attribute("colsep",SimpleTypes.TableSeparatorString,'COLSEP',False,"""Indicates if by default a line shall be drawn between the columns of this table group.""")
	rowsep=complexbase.Attribute("rowsep",SimpleTypes.TableSeparatorString,'ROWSEP',False,"""Indicates if by default a line shall be drawn at the bottom of the rows in this table group.""")
	def __init__(self):
		super().__init__()
		self._colspec_children=[]
		self._thead_child=ModelNone
		self._tfoot_child=ModelNone
		self._tbody_child=ModelNone

class TimeBaseProviderToPersistencyMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to define a mapping between a TimeBaseProvider and a PersistencyDeploymentElement for the purpose of storing and retrieving the time value."""
	def __init__(self):
		super().__init__()
		self._cyclicBackupInterval_child=ModelNone
		self._persistencyDeploymentElement_child=ModelNone
		self._timeBaseProvider_child=ModelNone

class TimeBaseResource(complexbase.GroupBase):
	"""This meta-class represents the attributes of one Time Base Resource for Time Synchronization."""

class TimeBaseResourceRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._TimeBaseResource_child=ModelNone
		self._variationPoint_child=ModelNone

class TimeRangeType(complexbase.GroupBase):
	"""The timeRange can be specified with the value attribute. Optionally a tolerance can be defined."""
	def __init__(self):
		super().__init__()
		self._tolerance_child=[]
		self._value_child=ModelNone

class TimeRangeTypeTolerance(complexbase.GroupBase):
	"""Maximum allowable deviation"""

class TimeSyncClientConfiguration(complexbase.GroupBase):
	"""Defines the configuration of the time synchronisation client."""
	def __init__(self):
		super().__init__()
		self._orderedMaster_children=[]
		self._timeSyncTechnology_child=ModelNone

class TimeSyncCorrection(complexbase.GroupBase):
	"""This meta-class represents the attributes used for the correction of time synchronization."""
	def __init__(self):
		super().__init__()
		self._allowProviderRateCorrection_child=ModelNone
		self._offsetCorrectionAdaptionInterval_child=ModelNone
		self._offsetCorrectionJumpThreshold_child=ModelNone
		self._rateCorrectionsPerMeasurementDuration_child=ModelNone
		self._rateDeviationMeasurementDuration_child=ModelNone

class TimeSyncModuleInstantiation(complexbase.GroupBase):
	"""This meta-class defines the attributes for the Time Synchronization configuration on a specific machine."""
	def __init__(self):
		super().__init__()
		self._timeBase_children=[]

class TimeSyncPortPrototypeToTimeBaseMapping(complexbase.GroupBase):
	"""This meta-class provides the ability to map a PortPrototype typed by a AbstractSynchronizedTimeBaseInterface to a TimeBaseResource in the context of a Process."""
	def __init__(self):
		super().__init__()
		self._process_child=ModelNone
		self._timeBaseResource_child=ModelNone
		self._timeSyncPPortPrototype_child=ModelNone
		self._timeSyncRPortPrototype_child=ModelNone

class TimeSyncServerConfiguration(complexbase.GroupBase):
	"""Defines the configuration of the time synchronisation server."""
	def __init__(self):
		super().__init__()
		self._priority_child=ModelNone
		self._syncInterval_child=ModelNone
		self._timeSyncServerIdentifier_child=ModelNone
		self._timeSyncTechnology_child=ModelNone

class TimeSynchronization(complexbase.GroupBase):
	"""Defines the servers / clients in a time synchronised network."""
	def __init__(self):
		super().__init__()
		self._timeSyncClient_child=ModelNone
		self._timeSyncServer_child=ModelNone

class TimeValueValueVariationPoint(complexbase.GroupBase):
	"""This class represents the ability to express a formula for a numerical time value."""

class TimingClock(complexbase.GroupBase):
	"""Describes an abstract clock."""
	def __init__(self):
		super().__init__()
		self._platformTimeBase_children=[]
		self._variationPoint_child=ModelNone

class TimingClockSyncAccuracy(complexbase.GroupBase):
	"""Describes the synchronization accuracy between exactly two TDClocks."""
	def __init__(self):
		super().__init__()
		self._accuracy_child=ModelNone
		self._lower_child=ModelNone
		self._upper_child=ModelNone
		self._variationPoint_child=ModelNone

class TimingCondition(complexbase.GroupBase):
	"""A TimingCondition describes a dependency on a specific condition. The element owns an expression which describes the timing condition dependency."""
	def __init__(self):
		super().__init__()
		self._timingConditionFormula_child=ModelNone
		self._variationPoint_child=ModelNone

class TimingConditionFormula(complexbase.GroupBase):
	"""A TimingConditionFormula describes a specific dependency. The expression shall be a boolean expression addressing modes, variables, arguments, and/or events."""
	def __init__(self):
		super().__init__()
		self._timingArgument_children=[]
		self._timingCondition_children=[]
		self._timingEvent_children=[]
		self._timingMode_children=[]
		self._timingVariable_children=[]

class TimingConstraint(complexbase.GroupBase):
	"""The abstract parent class of different timing constraints supported by the Timing extension.

A concrete timing constraint is used to bound the timing behavior of the model elements in its scope."""
	def __init__(self):
		super().__init__()
		self._timingCondition_child=ModelNone
		self._variationPoint_child=ModelNone

class TimingDescription(complexbase.GroupBase):
	"""The abstract parent class of the model elements that are used to define the scope of a timing constraint."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TimingDescriptionEvent(complexbase.GroupBase):
	"""A timing event is the abstract representation of a specific system behavior -- that can be observed at runtime -- in the AUTOSAR specification. Timing events are used to define the scope for timing constraints. Depending on the specific scope, the view on the system, and the level of abstraction different types of events are defined.

In order to avoid confusion with existing event descriptions in the AUTOSAR templates the timing specific event types use the prefix TD."""
	def __init__(self):
		super().__init__()
		self._clockReference_child=ModelNone
		self._occurrenceExpression_child=ModelNone

class TimingDescriptionEventChain(complexbase.GroupBase):
	"""An event chain describes the causal order for a set of functionally dependent timing events. Each event chain has a well defined stimulus and response, which describe its start and end point. Furthermore, it can be hierarchically decomposed into an arbitrary number of sub-chains, so called ''event chain segments''."""
	def __init__(self):
		super().__init__()
		self._isPipeliningPermitted_child=ModelNone
		self._stimulus_child=ModelNone
		self._response_child=ModelNone
		self._segment_children=[]

class TimingEvent(complexbase.GroupBase):
	"""This event is used to start RunnableEntities that shall be executed periodically."""
	def __init__(self):
		super().__init__()
		self._offset_child=ModelNone
		self._period_child=ModelNone

class TimingExtension(complexbase.GroupBase):
	"""The abstract parent class of the different template specific timing extensions.

Depending on the specific timing extension the timing descriptions and timing constraints, that can be used to specify the timing behavior, are restricted."""
	def __init__(self):
		super().__init__()
		self._timingClock_children=[]
		self._timingClockSyncAccuracy_children=[]
		self._timingCondition_children=[]
		self._timingDescription_children=[]
		self._timingGuarantee_children=[]
		self._timingRequirement_children=[]
		self._timingResource_child=ModelNone

class TimingExtensionResource(complexbase.GroupBase):
	"""A TimingExtensionResource provides the capability to contain instance references referred from within a timing condition formula."""
	def __init__(self):
		super().__init__()
		self._timingArgument_children=[]
		self._timingMode_children=[]
		self._timingVariable_children=[]

class TimingModeInstance(complexbase.GroupBase):
	"""This class specifies the mode declaration to be checked in a specific instance of a mode declaration group.
This is used in a timing condition formula as an operand of the unary timing function TIMEX_modeActive to check whether the mode declaration is active at the point in time this expression is evaluated."""
	def __init__(self):
		super().__init__()
		self._modeInstance_child=[]
		self._variationPoint_child=ModelNone

class TlsConnectionGroup(complexbase.GroupBase):
	"""Defines a collection of TlsCryptoServiceMappings which will not be active at the same time during runtime."""
	def __init__(self):
		super().__init__()
		self._tlsConnection_children=[]

class TlsCryptoCipherSuite(complexbase.GroupBase):
	"""This meta-class represents a cipher suite for describing cryptographic operations in the context of establishing a connection of ApplicationEndpoints that is protected by TLS."""
	def __init__(self):
		super().__init__()
		self._authentication_child=ModelNone
		self._certificate_child=ModelNone
		self._cipherSuiteId_child=ModelNone
		self._cipherSuiteShortLabel_child=ModelNone
		self._ellipticCurve_children=[]
		self._encryption_child=ModelNone
		self._keyExchangeAuthentication_children=[]
		self._keyExchange_children=[]
		self._preSharedKey_child=ModelNone
		self._priority_child=ModelNone
		self._props_child=ModelNone
		self._pskIdentity_child=ModelNone
		self._remoteCertificate_child=ModelNone
		self._signatureScheme_children=[]
		self._version_child=ModelNone

class TlsCryptoCipherSuiteProps(complexbase.GroupBase):
	"""This meta-class provides attributes to specify details of TLS Cipher Suites."""
	def __init__(self):
		super().__init__()
		self._tcpIpTlsUseSecurityExtensionForceEncryptThenMac_child=ModelNone

class TlsCryptoServiceMapping(complexbase.GroupBase):
	"""This meta-class has the ability to represent a crypto service mapping for the socket-based configuration of Transport Layer Security (TLS)."""
	def __init__(self):
		super().__init__()
		self._keyExchange_children=[]
		self._tlsCipherSuite_children=[]
		self._useClientAuthenticationRequest_child=ModelNone
		self._useSecurityExtensionRecordSizeLimit_child=ModelNone

class TlsDeployment(complexbase.GroupBase):
	"""The meta-class represents the ability to define a deployment of the TLS communication protocol configuration settings to crypto module entities."""
	def __init__(self):
		super().__init__()
		self._pskIdentityToKeySlotMapping_children=[]
		self._tlsJobMapping_children=[]

class TlsIamRemoteSubject(complexbase.GroupBase):
	"""This meta-class defines the proxy information about the remote node in case of TLS."""
	def __init__(self):
		super().__init__()
		self._acceptedCryptoCipherSuiteWithPsk_children=[]
		self._acceptedRemoteCertificate_children=[]
		self._certCommonName_child=ModelNone
		self._derivedCertificateAccepted_child=ModelNone
		self._iamRelevantTlsSecureComProps_children=[]

class TlsJobMapping(complexbase.GroupBase):
	"""This meta-class allows to map a TlsJobRequirement to a concrete crypto job that will fulfill the JobRequirement.

The crypto job represents a call to a specific routine that implements a crypto function and that uses a specific key and refers to a specific primitive as a formal representation of the crypto algorithm."""

class TlsPskIdentity(complexbase.GroupBase):
	"""This element is used to describe the pre-shared key shared during the handshake among the communication parties, to establish a TLS connection if the handshake is based on the existence of a pre-shared key."""
	def __init__(self):
		super().__init__()
		self._preSharedKey_child=ModelNone
		self._pskIdentity_child=ModelNone
		self._pskIdentityHint_child=ModelNone

class TlsSecureComProps(complexbase.GroupBase):
	"""Configuration of the Transport Layer Security protocol (TLS)."""
	def __init__(self):
		super().__init__()
		self._keyExchange_children=[]
		self._tlsCipherSuite_children=[]

class TlvDataIdDefinition(complexbase.GroupBase):
	"""This meta-class represents the ability to define the tlvDataId."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._tlvArgument_child=ModelNone
		self._tlvImplementationDataTypeElement_child=ModelNone
		self._tlvRecordElement_child=ModelNone

class TlvDataIdDefinitionSet(complexbase.GroupBase):
	"""This meta-class acts as a container of TlvDataIdDefinitions to be used in a given context"""
	def __init__(self):
		super().__init__()
		self._tlvDataIdDefinition_children=[]

class Topic1(complexbase.GroupBase):
	"""This meta-class represents a topic of a documentation. Topics are similar to chapters but they cannot be nested. 

They also do not appear in the table of content. Topics can be used to produce intermediate headlines thus structuring a chapter internally."""
	helpEntry=complexbase.Attribute("helpEntry",SimpleTypes.String,'HELP-ENTRY',False,"""This specifies an entry point in an online help system to be linked with the parent class. The syntax shall be defined by the applied help system respectively help system generator.""")
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone
		self._traceableTable_children=[]
		self._table_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]
		self._msrQueryP1_children=[]

class TopicContent(complexbase.GroupBase):
	"""This meta-class represents the content of a topic. It is mainly a documentation block, but can also be a table."""
	def __init__(self):
		super().__init__()
		self._table_children=[]
		self._traceableTable_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class TopicContentOrMsrQuery(complexbase.GroupBase):
	"""This meta-class represents a topic or a topic content which is generated using queries."""
	def __init__(self):
		super().__init__()
		self._msrQueryP1_children=[]
		self._traceableTable_children=[]
		self._table_children=[]
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class TopicOrMsrQuery(complexbase.GroupBase):
	"""This class provides the alternative of a Topic with an MsrQuery which delivers a topic."""
	def __init__(self):
		super().__init__()
		self._topic1_children=[]
		self._msrQueryTopic1_children=[]

class TpAddress(complexbase.GroupBase):
	"""An ECUs TP address on the referenced channel. This represents the diagnostic Address."""
	def __init__(self):
		super().__init__()
		self._tpAddress_child=ModelNone
		self._variationPoint_child=ModelNone

class TpConfig(complexbase.GroupBase):
	"""Contains all configuration elements for AUTOSAR TP."""
	def __init__(self):
		super().__init__()
		self._communicationCluster_child=ModelNone

class TpConnection(complexbase.GroupBase):
	"""TpConnection Base Class."""
	def __init__(self):
		super().__init__()
		self._ident_child=ModelNone

class TpConnectionIdent(complexbase.GroupBase):
	"""This meta-class is created to add the ability to become the target of a reference to the non-Referrable TpConnection."""

class TpPort(complexbase.GroupBase):
	"""Dynamic or direct assignment of a PortNumber."""
	def __init__(self):
		super().__init__()
		self._dynamicallyAssigned_child=ModelNone
		self._portNumber_child=ModelNone

class TraceReferrable(complexbase.GroupBase):
	"""This meta class is intended to add the category to the subclasses of Traceable. 

Even if the model seems to be a bit awkward, it ensures backwards compatibility of the schema.

This approach allows to have subclasses of Traceable which are either Identifiable or only Referrable while still maintaining the consistent sequence of shortName, longName, category."""

class TraceSwitchConfiguration(complexbase.GroupBase):
	"""This class maps a DltMessage to a trace switch type."""
	def __init__(self):
		super().__init__()
		self._traceMessage_child=ModelNone
		self._traceSwitch_child=ModelNone

class Traceable(complexbase.GroupBase):
	"""This meta class represents the ability to be subject to tracing within an AUTOSAR model.

Note that it is expected that its subclasses inherit either from MultilanguageReferrable or from Identifiable. Nevertheless it also inherits from MultilanguageReferrable in order to provide a common reference target for all Traceables."""
	def __init__(self):
		super().__init__()
		self._trace_children=[]

class TraceableTable(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a traceable table item such as requirements etc.."""
	def __init__(self):
		super().__init__()
		self._table_child=ModelNone

class TraceableText(complexbase.GroupBase):
	"""This meta-class represents the ability to denote a traceable text item such as requirements etc.

The following approach applies:

* '''shortName''' represents the tag for tracing
* '''longName''' represents the head line
* '''category''' represents the kind of the tagged text (see [constr_2540])"""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone
		self._structuredReq_children=[]
		self._trace_children=[]
		self._note_children=[]
		self._figure_children=[]
		self._formula_children=[]
		self._labeledList_children=[]
		self._defList_children=[]
		self._list_children=[]
		self._verbatim_children=[]
		self._p_children=[]
		self._msrQueryP2_children=[]

class TracedFailure(complexbase.GroupBase):
	"""Specifies the ability to report a specific failure to the error tracer.
The short name specifies the literal applicable for the Default Error Tracer."""
	def __init__(self):
		super().__init__()
		self._id_child=ModelNone
		self._variationPoint_child=ModelNone

class TransformationComSpecProps(complexbase.GroupBase):
	"""TransformationComSpecProps holds all the attributes for transformers that are port specific."""

class TransformationDescription(complexbase.GroupBase):
	"""The TransformationDescription is the abstract class that can be used by specific transformers to add transformer specific properties."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TransformationISignalProps(complexbase.GroupBase):
	"""TransformationISignalProps holds all the attributes for the different TransformationTechnologies that are ISignal specific."""
	def __init__(self):
		super().__init__()
		self._csErrorReaction_child=ModelNone
		self._dataPrototypeTransformationProps_children=[]
		self._transformer_child=ModelNone

class TransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class TransformationProps(complexbase.GroupBase):
	"""This meta-class represents a abstract base class for transformation settings."""

class TransformationPropsSet(complexbase.GroupBase):
	"""Collection of TransformationProps."""
	def __init__(self):
		super().__init__()
		self._transformationProps_children=[]

class TransformationPropsToServiceInterfaceElementMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to associate a ServiceInterface element with TransformationProps. The referenced elements of the Service Interface will be serialized according to the settings defined in the TransformationProps."""
	def __init__(self):
		super().__init__()
		self._event_children=[]
		self._field_children=[]
		self._methodCall_children=[]
		self._method_children=[]
		self._methodReturn_children=[]
		self._tlvDataIdDefinition_children=[]
		self._transformationProps_child=ModelNone
		self._trigger_children=[]

class TransformationTechnology(complexbase.GroupBase):
	"""A TransformationTechnology is a transformer inside a transformer chain."""
	def __init__(self):
		super().__init__()
		self._bufferProperties_child=ModelNone
		self._hasInternalState_child=ModelNone
		self._needsOriginalData_child=ModelNone
		self._protocol_child=ModelNone
		self._transformationDescription_children=[]
		self._transformerClass_child=ModelNone
		self._version_child=ModelNone
		self._variationPoint_child=ModelNone

class TransformerHardErrorEvent(complexbase.GroupBase):
	"""This event is raised when data are received which should trigger a Client/Server operation or an external Trigger but during transformation of the data a hard transformer error occurred."""
	def __init__(self):
		super().__init__()
		self._operation_child=ModelNone
		self._requiredTrigger_child=ModelNone
		self._trigger_child=ModelNone

class TransientFault(complexbase.GroupBase):
	"""The reported failure is classified as runtime error."""
	def __init__(self):
		super().__init__()
		self._possibleErrorReaction_children=[]

class TransmissionAcknowledgementRequest(complexbase.GroupBase):
	"""Requests transmission acknowledgement that data has been sent successfully. Success/failure is reported via a SendPoint of a RunnableEntity."""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone

class TransmissionComSpecProps(complexbase.GroupBase):
	"""This meta-class defines a set of transmission attributes which the application software is assumed to implement."""
	def __init__(self):
		super().__init__()
		self._dataUpdatePeriod_child=ModelNone
		self._minimumSendInterval_child=ModelNone
		self._transmissionMode_child=ModelNone

class TransmissionModeCondition(complexbase.GroupBase):
	"""Possibility to attach a condition to each signal within an I-PDU.

If at least one condition evaluates to true, TRANSMISSION MODE True shall be used for this I-Pdu. In all other cases, the TRANSMISSION MODE FALSE shall be used."""
	def __init__(self):
		super().__init__()
		self._dataFilter_child=ModelNone
		self._iSignalInIPdu_child=ModelNone

class TransmissionModeDeclaration(complexbase.GroupBase):
	"""AUTOSAR COM provides the possibility to define two different TRANSMISSION MODES (True and False) for each I-PDU.

As TransmissionMode selector the signal content can be evaluated via transmissionModeCondition (implemented directly in the COM module) or mode conditions can be defined with the modeDrivenTrueCondition or modeDrivenFalseCondition (evaluated by BswM and invoking Com_SwitchIpduTxMode COM API). If modeDrivenTrueCondition and modeDrivenFalseCondition are defined they shall never evaluate to true both at the same time. 

The mixing of Transmission Mode Switch via API and signal value is not allowed."""
	def __init__(self):
		super().__init__()
		self._modeDrivenFalseCondition_children=[]
		self._modeDrivenTrueCondition_children=[]
		self._transmissionModeCondition_children=[]
		self._transmissionModeFalseTiming_child=ModelNone
		self._transmissionModeTrueTiming_child=ModelNone

class TransmissionModeTiming(complexbase.GroupBase):
	"""If the COM Transmission Mode is false the timing is aggregated by the TransmissionModeTiming element in the role of transmissionModeFalseTiming. If the COM Transmission Mode is true the timing is aggregated by the TransmissionModeTiming element in the role of transmissionModeTrueTiming.  

COM supports the following Transmission Modes: 
* Periodic (Cyclic Timing)
* Direct /n-times (EventControlledTiming)
* Mixed (Cyclic and EventControlledTiming are assigned)
* None (no timing is assigned)"""
	def __init__(self):
		super().__init__()
		self._cyclicTiming_child=ModelNone
		self._eventControlledTiming_child=ModelNone

class TransportLayerRule(complexbase.GroupBase):
	"""Configuration of filter rules on Transport Layer level."""
	def __init__(self):
		super().__init__()
		self._checksumVerification_child=ModelNone
		self._maxDestinationPortNumber_child=ModelNone
		self._maxSourcePortNumber_child=ModelNone
		self._minDestinationPortNumber_child=ModelNone
		self._minSourcePortNumber_child=ModelNone

class TransportProtocolConfiguration(complexbase.GroupBase):
	"""Transport Protocol configuration."""

class Trigger(complexbase.GroupBase):
	"""'''begin restrict to AP'''
The Trigger represents a special kind of an event (without data) at which occurrence the Service Consumer shall react in a particular manner.
'''end restrict to AP'''
'''begin restrict to CP'''
A trigger which is provided (i.e. released) or required (i.e. used to activate something) in the given context.
'''end restrict to CP'''"""
	def __init__(self):
		super().__init__()
		self._swImplPolicy_child=ModelNone
		self._triggerPeriod_child=ModelNone
		self._variationPoint_child=ModelNone

class TriggerIPduSendCondition(complexbase.GroupBase):
	"""The condition defined by this class evaluates to true if one of the referenced modeDeclarations (OR associated) is active. 
The condition is used to define when the Pdu is triggered with the Com_TriggerIPDUSend API call."""
	def __init__(self):
		super().__init__()
		self._modeDeclaration_children=[]

class TriggerInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class TriggerInExecutableInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootSwComponentPrototype_child=ModelNone
		self._contextComponentPrototype_children=[]
		self._contextRPortPrototype_child=ModelNone
		self._targetTrigger_child=ModelNone

class TriggerInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComposition_child=ModelNone
		self._contextComponent_children=[]
		self._contextPort_child=ModelNone
		self._targetTrigger_child=ModelNone

class TriggerInterface(complexbase.GroupBase):
	"""A trigger interface declares a number of triggers that can be sent by an trigger source."""
	def __init__(self):
		super().__init__()
		self._trigger_children=[]

class TriggerInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of unequal named Triggers in context of two different TriggerInterfaces."""
	def __init__(self):
		super().__init__()
		self._triggerMapping_children=[]

class TriggerMapping(complexbase.GroupBase):
	"""Defines the mapping of two particular unequally named Triggers in the given context."""
	def __init__(self):
		super().__init__()
		self._firstTrigger_child=ModelNone
		self._secondTrigger_child=ModelNone

class TriggerPortAnnotation(complexbase.GroupBase):
	"""Annotation to a port used for calibration regarding a certain Trigger."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone

class TriggerRefConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._Trigger_child=ModelNone
		self._variationPoint_child=ModelNone

class TriggerToSignalMapping(complexbase.GroupBase):
	"""This meta-class represents the ability to map a trigger to a SystemSignal of size 0. The Trigger does not transport any other information than its existence, therefore the limitation in terms of signal length."""
	def __init__(self):
		super().__init__()
		self._trigger_child=ModelNone
		self._systemSignal_child=ModelNone

class TtcanAbsolutelyScheduledTiming(complexbase.GroupBase):
	"""Each frame in TTCAN is identified by its slot id and communication cycle. A description is provided by the usage of AbsolutelyScheduledTiming. 

A frame can be sent multiple times within one communication cycle. For describing this case multiple AbsolutelyScheduledTimings have to be used. The main use case would be that a frame is sent twice within one communication cycle."""
	def __init__(self):
		super().__init__()
		self._communicationCycle_child=[]
		self._timeMark_child=ModelNone
		self._trigger_child=ModelNone

class TtcanCluster(complexbase.GroupBase):
	"""TTCAN bus specific cluster attributes."""
	def __init__(self):
		super().__init__()
		self._ttcanClusterVariant_children=[]
		self._basicCycleLength_child=ModelNone
		self._ntu_child=ModelNone
		self._operationMode_child=ModelNone

class TtcanClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TtcanClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class TtcanCommunicationConnector(complexbase.GroupBase):
	"""TTCAN bus specific communication connector attributes."""

class TtcanCommunicationController(complexbase.GroupBase):
	"""TTCAN bus specific communication port attributes."""
	def __init__(self):
		super().__init__()
		self._ttcanCommunicationControllerVariant_children=[]
		self._applWatchdogLimit_child=ModelNone
		self._expectedTxTrigger_child=ModelNone
		self._externalClockSynchronisation_child=ModelNone
		self._initialRefOffset_child=ModelNone
		self._master_child=ModelNone
		self._timeMasterPriority_child=ModelNone
		self._timeTriggeredCanLevel_child=ModelNone
		self._txEnableWindowLength_child=ModelNone

class TtcanCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class TtcanCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class TtcanPhysicalChannel(complexbase.GroupBase):
	"""TTCAN bus specific physical channel attributes."""

class UcmDescription(complexbase.GroupBase):
	"""This meta-class represents the ability to define an identifier for a given UCM."""
	def __init__(self):
		super().__init__()
		self._identifier_child=ModelNone
		self._ucmModuleInstantiation_child=ModelNone

class UcmMasterModuleInstantiation(complexbase.GroupBase):
	"""This meta-class represents the ability to define the deployment of a UCM Master instantiation."""
	def __init__(self):
		super().__init__()
		self._blockInconsistent_child=ModelNone
		self._serviceBusy_child=ModelNone
		self._updateSessionRejected_child=ModelNone

class UcmModuleInstantiation(complexbase.GroupBase):
	"""This meta-class represents the ability to define the deployment of a UCM instantiation."""
	def __init__(self):
		super().__init__()
		self._identifier_child=ModelNone
		self._maxBlockSize_child=ModelNone
		self._version_child=ModelNone

class UcmRetryStrategy(complexbase.GroupBase):
	"""This meta-class describes the configuration of the retry strategy for a sub-class of UcmModuleImplementation."""
	def __init__(self):
		super().__init__()
		self._maximumNumberOfRetries_child=ModelNone
		self._retryIntervalTime_child=ModelNone

class UcmStep(complexbase.GroupBase):
	"""This meta-class represents the ability to define a rollout-condition for a vehicle update campaign."""
	def __init__(self):
		super().__init__()
		self._softwarePackageStep_children=[]
		self._ucm_child=ModelNone

class UcmSubordinateModuleInstantiation(complexbase.GroupBase):
	"""This meta-class represents the ability to define the deployment of a UCM Subordinate instantiation."""
	def __init__(self):
		super().__init__()
		self._maxAvailablePersistencyStorageSpace_child=ModelNone
		self._prepareRollback_child=ModelNone
		self._prepareUpdate_child=ModelNone
		self._verifyUpdate_child=ModelNone

class UcmToTimeBaseResourceMapping(complexbase.GroupBase):
	"""This meta-class maps the UCM Module Instantiation to the TimeSync Module Instantiation."""
	def __init__(self):
		super().__init__()
		self._timeBaseResource_child=ModelNone
		self._ucm_child=ModelNone

class UdpNmCluster(complexbase.GroupBase):
	"""Udp specific NmCluster attributes"""
	def __init__(self):
		super().__init__()
		self._networkConfiguration_child=ModelNone
		self._nmCbvPosition_child=ModelNone
		self._nmChannelActive_child=ModelNone
		self._nmImmediateNmCycleTime_child=ModelNone
		self._nmImmediateNmTransmissions_child=ModelNone
		self._nmMessageTimeoutTime_child=ModelNone
		self._nmMsgCycleTime_child=ModelNone
		self._nmNetworkTimeout_child=ModelNone
		self._nmNidPosition_child=ModelNone
		self._nmRemoteSleepIndicationTime_child=ModelNone
		self._nmRepeatMessageTime_child=ModelNone
		self._nmUserDataLength_child=ModelNone
		self._nmUserDataOffset_child=ModelNone
		self._nmWaitBusSleepTime_child=ModelNone
		self._vlan_child=ModelNone

class UdpNmClusterCoupling(complexbase.GroupBase):
	"""Udp attributes that are valid for each of the referenced (coupled) UdpNm clusters."""
	def __init__(self):
		super().__init__()
		self._coupledCluster_children=[]
		self._nmBusLoadReductionEnabled_child=ModelNone
		self._nmImmediateRestartEnabled_child=ModelNone

class UdpNmEcu(complexbase.GroupBase):
	"""Udp NM specific ECU attributes."""
	def __init__(self):
		super().__init__()
		self._nmRepeatMsgIndicationEnabled_child=ModelNone
		self._nmSynchronizationPointEnabled_child=ModelNone

class UdpNmNetworkConfiguration(complexbase.GroupBase):
	"""This meta-class defines the attributes for the configuration of a UDP port and UDP multicast IP address of the Nm communication on a VLAN."""
	def __init__(self):
		super().__init__()
		self._ipv4MulticastIpAddress_child=ModelNone
		self._ipv6MulticastIpAddress_child=ModelNone
		self._priority_child=ModelNone
		self._udpPort_child=ModelNone

class UdpNmNode(complexbase.GroupBase):
	"""Udp specific NM Node attributes."""
	def __init__(self):
		super().__init__()
		self._allNmMessagesKeepAwake_child=ModelNone
		self._communicationConnector_child=ModelNone
		self._nmMsgCycleOffset_child=ModelNone
		self._nmPnHandleMultipleNetworkRequests_child=ModelNone

class UdpProps(complexbase.GroupBase):
	"""This meta-class specifies the configuration options for UDP (User Datagram Protocol)."""
	def __init__(self):
		super().__init__()
		self._udpTtl_child=ModelNone

class UdpRule(complexbase.GroupBase):
	"""Configuration of UDP filter rules."""

class UdpTp(complexbase.GroupBase):
	"""Content Model for UDP configuration."""
	def __init__(self):
		super().__init__()
		self._udpTpPort_child=ModelNone

class UnassignFrameId(complexbase.GroupBase):
	"""Schedule entry for an Unassign Frame Id master request where the protected identifier is assigned the value 0x40. This will disable reception/transmission of a previously dynamically assigned frame identifier."""
	def __init__(self):
		super().__init__()
		self._messageId_child=ModelNone
		self._unassignedFrameTriggering_child=ModelNone

class Unit(complexbase.GroupBase):
	"""This is a physical measurement unit. All units that might be defined should stem from SI units. In order to convert one unit into another factor and offset are defined. 

For the calculation from SI-unit to the defined unit the factor (factorSiToUnit ) and the offset
(offsetSiToUnit ) are applied as follows:

     x  [{unit}]  :=  y * [{siUnit}] * factorSiToUnit [[unit]/{siUnit}] + offsetSiToUnit [{unit}]  

For the calculation from a unit to SI-unit the reciprocal of the factor (factorSiToUnit ) and the negation of the offset (offsetSiToUnit ) are applied. 
 
    y {siUnit} := (x*{unit} - offsetSiToUnit [{unit}]) / (factorSiToUnit [[unit]/{siUnit}]"""
	def __init__(self):
		super().__init__()
		self._displayName_child=ModelNone
		self._factorSiToUnit_child=ModelNone
		self._offsetSiToUnit_child=ModelNone
		self._physicalDimension_child=ModelNone

class UnitGroup(complexbase.GroupBase):
	"""This meta-class represents the ability to specify a logical grouping of units.The category denotes the unit system that the referenced units are associated to. 

In this way, e.g. country-specific unit systems (CATEGORY=\"COUNTRY\") can be defined as well as specific unit systems for certain application domains. 

In the same way a group of equivalent units, can be defined which are used in different countries, by setting CATEGORY=\"EQUIV_UNITS\".  KmPerHour and MilesPerHour could such be combined to one group named \"vehicle_speed\". The unit MeterPerSec would not belong to this group because it is normally not used for vehicle speed. But all of the mentioned units could be combined to one group named \"speed\".

Note that the UnitGroup does not ensure the physical compliance of the units. This is maintained by the physical dimension."""
	def __init__(self):
		super().__init__()
		self._unit_children=[]

class UnlimitedIntegerValueVariationPoint(complexbase.GroupBase):
	"""This class represents an attribute value variation point for unlimited Integer attributes.

Note that this class might be used in the extended meta-model only."""

class UnresolvedReferenceRestrictionWithSeverity(complexbase.GroupBase):
	"""This restriction defines the severity level of unresolved references."""

class UploadableDeploymentElement(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for all deployment-level meta-classes that need to be added to an uploadable software package in order to complete the manifest content."""

class UploadableDesignElement(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for all design-level meta-classes that need to be added to an uploadable software package in order to complete the manifest content."""

class UploadableExclusivePackageElement(complexbase.GroupBase):
	"""This meta-class represents an abstract base class for an uploadable package element that is not supposed to be referenced from a different software cluster."""

class UploadablePackageElement(complexbase.GroupBase):
	"""This meta-class acts as an abstract base class for all meta-classes that need to be added to an uploadable software package in order to complete the manifest content. This applies for both design-level and deployment-level meta-classes."""

class UserDefinedCluster(complexbase.GroupBase):
	"""This element allows the modeling of arbitrary Communication Clusters (e.g. bus systems that are not supported by AUTOSAR)."""
	def __init__(self):
		super().__init__()
		self._userDefinedClusterVariant_children=[]

class UserDefinedClusterConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class UserDefinedClusterContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class UserDefinedCommunicationConnector(complexbase.GroupBase):
	"""This element allows the modeling of arbitrary Communication Connectors."""

class UserDefinedCommunicationController(complexbase.GroupBase):
	"""This element allows the modeling of arbitrary Communication Controllers."""
	def __init__(self):
		super().__init__()
		self._userDefinedCommunicationControllerVariant_children=[]

class UserDefinedCommunicationControllerConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class UserDefinedCommunicationControllerContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class UserDefinedEthernetFrame(complexbase.GroupBase):
	"""UserDefinedEthernetFrame allows the description of a frame-based communication to Complex Drivers that are located above the EthDrv."""

class UserDefinedEventDeployment(complexbase.GroupBase):
	"""UserDefined configuration settings for an Event."""

class UserDefinedFieldDeployment(complexbase.GroupBase):
	"""UserDefined configuration settings for a Field."""
	def __init__(self):
		super().__init__()
		self._get_child=ModelNone
		self._notifier_child=ModelNone
		self._set_child=ModelNone

class UserDefinedGlobalTimeMaster(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeMaster for user defined communication."""

class UserDefinedGlobalTimeSlave(complexbase.GroupBase):
	"""This represents the specialization of the GlobalTimeSlave for user defined communication."""

class UserDefinedIPdu(complexbase.GroupBase):
	"""UserDefinedIPdu allows to describe PDU-based communication over Complex Drivers. If a new BSW module is added above the PduR (e.g. a Diagnostic Service ) then this IPdu element shall be used to describe the communication."""
	def __init__(self):
		super().__init__()
		self._cddType_child=ModelNone

class UserDefinedMethodDeployment(complexbase.GroupBase):
	"""UserDefined configuration settings for a Method."""

class UserDefinedPdu(complexbase.GroupBase):
	"""UserDefinedPdu allows to describe PDU-based communication over Complex Drivers. If a new BSW module is added above the BusIf (e.g. a new Nm module) then this Pdu element shall be used to describe the communication."""
	def __init__(self):
		super().__init__()
		self._cddType_child=ModelNone

class UserDefinedPhysicalChannel(complexbase.GroupBase):
	"""This element allows the modeling of arbitrary Physical Channels."""

class UserDefinedServiceInstanceToMachineMapping(complexbase.GroupBase):
	"""This meta-class allows to map UserDefinedServiceInstances to a CommunicationConnector of a Machine."""

class UserDefinedServiceInterfaceDeployment(complexbase.GroupBase):
	"""UserDefined configuration settings for a ServiceInterface."""

class UserDefinedTransformationComSpecProps(complexbase.GroupBase):
	"""The UserDefinedTransformationComSpecProps is used to specify port specific configuration properties for custom transformers."""

class UserDefinedTransformationDescription(complexbase.GroupBase):
	"""The UserDefinedTransformationDescription is used to specify details and documentation for custom transformers."""

class UserDefinedTransformationISignalProps(complexbase.GroupBase):
	"""The UserDefinedTransformationISignalProps is used to specify ISignal specific configuration properties for custom transformers."""
	def __init__(self):
		super().__init__()
		self._userDefinedTransformationISignalPropsVariant_children=[]

class UserDefinedTransformationISignalPropsConditional(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""
	def __init__(self):
		super().__init__()
		self._variationPoint_child=ModelNone

class UserDefinedTransformationISignalPropsContent(complexbase.GroupBase):
	"""This element was generated/modified due to an atpVariation stereotype."""

class UserDefinedTransformationProps(complexbase.GroupBase):
	"""The class UserDefinedTransformationProps specifies  specific configuration properties of a user defined serializer."""

class V2xDataManagerNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to define service needs for V2x Data Manager."""

class V2xFacUserNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to define service needs for V2x facilities."""

class V2xMUserNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to express service needs for the V2x management."""

class ValueGroup(complexbase.GroupBase):
	"""This element enables values  to be grouped. It can be used to perform row and column-orientated groupings, so that these can be rendered properly e.g. as a table."""
	def __init__(self):
		super().__init__()
		self._label_child=ModelNone
		self._vg_children=[]
		self._v_children=[]
		self._vt_children=[]
		self._vf_children=[]
		self._vtf_children=[]

class ValueList(complexbase.GroupBase):
	"""This is a generic list of numerical values."""
	def __init__(self):
		super().__init__()
		self._vf_children=[]
		self._v_children=[]

class ValueRestrictionWithSeverity(complexbase.GroupBase):
	"""Specifies valid values of primitive data types. A value is valid if all rules defined by this ValueRestriction evaluate to true."""

class ValueSpecification(complexbase.GroupBase):
	"""Base class for expressions leading to a value which can be used to initialize a data object."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._variationPoint_child=ModelNone

class VariableAccess(complexbase.GroupBase):
	"""The presence of a VariableAccess implies that a RunnableEntity needs access to a VariableDataPrototype. 

The kind of access is specified by the role in which the class is used."""
	def __init__(self):
		super().__init__()
		self._accessedVariable_child=ModelNone
		self._scope_child=ModelNone
		self._variationPoint_child=ModelNone

class VariableAccessInEcuInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextRootComposition_child=ModelNone
		self._contextAtomicComponent_child=ModelNone
		self._targetVariableAccess_child=ModelNone

class VariableAndParameterInterfaceMapping(complexbase.GroupBase):
	"""Defines the mapping of VariableDataPrototypes or ParameterDataPrototypes in context of two different SenderReceiverInterfaces, NvDataInterfaces or ParameterInterfaces."""
	def __init__(self):
		super().__init__()
		self._dataMapping_children=[]

class VariableDataPrototype(complexbase.GroupBase):
	"""A VariableDataPrototype represents a formalized generic piece of information that is typically mutable by the application software layer. VariableDataPrototype is used in various contexts and the specific context gives the otherwise generic VariableDataPrototype a dedicated semantics."""
	def __init__(self):
		super().__init__()
		self._initValue_child=[]
		self._variationPoint_child=ModelNone

class VariableDataPrototypeInCompositionInstanceRef(complexbase.GroupBase):
	"""This meta-class represents the ability to define an InstanceRef to a VariableDataPrototype in the context of a CompositionSwComponentType."""
	def __init__(self):
		super().__init__()
		self._contextSwComponentPrototype_children=[]
		self._contextPortPrototype_child=ModelNone
		self._targetVariableDataPrototype_child=ModelNone
		self._variationPoint_child=ModelNone

class VariableDataPrototypeInSystemInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._contextComposition_child=ModelNone
		self._contextPort_child=ModelNone
		self._targetDataPrototype_child=ModelNone

class VariableInAtomicSwcInstanceRef(complexbase.GroupBase):
	""

class VariableInAtomicSWCTypeInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._portPrototype_child=ModelNone
		self._rootVariableDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataPrototype_child=ModelNone

class VariableInComponentInstanceRef(complexbase.GroupBase):
	""
	def __init__(self):
		super().__init__()
		self._contextComponent_children=[]
		self._contextPortPrototype_child=ModelNone
		self._rootVariableDataPrototype_child=ModelNone
		self._contextDataPrototype_children=[]
		self._targetDataProtoype_child=ModelNone

class VariationPoint(complexbase.GroupBase):
	"""This meta-class represents the ability to express a \"structural variation point\". The container of the variation point is part of the selected variant if swSyscond evaluates to true and each postBuildVariantCriterion is fulfilled."""
	def __init__(self):
		super().__init__()
		self._shortLabel_child=ModelNone
		self._desc_child=ModelNone
		self._blueprintCondition_child=ModelNone
		self._formalBlueprintCondition_child=ModelNone
		self._formalBlueprintGenerator_child=ModelNone
		self._swSyscond_child=ModelNone
		self._postBuildVariantCondition_children=[]
		self._sdg_child=ModelNone

class VariationPointProxy(complexbase.GroupBase):
	"""The VariationPointProxy represents variation points of the C/C++ implementation. In case of bindingTime = compileTime the RTE provides defines which can be used for Pre Processor directives to implement compileTime variability."""
	def __init__(self):
		super().__init__()
		self._conditionAccess_child=ModelNone
		self._implementationDataType_child=ModelNone
		self._postBuildValueAccess_child=ModelNone
		self._postBuildVariantCondition_children=[]
		self._valueAccess_child=[]

class VariationRestrictionWithSeverity(complexbase.GroupBase):
	"""Defines constraints on the usage of variation and on the valid binding times."""

class VehicleDriverNotification(complexbase.GroupBase):
	"""This meta-class provides the ability to configure a notification of the vehicle driver with respect to the update of vehicle software."""
	def __init__(self):
		super().__init__()
		self._approvalRequired_child=ModelNone
		self._notificationState_child=ModelNone

class VehiclePackage(complexbase.GroupBase):
	"""This meta-class represents the ability to define a vehicle package for executing an update campaign."""
	def __init__(self):
		super().__init__()
		self._driverNotification_children=[]
		self._estimatedDurationOfCampaign_child=ModelNone
		self._maximumDurationOfCampaign_child=ModelNone
		self._minimumSupportedUcmMasterVersion_child=ModelNone
		self._packagerSignature_child=ModelNone
		self._repository_child=ModelNone
		self._rolloutQualification_children=[]
		self._ucm_children=[]
		self._ucmMasterFallback_children=[]
		self._vehicleDescription_child=ModelNone

class VehicleRolloutStep(complexbase.GroupBase):
	"""This meta-class represents the ability to define a rollout-condition for a vehicle update campaign."""
	def __init__(self):
		super().__init__()
		self._safetyCondition_children=[]
		self._ucmProcessing_children=[]
		self._violatedSafetyConditionBehavior_child=ModelNone

class VendorSpecificServiceNeeds(complexbase.GroupBase):
	"""This represents the ability to define vendor-specific service needs."""

class VfbTiming(complexbase.GroupBase):
	"""A model element used to define timing descriptions and constraints at VFB level.

TimingDescriptions aggregated by VfbTiming are restricted to event chains referring to events which are derived from the class TDEventVfb."""
	def __init__(self):
		super().__init__()
		self._component_child=ModelNone

class ViewMap(complexbase.GroupBase):
	"""The ViewMap allows to relate any number of elements on the \"first\" side to any number of elements on the \"second\" side. Since the ViewMap does not address a specific mapping use-case the roles \"first\" and \"second\" shall imply this generality.

This mapping allows to trace transformations of artifacts within the AUTOSAR environment. The references to the mapped elements can be plain references and/or InstanceRefs."""
	def __init__(self):
		super().__init__()
		self._role_child=ModelNone
		self._firstElement_children=[]
		self._secondElement_children=[]
		self._firstElementInstance_children=[]
		self._secondElementInstance_children=[]

class ViewMapSet(complexbase.GroupBase):
	"""Collection of ViewMaps that are used to establish relationships between different AUTOSAR artifacts."""
	def __init__(self):
		super().__init__()
		self._viewMap_children=[]

class VlanConfig(complexbase.GroupBase):
	"""VLAN Configuration attributes"""
	def __init__(self):
		super().__init__()
		self._vlanIdentifier_child=ModelNone

class VlanMembership(complexbase.GroupBase):
	"""Static logical channel or VLAN binding to a switch-port.

The reference to an EthernetPhysicalChannel without a VLAN defined represents the handling of untagged frames."""
	def __init__(self):
		super().__init__()
		self._defaultPriority_child=ModelNone
		self._dhcpAddressAssignment_child=ModelNone
		self._sendActivity_child=ModelNone
		self._vlan_child=ModelNone

class WaitPoint(complexbase.GroupBase):
	"""This defines a wait-point for which the RunnableEntity can wait."""
	def __init__(self):
		super().__init__()
		self._timeout_child=ModelNone
		self._trigger_child=ModelNone

class WarningIndicatorRequestedBitNeeds(complexbase.GroupBase):
	"""This meta-class represents the ability to explicitly request the existence of the WarningIndicatorRequestedBit."""

class WhitespaceControlled(complexbase.GroupBase):
	"""This meta-class represents the ability to control the white-space handling  e.g. in xml serialization. This is implemented by adding the attribute \"space\"."""
	xmlSpace=complexbase.Attribute("xmlSpace",SimpleTypes.String,'space',True,"""This attribute is used to signal an intention that in that element, white space should be preserved by applications. It is defined according to xml:space as declared by W3C.""")

class WorstCaseHeapUsage(complexbase.GroupBase):
	"""Provides a formal worst case heap usage."""
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class WorstCaseStackUsage(complexbase.GroupBase):
	"""Provides a formal worst case stack usage."""
	def __init__(self):
		super().__init__()
		self._memoryConsumption_child=ModelNone

class XcpPdu(complexbase.GroupBase):
	"""This element is deprecated and will be removed in future. The GeneralPurposeIPdu with the category \"XCP\" shall be used instead.

Old description:
AUTOSAR XCP Pdu."""

class Xdoc(complexbase.GroupBase):
	"""This meta-class represents the ability to refer to an external document which can be rendered as printed matter."""
	def __init__(self):
		super().__init__()
		self._number_child=ModelNone
		self._state_child=ModelNone
		self._date_child=ModelNone
		self._publisher_child=ModelNone
		self._url_child=ModelNone
		self._position_child=ModelNone

class Xfile(complexbase.GroupBase):
	"""This represents to reference an external file within a documentation."""
	def __init__(self):
		super().__init__()
		self._url_child=ModelNone
		self._tool_child=ModelNone
		self._toolVersion_child=ModelNone

class Xref(complexbase.GroupBase):
	"""This represents a cross-reference within documentation."""
	resolutionPolicy=complexbase.Attribute("resolutionPolicy",SimpleTypes.ResolutionPolicyEnum,'RESOLUTION-POLICY',False,"""Indicates if the content of the xref element follow a dedicated resolution policy. The default is \"NO-SLOPPY\".""")
	showContent=complexbase.Attribute("showContent",SimpleTypes.ShowContentEnum,'SHOW-CONTENT',False,"""Indicates if the content of the xref element shall be rendered. The default is \"NO-SHOW-CONTENT\".""")
	showResourceAliasName=complexbase.Attribute("showResourceAliasName",SimpleTypes.ShowResourceAliasNameEnum,'SHOW-RESOURCE-ALIAS-NAME',False,"""This indicates if the alias names of the referenced objects shall be rendered. This means this is some kind of backward searching: look whether there is an  alias for the referenced object, if yes, print it. 

If there is more than one AliasNameSet, Xref might render all of those.

If no alias is found and showResourceShortName is set to NoShowShortName, then the shortName of the reference target shall be displayed. By this showResourceAliasName is similar to showResourceShortName but shows the aliasName instead of the shortName.

Default is NO-SHOW-ALIAS-NAME.""")
	showResourceCategory=complexbase.Attribute("showResourceCategory",SimpleTypes.ShowResourceCategoryEnum,'SHOW-RESOURCE-CATEGORY',False,"""Indicates if the category of the referenced resource shall be rendered. Default is \"NO-SHOW-CATEGORY\".""")
	showResourceLongName=complexbase.Attribute("showResourceLongName",SimpleTypes.ShowResourceLongNameEnum,'SHOW-RESOURCE-LONG-NAME',False,"""Indicates if the longName of the referenced resource shall be rendered. Default is \"SHOW-LONG-NAME\".""")
	showResourceNumber=complexbase.Attribute("showResourceNumber",SimpleTypes.ShowResourceNumberEnum,'SHOW-RESOURCE-NUMBER',False,"""Indicates if the Number of the referenced resource shall be shown. Default is \"SHOW--NUMBER\"""")
	showResourcePage=complexbase.Attribute("showResourcePage",SimpleTypes.ShowResourcePageEnum,'SHOW-RESOURCE-PAGE',False,"""Indicates if the page number of the referenced resource shall be shown. Default is \"SHOW-PAGE\"""")
	showResourceShortName=complexbase.Attribute("showResourceShortName",SimpleTypes.ShowResourceShortNameEnum,'SHOW-RESOURCE-SHORT-NAME',False,"""Indicates if the shortJName of the referenced resource shall be shown. Default is \"SHOW-SHORT-NAME\"""")
	showResourceType=complexbase.Attribute("showResourceType",SimpleTypes.ShowResourceTypeEnum,'SHOW-RESOURCE-TYPE',False,"""Indicates if the type of the referenced Resource shall be shown. Default is \"SHOW-TYPE\"""")
	showSee=complexbase.Attribute("showSee",SimpleTypes.ShowSeeEnum,'SHOW-SEE',False,"""Indicates if the word \"see \" shall be shown before the reference. Default is \"NO-SHOW-SEE\". Note that this is there for compatibility reasons only.""")
	def __init__(self):
		super().__init__()
		self._label1_child=ModelNone
		self._referrable_child=ModelNone

class XrefTarget(complexbase.GroupBase):
	"""This element specifies a reference target which can be scattered throughout the text."""