# This file is part of the AutoMAT distribution (https://bitbucket.com/mahomaho/AutoMAT).
# Copyright (c) 2021 Mattias Holmqvist.
# 
# AutoMAT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AutoMAT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AutoMAT.  If not, see <https://www.gnu.org/licenses/>.

import re
from AutoMAT import *
import itertools
import os

try:
	autosar.test1
except AttributeError as e:
	pass
else:
	raise AssertionError('Error 1:should lead to error')
try:
	autosar.editor().test1
except AttributeError as e:
	pass
else:
	raise AssertionError('Error 2:should lead to error')

autosar.__dir__()
autosar.editor(file='test1.xml').delete()
autosar.editor(file='test2.xml').delete()
autosar.editor(file='test3.xml').delete()
autosar.editor(file='test4.xml').delete()
autosar.editor(file='test_conf1.xml').delete()
autosar.editor().save()
autosar.editor(file='test1.xml').arPackage['test1']
autosar.__dir__()
autosar.test1
autosar.editor().__dir__()
autosar.editor().test1

datatype=autosar.editor().test1.element.ImplementationDataType['mytype']

#set simple string
datatype.category='VALUE'
datatype.category='BOOLEAN'
datatype.swDataDefProps.additionalNativeTypeQualifier
datatype.swDataDefProps.displayFormat='%12.0f'
try:
	datatype.swDataDefProps.invalidValue.value=2.7
	assert 0,'It shall not be possible to create an invalid value when type of value not specified'
except:
	pass
datatype.swDataDefProps.invalidValue.NumericalValueSpecification.value=3.6
datatype.swDataDefProps.invalidValue.__dir__()
datatype.swDataDefProps.invalidValue.value=2.7
datatype.swDataDefProps.invalidValue.NumericalValueSpecification.value=2.7
#datatype.swDataDefProps.invalidValue.value.__dir__()
datatype.swDataDefProps.invalidValue.NumericalValueSpecification.value.__dir__()
assert 2.7==datatype.swDataDefProps.invalidValue[0].value.val()
datatype.swDataDefProps.stepSize=1.1
datatype.swDataDefProps.stepSize=datatype.swDataDefProps.stepSize.val()+2.6
#datatype.swDataDefProps.IMPLEMENTATION_DATA_TYPE_REF.type()
datatype.swDataDefProps.implementationDataType=datatype
assert datatype.swDataDefProps.implementationDataType.ref().category.val()=='BOOLEAN'
datatype.swDataDefProps.compuMethod='/ghgf/ghfhgf/hgfhgf'
datatype.swDataDefProps.compuMethod.DEST='COMPU-METHOD'
datatype.swDataDefProps.compuMethod='/ghgf/ghfhgf/hgfhgf/hgfhgf'
datatype.annotation[0]
datatype.annotation[1]
datatype.annotation[2]
datatype.annotation[0]
# mixed string array
datatype.annotation[1].annotationText
datatype.annotation[1].annotationText.p[0].l1[0]='ggg'
datatype.annotation[1].annotationText.p[0].l1[0].l='EN'
datatype.annotation[1].annotationText.p[0].l1[1]='rrr'
datatype.annotation[1].annotationText.p[0].l1[1].l='EN'
datatype.annotation[1].annotationText.p[0].l1[2]='qqq'
datatype.annotation[1].annotationText.p[0].l1[2].l='EN'
datatype.annotation[1].annotationText.p[0].l1[1]='qwe'
datatype.annotation[1].annotationText.p[0].l1.append('third')
datatype.annotation[1].annotationText.p[0].l1[3].l='EN'

datatype.annotation[1].annotationText.p[0].l1[2].ie[0]=3
datatype.annotation[1].annotationText.p[0].l1[2].ie[0].sub[0]='aassdd'
datatype.save()
assert len(datatype.model().annotation)==3

assert datatype.annotation[1].annotationText.p[0].l1.val() == ['ggg', 'qwe', 'qqq', 'third']
assert [e.__repr__() for e in datatype.annotation[1].annotationText.p[0].l1.model()] == ['autosar.test1.mytype.annotation[1].annotationText.p[0].l1[0]', 'autosar.test1.mytype.annotation[1].annotationText.p[0].l1[1]', 'autosar.test1.mytype.annotation[1].annotationText.p[0].l1[2]', 'autosar.test1.mytype.annotation[1].annotationText.p[0].l1[3]']

autosar.editor().test1.element.ApplicationSwComponentType['app_comp'].internalBehavior['mybeh'].runnable['Runnable'].canEnterExclusiveArea[0]='/can/enter'
autosar.editor().test1.app_comp.mybeh.Runnable.canEnterExclusiveArea[0].DEST='EXCLUSIVE-AREA'
autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[0]='/runs/inside'
autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[0].DEST='EXCLUSIVE-AREA'
autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[1]='/runs/inside2'
autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[1].DEST='EXCLUSIVE-AREA'
autosar.editor().test1.app_comp.mybeh.exclusiveArea['exclarea']
autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[2]=autosar.editor().test1.app_comp.mybeh.exclarea
autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[1]=autosar.editor().test1.app_comp.mybeh.exclarea
assert not autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[1].ref()==None
assert not autosar.editor().test1.app_comp.mybeh.Runnable.runsInsideExclusiveArea[2].ref()==None
autosar.editor().test1.shortName='anothername'
assert autosar.editor().anothername.app_comp.mybeh.Runnable.runsInsideExclusiveArea[1].ref()==None
assert autosar.editor().anothername.app_comp.mybeh.Runnable.runsInsideExclusiveArea[2].ref()==None
autosar.editor().anothername.app_comp.mybeh.runnable.runsInsideExclusiveArea[1]=autosar.editor().anothername.app_comp.mybeh.exclarea
assert not autosar.editor().anothername.app_comp.mybeh.runnable.runsInsideExclusiveArea[1].ref()==None
autosar.editor().anothername.shortName='test1'
assert autosar.editor().test1.app_comp.mybeh.runnable.runsInsideExclusiveArea[1].ref()==None
assert not autosar.editor().test1.app_comp.mybeh.runnable.runsInsideExclusiveArea[2].ref()==None
autosar.editor(file='test1.xml').save()
autosar.editor().test1.shortName='test2'
autosar.editor().test2.element.ApplicationSwComponentType['app_comp'].internalBehavior['mybeh'].runnable['Runnable'].editor(file='test2.xml').category='BOOLEAN'
autosar.editor(file='test2.xml').save()
autosar.editor(file='test2.xml').test2.app_comp.mybeh.shortName
del autosar.editor(file='test2.xml').test2.app_comp.mybeh

sr=autosar.editor(file='test2.xml').arPackage['SimpleArrayTest'].element.ApplicationPrimitiveDataType['ApplPrimType']
sr.shortNameFragment[0].fragment='asd'
sr.shortNameFragment[0].fragment='fsd'
sr.shortNameFragment[1].fragment='eff'
sr.shortNameFragment[3].fragment='ljhj'
sr2=autosar.editor(file='test2.xml').arPackage['SimpleArrayTest'].element.EcucModuleDef['kjkh']

sr2.supportedConfigVariant[0]=autosar_r4p0.SimpleTypes.EcucConfigurationVariantEnum.VariantPostBuild
sr2.supportedConfigVariant[0]=autosar_r4p0.SimpleTypes.EcucConfigurationVariantEnum.PreconfiguredConfiguration
sr2.supportedConfigVariant[1]=autosar_r4p0.SimpleTypes.EcucConfigurationVariantEnum.RecommendedConfiguration
try:
	sr2.supportedConfigVariant[3]=autosar_r4p0.SimpleTypes.EcucConfigurationVariantEnum.PreconfiguredConfiguration
	raise AssertionError('Should not be possible to create empty array properties')
except:pass
#datatype.annotation[1].annotationText.p[0].l1[2].editor(file='test2.xml')
#datatype.editor(file='test2.xml').annotation[1].annotationText.p[0].l1[2].IE[0].SUB[1].value='ppprrrr'
autosar.editor(file='test2.xml').save()
autosar.editor().save()
autosar.editor().test2.shortName='test3'
autosar.editor().save()
autosar.editor(file='test3.xml').test3.app_comp.mybeh.Runnable.save()#.runsInsideExclusiveArea[1].editor(file='test3.xml').save()
autosar.editor().save()
#autosar.test3.editor(file='merged.xml').merge()
autosar.editor(file='test4.xml').arPackage['somethingnew']
assert not os.path.exists('test4.xml')
autosar.editor().save()
#assert not os.path.exists('test4.xml')
autosar.editor(file='test4.xml').delete()
assert os.path.exists('test4.xml')
autosar.editor().save()
assert not os.path.exists('test4.xml')
autosar.editor(file='test4.xml').arPackage['somethingnew'].save()
assert os.path.exists('test4.xml')
autosar.editor(file='test4.xml').somethingnew.delete()
assert os.path.exists('test4.xml')
autosar.editor(file='test4.xml').delete()
assert os.path.exists('test4.xml')
autosar.editor().save()
assert not os.path.exists('test4.xml')
autosar.editor(file='test_conf1.xml').arPackage['MyConf'].element.EcucModuleConfigurationValues['Com'].definition=autosar.AUTOSAR.EcucDefs.Com
Com=autosar.editor(file='test_conf1.xml').MyConf.Com
Com.__dir__()
Com.ComConfig.__dir__()
Com.ComConfig.ComIPdu['ipdu']
try:
	Com.ComConfig.ComIPduGroup['ipdu']
	raise AssertionError('Shall not be possible to create another named container with different type but same name')
except:pass
Com.ComConfig.ipdu.ComIPduHandleId=2
assert Com.ComConfig.ipdu.ComIPduHandleId.val()==2
Com.ComConfig.ipdu.shortName='ipdu1'
try:
	Com.ComConfig.ipdu1.ComIPduCallout.__dir__()
	raise AssertionError('cannot create property without value')
except:pass
Com.ComConfig.ipdu1.ComIPduCallout='xcv'
assert Com.ComConfig.ComIPdu['ipdu1'].ComIPduCallout.val()=='xcv'
Com.ComConfig.ComIPdu['ipdu1'].ComIPduCallout='asd'
next(iter((e for e in Com.ComConfig.ipdu1.model().ecucAggregations() if e.name()=='ComIPduDirection'))).values()==['RECEIVE','SEND']
Com.ComConfig.ipdu1.ComIPduDirection='RECEIVE'
Com.ComConfig.ipdu1.ComIPduDirection='SEND'
assert Com.ComConfig.ipdu1.ComIPduDirection.val()=='SEND'
Com.ComConfig.ComIPduGroup['Group1']
Com.ComConfig.ComIPduGroup['Group2']
Com.ComConfig.ComIPduGroup['Group3'].ComIPduGroupHandleId=5
Com.ComConfig.ComIPduGroup['Group4'].ComIPduGroupGroupRef
Com.ComConfig.ComIPduGroup.__repr__()
Com.ComConfig.ComIPduGroup['Group1']
Com.ComConfig.ipdu1.ComIPduGroupRef[0]=Com.ComConfig.Group2
Com.ComConfig.ipdu1.ComIPduGroupRef[1]=Com.ComConfig.Group2
Com.ComConfig.ipdu1.ComIPduGroupRef[0]=Com.ComConfig.Group1
Com.ComConfig.ipdu1.ComIPduGroupRef.append(Com.ComConfig.Group3)
# validate indexed array sort order
assert Com.ComConfig.ipdu1.ComIPduGroupRef.val()==['/MyConf/Com/ComConfig/Group1', '/MyConf/Com/ComConfig/Group2', '/MyConf/Com/ComConfig/Group3'], 'Indexed group not correctly indexed'
Com.ComConfig.ipdu1.ComIPduGroupRef[1].move(Com.ComConfig.ipdu1,2)
assert Com.ComConfig.ipdu1.ComIPduGroupRef.val()==['/MyConf/Com/ComConfig/Group1', '/MyConf/Com/ComConfig/Group3', '/MyConf/Com/ComConfig/Group2'], 'Indexed group not correctly indexed'

#autosar.editor(file='test_conf1.xml').arPackage['Extract'].ELEMENTS.SYSTEM_SIGNAL['MySystemSignal']
autosar.editor(file='test_conf1.xml').arPackage['Extract'].element.ISignal['MyISignal']
autosar.editor(file='test_conf1.xml').arPackage['Extract'].element.ISignalIPdu['MyIpdu'].iSignalToPduMapping['MyISignalMapping'].iSignal=autosar.Extract.MyISignal
Com.ComConfig.ComSignal['sig1'].ComSystemTemplateSystemSignalRef=autosar.Extract.MyIpdu.MyISignalMapping
Com.save()

autosar.MyConf.editor(file='merged.xml').save()
pass

#import readline
#from rlcompleter import Completer
#readline.parse_and_bind("tab: complete")
#readline.set_completer(Completer(locals()).complete)
#import code
#code.InteractiveConsole(locals=locals()).interact('>>>')
